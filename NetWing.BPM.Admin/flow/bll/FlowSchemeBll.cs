using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class FlowSchemeBll
    {
        public static FlowSchemeBll Instance
        {
            get { return SingletonProvider<FlowSchemeBll>.Instance; }
        }

        public int Add(FlowSchemeModel model)
        {
            return FlowSchemeDal.Instance.Insert(model);
        }

        public int Update(FlowSchemeModel model)
        {
            return FlowSchemeDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return FlowSchemeDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return FlowSchemeDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
