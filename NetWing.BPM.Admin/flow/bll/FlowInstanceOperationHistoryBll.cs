using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class FlowInstanceOperationHistoryBll
    {
        public static FlowInstanceOperationHistoryBll Instance
        {
            get { return SingletonProvider<FlowInstanceOperationHistoryBll>.Instance; }
        }

        public int Add(FlowInstanceOperationHistoryModel model)
        {
            return FlowInstanceOperationHistoryDal.Instance.Insert(model);
        }

        public int Update(FlowInstanceOperationHistoryModel model)
        {
            return FlowInstanceOperationHistoryDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return FlowInstanceOperationHistoryDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return FlowInstanceOperationHistoryDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
