var actionURL = '/flow/ashx/FlowInstanceHandler.ashx?action=flow';
var formurl = '/flow/html/FlowInstance.html';

$(function () {

    //为避免数据字典加载不完整。这段在后面加载
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });

    $('#a_add').click(CRUD.add);
    $('#a_edit').click(CRUD.edit);
    $('#a_delete').click(CRUD.del);
    //高级查询
    $('#a_search').click(function () {
        search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#a_getSearch').click(function () {//自定义搜索框
        mySearch();
    });

    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });

    //批量删除
    $("#a_alldel").click(function () {
        var ids = [];
        var rows = $('#list').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].KeyId);
        }
        var allid = ids.join(',');//所有的id
        var o = {};
        o.action = "alldel";
        o.KeyIds = allid;
        var param = "json=" + JSON.stringify(o);

        if (confirm('确认要执行批量删除操作吗？')) {
            jQuery.ajaxjson(actionURL, param, function (d) {
                if (parseInt(d) > 0) {
                    msg.ok('批量删除成功！');
                    grid.reload();
                } else {
                    MessageOrRedirect(d);
                }
            });
        }


    });

});
//这段后加载，避免数据字典加载不完整的问题
window.onload = function () {
    autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
};


//editor:'datetimebox' 日期及时间选择框  editor:'datebox' 日期选择框    editor:'numberspinner' 数字调节器  editor:{type: 'numberspinner',options:{value:0,required:true}}

//定义一个$JQ为$.　以后在js 中就可以用${JQ}AJAX了在前台这样写(定义)://通用数据字典start
var getDic = {
    jsonData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?categoryId=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            //alert(newData);
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
    jsonParentData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?action=parent&parentid=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
}

//通用数据字典end
//调用数据字典和使用数据字典 如果不需要请不要打开否则会导致系统速度慢
//getDic.jsonData(9);//取得性别数据字典
//在onLoad:的地方如下使用
//top.$('#txt_user_sex').combobox({ data: eval($('body').data('data9')), valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '请选择性别', disabled: false });





var grid = {
    bind: function (winSize) {
        $('#list').datagrid({
            url: actionURL,
            toolbar: '#toolbar',
            title: "数据列表",
            iconCls: 'icon icon-list',
            width: winSize.width,
            height: winSize.height,
            nowrap: false, //折行
            rownumbers: true, //行号
            striped: true, //隔行变色
            idField: 'KeyId',//主键
            singleSelect: true, //单选
            frozenColumns: [[]],
            columns: [[//应为宽度不是很需要所以注释了宽度
                { title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                { title: '流程编号', field: 'KeyId', sortable: true, width: '', hidden: false },
                //{ title: '流程实例模板Id', field: 'InstanceSchemeId', sortable: true, width: '', hidden: false },
                { title: '实例编号', field: 'Code', sortable: true, width: '', hidden: false },
                { title: '实例名称', field: 'CustomName', sortable: true, width: '', hidden: false },
                { title: '当前节点ID', field: 'ActivityId', sortable: true, width: '', hidden: false },
                { title: '当前节点类型（0会签节点）', field: 'ActivityType', sortable: true, width: '', hidden: true },
                { title: '当前节点名称', field: 'ActivityName', sortable: true, width: '', hidden: false },
                { title: '前一个ID', field: 'PreviousId', sortable: true, width: '', hidden: true },
                { title: '流程模板内容', field: 'SchemeContent', sortable: true, width: '', hidden: true },
                { title: '流程模板ID', field: 'SchemeId', sortable: true, width: '', hidden: true },
                { title: '数据库名称', field: 'DbName', sortable: true, width: '', hidden: true },
                { title: '表单数据', field: 'FrmData', sortable: true, width: '', hidden: true },
                { title: '表单类型', field: 'FrmType', sortable: true, width: '', hidden: true },
                { title: '表单中的控件属性描述', field: 'FrmContentData', sortable: true, width: '', hidden: true },
                { title: '表单控件位置模板', field: 'FrmContentParse', sortable: true, width: '', hidden: true },
                { title: '表单ID', field: 'FrmId', sortable: true, width: '', hidden: true },
                { title: '流程类型', field: 'SchemeType', sortable: true, width: '', hidden: true },
                { title: '有效标志', field: 'Disabled', sortable: true, width: '', hidden: true },
                { title: '创建时间', field: 'CreateDate', sortable: true, width: '', hidden: false },
                { title: '创建用户主键', field: 'CreateUserId', sortable: true, width: '', hidden: true },
                { title: '创建用户', field: 'CreateUserName', sortable: true, width: '', hidden: false },
                { title: '等级', field: 'FlowLevel', sortable: true, width: '', hidden: true },
                { title: '备注', field: 'Description', sortable: true, width: '', hidden: false },
                {
                    title: '是否完成', field: 'IsFinish', sortable: true, width: '',
                    formatter: function (value, row, index) {
                        if (row.IsFinish == 0) {
                            return "正在运行";
                        } else if (row.IsFinish == 4) {
                            return "被驳回";
                        } else if (row.IsFinish == 3) {
                            return "不同意";
                        } else {
                            return "已完成";
                        }

                    },
                    hidden: false
                },
                { title: '执行人', field: 'MakerList', sortable: true, width: '', hidden: false },
                {
                    title: '操作', field: 'cz', sortable: false, width: '',
                    formatter: function (value, row, index) {
                        //alert($.cookie("currentUserID"));
                        if (row.CreateUserId === parseInt($.cookie("currentUserID"))) {
                            console.log("EasyUIRow数据:" + JSON.stringify(row));
                            if (row.IsFinish === 0 || row.IsFinish === 3) {//正在运行的才显示处理否则显示空
                                return "<input type='button' onclick='doFlow(" + JSON.stringify(row) + ")'; value='处理'>";
                            } else {
                                return "";
                            }
                            
                        } else {
                            return "";
                        }

                    },
                    hidden: false
                },


            ]],
            onEndEdit: onEndEdit,//结束编辑时函数 这里为了简洁 该函数写在下面
            onUnselect: onUnselect,
            onLoadSuccess: function (data) {
                //alert($('body').data('data70'));
                //alert($('body').data('data69'));
            },
            onCancelEdit: onCancelEdit,//在用户取消编辑一行的时候触发
            onSelect: onSelect,//在用户选择一行的时候触发
            onClickRow: onClickRow,//在用户点击一行的时候触发
            //onAfterEdit: onAfterEdit,//在用户完成编辑一行的时候触发
            onDblClickCell: onDblClickCell,//为了程序逻辑清楚函数写在外面
            onHeaderContextMenu: function (e, field) {//列菜单实现动态隐藏列
                e.preventDefault();
                if (!cmenu) {
                    createColumnMenu();
                }
                cmenu.menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            pagination: true,
            pageSize: PAGESIZE,
            pageList: [20, 40, 50, 100, 200]
        });
    },
    getSelectedRow: function () {
        return $('#list').datagrid('getSelected');
    },
    reload: function () {
        $('#list').datagrid('clearSelections').datagrid('reload', { filter: '' });
    }
};

function doFlow(r) {
    console.log("处理流程:" + r.KeyId);
    //弹出即全屏
    var index = layer.open({
        id: 'layfm',//设置该值后，不管是什么类型的层，都只允许同时弹出一个。一般用于页面层和iframe层模式
        title: '编号:"' + r.KeyId + '"流程',
        type: 2,
        content: '/flow/html/doFlow.html',
        area: ['320px', '195px'],
        maxmin: true,
        btn: ['处理'],
        success: function (layero, index) {//加载成功回调方法
            //console.log(layero, index);
            var childWindow = $("#layfm iframe")[0].contentWindow; //表示获取了嵌入在iframe中的子页面的window对象。  []将JQuery对象转成DOM对象，用DOM对象的contentWindow获取子页面window对象。
            //if (row.SchemeContent !== '') {
            childWindow.loadflow(r);  //调用子页面中的loadflow方法。
            //}

            //childWindow.setTitle(row.SchemeName);
        },
        yes: function (index, layero) {
            console.log("index:" + index + "layero:" + layero);
            // 获取iframe的VerificationOpinion元素 审核意见
            var VerificationOpinion = $("#layfm iframe").contents().find("#VerificationOpinion").val();
            console.log("审核意见是：" + VerificationOpinion);
            //contents() 方法获得匹配元素集合中每个元素的子节点，包括文本和注释节点。
            var VerificationFinally = $("#layfm iframe").contents().find(".radio:checked").val();
            console.log("审核结果：" + VerificationFinally);
            $.ajax({
                url: '/flow/ashx/FlowInstanceHandler.ashx',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {
                    VerificationOpinion: VerificationOpinion,
                    FlowInstanceId: r.KeyId,
                    VerificationFinally: VerificationFinally,
                    action: "doflow"
                },
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success: function (d, textStatus, jqXHR) {
                    if (parseInt(d) > 0) {
                        //提示层

                        layer.msg('保存成功！');
                        //hDialog.dialog('close');
                        grid.reload();
                        layer.close(index); //它获取的始终是最新弹出的某个层，值是由layer内部动态递增计算的
                    } else {
                        MessageOrRedirect(d);
                    }
                },

            });




            //按钮【按钮一】的回调
        },

        cancel: function () {
            //右上角关闭回调

            //return false 开启该代码可禁止点击该按钮关闭
        }
    });
    layer.full(index);

}


function createParam(action, keyid) {
    var o = {};
    var query = top.$('#uiform').serializeArray();
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}

function initflowbody() {//初始化编辑器统一入口
    //初始化后加工工厂
    //var gys = '{ "groupOp":"AND", "rules": [{ "field":"fenlei", "op":"cn", "data":"供应商"}], "groups": [] }';
    top.$("#txt_SchemeName").combogrid({
        delay: 500, //自动完成功能实现搜索
        mode: 'remote',//开启后系统会自动传一个参数q到后台 
        panelWidth: 350,
        required: true,
        queryParams: {
            //filter: gys
        },
        //value:'fullname',   
        editable: true,
        idField: 'SchemeName',
        //textField: 'comname',
        url: '/flow/ashx/FlowSchemeHandler.ashx',
        columns: [[
            { field: 'KeyId', title: '编号', width: 50 },
            { field: 'SchemeName', title: '工作流方案', width: 120 },


        ]],
        limitToList: true,//只能从下拉中选择值
        //reversed: true,//定义在失去焦点的时候是否恢复原始值。
        onHidePanel: function () {
            var t = top.$(this).combogrid('getValue');//获取combogrid的值
            var g = top.$(this).combogrid('grid');	// 获取数据表格对象
            var r = g.datagrid('getSelected');	// 获取选择的行
            console.log("选择的行是：" + r + "选择的值是:" + t);
            if (r == null || t != r.SchemeName) {//没有选择或者选项不相等时清除内容
                top.$.messager.alert('警告', '请选择，不要直接输入!');
                top.$(this).combogrid('setValue', '');
            } else {
                //do something...
            }
        },
        onSelect: function (rowIndex, rowData) {
            //console.log(JSON.stringify(rowData));
            console.log("设置的值是：" + rowData.KeyId);
            //$("#unitid").combogrid("setValue",rowData.KeyId);//给经手人设置ID
            top.$("#txt_SchemeId").numberspinner("setValue", rowData.KeyId);//工作流方案ID
            top.$("#txt_SchemeContent").textbox("setValue", rowData.SchemeContent);//流程内容json格式


        }
    });
}

var CRUD = {
    add: function () {
        var hDialog = top.jQuery.hDialog({
            title: '添加', width: 1000, height: 800, href: formurl, iconCls: 'icon-add',
            onLoad: function () {
                initflowbody();
            },
            submit: function () {
                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate')) {
                    var query = createParam('add', '0');
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (parseInt(d) > 0) {
                            msg.ok('添加成功！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                }
                msg.warning('请填写必填项！');
                return false;
            }
        });

        top.$('#uiform').validate();
    },
    edit: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '编辑', width: 1000, height: 700, href: formurl, iconCls: 'icon-save',
                onLoad: function () {
                    top.$('#txt_Id').numberspinner('setValue', row.Id);
                    top.$('#txt_InstanceSchemeId').numberspinner('setValue', row.InstanceSchemeId);
                    top.$('#txt_Code').textbox('setValue', row.Code);
                    top.$('#txt_CustomName').textbox('setValue', row.CustomName);
                    top.$('#txt_ActivityId').numberspinner('setValue', row.ActivityId);
                    top.$('#txt_ActivityType').numberspinner('setValue', row.ActivityType);
                    top.$('#txt_ActivityName').textbox('setValue', row.ActivityName);
                    top.$('#txt_PreviousId').numberspinner('setValue', row.PreviousId);
                    top.$('#txt_SchemeContent').textbox('setValue', row.SchemeContent);
                    top.$('#txt_SchemeId').numberspinner('setValue', row.SchemeId);
                    top.$('#txt_DbName').textbox('setValue', row.DbName);
                    top.$('#txt_FrmData').textbox('setValue', row.FrmData);
                    top.$('#txt_FrmType').numberspinner('setValue', row.FrmType);
                    top.$('#txt_FrmContentData').textbox('setValue', row.FrmContentData);
                    top.$('#txt_FrmContentParse').textbox('setValue', row.FrmContentParse);
                    top.$('#txt_FrmId').numberspinner('setValue', row.FrmId);
                    top.$('#txt_SchemeType').textbox('setValue', row.SchemeType);
                    top.$('#txt_Disabled').numberspinner('setValue', row.Disabled);
                    top.$('#txt_CreateDate').datetimebox('setValue', row.CreateDate);
                    top.$('#txt_CreateUserId').numberspinner('setValue', row.CreateUserId);
                    top.$('#txt_CreateUserName').textbox('setValue', row.CreateUserName);
                    top.$('#txt_FlowLevel').numberspinner('setValue', row.FlowLevel);
                    top.$('#txt_Description').textbox('setValue', row.Description);
                    top.$('#txt_IsFinish').numberspinner('setValue', row.IsFinish);
                    top.$('#txt_MakerList').textbox('setValue', row.MakerList);
                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');    
                },
                submit: function () {
                    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                    //alert(top.$("#uiform").form('validate'));
                    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {
                        var query = createParam('edit', row.KeyId);
                        jQuery.ajaxjson(actionURL, query, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    }
                    msg.warning('请填写必填项！');
                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

//实现动态隐藏列
var cmenu = null;
function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#list').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#list').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#list').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#list').datagrid('getColumnOption', field);
        cmenu.menu('appendItem', {
            text: col.title,
            name: field,
            iconCls: 'icon-ok'
        });
    }
}

//实现动态隐藏列结束

//双击编辑表单焦点消失后保存
var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#list').datagrid('validateRow', editIndex)) {
        $('#list').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function onDblClickCell(index, field) {
    if (editIndex != index) {
        if (endEditing()) {
            $('#list').datagrid('selectRow', index)
            $('#list').datagrid('beginEdit', index);
            var ed = $('#list').datagrid('getEditor', { index: index, field: field });
            if (ed) {
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                //这个写法很有意思
                //为了方便 ,类似字段要同事写两个值 1把部门id写入隐藏字段2把标题换成值写入 显示字段

            }
            editIndex = index;
        } else {
            setTimeout(function () {
                $('#list').datagrid('selectRow', editIndex);
            }, 0);
        }
    }
}

function onUnselect(index, row) {
    //alert(index);
}

function onCancelEdit(index, row) {
    //alert(index);
}
function onClickRow(index, row) {
    if (editIndex != undefined) {
        //alert("正在编辑的行是：" + editIndex);
        //alert("验证正在编辑的行：" + $('#list').datagrid('validateRow', editIndex));
        if ($('#list').datagrid('validateRow', editIndex)) {//如果验证正在编辑的行数据有效则
            $("#list").datagrid('endEdit', editIndex);//如果点其他行，结束编辑正在编辑的行
            editIndex = undefined;
        } else {
            msg.warning("还有一行没编辑完啦！！");
        }
    }
    $("#list").datagrid('selectRow', index);


}

function onSelect(index, row) {

}

function onEndEdit(index, row, changes) {
    //alert('结束编辑'+index+JSON.stringify(row));
    var o = {};
    var query = JSON.stringify(row);
    o.jsonEntity = query;
    o.action = 'edit';
    o.keyid = row.KeyId;
    query = "json=" + JSON.stringify(o);
    //alert(query);
    //表格内编辑模式编辑成功不提示信息
    jQuery.ajaxjson(actionURL, query, function (d) {
        //if (parseInt(d) > 0) {
        //    msg.ok('编辑成功！');
        //    hDialog.dialog('close');
        grid.reload();
        // } else {
        //     MessageOrRedirect(d);
        // }
    });

}
function append() {
    if (endEditing()) {
        $('#list').datagrid('appendRow', { status: 'P' });
        editIndex = $('#list').datagrid('getRows').length - 1;
        $('#list').datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
    }
}
function removeit() {
    if (editIndex == undefined) { return }
    $('#list').datagrid('cancelEdit', editIndex)
        .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept() {
    if (endEditing()) {
        $('#list').datagrid('acceptChanges');
    }
}
function reject() {
    $('#list').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges() {
    var rows = $('#list').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}
//双击编辑表单焦点消失后保存结束

//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var fenei = $("#type").combobox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    var i = 0;
    if (fenei != '' & fenei != undefined) {
        o.rules[i] = JSON.parse('{"field":"fenlei","op":"cn","data":"' + fenei + '"}');
    } else {
        o.rules[i] = JSON.parse('{"field":"fenlei","op":"ne","data":"999"}');
    }

    $('#list').datagrid('reload', { action: "flow", type: fenei });
}


//自定义搜索结束


//单选多选开关
$('#selectSwitch').switchbutton({
    checked: false,
    onChange: function (checked) {
        if (checked) {
            $('#list').datagrid({ singleSelect: false });//多选
        } else {
            $('#list').datagrid({ singleSelect: true });//单选
        }
    }
})

