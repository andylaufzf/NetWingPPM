using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;
using System.Data.Linq.Mapping;

namespace NetWing.Model
{
	[TableName("FlowInstanceOperationHistory")]
	[Description("工作流实例操作历史")]
    [Table(Name = "FlowInstanceOperationHistory")]//为了兼容linq
    public class FlowInstanceOperationHistoryModel
	{
				/// <summary>
		/// 主键Id
		/// </summary>
		[Description("主键Id")]
        [Column(IsPrimaryKey = true)]
        public int KeyId { get; set; }		
		/// <summary>
		/// 实例进程Id
		/// </summary>
		[Description("实例进程Id")]
        [Column]
		public int InstanceId { get; set; }		
		/// <summary>
		/// 操作内容
		/// </summary>
		[Description("操作内容")]
        [Column]
        public string Content { get; set; }		
		/// <summary>
		/// 创建时间
		/// </summary>
		[Description("创建时间")]
        [Column]
        public DateTime CreateDate { get; set; }		
		/// <summary>
		/// 创建用户主键
		/// </summary>
		[Description("创建用户主键")]
        [Column]
        public int CreateUserId { get; set; }		
		/// <summary>
		/// 创建用户
		/// </summary>
		[Description("创建用户")]
        [Column]
        public string CreateUserName { get; set; }


        /// <summary>
        /// 1:同意；2：不同意；3：驳回
        /// </summary>
        [Description("审核类型")]
        [Column]
        public string VerificationFinally { get; set; }

        /// <summary>
        /// 审核意见
        /// </summary>
        [Description("审核意见")]
        [Column]
        public string VerificationOpinion { get; set; }

        /// <summary>
        /// 节点类型
        /// </summary>
        [Description("流程节点类型")]
        [Column]
        public string NodeName { get; set; }

        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}