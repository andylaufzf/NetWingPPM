using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;
using System.Data.Linq.Mapping;

namespace NetWing.Model
{
    [TableName("FlowScheme")]
    [Description("流程设计管理")]
    [Table(Name = "FlowScheme")]//为了兼容linq
    public partial class FlowSchemeModel : NetWing.Common.EF.Entity
    {

        //public FlowSchemeModel()
        //{
        //    this.SchemeCode = string.Empty;
        //    this.SchemeName = string.Empty;
        //    this.SchemeType = string.Empty;
        //    this.SchemeVersion = string.Empty;
        //    this.SchemeCanUser = string.Empty;
        //    this.SchemeContent = string.Empty;
        //    this.FrmId = 0;
        //    this.FrmType = 0;
        //    this.AuthorizeType = 0;
        //    this.SortCode = 0;
        //    this.DeleteMark = 0;
        //    this.Disabled = 0;
        //    this.Description = string.Empty;
        //    this.CreateDate = DateTime.Now;
        //    this.CreateUserId = 0;
        //    this.CreateUserName = string.Empty;
        //    this.ModifyDate = DateTime.Now;
        //    this.ModifyUserId = 0;
        //    this.ModifyUserName = string.Empty;
        //}





        /// <summary>
        /// 主键Id
        /// </summary>
        [Description("主键Id")]
        [Column(IsPrimaryKey = true)]
        public int KeyId { get; set; }
        /// <summary>
        /// 流程编号
        /// </summary>
        [Description("流程编号")]
        [Column(Name = "SchemeCode")]
        public string SchemeCode { get; set; }
        /// <summary>
        /// 流程名称
        /// </summary>
        [Description("流程名称")]
        [Column(Name = "SchemeName")]
        public string SchemeName { get; set; }
        /// <summary>
        /// 流程分类
        /// </summary>
        [Description("流程分类")]
        [Column(Name = "SchemeType")]
        public string SchemeType { get; set; }
        /// <summary>
        /// 流程内容版本
        /// </summary>
        [Description("流程内容版本")]
        [Column(Name = "SchemeVersion")]
        public string SchemeVersion { get; set; }
        /// <summary>
        /// 流程模板使用者
        /// </summary>
        [Description("流程模板使用者")]
        [Column(Name = "SchemeCanUser")]
        public string SchemeCanUser { get; set; }
        /// <summary>
        /// 流程内容
        /// </summary>
        [Description("流程内容")]
        [Column(Name = "SchemeContent")]
        public string SchemeContent { get; set; }
        /// <summary>
        /// 表单ID
        /// </summary>
        [Description("表单ID")]
        [Column(Name = "FrmId")]
        public int FrmId { get; set; }
        /// <summary>
        /// 表单类型
        /// </summary>
        [Description("表单类型")]
        [Column(Name = "FrmType")]
        public int FrmType { get; set; }
        /// <summary>
        /// 模板权限类型：0完全公开,1指定部门/人员
        /// </summary>
        [Description("模板权限类型：0完全公开,1指定部门/人员")]
        [Column(Name = "AuthorizeType")]
        public int AuthorizeType { get; set; }
        /// <summary>
        /// 排序码
        /// </summary>
        [Description("排序码")]
        [Column(Name = "SortCode")]
        public int SortCode { get; set; }
        /// <summary>
        /// 删除标记
        /// </summary>
        [Description("删除标记")]
        [Column(Name = "DeleteMark")]
        public int DeleteMark { get; set; }
        /// <summary>
        /// 有效
        /// </summary>
        [Description("有效")]
        [Column(Name = "Disabled")]
        public int Disabled { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        [Column(Name = "Description")]
        public string Description { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        [Description("创建时间")]
        [Column(Name = "CreateDate")]
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// 创建用户主键
        /// </summary>
        [Description("创建用户主键")]
        [Column(Name = "CreateUserId")]
        public int CreateUserId { get; set; }
        /// <summary>
        /// 创建用户
        /// </summary>
        [Description("创建用户")]
        [Column(Name = "CreateUserName")]
        public string CreateUserName { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        [Description("修改时间")]
        [Column(Name = "ModifyDate")]
        public DateTime ModifyDate { get; set; }
        /// <summary>
        /// 修改用户主键
        /// </summary>
        [Description("修改用户主键")]
        [Column(Name = "ModifyUserId")]
        public int ModifyUserId { get; set; }
        /// <summary>
        /// 修改用户
        /// </summary>
        [Description("修改用户")]
        [Column(Name = "ModifyUserName")]
        public string ModifyUserName { get; set; }

        public override string ToString()
        {
            return JSONhelper.ToJson(this);
        }
    }
}