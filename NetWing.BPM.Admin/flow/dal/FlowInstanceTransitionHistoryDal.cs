using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class FlowInstanceTransitionHistoryDal : BaseRepository<FlowInstanceTransitionHistoryModel>
    {
        public static FlowInstanceTransitionHistoryDal Instance
        {
            get { return SingletonProvider<FlowInstanceTransitionHistoryDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(FlowInstanceTransitionHistoryModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}