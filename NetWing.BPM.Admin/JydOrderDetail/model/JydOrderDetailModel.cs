using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
    [TableName("jydOrderDetail")]
    [Description("订单明细")]
    public class JydOrderDetailModel
    {
        /// <summary>
        /// KeyId
        /// </summary>
        [Description("KeyId")]
        public int KeyId { get; set; }

        [Description("主表id关联")]
        public int omainid { get; set; }

        /// <summary>
        /// orderid
        /// </summary>
        [Description("orderid")]
        public string orderid { get; set; }
        /// <summary>
        /// yspmc
        /// </summary>
        [Description("yspmc")]
        public string yspmc { get; set; }
        /// <summary>
        /// PrintType
        /// </summary>
        [Description("PrintType")]
        public string PrintType { get; set; }
        /// <summary>
        /// cpcc
        /// </summary>
        [Description("cpcc")]
        public string cpcc { get; set; }
        /// <summary>
        /// zzf
        /// </summary>
        [Description("zzf")]
        public string zzf { get; set; }
        /// <summary>
        /// zzn
        /// </summary>
        [Description("zzn")]
        public string zzn { get; set; }
        /// <summary>
        /// ym
        /// </summary>
        [Description("ym")]
        public string ym { get; set; }
        /// <summary>
        /// sj
        /// </summary>
        [Description("sj")]
        public string sj { get; set; }
        /// <summary>
        /// rj
        /// </summary>
        [Description("rj")]
        public string rj { get; set; }
        /// <summary>
        /// jp
        /// </summary>
        [Description("jp")]
        public string jp { get; set; }
        /// <summary>
        /// zz
        /// </summary>
        [Description("zz")]
        public string zz { get; set; }
        /// <summary>
        /// sl
        /// </summary>
        [Description("sl")]
        public decimal sl { get; set; }
        /// <summary>
        /// cc
        /// </summary>
        [Description("cc")]
        public string cc { get; set; }
        /// <summary>
        /// dj
        /// </summary>
        [Description("dj")]
        public decimal dj { get; set; }
        /// <summary>
        /// je
        /// </summary>
        [Description("je")]
        public decimal je { get; set; }
        /// <summary>
        /// note
        /// </summary>
        [Description("note")]
        public string note { get; set; }
        /// <summary>
        /// kzcc
        /// </summary>
        [Description("kzcc")]
        public string kzcc { get; set; }
        /// <summary>
        /// zs
        /// </summary>
        [Description("zs")]
        public string zs { get; set; }
        /// <summary>
        /// fs
        /// </summary>
        [Description("fs")]
        public string fs { get; set; }
        /// <summary>
        /// pss
        /// </summary>
        [Description("pss")]
        public string pss { get; set; }
        /// <summary>
        /// pst
        /// </summary>
        [Description("pst")]
        public string pst { get; set; }
        /// <summary>
        /// fbyq
        /// </summary>
        [Description("fbyq")]
        public string fbyq { get; set; }
        /// <summary>
        /// jx
        /// </summary>
        [Description("jx")]
        public string jx { get; set; }
        /// <summary>
        /// fumoa
        /// </summary>
        [Description("fumoa")]
        public string fumoa { get; set; }
        /// <summary>
        /// fumob
        /// </summary>
        [Description("fumob")]
        public string fumob { get; set; }
        /// <summary>
        /// yaa
        /// </summary>
        [Description("yaa")]
        public string yaa { get; set; }
        /// <summary>
        /// yab
        /// </summary>
        [Description("yab")]
        public string yab { get; set; }
        /// <summary>
        /// tana
        /// </summary>
        [Description("tana")]
        public string tana { get; set; }
        /// <summary>
        /// tanb
        /// </summary>
        [Description("tanb")]
        public string tanb { get; set; }
        /// <summary>
        /// zheye
        /// </summary>
        [Description("zheye")]
        public string zheye { get; set; }
        /// <summary>
        /// uva
        /// </summary>
        [Description("uva")]
        public string uva { get; set; }
        /// <summary>
        /// uvb
        /// </summary>
        [Description("uvb")]
        public string uvb { get; set; }
        /// <summary>
        /// zd
        /// </summary>
        [Description("zd")]
        public string zd { get; set; }
        /// <summary>
        /// db
        /// </summary>
        [Description("db")]
        public string db { get; set; }
        /// <summary>
        /// bh
        /// </summary>
        [Description("bh")]
        public string bh { get; set; }
        /// <summary>
        /// sh
        /// </summary>
        [Description("sh")]
        public string sh { get; set; }
        /// <summary>
        /// sk
        /// </summary>
        [Description("sk")]
        public string sk { get; set; }
        /// <summary>
        /// sjwcsj
        /// </summary>
        [Description("sjwcsj")]
        public string sjwcsj { get; set; }
        /// <summary>
        /// cpwcsj
        /// </summary>
        [Description("cpwcsj")]
        public string cpwcsj { get; set; }
        /// <summary>
        /// ctpsj
        /// </summary>
        [Description("ctpsj")]
        public string ctpsj { get; set; }
        /// <summary>
        /// sbwcsj
        /// </summary>
        [Description("sbwcsj")]
        public string sbwcsj { get; set; }
        /// <summary>
        /// qslsj
        /// </summary>
        [Description("qslsj")]
        public string qslsj { get; set; }
        /// <summary>
        /// yswcsj
        /// </summary>
        [Description("yswcsj")]
        public string yswcsj { get; set; }
        /// <summary>
        /// hjgkssj
        /// </summary>
        [Description("hjgkssj")]
        public string hjgkssj { get; set; }
        /// <summary>
        /// hjgwcsj
        /// </summary>
        [Description("hjgwcsj")]
        public string hjgwcsj { get; set; }
        /// <summary>
        /// wjgjcsj
        /// </summary>
        [Description("wjgjcsj")]
        public string wjgjcsj { get; set; }
        /// <summary>
        /// qcpsj
        /// </summary>
        [Description("qcpsj")]
        public string qcpsj { get; set; }
        /// <summary>
        /// zdkssj
        /// </summary>
        [Description("zdkssj")]
        public string zdkssj { get; set; }
        /// <summary>
        /// dzwcsj
        /// </summary>
        [Description("dzwcsj")]
        public string dzwcsj { get; set; }
        /// <summary>
        /// shryccsj
        /// </summary>
        [Description("shryccsj")]
        public string shryccsj { get; set; }
        /// <summary>
        /// shryhcsj
        /// </summary>
        [Description("shryhcsj")]
        public string shryhcsj { get; set; }
        /// <summary>
        /// dbsj
        /// </summary>
        [Description("dbsj")]
        public string dbsj { get; set; }
        /// <summary>
        /// rksj
        /// </summary>
        [Description("rksj")]
        public string rksj { get; set; }
        /// <summary>
        /// cksj
        /// </summary>
        [Description("cksj")]
        public string cksj { get; set; }
        /// <summary>
        /// zrr
        /// </summary>
        [Description("zrr")]
        public string zrr { get; set; }

        [Description("责任人ID")]
        public int zrrid { get; set; }
        /// <summary>
        /// kkyy
        /// </summary>
        [Description("kkyy")]
        public string kkyy { get; set; }
        /// <summary>
        /// sxj
        /// </summary>
        [Description("sxj")]
        public string sxj { get; set; }
        [Description("收现金金额")]
        public decimal sxjje { get; set; }
        /// <summary>
        /// szp
        /// </summary>
        [Description("szp")]
        public string szp { get; set; }
        /// <summary>
        /// zph
        /// </summary>
        [Description("zph")]
        public string zph { get; set; }
        [Description("收支票金额")]
        public decimal szpje { get; set; }
        /// <summary>
        /// myws
        /// </summary>
        [Description("myws")]
        public string myws { get; set; }
        /// <summary>
        /// myysf
        /// </summary>
        [Description("myysf")]
        public string myysf { get; set; }
        /// <summary>
        /// O_myhjg
        /// </summary>
        [Description("O_myhjg")]
        public string O_myhjg { get; set; }
        /// <summary>
        /// qianshou
        /// </summary>
        [Description("qianshou")]
        public int qianshou { get; set; }
        /// <summary>
        /// skje
        /// </summary>
        [Description("skje")]
        public decimal skje { get; set; }
        /// <summary>
        /// wsjgbz
        /// </summary>
        [Description("wsjgbz")]
        public string wsjgbz { get; set; }
        /// <summary>
        /// dddd
        /// </summary>
        [Description("dddd")]
        public string dddd { get; set; }
        /// <summary>
        /// kkkje
        /// </summary>
        [Description("kkkje")]
        public int kkkje { get; set; }
        /// <summary>
        /// yspmcb
        /// </summary>
        [Description("yspmcb")]
        public string yspmcb { get; set; }
        /// <summary>
        /// FileTypeb
        /// </summary>
        [Description("FileTypeb")]
        public string FileTypeb { get; set; }
        /// <summary>
        /// mianb
        /// </summary>
        [Description("mianb")]
        public string mianb { get; set; }
        /// <summary>
        /// munb
        /// </summary>
        [Description("munb")]
        public string munb { get; set; }
        /// <summary>
        /// colorb
        /// </summary>
        [Description("colorb")]
        public string colorb { get; set; }
        /// <summary>
        /// pagerb
        /// </summary>
        [Description("pagerb")]
        public string pagerb { get; set; }
        /// <summary>
        /// O_my_jpb
        /// </summary>
        [Description("O_my_jpb")]
        public string O_my_jpb { get; set; }
        /// <summary>
        /// O_my_zzb
        /// </summary>
        [Description("O_my_zzb")]
        public string O_my_zzb { get; set; }
        /// <summary>
        /// ddddb
        /// </summary>
        [Description("ddddb")]
        public string ddddb { get; set; }
        /// <summary>
        /// zznb
        /// </summary>
        [Description("zznb")]
        public string zznb { get; set; }
        /// <summary>
        /// ymb
        /// </summary>
        [Description("ymb")]
        public string ymb { get; set; }
        /// <summary>
        /// sjb
        /// </summary>
        [Description("sjb")]
        public string sjb { get; set; }
        /// <summary>
        /// rjb
        /// </summary>
        [Description("rjb")]
        public string rjb { get; set; }
        /// <summary>
        /// jpb
        /// </summary>
        [Description("jpb")]
        public string jpb { get; set; }
        /// <summary>
        /// zzb
        /// </summary>
        [Description("zzb")]
        public string zzb { get; set; }
        /// <summary>
        /// fumoab
        /// </summary>
        [Description("fumoab")]
        public string fumoab { get; set; }
        /// <summary>
        /// fumobb
        /// </summary>
        [Description("fumobb")]
        public string fumobb { get; set; }
        /// <summary>
        /// yaab
        /// </summary>
        [Description("yaab")]
        public string yaab { get; set; }
        /// <summary>
        /// yabb
        /// </summary>
        [Description("yabb")]
        public string yabb { get; set; }
        /// <summary>
        /// tanab
        /// </summary>
        [Description("tanab")]
        public string tanab { get; set; }
        /// <summary>
        /// tanbb
        /// </summary>
        [Description("tanbb")]
        public string tanbb { get; set; }
        /// <summary>
        /// zheyeb
        /// </summary>
        [Description("zheyeb")]
        public string zheyeb { get; set; }
        /// <summary>
        /// uvab
        /// </summary>
        [Description("uvab")]
        public string uvab { get; set; }
        /// <summary>
        /// uvbb
        /// </summary>
        [Description("uvbb")]
        public string uvbb { get; set; }
        /// <summary>
        /// zdb
        /// </summary>
        [Description("zdb")]
        public string zdb { get; set; }
        /// <summary>
        /// dbb
        /// </summary>
        [Description("dbb")]
        public string dbb { get; set; }
        /// <summary>
        /// bhb
        /// </summary>
        [Description("bhb")]
        public string bhb { get; set; }
        /// <summary>
        /// shb
        /// </summary>
        [Description("shb")]
        public string shb { get; set; }
        /// <summary>
        /// skb
        /// </summary>
        [Description("skb")]
        public string skb { get; set; }
        /// <summary>
        /// PrintTypeb
        /// </summary>
        [Description("PrintTypeb")]
        public string PrintTypeb { get; set; }
        /// <summary>
        /// zzfb
        /// </summary>
        [Description("zzfb")]
        public string zzfb { get; set; }
        /// <summary>
        /// noteb
        /// </summary>
        [Description("noteb")]
        public string noteb { get; set; }
        /// <summary>
        /// cpccb
        /// </summary>
        [Description("cpccb")]
        public string cpccb { get; set; }
        /// <summary>
        /// printyes
        /// </summary>
        [Description("printyes")]
        public int printyes { get; set; }
        /// <summary>
        /// kzccb
        /// </summary>
        [Description("kzccb")]
        public string kzccb { get; set; }
        /// <summary>
        /// zsb
        /// </summary>
        [Description("zsb")]
        public string zsb { get; set; }
        /// <summary>
        /// fsb
        /// </summary>
        [Description("fsb")]
        public string fsb { get; set; }
        /// <summary>
        /// pssb
        /// </summary>
        [Description("pssb")]
        public string pssb { get; set; }
        /// <summary>
        /// pstb
        /// </summary>
        [Description("pstb")]
        public string pstb { get; set; }
        /// <summary>
        /// fbyqb
        /// </summary>
        [Description("fbyqb")]
        public string fbyqb { get; set; }
        /// <summary>
        /// jxb
        /// </summary>
        [Description("jxb")]
        public string jxb { get; set; }
        /// <summary>
        /// kkkjeb
        /// </summary>
        [Description("kkkjeb")]
        public string kkkjeb { get; set; }
        /// <summary>
        /// nysl
        /// </summary>
        [Description("nysl")]
        public int nysl { get; set; }
        /// <summary>
        /// ctpa
        /// </summary>
        [Description("ctpa")]
        public string ctpa { get; set; }
        /// <summary>
        /// ctpb
        /// </summary>
        [Description("ctpb")]
        public string ctpb { get; set; }
        /// <summary>
        /// zhizhangbb
        /// </summary>
        [Description("zhizhangbb")]
        public string zhizhangbb { get; set; }
        /// <summary>
        /// songhuo
        /// </summary>
        [Description("songhuo")]
        public DateTime songhuo { get; set; }
        /// <summary>
        /// xiaoyang
        /// </summary>
        [Description("xiaoyang")]
        public string xiaoyang { get; set; }
        /// <summary>
        /// danwei_m
        /// </summary>
        [Description("danwei_m")]
        public string danwei_m { get; set; }
        /// <summary>
        /// danwei_mb
        /// </summary>
        [Description("danwei_mb")]
        public string danwei_mb { get; set; }
        /// <summary>
        /// bz_hx
        /// </summary>
        [Description("bz_hx")]
        public string bz_hx { get; set; }
        /// <summary>
        /// bz_wbz_cc
        /// </summary>
        [Description("bz_wbz_cc")]
        public string bz_wbz_cc { get; set; }
        /// <summary>
        /// bz_wbz_gy
        /// </summary>
        [Description("bz_wbz_gy")]
        public string bz_wbz_gy { get; set; }
        /// <summary>
        /// bz_nbz_zz
        /// </summary>
        [Description("bz_nbz_zz")]
        public string bz_nbz_zz { get; set; }
        /// <summary>
        /// bz_nbz_cc
        /// </summary>
        [Description("bz_nbz_cc")]
        public string bz_nbz_cc { get; set; }
        /// <summary>
        /// bz_nbz_gy
        /// </summary>
        [Description("bz_nbz_gy")]
        public string bz_nbz_gy { get; set; }
        /// <summary>
        /// bz_wkz_zz
        /// </summary>
        [Description("bz_wkz_zz")]
        public string bz_wkz_zz { get; set; }
        /// <summary>
        /// bz_wkz_cc
        /// </summary>
        [Description("bz_wkz_cc")]
        public string bz_wkz_cc { get; set; }
        /// <summary>
        /// bz_wkz_gy
        /// </summary>
        [Description("bz_wkz_gy")]
        public string bz_wkz_gy { get; set; }
        /// <summary>
        /// bz_bc_yl
        /// </summary>
        [Description("bz_bc_yl")]
        public string bz_bc_yl { get; set; }
        /// <summary>
        /// bz_bc_sl
        /// </summary>
        [Description("bz_bc_sl")]
        public string bz_bc_sl { get; set; }
        /// <summary>
        /// bz_bc_mx
        /// </summary>
        [Description("bz_bc_mx")]
        public string bz_bc_mx { get; set; }
        /// <summary>
        /// bz_nt
        /// </summary>
        [Description("bz_nt")]
        public string bz_nt { get; set; }
        /// <summary>
        /// bz_nt_cc
        /// </summary>
        [Description("bz_nt_cc")]
        public string bz_nt_cc { get; set; }
        /// <summary>
        /// bz_nt_mx
        /// </summary>
        [Description("bz_nt_mx")]
        public string bz_nt_mx { get; set; }
        /// <summary>
        /// bz_wbz
        /// </summary>
        [Description("bz_wbz")]
        public string bz_wbz { get; set; }

        /// <summary>
        /// 自带汇总,新系统兼容老系统
        /// </summary>
        /// <returns></returns>
        [Description("自带汇总,新系统兼容老系统")]
        public string zidaihz { get; set; }
        [Description("画册内页自带汇总,新系统兼容老系统")]
        public string zidaihchz { get; set; }

        [Description("后加工下单人")]
        public string myxd { get; set; }

        [Description("后加工下单人ID")]
        public int myxdid { get; set; }

        [Description("后加工下单日期")]
        public DateTime myxdrq { get; set; }

        [Description("后加工收货人")]
        public string mysh { get;set;}

        [Description("后加工收货人ID")]
        public int myshid { get; set; }

        [Description("后加工收货日期")]
        public DateTime myjhrq { get; set; }

        [Description("后加工尺寸")]
        public string mycc { get; set; }

        [Description("后加工数量")]
        public int mynum { get; set; }

        [Description("后加工单价")]
        public decimal mydj { get; set; }

        [Description("后加工金额")]
        public decimal myje { get; set; }

        [Description("后加工制作项目")]
        public string myzzxm { get; set; }

        [Description("二维码")]
        public string order_img { get; set; }

        [Description("收货时间")]
        public DateTime receiving_time { get; set; }

        [Description("实收数量")]
        public int actual_quantity { get; set; }

        [Description("后加工付款")]
        public string status_fk { get; set; }
        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}