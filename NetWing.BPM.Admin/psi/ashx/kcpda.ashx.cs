﻿using NetWing.BPM.Core;
using NetWing.Common;
using NetWing.Common.Data;
using NetWing.Common.Data.SqlServer;
using NetWing.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace NetWing.BPM.Admin.psi.ashx
{
    /// <summary>
    /// kcpda 的摘要说明
    /// </summary>
    public class kcpda : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.Write("Hello World");
            var main = HttpContext.Current.Request["main"];//得到主表json数据
            var detail = HttpContext.Current.Request["detail"];//得到明细表json数据
            //这里可以执行一些逻辑
            if (string.IsNullOrEmpty(detail))
            {
                context.Response.Write("{\"status\":0,\"msg\":\"失败！主表或子表信息为空！\"}");
                context.Response.End();//输出结束
            }



            Psi_inventoryModel mainModel = new Psi_inventoryModel();
            #region 子表明细表处理
            JArray detailArr = JArray.Parse(detail);//把明细表转化为数组
            var k = detailArr[0].ToString();

            JObject jo = (JObject)JsonConvert.DeserializeObject(k);
            #endregion
            #region 数据库事务
            SqlTransaction tran = SqlHelper.BeginTransaction(SqlEasy.connString);//取得一个事务
            try
            {
                #endregion
                //循环明细表
                for (int i = 0; i < detailArr.Count; i++)//有几条就执行几次
                {
                    var detailobj = JSONhelper.ConvertToObject<Psi_inventoryModel>(detailArr[i].ToString());//根据模型把明细表转化为obj
                    Psi_inventoryModel detailModel = new Psi_inventoryModel();//初始化明细表模型
                    detailModel.InjectFrom(detailobj);//把obj插入模型
                    detailModel.goods_id = string.IsNullOrEmpty(jo["goodsId"].ToString())?0: int.Parse(jo["goodsId"].ToString());//商品ID
                    detailModel.goods_name = string.IsNullOrEmpty(jo["goodsClassName"].ToString())?"": jo["goodsClassName"].ToString(); //商品名称
                    detailModel.inventorytime = DateTime.Now;//盘日期
                    detailModel.discl = string.IsNullOrEmpty(jo["note"].ToString())?0:int.Parse(jo["note"].ToString());//盘数量
                    detailModel.actuall = string.IsNullOrEmpty(jo["stock"].ToString())?0: int.Parse(jo["stock"].ToString());//实际数量
                    detailModel.quantity = string.IsNullOrEmpty(jo["buyNum"].ToString())?0:int.Parse(jo["buyNum"].ToString());//差异量
                    detailModel.discname_id = string.IsNullOrEmpty(SysVisitor.Instance.cookiesUserId)?0:int.Parse(SysVisitor.Instance.cookiesUserId);//盘数人id
                    detailModel.discname = string.IsNullOrEmpty(SysVisitor.Instance.cookiesUserName) ?"": SysVisitor.Instance.cookiesUserName;//盘点人
                    //detailModel.toexamine_id = mainModel.unitid;//审核人id
                    //detailModel.toexamine_name = mainModel.unit_man;//审核人
                    //detailModel.toexamine_time = DateTime.Now;//审核时间
                    //detailModel.toexamine_status = k[8];//审核状态
                    detailModel.Accouprice = string.IsNullOrEmpty(jo["buyPrice"].ToString())?"":jo["buyPrice"].ToString();//核算单价
                    detailModel.Profitandamount = string.IsNullOrEmpty(jo["buyAllMoney"].ToString())?"":jo["buyAllMoney"].ToString();//盈亏金额
                    DbUtils.tranInsert(detailModel, tran);//执行写入明细表



                }
                tran.Commit();//提交执行事务
                context.Response.Write("{\"status\":1,\"msg\":\"提交成功！\"}");
            }
            catch (Exception e)
            {
                tran.Rollback();//错误！事务回滚！
                context.Response.Write("{\"status\":0,\"msg\":\"" + e.ToString() + "\"}");
                throw;
            }

        }



        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}