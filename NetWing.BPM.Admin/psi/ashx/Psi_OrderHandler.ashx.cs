using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Web.SessionState;
using NetWing.Model;
using NetWing.Bll;
using Omu.ValueInjecter;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Bll;
using NetWing.Common;
using NetWing.Common.Data;
using NetWing.Common.Data.SqlServer;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Excel;
using NetWing.Common.IO;
using NetWing.Common.IO.DirFile;
using NetWing.Common.Data.Filter;
using NetWing.BPM.Core.Bll;
using NetWing.BPM.Core.Model;
using NetWing.BPM.Core.Dal;

namespace NetWing.BPM.Admin.Psi_Order.ashx
{
    /// <summary>
    /// 采购订单明细 的摘要说明
    /// </summary>
    public class Psi_OrderHandler : IHttpHandler,IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            int k;
            var json = HttpContext.Current.Request["json"];
            var rpm = new RequestParamModel<Psi_OrderModel>(context) { CurrentContext = context };
            if (!string.IsNullOrEmpty(json))
            {
                rpm = JSONhelper.ConvertToObject<RequestParamModel<Psi_OrderModel>>(json);
                rpm.CurrentContext = context;
            }

            switch (rpm.Action)
            {
                case "add":
                    context.Response.Write(Psi_OrderBll.Instance.Add(rpm.Entity));
                    break;
                case "edit":
                    Psi_OrderModel d = new Psi_OrderModel();
                    d.InjectFrom(rpm.Entity);
                    d.KeyId = rpm.KeyId;
                    context.Response.Write(Psi_OrderBll.Instance.Update(d));
                    break;
                case "export":
                    //string fields = rpm.fields;
                    string fields = rpm.CurrentContext.Request["fields"];
                    string tablename = TableConvention.Resolve(typeof(Psi_OrderModel));//得到表名
                    DataTable xlsDt = SqlEasy.ExecuteDataTable("select " + fields + " from " + tablename + "");
                    ExcelHelper.NPIOtoExcel(xlsDt, HttpContext.Current.Server.MapPath("\\upload\\excel\\" + tablename + ".xls"));
                    context.Response.Write("{\"status\":\"ok\",\"filename\":\"" + tablename + ".xls\"}");
                    break;
                case "inport"://从Excel导入到数据库
                    if (context.Request["REQUEST_METHOD"] == "OPTIONS")
                    {
                        context.Response.End();
                    }
                    SaveFile("~/temp/", context);
                    break;
                case "delete":
                    //context.Response.Write(Psi_OrderBll.Instance.Delete(rpm.KeyId));
                    DataRow dr = SqlEasy.ExecuteDataRow("select * from Psi_Order where keyid=" + rpm.KeyId + "");
                    string edNumber = dr["edNumber"].ToString();
                    string orderType = dr["orderType"].ToString();
                    SqlTransaction tran = SqlHelper.BeginTransaction(SqlEasy.connString);//取得一个事务
                    try
                    {
                        DbUtils.tranExecuteNonQuery("delete from Psi_Order where edNumber='"+ edNumber + "'", tran);//删除进销存订单主表
                        //库存处理
                        if (orderType=="进货单")//进货单删除后减库存
                        {
                            SqlDataReader sdr = SqlEasy.ExecuteDataReader("select * from Psi_BuyDetails where edNumber='" + edNumber + "'");
                            while (sdr.Read())
                            {
                                //循环更新库存信息，同时更新进价，因为每个时候的进价都会不一样
                                string stocksql = "update Psi_Goods set stock=stock-" + sdr["buynum"].ToString() + ",buyprice=" + sdr["buyprice"].ToString() + ",up_time='" + DateTime.Now.ToString() + "' where keyid=" + sdr["goodsid"].ToString() + "";
                                DbUtils.tranExecuteNonQuery(stocksql, tran);
                            }
                            //删除之前加银行存款
                           
                            int c = DbUtils.tranExecuteNonQuery("update MJBankAccount set accountBalance=accountBalance+" + dr["total"].ToString() + " where KeyId=" + dr["paybankid"].ToString() + "", tran);
                        }
                        if (orderType == "出货单")//出货单删除后加库存
                        {
                            SqlDataReader sdr = SqlEasy.ExecuteDataReader("select * from Psi_BuyDetails where edNumber='" + edNumber + "'");
                            while (sdr.Read())
                            {
                                //循环更新库存信息，同时更新进价，因为每个时候的进价都会不一样
                                string stocksql = "update Psi_Goods set stock=stock+" + sdr["buynum"].ToString() + ",buyprice=" + sdr["buyprice"].ToString() + ",up_time='" + DateTime.Now.ToString() + "' where keyid=" + sdr["goodsid"].ToString() + "";
                                DbUtils.tranExecuteNonQuery(stocksql, tran);
                            }
                            //删除之前减银行存款
                            
                            int c = DbUtils.tranExecuteNonQuery("update MJBankAccount set accountBalance=accountBalance-" + dr["total"].ToString() + " where KeyId=" + dr["paybankid"].ToString() + "", tran);
                        }

                        DbUtils.tranExecuteNonQuery("delete from Psi_BuyDetails where edNumber='" + edNumber + "'", tran);//删除进销存订单明细表
                       

                        DbUtils.tranExecuteNonQuery("delete from MJFinanceMain where edNumber='" + edNumber + "'", tran);//删除财务主表
                        DbUtils.tranExecuteNonQuery("delete from MJFinanceDetail where edNumber='" + edNumber + "'", tran);//删除财务明细表
                        tran.Commit();//提交执行事务
                        LogModel log = new LogModel();
                        log.BusinessName = "进销存单据删除成功";
                        log.OperationIp = PublicMethod.GetClientIP();
                        log.OperationTime = DateTime.Now;
                        log.PrimaryKey = "";
                        log.UserId = int.Parse(SysVisitor.Instance.cookiesUserId);
                        log.TableName = "";
                        log.note = "事务顺利执行";
                        log.OperationType = (int)OperationType.Other;
                        LogDal.Instance.Insert(log);
                        context.Response.Write("1");
                    }
                    catch (Exception err)
                    {
                        LogModel log = new LogModel();
                        log.BusinessName = "进销存单据删除失败";
                        log.OperationIp = PublicMethod.GetClientIP();
                        log.OperationTime = DateTime.Now;
                        log.PrimaryKey = "";
                        log.UserId = int.Parse(SysVisitor.Instance.cookiesUserId);
                        log.TableName = "";
                        log.note = err.Message;
                        log.OperationType = (int)OperationType.Other;
                        LogDal.Instance.Insert(log);
                        tran.Rollback();//回滚事务
                        context.Response.Write("0");
                        //throw;
                    }
                    
                    break;
                case "alldel"://2017-04-05新增的功能 批量删除删除结果返回删除条数
                    context.Response.Write(NetWing.Dal.Psi_OrderDal.Instance.Delete(rpm.KeyIds));
                    break;
                default:
                    string sqlwhere = "(depid in (" + SysVisitor.Instance.Departments + "))";
                    if (SysVisitor.Instance.IsAdmin)
                    { //判断是否是超管如果是超管理，所有显示
                        sqlwhere = " 1=1 ";//如果是超管则不显示
                    }
                    if (!string.IsNullOrEmpty(rpm.Filter))//如果筛选不为空
                    {
                        string str = " and " + FilterTranslator.ToSql(rpm.Filter);
                        sqlwhere = sqlwhere + str;
                    }

                    string sort = rpm.Sort;
                    if (sort == null)
                    {
                        sort = "keyid desc";
                    }


                    //TableConvention.Resolve(typeof(MJUserModel)) 转换成表名
                    var pcp = new ProcCustomPage(TableConvention.Resolve(typeof(Psi_OrderModel)))
                    {
                        PageIndex = rpm.Pageindex,
                        PageSize = rpm.Pagesize,
                        OrderFields = sort,
                        WhereString = sqlwhere

                    };
                    int recordCount;
                    DataTable dt = DbUtils.GetPageWithSp(pcp, out recordCount);
                    context.Response.Write(JSONhelper.FormatJSONForEasyuiDataGrid(recordCount, dt));
                    //context.Response.Write(Psi_OrderBll.Instance.GetJson(rpm.Pageindex, rpm.Pagesize, rpm.Filter, rpm.Sort, rpm.Order));
                    break;
            }
        }

        /// <summary>
        /// 文件保存操作
        /// </summary>
        /// <param name="basePath"></param>
        private void SaveFile(string basePath, HttpContext context)
        {
            var name = string.Empty;
            basePath = (basePath.IndexOf("~") > -1) ? context.Server.MapPath(basePath) :
            basePath;
            HttpFileCollection files = context.Request.Files;

            if (!Directory.Exists(basePath))//如果文件夹不存在创建文件夹
                Directory.CreateDirectory(basePath);
            //清空temp文件夹
            DirFileHelper.ClearDirectory(basePath);

            var suffix = files[0].ContentType.Split('/');
            var _suffix = suffix[1].Equals("jpeg", StringComparison.CurrentCultureIgnoreCase) ? "" : suffix[1];
            var _temp = System.Web.HttpContext.Current.Request["name"];

            if (!string.IsNullOrEmpty(_temp))
            {
                name = _temp;
            }
            else
            {
                Random rand = new Random(24 * (int)DateTime.Now.Ticks);
                name = rand.Next() + "." + _suffix;
            }

            var full = basePath + name;
            files[0].SaveAs(full);

            DataTable dt = NPOIHelper.ImportExceltoDt(full);
            string connectionString = SqlEasy.connString;
            SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(connectionString, SqlBulkCopyOptions.UseInternalTransaction);
            sqlbulkcopy.DestinationTableName = TableConvention.Resolve(typeof(Psi_OrderModel));//数据库中的表名
                                                                                   //自定义的datatable和数据库的字段进行对应  
                                                                                   //sqlBC.ColumnMappings.Add("id", "tel");  
                                                                                   //sqlBC.ColumnMappings.Add("name", "neirong");  
            int k = dt.Rows.Count - 1;                                                                       //注意一个问题，最后一列是字段数
            for (int i = 0; i < (dt.Columns.Count - 1); i++)
            {
                sqlbulkcopy.ColumnMappings.Add(dt.Columns[i].ColumnName.ToString(), dt.Columns[i].ColumnName.ToString());

            }
            var _result = "";
            try
            {
                sqlbulkcopy.WriteToServer(dt);
                _result = "{\"msg\" : \"导入数据库成功!\", \"result\" : " + k + ", \"filename\" : \"" + name + "\"}";
            }
            catch (Exception)
            {
                _result = "{\"msg\" : \"导入失败,可能模板不对，或其他原因，建议导出数据作为模板重新处理。注意导入没有校验数据重复功能。请人工校验数据!\", \"result\" : 0, \"filename\" : \"" + name + "\"}";
                //throw;
            }



            System.Web.HttpContext.Current.Response.Write(_result);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}