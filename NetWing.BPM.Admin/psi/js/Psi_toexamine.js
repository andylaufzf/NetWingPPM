﻿var actionURL = '/psi/ashx/Psi_BuyDetailsHandler.ashx?orderType=进货单';
var formurl = '/psi/html/Psi_BuyDetails.html';

$(function () {

    //为避免数据字典加载不完整。这段在后面加载
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });

    $('#a_add').click(CRUD.add);
    $('#a_edit').click(CRUD.edit);
    $('#a_delete').click(CRUD.del);
    //高级查询
    $('#a_search').click(function () {
        mysearch();//
        //search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#a_getSearch').click(function () {//自定义搜索框
        //mySearch();
    });

    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });

    //批量删除
    $("#a_alldel").click(function () {
        var ids = [];
        var rows = $('#list').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].KeyId);
        }
        var allid = ids.join(',');//所有的id
        var o = {};
        o.action = "alldel";
        o.KeyIds = allid;
        var param = "json=" + JSON.stringify(o);

        if (confirm('确认要执行批量删除操作吗？')) {
            jQuery.ajaxjson(actionURL, param, function (d) {
                if (parseInt(d) > 0) {
                    msg.ok('批量删除成功！');
                    grid.reload();
                } else {
                    MessageOrRedirect(d);
                }
            });
        }


    });

});
//这段后加载，避免数据字典加载不完整的问题
window.onload = function () {
    autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
};


//editor:'datetimebox' 日期及时间选择框  editor:'datebox' 日期选择框    editor:'numberspinner' 数字调节器  editor:{type: 'numberspinner',options:{value:0,required:true}}

//定义一个$JQ为$.　以后在js 中就可以用${JQ}AJAX了在前台这样写(定义)://通用数据字典start
var getDic = {
    jsonData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?categoryId=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            //alert(newData);
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
    jsonParentData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?action=parent&parentid=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
}

//通用数据字典end
//调用数据字典和使用数据字典 如果不需要请不要打开否则会导致系统速度慢
//getDic.jsonData(9);//取得性别数据字典
//在onLoad:的地方如下使用
//top.$('#txt_user_sex').combobox({ data: eval($('body').data('data9')), valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '请选择性别', disabled: false });





var grid = {
    bind: function (winSize) {
        $('#list').datagrid({
            url: actionURL,
            toolbar: '#toolbar',
            title: "进货统计",
            onLoadSuccess: compute,//加载完毕后执行计算
            iconCls: 'icon icon-list',
            width: winSize.width,
            height: winSize.height,
            nowrap: false, //折行
            showFooter: true,//显示页脚
            rownumbers: true, //行号
            striped: true, //隔行变色
            idField: 'KeyId',//主键
            singleSelect: true, //单选
            frozenColumns: [[]],
            columns: [[//应为宽度不是很需要所以注释了宽度
                //{ title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                { title: '商品分类', field: 'goodsClassName', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '规格', field: 'specs', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                //{ title: '系统编号', field: 'KeyId', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '入库订单编号', field: 'edNumber', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '编号', field: 'goodsNo', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '名称', field: 'goodsName', sortable: true, width: '150', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                //{ title: '商品分类ID', field: 'goodsClassID', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '单位', field: 'unit', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '数量', field: 'buyNum', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '价格', field: 'buyPrice', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '金额', field: 'buyAllMoney', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '经手人', field: 'Contacts', sortable: true, width: '', hidden: false },
                { title: '进货单位', field: 'unitname', sortable: true, width: '', hidden: false },
                { title: '联系人', field: 'unit_man', sortable: true, width: '', hidden: false },
                { title: '联系电话', field: 'unit_moble', sortable: true, width: '', hidden: false },
                {
                    title: '时间', field: 'add_time', sortable: true, width: '', hidden: false, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } }, formatter: function (value, row, index) {
                        return new Date(row.add_time).Format("yyyy-MM-dd");
                    }
                },

                { title: '备注', field: 'note', sortable: true, width: '', hidden: false },
                {
                    title: '审核', field: 'examine_status', sortable: true, width: '', hidden: false,
                        
                    formatter: function (fd, rhfhfgow, fghfg) {
                        if (rhfhfgow.examine_status == 1) {
                            var btnnn = '<a onclick="shenhe(\'' + rhfhfgow.KeyId + '\',\'' + rhfhfgow.goodsId + '\')"  href="javascript:void(0)" style="background-color: #ff9000;padding: 3px 14px; color: #fff;font-size: 12px;">审核</a>&nbsp;';
                            return btnnn;
                        } else {
                            var btnnn = '<p href="javascript:void(0)" style="background-color: #985fa5;padding: 3px 14px; color: #fff;font-size: 12px;">已审核</p>&nbsp;';
                            return btnnn;
                        }
                                                 
                    }
                },
                //{ title: '更新时间', field: 'up_time', sortable: true, width: '', hidden: false, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } } },
                //{ title: '数据所有者', field: 'ownner', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
                //{ title: '部门所有者', field: 'depid', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },

            ]],
            onEndEdit: onEndEdit,//结束编辑时函数 这里为了简洁 该函数写在下面
            onUnselect: onUnselect,
            //onLoadSuccess: function (data) {
            //    //alert($('body').data('data70'));
            //    //alert($('body').data('data69'));
            //},
            onCancelEdit: onCancelEdit,//在用户取消编辑一行的时候触发
            onSelect: onSelect,//在用户选择一行的时候触发
            onClickRow: onClickRow,//在用户点击一行的时候触发
            //onAfterEdit: onAfterEdit,//在用户完成编辑一行的时候触发
            onDblClickCell: onDblClickCell,//为了程序逻辑清楚函数写在外面
            onHeaderContextMenu: function (e, field) {//列菜单实现动态隐藏列
                e.preventDefault();
                if (!cmenu) {
                    createColumnMenu();
                }
                cmenu.menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            pagination: true,
            pageSize: PAGESIZE,
            pageList: [20, 40, 50, 100, 200]
        });
    },
    getSelectedRow: function () {
        return $('#list').datagrid('getSelected');
    },
    reload: function () {
        $('#list').datagrid('clearSelections').datagrid('reload', { filter: '' });
    }
};

function createParam(action, keyid) {
    var o = {};
    var query = top.$('#uiform').serializeArray();
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}


var CRUD = {
    add: function () {
        var hDialog = top.jQuery.hDialog({
            title: '添加', width: 1000, height: 800, href: formurl, iconCls: 'icon-add',
            onLoad: function () {
                top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
            },
            submit: function () {
                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate')) {
                    var query = createParam('add', '0');
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (parseInt(d) > 0) {
                            msg.ok('添加成功！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                }
                msg.warning('请填写必填项！');
                return false;
            }
        });

        top.$('#uiform').validate();
    },
    edit: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '编辑', width: 1000, height: 700, href: formurl, iconCls: 'icon-save',
                onLoad: function () {
                    top.$('#txt_goodsClassName').textbox('setValue', row.goodsClassName);
                    top.$('#txt_specs').textbox('setValue', row.specs);
                    top.$('#txt_KeyId').numberspinner('setValue', row.KeyId);
                    top.$('#txt_PsiOrderID').numberspinner('setValue', row.PsiOrderID);
                    top.$('#txt_goodsNo').textbox('setValue', row.goodsNo);
                    top.$('#txt_goodsName').textbox('setValue', row.goodsName);
                    top.$('#txt_goodsClassID').numberspinner('setValue', row.goodsClassID);
                    top.$('#txt_unit').textbox('setValue', row.unit);
                    top.$('#txt_buyNum').numberspinner('setValue', row.buyNum);
                    top.$('#txt_buyPrice').textbox('setValue', row.buyPrice);
                    top.$('#txt_buyAllMoney').textbox('setValue', row.buyAllMoney);
                    top.$('#txt_add_time').datetimebox('setValue', row.add_time);
                    top.$('#txt_up_time').datetimebox('setValue', row.up_time);
                    top.$('#txt_ownner').numberspinner('setValue', row.ownner);
                    top.$('#txt_depid').numberspinner('setValue', row.depid);
                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');    
                },
                submit: function () {
                    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                    //alert(top.$("#uiform").form('validate'));
                    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {
                        var query = createParam('edit', row.KeyId);;
                        jQuery.ajaxjson(actionURL, query, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    }
                    msg.warning('请填写必填项！');
                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

//实现动态隐藏列
var cmenu = null;
function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#list').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#list').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#list').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#list').datagrid('getColumnOption', field);
        cmenu.menu('appendItem', {
            text: col.title,
            name: field,
            iconCls: 'icon-ok'
        });
    }
}

//实现动态隐藏列结束

//双击编辑表单焦点消失后保存
//var editIndex = undefined;
//function endEditing() {
//    if (editIndex == undefined) { return true }
//    if ($('#list').datagrid('validateRow', editIndex)) {
//        $('#list').datagrid('endEdit', editIndex);
//        editIndex = undefined;
//        return true;
//    } else {
//        return false;
//    }
//}
function onDblClickCell(index, field) {
    if (editIndex != index) {
        if (endEditing()) {
            $('#list').datagrid('selectRow', index)
            $('#list').datagrid('beginEdit', index);
            var ed = $('#list').datagrid('getEditor', { index: index, field: field });
            if (ed) {
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                //这个写法很有意思
                //为了方便 ,类似字段要同事写两个值 1把部门id写入隐藏字段2把标题换成值写入 显示字段

            }
            editIndex = index;
        } else {
            setTimeout(function () {
                $('#list').datagrid('selectRow', editIndex);
            }, 0);
        }
    }
}

function onUnselect(index, row) {
    //alert(index);
}

function onCancelEdit(index, row) {
    //alert(index);
}
function onClickRow(index, row) {
    if (editIndex != undefined) {
        //alert("正在编辑的行是：" + editIndex);
        //alert("验证正在编辑的行：" + $('#list').datagrid('validateRow', editIndex));
        if ($('#list').datagrid('validateRow', editIndex)) {//如果验证正在编辑的行数据有效则
            $("#list").datagrid('endEdit', editIndex);//如果点其他行，结束编辑正在编辑的行
            editIndex = undefined;
        } else {
            msg.warning("还有一行没编辑完啦！！");
        }
    }
    $("#list").datagrid('selectRow', index);


}

function onSelect(index, row) {

}

function onEndEdit(index, row, changes) {
    //alert('结束编辑'+index+JSON.stringify(row));
    var o = {};
    var query = JSON.stringify(row);
    o.jsonEntity = query;
    o.action = 'edit';
    o.keyid = row.KeyId;
    query = "json=" + JSON.stringify(o);
    //alert(query);
    //表格内编辑模式编辑成功不提示信息
    jQuery.ajaxjson(actionURL, query, function (d) {
        //if (parseInt(d) > 0) {
        //    msg.ok('编辑成功！');
        //    hDialog.dialog('close');
        grid.reload();
        // } else {
        //     MessageOrRedirect(d);
        // }
    });

}
function append() {
    if (endEditing()) {
        $('#list').datagrid('appendRow', { status: 'P' });
        editIndex = $('#list').datagrid('getRows').length - 1;
        $('#list').datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
    }
}
function removeit() {
    if (editIndex == undefined) { return }
    $('#list').datagrid('cancelEdit', editIndex)
        .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept() {
    if (endEditing()) {
        $('#list').datagrid('acceptChanges');
    }
}
function reject() {
    $('#list').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges() {
    var rows = $('#list').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}
//双击编辑表单焦点消失后保存结束

//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var myunit = $("#myUnit").textbox('getValue');
    var connman = $("#myConnMan").textbox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    var i = 0;
    if (myunit != '' && myunit != undefined) {//假如单位搜索不为空
        o.rules[i] = JSON.parse('{"field":"unit","op":"cn","data":"' + myunit + '"}');
        i = i + 1;
    }
    if (connman != '' & connman != undefined) {//联系人不为空
        o.rules[i] = JSON.parse('{"field":"connman","op":"cn","data":"' + connman + '"}');
    }

    $('#list').datagrid('reload', { filter: JSON.stringify(o) });


}


//自定义搜索结束


//单选多选开关
$('#selectSwitch').switchbutton({
    checked: false,
    onChange: function (checked) {
        if (checked) {
            $('#list').datagrid({ singleSelect: false });//多选
        } else {
            $('#list').datagrid({ singleSelect: true });//单选
        }
    }
})


// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
//调用： 

//var time1 = new Date().Format("yyyy-MM-dd");
//var time2 = new Date().Format("yyyy-MM-dd HH:mm:ss"); 
function mysearch() {
    //alert("d");
    //规则
    //{ "field":"dcname", "op":"cn", "data":"包含" },
    //{ "field":"dcname", "op":"eq", "data":"等于" },
    //{ "field":"dcname", "op":"ge", "data":"大于等于" },
    //{ "field":"dcname", "op":"le", "data":"小于等于" },
    //{ "field":"dcname", "op":"ne", "data":"不等于" },
    //{ "field":"dcname", "op":"gt", "data":"大于" },
    //{ "field":"dcname", "op":"lt", "data":"小于" },
    //{ "field":"dcname", "op":"bw", "data":"以xx开始" },
    //{ "field":"dcname", "op":"bw", "data":"以xx结束" }
    var goodsName = $('#txtgoodsName').textbox("getValue");//产品名称
    var Contacts = $('#txtContacts').textbox("getValue");//经手人
    var unitname = $('#txtunitname').textbox("getValue");//出货单位
    var unit_man = $('#txtunit_man').textbox("getValue");//出货单位联系人
    var unit_moble = $('#txtunit_moble').textbox("getValue");//出货单位联系人手机
    var dtOpstart = $('#dtOpstart').datebox('getValue');
    var dtOpend = $('#dtOpend').datebox('getValue');
    var ruleArr = [];
    if (goodsName != '')
        ruleArr.push({ "field": "goodsName", "op": "cn", "data": goodsName });//包含商品名
    if (Contacts != '')
        ruleArr.push({ "field": "Contacts", "op": "cn", "data": Contacts });//包含经手人
    if (unit_man != '')
        ruleArr.push({ "field": "unit_man", "op": "cn", "data": unit_man });//包含出货单位联系人
    if (unit_moble != '')
        ruleArr.push({ "field": "unit_moble", "op": "cn", "data": unit_moble });//包含出货单位联系人电话
    if (unitname != '')
        ruleArr.push({ "field": "unitname", "op": "cn", "data": unitname });//包含出货单位
    if (dtOpstart != '')
        ruleArr.push({ "field": "add_time", "op": "ge", "data": dtOpstart });//大于等于开始时间
    if (dtOpend != '')
        ruleArr.push({ "field": "add_time", "op": "le", "data": dtOpend });//小于等于结束时间

    //if (opType != '')
    //    ruleArr.push({ "field": "OperationType", "op": "in", "data": opType });
    //if (busingessName != "")
    //    ruleArr.push({ "field": "BusinessName", "op": "eq", "data": escape(busingessName) });

    if (ruleArr.length > 0) {
        var filterObj = { groupOp: 'AND', rules: ruleArr };
        //alert(JSON.stringify(filterObj));
        $('#list').datagrid('load', { filter: JSON.stringify(filterObj) });
    } else {
        grid.reload();
    }

}
//统计函数
function compute() {//计算函数
    var rows = $('#list').datagrid('getRows')//获取当前的数据行
    var numAll = 0//计算listprice的总和
        , moneyAll = 0;//统计unitcost的总和
    for (var i = 0; i < rows.length; i++) {
        numAll += rows[i]['buyNum'];
        moneyAll += rows[i]['buyAllMoney'];
    }
    //新增一行显示统计信息
    // 更新页脚行并载入新数据






//$('#list').datagrid('appendRow', { itemid: '<b>统计：</b>'});
}

//管理员修改审核状态
function shenhe(KeyId,gooid) {
    //var mydd = confirm("如此单已通知工厂加工,请点击确定");
    //if (!mydd) {
    //    return false;
    //}
    console.log('haohahoaaohaoaoahohaoaoa' + gooid);
    $.ajax({
        url: "/JydOrder/ashx/JydOrderHandler.ashx",
        type: "post",
        dataType: "json",
        data: {
            action: 'shenhe',
            KeyId: KeyId ,
            gooid: gooid
        },
        success: function (d) {
            if (d.code === 0) {
                window.location.reload();
                top.$.messager.alert('系统提示', d.msg, 'warning');
            }
        }
    });
}




