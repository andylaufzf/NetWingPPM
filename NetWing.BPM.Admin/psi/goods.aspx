﻿<%@ Page Language="C#"  MasterPageFile="../Site1.Master"  AutoEventWireup="true" CodeBehind="goods.aspx.cs" Inherits="NetWing.BPM.Admin.psi.goods" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="layout">
        <div region="west" iconcls="icon-chart_organisation" split="true" title="商品类别" style="width: 220px; padding: 0px" collapsible="false">
            <div class="toolbar" style="background: #efefef; border-bottom: 0px solid #ccc;margin:0 0 0 0;">
                <a id="a_addCategory" style="float: left" href="javascript:;" plain="true" class="easyui-linkbutton" icon="icon-add" title="添加字典类别">添加</a>
                <a id="a_editCategory" style="float: left" href="javascript:;" plain="true" class="easyui-linkbutton" icon="icon-edit" title="修改字典类别">修改</a>
                <a id="a_delCategory" style="float: left" href="javascript:;" plain="true" class="easyui-linkbutton" icon="icon-delete" title="删除字典类别">删除</a>
            </div>
            <div style="padding: 0px;">
                <ul id="dataDicType"></ul>
            </div>
            <div id="noCategoryInfo" style="font-family: 微软雅黑; font-size: 18px; color: #BCBCBC; padding: 40px 5px; display: none;">
                还没有商品类别哦，点击 添加 按钮创建新的类别。
            </div>
        </div>

        <div region="center" border="false" style="padding: 0px; overflow: hidden;">
            <div id="toolbar"><%=base.BuildToolbar() %></div>
            <table id="dicGrid"></table>
        </div>
    </div>
    <script type="text/javascript" src="js/goods.js?v=90"></script>
</asp:Content>
