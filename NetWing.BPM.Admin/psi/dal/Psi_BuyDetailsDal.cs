using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class Psi_BuyDetailsDal : BaseRepository<Psi_BuyDetailsModel>
    {
        public static Psi_BuyDetailsDal Instance
        {
            get { return SingletonProvider<Psi_BuyDetailsDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(Psi_BuyDetailsModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}