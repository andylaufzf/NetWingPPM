﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="psi_supplement.aspx.cs" Inherits="NetWing.BPM.Admin.psi.psi_supplement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <!-- 工具栏按钮 -->
    <!-- datagrid 列表 -->
    <div id="tt" class="easyui-tabs" style="width: ; height:800px ;">
               <div title="出货单" style="padding: 20px; display: none;">
            <!--请修改项目顺序和修改样式-->
          <!--  系统ID：<input id="KeyId" class="easyui-textbox" name="KeyId" style="width: 100px;">
            订单类型：进、销：<input id="orderType" class="easyui-textbox" name="orderType" style="width: 100px;">
            -->
            补料人：<input id="unit" class="easyui-textbox" name="unit" style="width: 100px;">
        <!--    往来单位ID：<input id="unitid" class="easyui-textbox" name="unitid" style="width: 100px;">-->
            补料联系人：<input id="unit_man" class="easyui-textbox" name="unit_man" style="width: 100px;">
            补料联系电话：<input id="unit_moble" class="easyui-textbox" name="unit_moble" style="width: 100px;">
           <!--隐藏经手人姓名--><div style="display:none;"> 经手人：<input id="Contacts" class="easyui-textbox" name="Contacts" style="width: 100px;"></div>
            经手人：<input id="ContactsId" class="easyui-textbox" name="ContactsId" style="width: 100px;">
            
            <br /><br />备注：<input id="note" class="easyui-textbox" name="note" style="width: 300px;">
            
         <!--   添加日期：<input id="add_time" class="easyui-textbox" name="add_time" style="width: 100px;">
            更新日期：<input id="up_time" class="easyui-textbox" name="up_time" style="width: 100px;">
            数据所有者：<input id="ownner" class="easyui-textbox" name="ownner" style="width: 100px;">
            数据部门所有者：<input id="depid" class="easyui-textbox" name="depid" style="width: 100px;">-->
            <br />
            <br />
            <table id="list"></table>
            <br />
            <br />
            
            合计金额：<input id="total" class="easyui-numberbox" name="total" data-options="precision:2" style="width: 100px;">
            实付金额：<input id="shifu" class="easyui-numberbox" data-options="precision:2" name="shifu" value="0" style="width: 100px;">
            合计数量：<input id="sumnum" class="easyui-numberbox" name="sumnum" style="width: 100px;">
            <div style="display:none;">收款账户：<input id="paybank" class="easyui-textbox" name="paybank" style="width: 100px;"></div>
            收款账户：<input id="paybankid" class="easyui-textbox accountid"   name="paybankid" style="width: 100px;">
            <br />
            <br />
            注:如果内部补料用、售价请填"进价",并注明用途。
            <br />
            <br />
            <a id="submit" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'">提交</a>
            <br />
            <br />
             小贴士:提交只能点击一次！！！如补料单中单笔录入错误。可录入一条“申购进货单”来调整库存。调整时请备注。如果整单错误。可以到“单据管理”删除整张单子重新录入。删除时限是24小时。也就是说录入错误的单子可以24小时内删除补录。超过24小时不能删除。
            <br />

        </div>

    </div>


    <!--Uploader-->
    <!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
    <!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>
    <!-- 引入主表js文件 -->
    <script src="js/psi_supplement.js"></script>
</asp:Content>
