﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Psi_Buyinfo.aspx.cs" Inherits="NetWing.BPM.Admin.psi.Psi_Buyinfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->



    <!-- 工具栏按钮 -->
    <div id="toolbar">
            <div style="float: left; margin-top: 2px;">
            产品名称：<input type="text" class="easyui-textbox" name="goodsName" id="txtgoodsName" class="txt03" style="width: 80px;" />&nbsp;
            经手人：<input type="text" class="easyui-textbox" name="Contacts" id="txtContacts" class="txt03" style="width: 80px;" />&nbsp;
            进货单位：<input type="text" class="easyui-textbox" name="unitname" id="txtunitname" class="txt03" style="width: 80px;" />&nbsp;
            联系人：<input type="text" class="easyui-textbox" name="unit_man" id="txtunit_man" class="txt03" style="width: 80px;" />&nbsp;
            联系电话：<input type="text" class="easyui-textbox" name="unit_moble" id="txtunit_moble" class="txt03" style="width: 80px;" />&nbsp;
            日期：<input type="text" id="dtOpstart" style="width: 120px;" class="easyui-datetimebox" />
                至
            <input type="text" id="dtOpend" style="width: 120px;" class="easyui-datetimebox" />
                <a id="a_search" style="float:left" href="javascript:;" plain="true" class="easyui-linkbutton l-btn l-btn-small l-btn-plain" title="查询" group="">
                    <span class="l-btn-left l-btn-icon-left">
                        <span class="l-btn-text">查询</span>
                        <span class="l-btn-icon icon-search">&nbsp;</span>

                    </span>

                </a>
            </div>
            &nbsp;&nbsp;<%=base.BuildToolbar() %>

    </div>

    <!-- datagrid 列表 -->
    <table id="list"></table>
    <!--Uploader-->
    <!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
    <!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>


    <!-- 引入js文件 -->
    <script src="js/Psi_BuyInfo.js"></script>
</asp:Content>




