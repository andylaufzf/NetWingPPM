using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
    [TableName("Psi_Order")]
    [Description("采购订单表")]
    public class Psi_OrderModel
    {
        /// <summary>
        /// 系统ID
        /// </summary>
        [Description("系统ID")]
        public int KeyId { get; set; }
        /// <summary>
        /// 订单类型：进、销
        /// </summary>
        [Description("订单类型：进、销")]
        public string orderType { get; set; }
        /// <summary>
        /// 采购单位
        /// </summary>
        [Description("采购单位")]
        public string unit { get; set; }
        /// <summary>
        /// 往来单位ID
        /// </summary>
        [Description("往来单位ID")]
        public int unitid { get; set; }
        /// <summary>
        /// 采购单位联系人
        /// </summary>
        [Description("采购单位联系人")]
        public string unit_man { get; set; }
        /// <summary>
        /// 采购单位联系电话
        /// </summary>
        [Description("采购单位联系电话")]
        public string unit_moble { get; set; }
        /// <summary>
        /// 经手人
        /// </summary>
        [Description("经手人")]
        public string Contacts { get; set; }
        /// <summary>
        /// 经手人ID
        /// </summary>
        [Description("经手人ID")]
        public int ContactsId { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public string note { get; set; }
        /// <summary>
        /// 合计金额
        /// </summary>
        [Description("合计金额")]
        public decimal total { get; set; }
        [Description("实付金额")]
        public decimal shifu { get; set; }
        /// <summary>
        /// 合计数量
        /// </summary>
        [Description("合计数量")]
        public int sumnum { get; set; }
        /// <summary>
        /// 付款方式
        /// </summary>
        [Description("付款方式")]
        public string paybank { get; set; }

        /// <summary>
        /// 系统单号
        /// </summary>
        [Description("系统单号")]
        public string edNumber { get; set; }

        /// <summary>
        /// 付款方式ID
        /// </summary>
        [Description("付款方式ID")]
        public int paybankid { get; set; }
        /// <summary>
        /// 添加日期
        /// </summary>
        [Description("添加日期")]
        public DateTime add_time { get; set; }
        /// <summary>
        /// 更新日期
        /// </summary>
        [Description("更新日期")]
        public DateTime up_time { get; set; }
        /// <summary>
        /// 数据所有者
        /// </summary>
        [Description("数据所有者")]
        public int ownner { get; set; }
        /// <summary>
        /// 数据部门所有者
        /// </summary>
        [Description("数据部门所有者")]
        public int depid { get; set; }
        /// <summary>
        /// 数据部门所有者
        /// </summary>
        [Description("数据部门所有者")]
        public int orderid { get; set; }

        public override string ToString()
        {
            return JSONhelper.ToJson(this);
        }
    }
}