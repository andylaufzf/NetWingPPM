using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
    [TableName("Psi_BuyDetails")]
    [Description("采购订单明细")]
    public class Psi_BuyDetailsModel
    {
        /// <summary>
        /// 系统编号
        /// </summary>
        [Description("系统编号")]
        public int KeyId { get; set; }
        /// <summary>
        /// 购买订单编号
        /// </summary>
        [Description("购买订单编号")]
        public int PsiOrderID { get; set; }
        /// <summary>
        /// 商品编号
        /// </summary>
        [Description("商品编号")]
        public string goodsNo { get; set; }


        [Description("商品ID")]
        public int goodsId { get; set; }

        /// <summary>
        /// 商品名称
        /// </summary>
        [Description("商品名称")]
        public string goodsName { get; set; }
        /// <summary>
        /// 商品分类ID
        /// </summary>
        [Description("商品分类ID")]
        public int goodsClassID { get; set; }
        /// <summary>
        /// 商品分类名称
        /// </summary>
        [Description("商品分类名称")]
        public string goodsClassName { get; set; }
        /// <summary>
        /// 商品规格
        /// </summary>
        [Description("商品规格")]
        public string specs { get; set; }
        /// <summary>
        /// 采购单位
        /// </summary>
        [Description("采购单位")]
        public string unit { get; set; }
        /// <summary>
        /// 采购数量
        /// </summary>
        [Description("采购数量")]
        public int buyNum { get; set; }
        /// <summary>
        /// 采购价格
        /// </summary>
        [Description("采购价格")]
        public decimal buyPrice { get; set; }
        [Description("卖价")]
        public decimal sellPrice { get; set; }

        /// <summary>
        /// 金额
        /// </summary>
        [Description("金额")]
        public decimal buyAllMoney { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        [Description("添加时间")]
        public DateTime add_time { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Description("更新时间")]
        public DateTime up_time { get; set; }
        /// <summary>
        /// 数据所有者
        /// </summary>
        [Description("数据所有者")]
        public int ownner { get; set; }
        /// <summary>
        /// 部门所有者
        /// </summary>
        [Description("部门所有者")]
        public int depid { get; set; }

        [Description("单据类型")]
        public string orderType { get; set; }

        [Description("备注")]
        public string note { get; set; }
        [Description("付款银行")]
        public string paybank { get; set; }
        [Description("付款银行ID")]
        public int paybankid { get; set; }
        [Description("单位名称")]
        public string unitname { get; set; }
        [Description("单位ID")]
        public int unitid { get; set; }
        [Description("单位联系人")]
        public string unit_man { get; set; }
        [Description("单位联系人手机")]
        public string unit_moble { get; set; }
        [Description("经办人")]
        public string Contacts { get; set; }
        [Description("经办人ID")]
        public int ContactsId { get; set; }
        [Description("毛利")]
        public decimal profit { get; set; }
        [Description("车牌或车架号")]
        public decimal carno { get; set; }

        /// <summary>
        /// 系统单号
        /// </summary>
        [Description("系统单号")]
        public string edNumber { get; set; }
        /// <summary>
        /// 审核态
        /// </summary>
        /// <returns></returns>
        [Description("审核tai")]
        public int examine_status { get; set; }
        public override string ToString()
        {
            return JSONhelper.ToJson(this);
        }
    }
}