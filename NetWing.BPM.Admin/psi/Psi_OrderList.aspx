﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Psi_OrderList.aspx.cs" Inherits="NetWing.BPM.Admin.psi.Psi_OrderList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->



<!-- 工具栏按钮 -->
    <div id="toolbar"><%= base.BuildToolbar()%>
        
        类型:<select id="fenei" name="fenei" class="easyui-combobox" style="width: 80px;">
                <option value="">全部</option>
                <option value="出货单">出货单</option>
                <option value="进货单">进货单</option>
           </select>


        <a href="#" plain="true" class="easyui-linkbutton" title="采购单位">采购单位</a><input id="OrderID" name="OrderID"  style="width: 80px" class="easyui-textbox"/>
        
        日期：<input type="text" id="dtOpstart" style="width: 120px;" class="easyui-datetimebox" />
                至
            <input type="text" id="dtOpend" style="width: 120px;" class="easyui-datetimebox" />
        <a id="a_getSearch" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
    </div>

    
    <!-- datagrid 列表 -->
    <table id="list" ></table>  
    <!--隐藏时间，用于比对是否过了一天-->
    <div id="sysdate" style="display:none;"><%=DateTime.Now.ToString("yyyy-MM-dd hh:MM:ss") %></div>


	<!--Uploader-->
	<!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
	<!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>


    <!-- 引入js文件 -->
      <script src="js/Psi_OrderList.js"></script>
</asp:Content>




