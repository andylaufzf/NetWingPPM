using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJaccountingSubjects")]
	[Description("费用科目管理")]
	public class MJaccountingSubjectsModel
	{
				/// <summary>
		/// 科目编号
		/// </summary>
		[Description("科目编号")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 科目类型
		/// </summary>
		[Description("科目类型")]
		public string subjectType { get; set; }		
		/// <summary>
		/// 科目名称
		/// </summary>
		[Description("科目名称")]
		public string subjectName { get; set; }		
		/// <summary>
		/// 科目简拼
		/// </summary>
		[Description("科目简拼")]
		public string subjectNamePinyin { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string note { get; set; }		
		/// <summary>
		/// 停用标记
		/// </summary>
		[Description("停用标记")]
		public string is_del { get; set; }

        [Description("数据所有者ID")]
        public int ownner { get; set; }
        [Description("数据所有组ID")]
        public int depid { get; set; }
        /// <summary>
        /// 持续性收费0持续1一次性收费
        /// </summary>
        [Description("持续性收费")]
        public int theonce { get; set; }
        /// <summary>
        /// 是否允许退1允许0不允许
        /// </summary>
        [Description("是否允许退")]
        public int istui { get; set; }
        [Description("对应退科目ID")]
        public int reverseSubjects { get; set; }

        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}