using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("jydorderfenqifk")]
	[Description("分期付款")]
	public class JydorderfenqifkModel
	{
				/// <summary>
		/// ID
		/// </summary>
		[Description("ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 客户名称
		/// </summary>
		[Description("客户名称")]
		public string comname { get; set; }		
		/// <summary>
		/// 接件单号
		/// </summary>
		[Description("接件单号")]
		public string orderid { get; set; }		
		/// <summary>
		/// 印刷品名称
		/// </summary>
		[Description("印刷品名称")]
		public string yspmc { get; set; }		
		/// <summary>
		/// 数量
		/// </summary>
		[Description("数量")]
		public int sl { get; set; }		
		/// <summary>
		/// 单价
		/// </summary>
		[Description("单价")]
		public decimal dj { get; set; }		
		/// <summary>
		/// 金额
		/// </summary>
		[Description("金额")]
		public decimal je { get; set; }		
		/// <summary>
		/// 小计
		/// </summary>
		[Description("小计")]
		public decimal jexx { get; set; }		
		/// <summary>
		/// 预付款
		/// </summary>
		[Description("预付款")]
		public decimal yufuk { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}