using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydorderfenqifkBll
    {
        public static JydorderfenqifkBll Instance
        {
            get { return SingletonProvider<JydorderfenqifkBll>.Instance; }
        }

        public int Add(JydorderfenqifkModel model)
        {
            return JydorderfenqifkDal.Instance.Insert(model);
        }

        public int Update(JydorderfenqifkModel model)
        {
            return JydorderfenqifkDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydorderfenqifkDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydorderfenqifkDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
