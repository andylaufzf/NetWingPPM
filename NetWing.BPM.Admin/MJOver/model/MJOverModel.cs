using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJOver")]
	[Description("年结转")]
	public class MJOverModel
	{
				/// <summary>
		/// 账户ID
		/// </summary>
		[Description("账户ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 银行账户名称
		/// </summary>
		[Description("银行账户名称")]
		public string account { get; set; }		
		/// <summary>
		/// 银行账户ID
		/// </summary>
		[Description("银行账户ID")]
		public int accountid { get; set; }		
		/// <summary>
		/// 结转金额
		/// </summary>
		[Description("结转金额")]
		public decimal overmoney { get; set; }		
		/// <summary>
		/// 添加日期
		/// </summary>
		[Description("添加日期")]
		public DateTime add_time { get; set; }		
		/// <summary>
		/// 更新日期
		/// </summary>
		[Description("更新日期")]
		public DateTime up_time { get; set; }		
		/// <summary>
		/// 部门ID
		/// </summary>
		[Description("部门ID")]
		public int depid { get; set; }		
		/// <summary>
		/// 数据所有者
		/// </summary>
		[Description("数据所有者")]
		public int ownner { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		

        [Description("系统单号")]
        public string edNumber { get; set; }

        [Description("备注")]
        public string note { get; set; }
        [Description("操作人")]
        public string operate { get; set; }

        [Description("操作人ID")]
        public int operateid { get; set; }


        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}