
/*表单AJAX提交封装(包含验证)*/
function AjaxInitForm(formId, btnId, isDialog, urlId, callback) {
	//alert("ok");
	var formObj = $('#' + formId);
	var btnObj = $("#" + btnId);
	var urlObj = $("#" + urlId);
	var argNum = arguments.length; //参数个数
	//alert(formObj + formId + urlObj + argNum);
	formObj.Validform({
		tiptype: 4, //可用的值有：1、2、3、4和function函数，默认tiptype为1。 3、4是5.2.1版本新增
		tiptype: function(msg, o, cssctl) {
			if (o.type == 2) { //如果通过验证不提示信息

			} else {
				//layer mobile的方法
				layer.open({
					content: msg,
					//style: 'background-color:#09C1FF; color:#fff; border:none;',
					time: 2
				});
			}
			//alert(msg);
			//alert(o.type);
			//msg：提示信息;
			//o:{obj:*,type:*,curform:*},
			//obj指向的是当前验证的表单元素（或表单对象，验证全部验证通过，提交表单时o.obj为该表单对象），
			//type指示提示的状态，值为1、2、3、4， 1：正在检测/提交数据，2：通过验证，3：验证失败，4：提示ignore状态, 
			//curform为当前form对象;
			//cssctl:内置的提示信息样式控制函数，该函数需传入两个参数：显示提示信息的对象 和 当前提示的状态（既形参o中的type）;
		},
		callback: function(form) {
			//AJAX提交表单
			$(form).ajaxSubmit({
				beforeSubmit: formRequest,
				success: formResponse,
				error: formError,
				url: formObj.attr("url"),
				type: "post",
				dataType: "json",
				timeout: 60000
			});
			//alert("执行完coback");
			return false;
		}
	});
	//alert("ok");
	//表单提交前
	function formRequest(formData, jqForm, options) {
		//alert("调试:提交中");
		btnObj.prop("disabled", true);
		btnObj.val("提交中...");
	}

	//表单提交后
	function formResponse(data, textStatus) {
			//alert("调试:提交成功");
			if (data.status == 1) {
//				btnObj.val("提交成功");
				//是否提示，默认不提示
				if (isDialog == 1) {
//					layer.open({
						location.replace(location.href);//刷新当前页面
						//layer.msg(data.msg, {icon: 6});
//						content: data.msg,
						//style: 'background-color:#09C1FF; color:#fff; border:none;',
//						time: 2
//					});
				//如果保存成功！跳转网址 网址是系统返回的
//				location.href='usercenter.aspx?action=index';
					
				} else {
					if (argNum == 5) {
						callback();
					} else if (data.url) {
						location.href = data.url;
					} else if (urlObj) {
						location.href = urlObj.val();
					} else {
						location.reload();
					}
				}
			} else {
				//$.dialog.alert(data.msg);
				layer.open({
					content: data.msg,
					//style: 'background-color:#09C1FF; color:#fff; border:none;',
					time: 1
				});
				btnObj.prop("disabled", false);
				btnObj.val("再次提交");
			}
		}
		//表单提交出错

	function formError(XMLHttpRequest, textStatus, errorThrown) {
		//layer mobile的方法
		layer.open({
			content: "状态：" + textStatus + "；出错提示：" + errorThrown,
			//style: 'background-color:#09C1FF; color:#fff; border:none;',
			time: 2
		});
		btnObj.prop("disabled", false);
		btnObj.val("再次提交");
	}
}

/*切换验证码*/
function ToggleCode(obj, codeurl) {
	$(obj).children("img").eq(0).attr("src", codeurl + "?time=" + Math.random());
	return false;
}


//单击执行AJAX请求操作
function clickSubmit(sendUrl){
	$.ajax({
		type: "POST",
		url: sendUrl,
		dataType: "json",
		timeout: 20000,
		success: function(data, textStatus) {
			if (data.status == 1){
					layer.open({content:data.msg,time:1});
					location.reload();
			    
			} else {
				layer.open({content:data.msg,time:1});
			}
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			
			layer.open({content:"状态：" + textStatus + "；出错提示：" + errorThrown,time:1});
		}
	});
}

//全选取消按钮函数，调用样式如：
function checkAll(chkobj){
	if($(chkobj).text()=="全选"){
	    $(chkobj).text("取消");
		$(".checkall").prop("checked", true);
	}else{
    	$(chkobj).text("全选");
		$(".checkall").prop("checked", false);
	}
}


//执行删除操作
function ExecDelete(sendUrl, checkValue, urlId){
	var urlObj = $('#' + urlId);
	//检查传输的值
	if (!checkValue) {
		layer.open({content:"请选中记录!",time:1});
        return false;
	}
	layer.open({
	content: '删除记录后不可恢复，您确定吗？',
		btn: ['确认'],
		yes:function(){
			$.ajax({
			type: "POST",
			url: sendUrl,
			dataType: "json",
			data: {
				"checkId":  checkValue
			},
			timeout: 20000,
			success: function(data, textStatus) {
				if (data.status == 1){
						if(urlObj){
							location.href = urlObj.val();
						}else{
							location.reload();
						}
					
				} else {
					layer.open({content:data.msg,time:1});
				}
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				layer.open({content:"状态：" + textStatus + "；出错提示：" + errorThrown,time:1});
				
			}
		});
		}
	});

}



//删除元素
function HintRemove(obj) {
	$("#" + obj).remove();
}

//添加进购物车
function CartAdd(obj, webpath, linktype, linkurl) {
	if ($("#goods_id").val() == "" || $("#goods_quantity").val() == "") {
		return false;
	}
	$.ajax({
		type: "post",
		url: webpath + "tools/submit_ajax.ashx?action=cart_goods_add",
		data: {
			"goods_id": $("#goods_id").val(),
			"goods_quantity": $("#goods_quantity").val()
		},
		dataType: "json",
		beforeSend: function(XMLHttpRequest) {
			//发送前动作
		},
		success: function(data, textStatus) {
			if (data.status == 1) {
				if (linktype == 1) {
					location.href = linkurl;
				} else {
					$("#cart_info_hint").remove();
					var HintHtml = '<div id="cart_info_hint" class="msg_tips cart_info">' + '<div class="ico"></div>' + '<div class="msg">' + '<strong>商品已成功添加到购物车！</strong>' + '<p>购物车共有<b>' + data.quantity + '</b>件商品，合计：<b class="red">' + data.amount + '</b>元</p>' + '<a class="btn btn-success" title="去购物车结算" href="' + linkurl + '">去结算</a>&nbsp;&nbsp;' + '<a title="再逛逛" href="javascript:;" onclick="HintRemove(\'cart_info_hint\');">再逛逛</a>' + '<a class="close" title="关闭" href="javascript:;" onclick="HintRemove(\'cart_info_hint\');"><span>关闭</span></a>' + '</div>' + '</div>'
					$(obj).after(HintHtml); //添加节点
					$("#shoppingCartCount").text(data.quantity); //赋值给显示购物车数量的元素
				}
			} else {
				$("#cart_info_hint").remove();
				var HintHtml = '<div id="cart_info_hint" class="msg_tips cart_info">' + '<div class="ico error"></div>' + '<div class="msg">' + '<strong>商品添加到购物车失败！</strong>' + '<p>' + data.msg + '</p>' + '<a class="close" title="关闭" href="javascript:void(0);" onclick="HintRemove(\'cart_info_hint\');"><span>关闭</span></a>' + '</div>' + '</div>'
				$(obj).after(HintHtml); //添加节点
				//alert(data.msg);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("状态：" + textStatus + "；出错提示：" + errorThrown);
		},
		timeout: 20000
	});
	return false;
}


//添加进购物车-列表页方法
function CartAddList(obj, webpath, linktype, linkurl, goods_id, goods_quantity) {
	if (goods_id == "" || goods_quantity == "") {
		return false;
	}
	$.ajax({
		type: "post",
		url: webpath + "tools/submit_ajax.ashx?action=cart_goods_add",
		data: {
			"goods_id": goods_id,
			"goods_quantity": goods_quantity
		},
		dataType: "json",
		beforeSend: function(XMLHttpRequest) {
			//发送前动作
		},
		success: function(data, textStatus) {
			if (data.status == 1) {
				if (linktype == 1) {
					location.href = linkurl;
				} else {
					$("#cart_info_hint").remove();
					var HintHtml = '<div id="cart_info_hint" class="msg_tips cart_info">' + '<div class="ico"></div>' + '<div class="msg">' + '<strong>商品已成功添加到购物车！</strong>' + '<p>购物车共有<b>' + data.quantity + '</b>件商品，合计：<b class="red">' + data.amount + '</b>元</p>' + '<a class="btn btn-success" title="去购物车结算" href="' + linkurl + '">去结算</a>&nbsp;&nbsp;' + '<a title="再逛逛" href="javascript:;" onclick="HintRemove(\'cart_info_hint\');">再逛逛</a>' + '<a class="close" title="关闭" href="javascript:;" onclick="HintRemove(\'cart_info_hint\');"><span>关闭</span></a>' + '</div>' + '</div>'
					$(obj).after(HintHtml); //添加节点
					$("#shoppingCartCount").text(data.quantity); //赋值给显示购物车数量的元素
				}
			} else {
				$("#cart_info_hint").remove();
				var HintHtml = '<div id="cart_info_hint" class="msg_tips cart_info">' + '<div class="ico error"></div>' + '<div class="msg">' + '<strong>商品添加到购物车失败！</strong>' + '<p>' + data.msg + '</p>' + '<a class="close" title="关闭" href="javascript:void(0);" onclick="HintRemove(\'cart_info_hint\');"><span>关闭</span></a>' + '</div>' + '</div>'
				$(obj).after(HintHtml); //添加节点
				//alert(data.msg);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("状态：" + textStatus + "；出错提示：" + errorThrown);
		},
		timeout: 20000
	});
	return false;
}


//删除购物车商品
function DeleteCart(obj, webpath, goods_id) {
	if (!confirm("您确认要从购物车中移除吗？") || goods_id == "") {
		return false;
	}
	$.ajax({
		type: "post",
		url: webpath + "tools/submit_ajax.ashx?action=cart_goods_delete",
		data: {
			"goods_id": goods_id
		},
		dataType: "json",
		beforeSend: function(XMLHttpRequest) {
			//发送前动作
		},
		success: function(data, textStatus) {
			if (data.status == 1) {
				location.reload();
			} else {
				alert(data.msg);
			}
		},
		error: function(XMLHttpRequest, textStatus, errorThrown) {
			alert("状态：" + textStatus + "；出错提示：" + errorThrown);
		},
		timeout: 20000
	});
	return false;
}

//计算购物车金额
function CartAmountTotal(obj, webpath, goods_id) {
		if (isNaN($(obj).val())) {
			alert('商品数量只能输入数字!');
			$(obj).val("1");
		}
		$.ajax({
			type: "post",
			url: webpath + "tools/submit_ajax.ashx?action=cart_goods_update",
			data: {
				"goods_id": goods_id,
				"goods_quantity": $(obj).val()
			},
			dataType: "json",
			beforeSend: function(XMLHttpRequest) {
				//发送前动作
			},
			success: function(data, textStatus) {
				if (data.status == 1) {
					location.reload();
				} else {
					alert(data.msg);
					location.reload();
				}
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				alert("状态：" + textStatus + "；出错提示：" + errorThrown);
			},
			timeout: 20000
		});
		return false;
	}
	//购物车数量加减

function CartComputNum(obj, webpath, goods_id, num) {
		if (num > 0) {
			var goods_quantity = $(obj).prev("input[name='goods_quantity']");
			$(goods_quantity).val(parseInt($(goods_quantity).val()) + 1);
			//计算购物车金额
			CartAmountTotal($(goods_quantity), webpath, goods_id);
		} else {
			var goods_quantity = $(obj).next("input[name='goods_quantity']");
			if (parseInt($(goods_quantity).val()) > 1) {
				$(goods_quantity).val(parseInt($(goods_quantity).val()) - 1);
				//计算购物车金额
				CartAmountTotal($(goods_quantity), webpath, goods_id);
			}
		}
	}
	//计算支付手续费总金额

function PaymentAmountTotal(obj) {
		var payment_price = $(obj).next("input[name='payment_price']").val();
		$("#payment_fee").text(payment_price); //运费
		OrderAmountTotal();
	}
	//计算配送费用总金额

function FreightAmountTotal(obj) {
		var express_price = $(obj).next("input[name='express_price']").val();
		$("#express_fee").text(express_price); //运费
		OrderAmountTotal();
	}
	//计算订单总金额

function OrderAmountTotal() {
	var goods_amount = $("#goods_amount").text(); //商品总金额
	var payment_fee = $("#payment_fee").text(); //手续费
	var express_fee = $("#express_fee").text(); //运费
	var order_amount = parseFloat(goods_amount) + parseFloat(payment_fee) + parseFloat(express_fee); //订单总金额 = 商品金额 + 手续费 + 运费
	$("#order_amount").text(order_amount.toFixed(2));
}







function getPagingGlobal(obj, showjuli, isFirstPage) {
	keyvalues = $.extend({
		'p': '1'
	}, keyvalues, obj);

	var current_host = window.location;
	var url_obj = $.url(current_host).param();
	var iPage = keyvalues['p'];
	if (url_obj['page'] !== '' && typeof url_obj['page'] !== 'undefined') {
		iPage = parseInt(url_obj['page']);
	}
	if (typeof isFirstPage == 'undefined') {
		keyvalues['p'] = iPage; //刷新或返回 当前页
	} else if (isFirstPage == '2') {
		keyvalues['p'] = '1'; //真正的第一页
	} else if (isFirstPage == '0') {
		keyvalues['p'] = parseInt(iPage) + 1; //正常至底部加载下一页
	} else {}
	get_data_paging(callback);

	function callback(data) {
		if (data[0].islogin !== '1') {
			MSGwindowShow('revert', '0', data[0].error, '', '');
			return;
		}

		function showPaging() {
			$('#pagingList').append(data[0].MSG);
			$('#pageNavigation').html(data[0].PageSplit);
			getkill();
			showjuli && showMapBD(keyvalues.x, keyvalues.y);
			setTimeout(function() {
				lazyImg('#pagingList', false);
			}, 50);
		}
		if (typeof window['isIscroll5'] === 'undefined') {
			$('#pagingList').empty();
			showPaging();
			return false;
		}
		$('#pullUp').hide();
		$('#pageLoader').hide();
		if (keyvalues["p"] === '1') {
			ifNoMore = false;
			$('#pagingList').empty();
			$('#pullDown').find('.loader').hide();
			$('#pullDown').hide();
			$('#reload').find('.txt').html('下拉可以刷新');
			$('#reload').find('.s').removeClass('s_ok');
			setTimer();
		}
		showPaging();
		if (keyvalues["p"] == data[0].PageCount || data[0].PageCount == '0') {
			ifNoMore = true;
			lis = document.createElement('li');
			lis.innerText = '没有更多了';
			lis.className = 'noMore';
			lis.id = 'noMore';
			$('#pagingList').append(lis);
		}
		setTimeout(function() {
			myScroll.refresh();
		}, 10);
		history.pushState(null, '', '?page=' + keyvalues['p']);
	}
	return false;
}









$.fn.pagelist2 = function() { //便利店
	alert("da");
	var node = $(this),
		list = node.find('.item');
	list.each(function() {
		if ($(this).attr('data-styleid') === '1') {
			$(this).find('.buycar').html('点击进入购买').attr({
				'href': $(this).attr('data-httpurl')
			});
			$(this).find('.link').attr({
				'href': $(this).attr('data-httpurl')
			});
		} else {
			$(this).find('.buycar').bind('click', function(e) {
				e.preventDefault();
				setShoppingCart2($(this), $(this).attr('data-id'), '1');
			});
		}
		if ($(this).attr('data-styleid') === '0' && $(this).attr('data-kcnum') === '0') {
			$(this).find('.maiguang').css('display', 'block');
		}
	});
}
$.fn.pagelist1 = function() { //餐饮
	var node = $(this),
		list = node.find('.item');
	list.each(function() {
		var t = $(this);
		if ($(this).attr('data-styleid') === '0' && $(this).attr('data-kcnum') === '0') {
			$(this).find('.maiguang').css('display', 'block');
			$(this).find('.fen').css('display', 'none');
		}
		$(this).find('.add').click(function(e) {
			e.preventDefault();
			var num = parseInt(t.find('.amount').html()) + 1;
			setShoppingCart1($(this).parent(), t.attr('data-id'), num);
		});
		$(this).find('.dec').click(function(e) {
			e.preventDefault();
			var num = parseInt(t.find('.amount').html()) - 1;
			setShoppingCart1($(this).parent(), t.attr('data-id'), num);
		});
	});
}

function showQison(val1, val2) {
	if ((val1 > 0) && (val1 > val2)) {
		$('#myShopCartSubmit').html('还差' + (val1 - val2) + '元起送').addClass('disabled').prop('disabled', true);
	} else {
		$('#myShopCartSubmit').html('立即下单').removeClass('disabled').prop('disabled', false);
	}
}

function getShoppingCart(callback) {
	var url = siteUrl + 'request.ashx?action=getmyshopping&shopid=' + window['SHOPID'] + '&ishtml=' + window['ISHTML'] + '&delid=&jsoncallback=?';
	$.getJSON(url, function(data) {
		if (data[0].islogin === '1') {
			uploadShoppingCart(data[0].CHRMONEY);
			callback && callback.call(this, data[0].JSONMSG);
		} else {
			MSGwindowShow('shopping', '0', data[0].error, '', '');
		}
	});
}

function uploadShoppingCart(data) {
	$('#numAll').html(data.numAll);
	$('#chrmoneyAll').html(data.chrmoneyAll);
	$('#chrmoneyYunfei').html(parseInt(data.chrmoneyYunfei));
	showQison(parseFloat($('#myShopCart').attr('data-shipmoney1')), parseFloat(data.chrmoneyAll));
}

function setShoppingCart1(node, sid, num) {
		alert("d");
		var url = siteUrl + 'request.ashx?action=addmyshopping&id=' + sid + '&styleid=' + window['GOODSTYLEID'] + '&num=' + num + '&shopid=' + window['SHOPID'] + '&ishtml=' + window['ISHTML'] + '&delid=&jsoncallback=?&timer=' + Math.random();
		$.getJSON(url, function(data) {
			if (data[0].islogin === '1') {
				node.find('.amount').html(num);
				uploadShoppingCart(data[0].CHRMONEY);
				uploadSubCatNum1();
				if (num === 0) {
					node.find('.dec').hide();
					node.find('.amount').hide();
				} else {
					node.find('.dec').show();
					node.find('.amount').show();
				}
			} else {
				MSGwindowShow('shopping', '0', data[0].error, '', '');
			}
		});
	}
	//购物js

function setShoppingCart2(node, sid, num) {
	alert("d2");
	var url = siteUrl + 'request.ashx?action=addmyshopping&id=' + sid + '&styleid=' + window['GOODSTYLEID'] + '&num=' + num + '&shopid=' + window['SHOPID'] + '&ishtml=' + window['ISHTML'] + '&delid=&jsoncallback=?&timer=' + Math.random();

	$.getJSON(url, function(data) {
		if (data[0].islogin === '1') {
			uploadShoppingCart(data[0].CHRMONEY);
			node.hide().parent().find('.buycar2').show();
		} else {
			if (typeof typeid !== 'undefined') {
				if ('increase' === typeid) {
					$('#gouwuche' + sid).val(parseInt($('#gouwuche' + sid).val()) - 1);
				}
			}
			MSGwindowShow('shopping', '0', data[0].error, '', '');
		}
	});
}

function showProductList1(data) {
	var i = 0,
		len = data.length,
		num = 0;
	for (; i < len; i++) {
		var t_item = $('#pro_item_' + data[i].goodid);
		t_item.find('.dec').show().end().find('.amount').html(data[i].num).show();
	}
	uploadSubCatNum1();
}

function showProductList2(data) {
	var i = 0,
		len = data.length;
	for (; i < len; i++) {
		$('#item_' + data[i].goodid).find('.buycar').hide().end().find('.buycar2').show();
	}
}

function uploadSubCatNum1() {
	var prolist = $('#prolist'),
		items = prolist.find('.pro_item');
	items.each(function() {
		var num = 0,
			node = $('#scrolld_plist_' + $(this).attr('data-id'));
		$(this).find('.amount').each(function() {
			num = num + parseInt($(this).html());
		});
		node.html(num);
		if (num === 0) {
			node.addClass('display10');
		} else {
			node.removeClass('display10');
		}

	});
}

function addFav(o, data) {
	if ($('#isLogin').val() === '0') {
		var url = siteUrl + 'member/login.html?from=' + encodeURIComponent(window.location.href);
		MSGwindowShow('revert', '1', '对不起，请登录后再进行收藏！', url, '');
		return false;
	}
	var url = siteUrl + 'request.ashx?action=addshoucang&shopid=' + data.shopid + '&id=' + data.productid + '&styleid=' + data.styleid + '&jsoncallback=?&timer=' + Math.random();
	$.getJSON(url, function(data) {
		if (data[0].islogin === '1') {
			$(o).addClass('favok').html('收藏成功');
		} else {
			MSGwindowShow('shopping', '0', data[0].error, '', '');
		}
	});
}

function delFav(data) {
	var url = siteUrl + 'request.ashx?action=delshoucang&shopid=' + data.shopid + '&id=' + data.productid + '&jsoncallback=?&timer=' + Math.random();
	$.getJSON(url, function(data) {
		if (data[0].islogin === '1') {
			MSGwindowShow('shopping', '1', '删除收藏成功！', window.location.href, '');
		} else {
			MSGwindowShow('shopping', '0', data[0].error, '', '');
		}
	});
}


$.fn.radioForm = function() {
	this.each(function() {
		var list = $(this).find('.gx_radio');
		var forname = $(this).attr('data-name');


		var sid = $('input[name="' + forname + '"]:checked').attr('value');
		if (sid !== '' && !!sid) {
			$(this).find('.gx_radio').removeClass('current');
			$(this).find('.gx_radio[data-val="' + sid + '"]').addClass('current');
		}

		list.click(function(e) {
			e.preventDefault();
			list.removeClass('current');
			$(this).addClass('current');
			$('input[name="' + forname + '"][value="' + $(this).attr('data-val') + '"]').prop('checked', true);
		});
	});
}

//filter 显示筛选层
function showFilter(option){
	var node = $('#'+option.ibox),
		fullbg = $('#'+option.fullbg),
		ct1 = $('#'+option.content1),
		ct2 = $('#'+option.content2),
		ctp1 = ct1.find('.innercontent'),
		ctp2 = ct2.find('.innercontent'),
		currentClass = 'current';
	var tabs = node.find('.tab .item'),
		conts = node.find('.inner');
	fullbg.css({'height':$(document).height()+'px'});
	
	var timelist = node.find('.inner > ul > li').filter(function(index) {
			return $('ul', this).length > 0;
		}),
		childUL = null;
	timelist.each(function(){
		var that = $(this);
		that.addClass('hasUL');
		that.children('a').addClass('hasUlLink');
	});
	ct1.on("click",".hasUlLink",function(e){
		e.preventDefault();
		var that = $(this).parent();
		if(!window['myScroll_inner']){
			window['myScroll_inner'] = new IScroll('#'+option.content2, {
				click: true,
				scrollX: false,
				scrollY: true,
				scrollbars: true,
				interactiveScrollbars: true,
				shrinkScrollbars: 'scale',
				fadeScrollbars: true
			});
		}
		setTimeout(function(){
			ctp1.find('.hasUL_current').removeClass('hasUL_current');
			that.addClass('hasUL_current');
			ctp2.html('<ul>'+that.find('ul').html()+'</ul>').show();
			ct1.css({'width':'50%'});
			ct2.show();
			window['myScroll_inner'].refresh();
		},100);
	});
	tabs.each(function(i){
		$(this).bind("click",function(e){
			e.preventDefault();
			if(!window['myScroll_parent']){
				window['myScroll_parent'] = new IScroll('#'+option.content1, {
					click: true,
					scrollX: false,
					scrollY: true,
					scrollbars: true,
					interactiveScrollbars: true,
					shrinkScrollbars: 'scale',
					fadeScrollbars: true
				});
			}
			setTimeout(function(){
				node.addClass('filter-fixed');
				ct2.hide();
				ctp1[0].innerHTML = conts.eq(i).html();
				ct1.css('width','100%').show();
				fullbg.fadeIn('fast');
				tabs.removeClass(currentClass);
				tabs.eq(i).addClass(currentClass);
				window['myScroll_parent'].refresh();
			},100);
		});
	});
	fullbg.bind('click',function(e){
		e.preventDefault();
		hide_nav();
	});
	function hide_nav(){
		node.removeClass('filter-fixed');
		fullbg.fadeOut('fast');
		timelist.removeClass('hasUL_current');
		tabs.removeClass(currentClass);
		ct1.css('width','100%').hide();
		ct2.hide();
	}
}
