﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetWing.BPM.Admin.weixin
{
    public partial class error : System.Web.UI.Page
    {
        protected string copyRight = Global.copyRight;
        protected string appName = Global.appName;
        protected string msgs;
        protected string type;
        protected string tip;
        protected string style;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                type = Request["type"];
                msgs = Request["msgs"];
                if (type == "success")
                {
                    tip = "成功";
                    style = "weui-icon-success weui-icon_msg";
                }
                else if (type == "info")
                {
                    tip = "提示";
                    style = "weui-icon-info weui-icon_msg";
                }
                else if (type == "warnp")
                {
                    tip = "警告";
                    style = "weui-icon-warn weui-icon_msg-primary";
                }
                else if (type == "warn")
                {
                    tip = "警告";
                    style = "weui-icon-warn weui-icon_msg";
                }
                else if (type == "waiting")
                {
                    tip = "等待";
                    style = "weui-icon-waiting weui-icon_msg";
                }
            }
        }
    }
}