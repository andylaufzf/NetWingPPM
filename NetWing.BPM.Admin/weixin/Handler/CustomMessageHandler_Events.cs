﻿using System;
using System.Diagnostics;
using System.Web;
using Senparc.Weixin.MP.Agent;
using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.User;
using Senparc.Weixin.MP.Entities;
using Senparc.Weixin.MP.Helpers;
using Senparc.Weixin.MP.MessageHandlers;
using System.Text;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using Senparc.NeuChar.Entities;

namespace NetWing.BPM.Admin.weixin.Handler
{
    /// <summary>
    /// 自定义MessageHandler
    /// </summary>
    public partial class CustomMessageHandler
    {
        /// <summary>
        /// 菜单点击事件
        /// </summary>
        public override IResponseMessageBase OnEvent_ClickRequest(RequestMessageEvent_Click requestMessage)
        {
            IResponseMessageBase reponseMessage = null;
            //Common.MessageFunction cmfun = new Common.MessageFunction();

            string keywords = requestMessage.EventKey; //查询关健字

            string EventName = "";
            if (requestMessage.Event.ToString().Trim() != "")
            {
                EventName = requestMessage.Event.ToString();
            }
            else if (requestMessage.EventKey != null)
            {
                EventName += requestMessage.EventKey.ToString();
            }



            int responseType = 0; //回复类型
            //int ruleId = new BLL.weixin_request_rule().GetKeywordsRuleId(keywords, out responseType);
            //int ruleId = new BLL.weixin_request_rule().GetKeywordsRuleId(string.Empty, out responseType);

            return reponseMessage;
        }

        /// <summary>
        /// 订阅（关注）事件
        /// </summary>
        public override IResponseMessageBase OnEvent_SubscribeRequest(RequestMessageEvent_Subscribe requestMessage)
        {
            
            //通过扫描关注
            //string scene_id = requestMessage.EventKey;//事件KEY值，是一个32位无符号整数，即创建二维码时的二维码scene_id
            //string[] scenearr = scene_id.Split('_');//得到的值是这样所以要拆分qrscene_3
            //scene_id = scenearr[1];
            //string ticket = requestMessage.Ticket;
            //string openid = requestMessage.FromUserName;//取得的openid
            //string Event = requestMessage.Event.ToString();
            //注意以上是扫描关注 以下是发纯文本关注

            var responseMessage = ResponseMessageBase.CreateFromRequestMessage<ResponseMessageText>(requestMessage);//推送纯文字
            var txtwenjian = NetWing.Common.ConfigHelper.GetValue("guanzhu");
            responseMessage.Content = txtwenjian;
            return responseMessage;
        }
        
        /// <summary>
        /// 退订事件
        /// </summary>
        public override IResponseMessageBase OnEvent_UnsubscribeRequest(RequestMessageEvent_Unsubscribe requestMessage)
        {
            //return new Common.MessageFunction().EventSubscribe(7, requestMessage);
            return null;
        }

        /*public override IResponseMessageBase OnTextOrEventRequest(RequestMessageText requestMessage)
        {
            // 预处理文字或事件类型请求。
            // 这个请求是一个比较特殊的请求，通常用于统一处理来自文字或菜单按钮的同一个执行逻辑，
            // 会在执行OnTextRequest或OnEventRequest之前触发，具有以下一些特征：
            // 1、如果返回null，则继续执行OnTextRequest或OnEventRequest
            // 2、如果返回不为null，则终止执行OnTextRequest或OnEventRequest，返回最终ResponseMessage
            // 3、如果是事件，则会将RequestMessageEvent自动转为RequestMessageText类型，其中RequestMessageText.Content就是RequestMessageEvent.EventKey

            if (requestMessage.Content == "OneClick")
            {
                var strongResponseMessage = CreateResponseMessage<ResponseMessageText>();
                strongResponseMessage.Content = "您点击了底部按钮。\r\n为了测试微信软件换行bug的应对措施，这里做了一个——\r\n换行";
                return strongResponseMessage;
            }
            return null;//返回null，则继续执行OnTextRequest或OnEventRequest
        }*/

        /*public override IResponseMessageBase OnEvent_EnterRequest(RequestMessageEvent_Enter requestMessage)
        {
            var responseMessage = ResponseMessageBase.CreateFromRequestMessage<ResponseMessageText>(requestMessage);
            responseMessage.Content = "您刚才发送了ENTER事件请求。";
            return responseMessage;
        }*/

        /*public override IResponseMessageBase OnEvent_LocationRequest(RequestMessageEvent_Location requestMessage)
        {
            //这里是微信客户端（通过微信服务器）自动发送过来的位置信息
            var responseMessage = CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "这里写什么都无所谓，比如：上帝爱你！";
            return responseMessage; //这里也可以返回null（需要注意写日志时候null的问题）
        }*/

        /// <summary>
        /// 扫渠道二维码事件
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        public override IResponseMessageBase OnEvent_ScanRequest(RequestMessageEvent_Scan requestMessage)
        {


            //通过扫描关注
            string scene_id = requestMessage.EventKey;//事件KEY值，是一个32位无符号整数，即创建二维码时的二维码scene_id
            string ticket = requestMessage.Ticket;
            string openid = requestMessage.FromUserName;//取得的openid
            string Event = requestMessage.Event.ToString();



            var responseMessage = CreateResponseMessage<ResponseMessageText>();
            //responseMessage.Content = "" + item.nickname + "您好，感谢您关注[" + model.name + "]。";
            return responseMessage;
        }

        /*public override IResponseMessageBase OnEvent_ViewRequest(RequestMessageEvent_View requestMessage)
        {
            //说明：这条消息只作为接收，下面的responseMessage到达不了客户端，类似OnEvent_UnsubscribeRequest
            var responseMessage = CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "您点击了view按钮，将打开网页：" + requestMessage.EventKey;
            return responseMessage;
        }*/

        /*public override IResponseMessageBase OnEvent_MassSendJobFinishRequest(RequestMessageEvent_MassSendJobFinish requestMessage)
        {
            var responseMessage = CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "接收到了群发完成的信息。";
            return responseMessage;
        }*/

        /// <summary>
        /// 事件之扫码推事件(scancode_push)
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        /*public override IResponseMessageBase OnEvent_ScancodePushRequest(RequestMessageEvent_Scancode_Push requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之扫码推事件";
            return responseMessage;
        }*/

        /// <summary>
        /// 事件之扫码推事件且弹出“消息接收中”提示框(scancode_waitmsg)
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        /*public override IResponseMessageBase OnEvent_ScancodeWaitmsgRequest(RequestMessageEvent_Scancode_Waitmsg requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之扫码推事件且弹出“消息接收中”提示框";
            return responseMessage;
        }*/

        /// <summary>
        /// 事件之弹出拍照或者相册发图（pic_photo_or_album）
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        /*public override IResponseMessageBase OnEvent_PicPhotoOrAlbumRequest(RequestMessageEvent_Pic_Photo_Or_Album requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之弹出拍照或者相册发图";
            return responseMessage;
        }*/

        /// <summary>
        /// 事件之弹出系统拍照发图(pic_sysphoto)
        /// 实际测试时发现微信并没有推送RequestMessageEvent_Pic_Sysphoto消息，只能接收到用户在微信中发送的图片消息。
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        /*public override IResponseMessageBase OnEvent_PicSysphotoRequest(RequestMessageEvent_Pic_Sysphoto requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之弹出系统拍照发图";
            return responseMessage;
        }*/

        /// <summary>
        /// 事件之弹出微信相册发图器(pic_weixin)
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        /*public override IResponseMessageBase OnEvent_PicWeixinRequest(RequestMessageEvent_Pic_Weixin requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之弹出微信相册发图器";
            return responseMessage;
        }*/

        /// <summary>
        /// 事件之弹出地理位置选择器（location_select）
        /// </summary>
        /// <param name="requestMessage"></param>
        /// <returns></returns>
        /*public override IResponseMessageBase OnEvent_LocationSelectRequest(RequestMessageEvent_Location_Select requestMessage)
        {
            var responseMessage = base.CreateResponseMessage<ResponseMessageText>();
            responseMessage.Content = "事件之弹出地理位置选择器";
            return responseMessage;
        }*/

    }


    /// <summary>
    /// 公众平台回复信息
    /// </summary>
    [Serializable]
    public partial class weixin_response_content
    {
        public weixin_response_content()
        { }

        #region Model
        private int _id;
        private int _account_id;
        private string _openid;
        private string _request_type;
        private string _request_content;
        private string _response_type;
        private string _reponse_content;
        private string _create_time;
        private string _xml_content;
        private DateTime _add_time = DateTime.Now;
        /// <summary>
        /// 自增ID
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 公众账户ID
        /// </summary>
        public int account_id
        {
            set { _account_id = value; }
            get { return _account_id; }
        }
        /// <summary>
        /// 用户微信ID
        /// </summary>
        public string openid
        {
            set { _openid = value; }
            get { return _openid; }
        }
        /// <summary>
        /// 数据类型 文本消息：text 图片消息:image 地理位置消息:location 链接消息:link 事件:event
        /// </summary>
        public string request_type
        {
            set { _request_type = value; }
            get { return _request_type; }
        }
        /// <summary>
        /// 数据内容
        /// </summary>
        public string request_content
        {
            set { _request_content = value; }
            get { return _request_content; }
        }
        /// <summary>
        /// 回复的类型 文本消息：text 图片消息:image 地理位置消息:location 链接消息:link
        /// </summary>
        public string response_type
        {
            set { _response_type = value; }
            get { return _response_type; }
        }
        /// <summary>
        /// 系统回复的内容
        /// </summary>
        public string reponse_content
        {
            set { _reponse_content = value; }
            get { return _reponse_content; }
        }
        /// <summary>
        /// 消息创建时间
        /// </summary>
        public string create_time
        {
            set { _create_time = value; }
            get { return _create_time; }
        }
        /// <summary>
        /// xml原始内容
        /// </summary>
        public string xml_content
        {
            set { _xml_content = value; }
            get { return _xml_content; }
        }
        /// <summary>
        /// 录入系统的时间
        /// </summary>
        public DateTime add_time
        {
            set { _add_time = value; }
            get { return _add_time; }
        }
        #endregion Model

    }
}