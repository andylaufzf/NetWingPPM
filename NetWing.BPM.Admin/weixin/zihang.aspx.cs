﻿using NetWing.Common;
using NetWing.Common.Data.SqlServer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetWing.BPM.Admin.weixin
{
    public partial class tjbb : System.Web.UI.Page
    {

        public string zhheadimgurl = "";
        public string zhnickname = "";
        public string zhugroup = "";//会员组
        public int zhclientcount = 0;//客户统计

        public string zhlianxren = "";//联系人
        public string zhlianxdh = "";//联系电话

        public string zhaddtime = "";//开通时间
        public string zhlastloginip = "";//本次登录ip
        public string zhlastlogintime = "";//本次登录时间
        public string zhcompanyname = "";//公司名称
        public string zhtruename = "";//真实姓名

        public DataTable zhdt = null;//代理商客户人数统计
        public DataTable zhkhdt = null;//代理商客户金额统计
        public DataTable zhkhhydt = null;//代理商客户行业统计
        protected void zhPage_Load(object sender, EventArgs e)
        {

            string uid = CookieHelper.GetCookie("currentUserID");//得到用户id
            if (string.IsNullOrEmpty(uid))//说明没有登录
            {
                Response.Redirect("login.aspx");
            }
            else
            {
                DataRow dr = SqlEasy.ExecuteDataRow("select * from Sys_Users where keyid=" + uid + "");
                ugroup = HttpUtility.UrlDecode(CookieHelper.GetCookie("currentDepName"));
                clientcount = (int)SqlEasy.ExecuteScalar("select count(keyid) from wzsh_khxx  where onwer=" + uid + "");

                lianxren = dr["TrueName"].ToString();//联系人
                lianxdh = dr["Mobile"].ToString();//联系电话

                addtime = dr["addtime"].ToString();
                lastloginip = dr["lastloginip"].ToString();
                lastlogintime = dr["lastlogintime"].ToString();
                headimgurl = CookieHelper.GetCookie("headimgurl");//头像
                companyname = dr["companyname"].ToString();
                truename = dr["truename"].ToString();
                nickname = HttpUtility.UrlDecode(CookieHelper.GetCookie("nickname"));//昵称
            }

            dt = SqlEasy.ExecuteDataTable("select COUNT(*) as rshu,(select COUNT(*) from wzsh_khxx k where k.onwer="+uid+" and spje=0 and k.shifotgsh='0') as sqje,(select COUNT(*) from wzsh_khxx k where k.onwer="+uid+" and spje!=0 and k.shifotgsh='是') as spje,(select COUNT(*) from wzsh_khxx k where k.onwer="+uid+" and spje=0 and k.shifotgsh='否') as sytg from wzsh_khxx  where onwer="+uid+"");

            khdt = SqlEasy.ExecuteDataTable("select COUNT(*) as rshu,Convert(decimal(18,2),(SUM(sqje)/10000)) as sqje,Convert(decimal(18,2),((SUM(sqje)/10000)/COUNT(*))) as sqjepj,Convert(decimal(18,2),(SUM(spje)/10000)) as spje,Convert(decimal(18,2),((SUM(spje)/10000))/COUNT(*)) as spjepj from wzsh_khxx where onwer=" + uid);

            khhydt = SqlEasy.ExecuteDataTable("select industry,COUNT(industry) as rshu,(select COUNT(*) from wzsh_khxx where onwer=" + uid + ") as shu from wzsh_khxx where onwer=" + uid + " group by industry");

        }

        protected void zhlogout_Click(object sender, EventArgs e)
        {
            //Response.Cookies.Clear();
            //CookieHelper.WriteCookie("currentUserID","");
            CookieHelper.ClearUserCookie(HttpContext.Current.Request.Url.Host.ToString(), "currentUserID");
            Response.Redirect("login.aspx");
        }

    }
}