﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetWing.Common.SMS;
using NetWing.Common;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;

namespace NetWing.BPM.Admin.weixin
{
    /// <summary>
    /// wxHandler 的摘要说明
    /// </summary>
    public class wxHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                context.Response.ContentType = "text/plain";
                //context.Response.Write("Hello World");
                string action = context.Request["action"];
                switch (action)
                {
                    case "getsms":

                        string tels = context.Request["tels"];
                        
                        #region 判断手机号是否在库里
                        int j = (int)SqlEasy.ExecuteScalar("select count(keyid) from Sys_Users where mobile='" + tels + "'");

                        if (j == 0)
                        {
                            //有两个以上手机号不给绑定
                            context.Response.Write("{\"status\":0,\"msg\":\"您号码不存在,不能绑定,请联系管理员!\"}");
                            context.Response.End();
                        }


                        if (j > 1)
                        {
                            //有两个以上手机号不给绑定
                            context.Response.Write("{\"status\":0,\"msg\":\"您有两个手机号不能绑定,请联系管理员!\"}");
                            context.Response.End();
                        }
                        #endregion


                        string templateno = context.Request["templateno"];
                        Random rd = new Random();
                        string yzm = rd.Next(1000, 10000).ToString();//获取四位随机数
                        CookieHelper.WriteCookie("yzm", yzm);//存于cookies便于验证
                        string smsparam = "{\"code\":\"" + yzm + "\",\"product\":\"聚源达\"}"; //短信参数
                        aliyunsms alisms = new aliyunsms();
                        string json = alisms.send(tels, templateno, smsparam);
                        //context.Response.Write(json);
                        context.Response.Write("{\"status\":1,\"msg\":\"短信发送成功!\"}");
                           
                       
                        break;
                    case "bind":
                        #region 验证验证码
                        //获取发送验证码
                        string myyzm = CookieHelper.GetCookie("yzm");
                        //获取用户输入验证码
                        string ryzm = context.Request["yzm"];

                        if (!string.IsNullOrEmpty(CookieHelper.GetCookie("yzm")))
                        {
                            #region 核对验证码


                            if (context.Request["yzm"] == CookieHelper.GetCookie("yzm"))
                            {
                                string lki = context.Request["tels"];
                                //成功 绑定开始,判断员工是不是在库里面
                                int q = (int)SqlEasy.ExecuteScalar("select count(keyid) from Sys_Users where mobile='" + lki + "'");
                               //返回
                                int code = 0;
                                //唯一性
                                if (q > 0)
                                {
                                    code = SqlEasy.ExecuteNonQuery("update sys_users set weixin='" +HttpUtility.UrlDecode(CookieHelper.GetCookie("nickname")) + "',headimgurl='" + CookieHelper.GetCookie("headimgurl") + "',openid='" + CookieHelper.GetCookie("openid") + "' where mobile='" + context.Request["tels"] + "'");
                                }
                               
                                if (code > 0)
                                {
                                    context.Response.Write("{\"status\":1,\"msg\":\"绑定成功!\"}");
                                    context.Response.End();
                                }
                                else
                                {
                                    context.Response.Write("{\"status\":0,\"msg\":\"绑定失败!\"}");
                                    context.Response.End();
                                }
                            }
                            else
                            {
                                context.Response.Write("{\"status\":0,\"msg\":\"验证码输入错误!\"}");
                                context.Response.End();
                            }
                            #endregion

                        }
                        else
                        {
                            context.Response.Write("{\"status\":0,\"msg\":\"验证码已失效,请重新获得验证码!\"}");
                            context.Response.End();

                        }
                        #endregion

                        break;
                    case "khsjh":
                        string telde = context.Request["tels"];
                        
                        #region 判断手机号是否在库里
                        int y = (int)SqlEasy.ExecuteScalar("select count(keyid) from jydUser where tell='" + telde + "'");

                        if (y == 0)
                        {
                            //有两个以上手机号不给绑定
                            context.Response.Write("{\"status\":0,\"msg\":\"您号码不存在,不能绑定,请联系管理员!\"}");
                            context.Response.End();
                        }


                        if (y > 1)
                        {
                            //有两个以上手机号不给绑定
                            context.Response.Write("{\"status\":0,\"msg\":\"您有两个手机号不能重复绑定,请联系管理员!\"}");
                            context.Response.End();
                        }
                            
                            #endregion
                       

                        string templatno = context.Request["templateno"];
                        Random rdo = new Random();
                        string xyzm = rdo.Next(1000, 10000).ToString();//获取四位随机数
                        CookieHelper.WriteCookie("yzm", xyzm);//存于cookies便于验证
                        string smspaam = "{\"code\":\"" + xyzm + "\",\"product\":\"聚源达\"}"; //短信参数
                        aliyunsms aliisms = new aliyunsms();
                        string jsono = aliisms.send(telde, templatno, smspaam);
                        //context.Response.Write(json);
                        context.Response.Write("{\"status\":1,\"msg\":\"短信发送成功!\"}");
                         
                        break;
                    case "bangd":
                        #region 验证验证码
                        //获取发送验证码
                        string myxyzm = CookieHelper.GetCookie("yzm");
                        //获取用户输入验证码
                        string xryzm = context.Request["yzm"];
                        if (!string.IsNullOrEmpty(CookieHelper.GetCookie("yzm")))
                        {
                            #region 核对验证码


                            if (context.Request["yzm"] == CookieHelper.GetCookie("yzm"))
                            {
                                string lki = context.Request["tels"];
                                //成功 绑定开始,判断员工是不是在库里面
                                int w = (int)SqlEasy.ExecuteScalar("select count(keyid) from jydUser where tell='" + lki + "'");
                                //返回
                                int code = 0;
                                //唯一性
                                
                                if (w > 0)
                                {
                                    code = SqlEasy.ExecuteNonQuery("update jydUser set weixin='" + CookieHelper.GetCookie("nickname") + "',headimgurl='" + CookieHelper.GetCookie("headimgurl") + "',OpenID='" + CookieHelper.GetCookie("openid") + "' where tell='" + context.Request["tels"] + "'");

                                }
                                if (code > 0)
                                {
                                    context.Response.Write("{\"status\":1,\"msg\":\"绑定成功!\"}");
                                    context.Response.End();
                                }
                                else
                                {
                                    context.Response.Write("{\"status\":0,\"msg\":\"绑定失败!\"}");
                                    context.Response.End();
                                }
                            }
                            else
                            {
                                context.Response.Write("{\"status\":0,\"msg\":\"验证码输入错误!\"}");
                                context.Response.End();
                            }
                            #endregion

                        }
                        else
                        {
                            context.Response.Write("{\"status\":0,\"msg\":\"验证码已失效,请重新获得验证码!\"}");
                            context.Response.End();

                        }
                        #endregion

                        break;

                    default:
                        break;
                }
            }
            catch (Exception e)
            {
                WriteLogs.WriteLogsE("Logs", "sqlerr >> addajax", e.Message + " >>> " + e.StackTrace);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}