﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="error.aspx.cs" Inherits="NetWing.BPM.Admin.weixin.error" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <title><%=tip %></title>
    <link href="weiui/lib/weui.min.css" rel="stylesheet" />
    <link href="weiui/css/jquery-weui.min.css" rel="stylesheet" />
    <link href="../JydModleOrder/dist/demos/css/demos.css" rel="stylesheet" />
</head>
<body>
    <div class="weui-msg">
        <div class="weui-msg__icon-area"><i class="<%=style %>"></i></div>
        <div class="weui-msg__text-area">
            <h2 class="weui-msg__title"><%=tip %></h2>
            <p class="weui-msg__desc"><%=msgs %></p>
        </div>
        <div class="weui-msg__opr-area">
            <p class="weui-btn-area">
                <a href="../JydModleOrder/JydWapindex.aspx" class="weui-btn weui-btn_primary">返回</a>
            </p>
        </div>
        <div class="weui-msg__extra-area">
            <div class="weui-footer">
                <p class="weui-footer__links">
                    <a href="javascript:void(0);" class="weui-footer__link"><%=appName %></a>
                </p>
                <p class="weui-footer__text"><%=copyRight %></p>
            </div>
        </div>
    </div>
</body>
</html>
