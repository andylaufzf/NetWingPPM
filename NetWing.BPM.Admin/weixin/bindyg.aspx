﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bindyg.aspx.cs" Inherits="NetWing.BPM.Admin.weixin.bindyg" %>
<%@ Import Namespace="NetWing.Common" %>

<html>
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>员工绑定</title>
    <style>
        .layui-form-label {
            width: 20% !important;
            float: left;
        }
    </style>
</head>
<body>

    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label">手机号码</label>
            <div class="layui-input-inline">
                <input type="tel" name="phone" id="phone" lay-verify="required|phone" autocomplete="off" class="layui-input" placeholder="请输入手机号">
            </div>
        </div>
        <div class="layui-inline">
            <label class="layui-form-label">验证码</label>
            <div class="layui-input-inline">
                <input type="text" name="vcode" id="vcode" lay-verify="required|number" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" id="getsms" lay-submit="" onclick="settime(this);" lay-filter="demo1">获取验证码</button>
                <button type="reset" id="bind" class="layui-btn layui-btn-primary">提交绑定</button>
            </div>
        </div>
    </div>

    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <script src="../scripts/layui/layui.all.js"></script>
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <script>

        function showloading(t) {
            if (t) {//如果是true则显示loading
                console.log(t);
                loading = layer.load(1, {
                    shade: [0.1, '#fff'] //0.1透明度的白色背景
                });
            } else {//如果是false则关闭loading
                console.log("关闭loading层:" + t);
                layer.closeAll('loading');
            }
        }


        $(function () {
            //获取验证码

            $("#getsms").click(function () {
                var p = $("#phone").val();
                if (p === null || p === '') {
                    layer.msg('手机号不能为空!');
                    return false;//不往下执行
                }


                console.log("点击了");
                $.ajax({
                    url: '/weixin/wxhandler.ashx',
                    type: 'POST', //GET
                    async: true,    //或false,是否异步
                    data: {
                        action: 'getsms',
                        tels: $("#phone").val(),
                        templateno: 'SMS_5009300',
                        smsparam: '{\"code\":\"1234\",\"product\":\"聚源达\"}'  //{\"code\":\"1234\",\"product\":\"云南便民\"}
                    },
                    timeout: 5000,    //超时时间
                    dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    beforeSend: function () {
                        // Handle the beforeSend event
                        //loading层
                        showloading(true);
                    },
                    success: function (data, textStatus, jqXHR) {
                        //最后数据加载完 让 loading层消失
                        showloading(false);
                        if (data.status === 0) {
                            alert(data.msg);
                        }
                    }

                });
            });
            //绑定
            $("#bind").click(function () {
                console.log("绑定");
                var p = $("#phone").val();
                var v = $("#vcode").val();
                if (p === null || p === undefined || p === '') {
                    layer.msg('手机号不能为空!');
                    return false;
                }
                if (v === null || v === undefined || v === '') {
                    layer.msg('验证码不能为空!');
                    return false;
                }


                $.ajax({
                    url: '/weixin/wxhandler.ashx',
                    type: 'POST', //GET
                    async: true,    //或false,是否异步
                    data: {
                        action: 'bind',
                        tels: $("#phone").val(),
                        yzm: $("#vcode").val()
                    },
                    timeout: 5000,    //超时时间
                    dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                    beforeSend: function () {
                        // Handle the beforeSend event
                        //loading层
                        showloading(true);//显示loading层让用户感知已经点了
                    },
                    success: function (data, textStatus, jqXHR) {
                        showloading(false);//关闭loading显示
                        console.log(JSON.stringify(data));
                        //alert(data.status);
                        if (data.status === 1) {
                            layer.msg(data.msg);
                            console.log(data.msg);
                            location.href = '<%=ConfigHelper.GetValue("website")%>/JydModleOrder/JydWapindex.aspx';
                            console.log("跳转过后");
                        } else {
                            layer.msg(data.msg);
                        }
                    }


                });
            });


        });

        var countdown = 60;
        function settime(obj) {
            var p = $("#phone").val();
            if (p === null || p === '') {
                layer.msg('手机号不能为空!');
                return false;
            }
            if (countdown === 0) {
                $(obj).attr("disabled", false);
                $(obj).attr("mark", "1");
                $(obj).html("获取验证码");
                countdown = 60;
                return;
            } else {
                $(obj).attr("disabled", true);
                $(obj).attr("mark", "0");
                $(obj).html("重新发送(" + countdown + ")");
                countdown--;
            }
            setTimeout(function () {
                settime(obj)
            }
                , 1000)

        }

    </script>
    <script>


</script>
</body>
</html>
