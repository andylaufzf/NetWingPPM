﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tjbb.aspx.cs" Inherits="NetWing.BPM.Admin.weixin.tjbb" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>

<!DOCTYPE html>
<!-- saved from url=(0033)http://www.mana.com/card/detail/3 -->
<html style="font-size: 37.5px;">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <title>客户统计</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1">
    <meta name="Keywords" content="">
    <meta name="Description" content="">
    <script type="text/javascript" src="cssandjs/jquery-1.10.2.min.js"></script>
   	<script src="/scripts/echarts.min.js"></script>
    <html lang="zh-cmn-Hans">
	<link href="../../weixin/weiui/css/jquery-weui.min.css" rel="stylesheet" />
    <link href="../../weixin/weiui/lib/weui.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="cssandjs/creditcard.css">

</head>

<body background="#2c343c">
    <div id="creditcard">
        <header>
            <div class="logo">
                <a href="javascript:history.back(-1);" class="">
                    <samp class="fanhui"></samp></a>
                
            </div>
            
            <div class="title">客户统计</div>
            
        </header>

        <div class="flnav_box">
            <%--<ul>
                 
                <li>
                   <a href="/weixin/tjbb.aspx"><i></i>统计</a>
                </li>
              
            </ul>--%>
           
        </div>

        <!--循环开始-->
        <div class="card-info">
            <div class="mainlist" id="productlist">
            	
            	<div class="pro_list">
                    <div class="text" style=" text-align:center;"><h1>客户人数统计</h1></div>
                    
                    <div id="mainkhtj" style="width: 100%; height: 550px; "></div>
		        	<script type="text/javascript">
		        // 基于准备好的dom，初始化echarts实例
		        var myChart = echarts.init(document.getElementById('mainkhtj'));
		
		        option = {
						    backgroundColor: '#2c343c',
				    title : {
				        text: '',
				        subtext: '',
				        x:'center'
				    },
				    tooltip : {
				        trigger: 'item',
                        formatter: "{a} <br/>{b} <br/> ({d}%)"
				    },
				    legend: {
				        orient: 'vertical',
				        left: 'left',
				        data: []
				    },
				    series : [
				        {
				            name: '单位:人',
				            type: 'pie',
				            radius : '50%',
				            center: ['50%', '60%'],
				            data:[
				                  
				                   
				                  <%foreach (DataRow dr in dt.Rows)
                            {%>
				                     { value: <%=dr["rshu"].ToString()%>, name: '总人数：<%=dr["rshu"].ToString()%>人' },
		                             { value: <%=dr["sqje"].ToString()%>, name: '审核中：<%=dr["sqje"].ToString()%>人' },
                                     { value: <%=dr["spje"].ToString()%>, name: '通过：<%=dr["spje"].ToString()%>人' },
                                     { value: <%=dr["sytg"].ToString()%>, name: '没通过：<%=dr["sytg"].ToString()%>人' }
		                             
				                  <%} %>
				            ],
				            itemStyle: {
				                emphasis: {
				                    shadowBlur: 10,
				                    shadowOffsetX: 0,
				                    shadowColor: 'rgba(0, 0, 0, 0.5)'
				                }
				            }
				        }
				    ]
				};
						
		
		
		        // 使用刚指定的配置项和数据显示图表。
		        myChart.setOption(option);
		            // Enable data zoom when user click bar.
		
		    </script>

                    

                </div>


            


           </div>

        </div>

        </div>
         <!-- #include file="bottom.html" -->
</body>

</html>
