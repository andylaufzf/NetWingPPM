﻿using Senparc.Weixin.MP.AdvancedAPIs;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using Senparc.Weixin.MP.Containers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NetWing.Common.Weixin
{
    public class TemplateMSG
    {
        /// <summary>
        /// 审核结果通知 
        /// 行业IT科技 - 互联网|电子商务
        /// {{first.DATA}}
        /// 会员名称：{{keyword1.DATA}}
        /// 审核结果：{{keyword2.DATA}}
        /// 处理时间：{{keyword3.DATA}}
        /// {{remark.DATA}}
        /// </summary>
        /// <param name="openId">openid</param>
        /// <param name="templateId">模版ID</param>
        /// <param name="linkUrl">返回链接</param>
        /// <param name="first">标题</param>
        /// <param name="username">用户昵称</param>
        /// <param name="result">审核结果</param>
        /// <param name="remark">备注内容</param>
        /// <returns></returns>
        public static string sendExam(string openId, string templateId, string linkUrl, string first, string username, string result, string remark)
        {
            //根据appId判断获取  
            if (!AccessTokenContainer.CheckRegistered(NetWing.Common.ConfigHelper.GetValue("AppID")))    //检查是否已经注册  
            {
                AccessTokenContainer.Register(NetWing.Common.ConfigHelper.GetValue("AppID"), NetWing.Common.ConfigHelper.GetValue("AppSecret"));    //如果没有注册则进行注册  
            }
            string access_token = AccessTokenContainer.GetAccessTokenResult(NetWing.Common.ConfigHelper.GetValue("AppID")).access_token; //AccessToken
            //为模版中的各属性赋值
            var templateData = new ProductTemplateData()
            {
                first = new TemplateDataItem(first, "#000000"),
                keyword1 = new TemplateDataItem(username, "#000000"),
                keyword2 = new TemplateDataItem(result, "#000000"),
                keyword3 = new TemplateDataItem(DateTime.Now.ToString(), "#000000"),
                remark = new TemplateDataItem(remark, "#000000")
            };
            SendTemplateMessageResult sendResult = TemplateApi.SendTemplateMessage(access_token, openId, templateId, linkUrl, templateData);
            //发送成功
            string r = "";//发送结果
            if (sendResult.errcode.ToString() == "请求成功")
            {
                r = "请求成功";
            }
            else
            {
                r = sendResult.errmsg;
            }
            return r;
        }


        /// <summary>
        /// 提现申请通知 
        /// 行业IT科技 - 互联网|电子商务
        /// {{first.DATA}}
        /// 昵称：{{keyword1.DATA}}
        /// 时间：{{keyword2.DATA}}
        /// 金额：{{keyword3.DATA}}
        /// 方式：{{keyword4.DATA}}
        /// {{remark.DATA}}
        /// </summary>
        /// <param name="openId">openid</param>
        /// <param name="templateId">模版ID</param>
        /// <param name="linkUrl">返回链接</param>
        /// <param name="first">标题</param>
        /// <param name="username">用户昵称</param>
        /// <param name="money">金额</param>
        /// <param name="remark">备注内容</param>
        /// <returns></returns>
        public static string tixianExam(string openId, string templateId, string linkUrl, string first, string username, string money, string remark)
        {
            //根据appId判断获取  
            if (!AccessTokenContainer.CheckRegistered(NetWing.Common.ConfigHelper.GetValue("AppID")))    //检查是否已经注册  
            {
                AccessTokenContainer.Register(NetWing.Common.ConfigHelper.GetValue("AppID"), NetWing.Common.ConfigHelper.GetValue("AppSecret"));    //如果没有注册则进行注册  
            }
            string access_token = AccessTokenContainer.GetAccessTokenResult(NetWing.Common.ConfigHelper.GetValue("AppID")).access_token; //AccessToken
            //为模版中的各属性赋值
            var templateData = new ProductTemplateData()
            {
                first = new TemplateDataItem(first, "#000000"),
                keyword1 = new TemplateDataItem(username, "#000000"),
                keyword2 = new TemplateDataItem(DateTime.Now.ToString(), "#000000"),
                keyword3 = new TemplateDataItem(money, "#000000"),
                keyword4 = new TemplateDataItem("微信", "#000000"),
                remark = new TemplateDataItem(remark, "#000000")
            };
            SendTemplateMessageResult sendResult = TemplateApi.SendTemplateMessage(access_token, openId, templateId, linkUrl, templateData);
            //发送成功
            string r = "";//发送结果
            if (sendResult.errcode.ToString() == "请求成功")
            {
                r = "请求成功";
            }
            else
            {
                r = sendResult.errmsg;
            }
            return r;
        }


        /// <summary>
        /// 密码修改通知 
        /// 行业IT科技 - 互联网|电子商务
        /// {{first.DATA}}
        ///用户名：{{keyword1.DATA}}
        ///新密码：{{keyword2.DATA}}
        ///{{remark.DATA}}
        /// </summary>
        /// <param name="openId">openid</param>
        /// <param name="templateId">模版ID</param>
        /// <param name="linkUrl">返回链接</param>
        /// <param name="first">标题</param>
        /// <param name="username">用户昵称</param>
        /// <param name="pass">密码</param>
        /// <param name="remark">备注内容</param>
        /// <returns></returns>
        public static string passwordExam(string openId, string templateId, string linkUrl, string first, string username, string pass, string remark)
        {
            //根据appId判断获取  
            if (!AccessTokenContainer.CheckRegistered(NetWing.Common.ConfigHelper.GetValue("AppID")))    //检查是否已经注册  
            {
                AccessTokenContainer.Register(NetWing.Common.ConfigHelper.GetValue("AppID"), NetWing.Common.ConfigHelper.GetValue("AppSecret"));    //如果没有注册则进行注册  
            }
            string access_token = AccessTokenContainer.GetAccessTokenResult(NetWing.Common.ConfigHelper.GetValue("AppID")).access_token; //AccessToken
            //为模版中的各属性赋值
            var templateData = new ProductTemplateData()
            {
                first = new TemplateDataItem(first, "#000000"),
                keyword1 = new TemplateDataItem(username, "#000000"),
                keyword2 = new TemplateDataItem(pass, "#000000"),
                remark = new TemplateDataItem(remark, "#000000")
            };
            SendTemplateMessageResult sendResult = TemplateApi.SendTemplateMessage(access_token, openId, templateId, linkUrl, templateData);
            //发送成功
            string r = "";//发送结果
            if (sendResult.errcode.ToString() == "请求成功")
            {
                r = "请求成功";
            }
            else
            {
                r = sendResult.errmsg;
            }
            return r;
        }


    }

    /// <summary>
    /// 定义模版中的字段属性（需与微信模版中的一致）
    /// </summary>
    public class ProductTemplateData
    {
        public TemplateDataItem first { get; set; }
        public TemplateDataItem keyword1 { get; set; }
        public TemplateDataItem keyword2 { get; set; }
        public TemplateDataItem keyword3 { get; set; }
        public TemplateDataItem keyword4 { get; set; }
        public TemplateDataItem remark { get; set; }
    }



}
