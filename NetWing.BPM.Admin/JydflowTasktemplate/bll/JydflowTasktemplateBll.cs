using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydflowTasktemplateBll
    {
        public static JydflowTasktemplateBll Instance
        {
            get { return SingletonProvider<JydflowTasktemplateBll>.Instance; }
        }

        public int Add(JydflowTasktemplateModel model)
        {
            return JydflowTasktemplateDal.Instance.Insert(model);
        }

        public int Update(JydflowTasktemplateModel model)
        {
            return JydflowTasktemplateDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydflowTasktemplateDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydflowTasktemplateDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
