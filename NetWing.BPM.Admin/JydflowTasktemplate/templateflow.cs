﻿using NetWing.BPM.Core;
using NetWing.BPM.Core.Bll;
using NetWing.BPM.Core.Dal;
using NetWing.BPM.Core.Model;
using NetWing.Common;
using NetWing.Common.Data;
using NetWing.Common.Data.SqlServer;
using NetWing.Model;
using Newtonsoft.Json.Linq;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using static NetWing.BPM.Admin.JydModleOrder.ashx.ajax_submit;

namespace NetWing.BPM.Admin.JydflowTasktemplate
{
    public class templateflow
    {
        public static bool CreateInstance(JObject obj)
        {
            SqlTransaction ctran = SqlHelper.BeginTransaction(SqlEasy.connString);//取得一个事务
            var flowInstance = obj.ToObject<JydflowtasklistModel>();
            //获取提交的表单数据
            var frmdata = new JObject();
            foreach (var property in obj.Properties().Where(U => U.Name.Contains("data_")))
            {
                frmdata[property.Name] = property.Value;
            }
            try
            {
                //结合订单信息写任务清单
                #region 将订单写进任务表中
                JydflowtasklistModel jydflowtasklistModel = new JydflowtasklistModel
                {
                    nodename = flowInstance.nodename,
                    operatorID = flowInstance.operatorID,
                    operatorname = flowInstance.operatorname,
                    orderID = flowInstance.orderID,
                    ordername = flowInstance.ordername,
                    PrintedmatterID = flowInstance.PrintedmatterID,
                    Printedmattername = flowInstance.Printedmattername,
                    addtime = flowInstance.addtime,
                    isitcomplete = flowInstance.isitcomplete,
                    completeofth = flowInstance.completeofth,
                    orderkey = flowInstance.orderkey,
                    timestamp = flowInstance.timestamp
                };
                DbUtils.tranInsert(jydflowtasklistModel, ctran);//提交流程操作记录
                ctran.Commit();//提交事务
                #endregion
                SqlTransaction tran = SqlHelper.BeginTransaction(SqlEasy.connString);//取得一个事务
                JydflowRecordModel jydflowRecordModel = new JydflowRecordModel
                {
                    processID = 0,
                    Operationalcontent = "开单",
                    Operationtime = DateTime.Now,
                    addtime = flowInstance.timestamp,
                    Operctionname = flowInstance.operatorname,
                    orderid = flowInstance.orderID,
                    Remarks = flowInstance.completeofth,
                    nikeyid = flowInstance.orderkey,
                    sortid = flowInstance.isitcomplete,
                };
                DbUtils.tranInsert(jydflowRecordModel, tran);//提交流程操作记录
                tran.Commit();//提交事务



                //开始微信发放任务
                #region 开始发放任务
                string dt = "select * from jydflowTasktemplate where PrintedmatterID='" + flowInstance.PrintedmatterID + "' and softdeletion != 1 order by sortid asc";
                DataTable tableData = SqlEasy.ExecuteDataTable(dt);
                DataTable arrayi;
                try {
                    foreach (DataRow dr in tableData.Rows)
                    {
                        if(int.Parse(dr["priority"].ToString()) == 1) { //如果优先级等于1就发送带超链接的,不等于1的就发送没有连接的
                        //为模版中的各属性赋值
                        var templateData = new ProductTemplateData()
                        {
                            first = new TemplateDataItem(flowInstance.ordername+"通知", "#000000"),
                            keyword1 = new TemplateDataItem("JYD"+ flowInstance.orderID, "#000000"),
                            keyword2 = new TemplateDataItem(flowInstance.completeofth, "#000000"),
                            keyword3 = new TemplateDataItem(DateTime.Now.ToString("yyyy-MM-dd")),
                            keyword4 = new TemplateDataItem(dr["nodename"].ToString(),"#000000"),
                            keyword5 = new TemplateDataItem(dr["nodename"].ToString(),"#000000"),
                            remark = new TemplateDataItem("流程创建人:<"+ SysVisitor.Instance.cookiesUserName +">此任务中你所做操作<"+ dr["nodename"].ToString()+">", "#66ccff")
                        };
                        string templateid = NetWing.Common.ConfigHelper.GetValue("templageid");//从web.config 获得模板ID
                                                                                               //通知所有员工就是openid不同
                            string str = dr["operatorname"].ToString();
                            string[] arraya = str.Split(',');
                            foreach (string i in arraya)
                            {
                                arrayi = SqlEasy.ExecuteDataTable("select * from sys_users where username='" + i.ToString() + "'");


                                foreach (DataRow data in arrayi.Rows)
                                {
                                    string r = NetWing.BPM.Admin.weixin.wxhelper.sendTemplateMsg(data["openid"].ToString(), templateid, NetWing.Common.ConfigHelper.GetValue("website") + "/JydModleOrder/taskQRcode.aspx?keyid=" + flowInstance.orderkey + "", templateData);
                                }
                            }
                        }
                        else
                        {
                            //为模版中的各属性赋值
                            var templateData = new ProductTemplateData()
                            {
                                first = new TemplateDataItem(flowInstance.ordername + "通知", "#000000"),
                                keyword1 = new TemplateDataItem("JYD" + flowInstance.orderID, "#000000"),
                                keyword2 = new TemplateDataItem(flowInstance.completeofth, "#000000"),
                                keyword3 = new TemplateDataItem(DateTime.Now.ToString("yyyy-MM-dd")),
                                keyword4 = new TemplateDataItem(dr["nodename"].ToString(), "#000000"),
                                keyword5 = new TemplateDataItem(dr["nodename"].ToString(), "#000000"),
                                remark = new TemplateDataItem("流程创建人:<" + SysVisitor.Instance.cookiesUserName + ">此任务中你所做操作<" + dr["nodename"].ToString() + ">,此为开单通知,不需要操作", "#66ccff")
                            };
                            string templateid = NetWing.Common.ConfigHelper.GetValue("templageid");//从web.config 获得模板ID
           
                                                                                                   //通知所有员工就是openid不同

                            string str = dr["operatorname"].ToString();
                            string[] arraya = str.Split(',');
                            foreach (string i in arraya) { 
                            arrayi = SqlEasy.ExecuteDataTable("select * from sys_users where username='"+i.ToString()+"'");

                            
                            foreach(DataRow data in arrayi.Rows) { 
                                string r = NetWing.BPM.Admin.weixin.wxhelper.sendTemplateMsg(data["openid"].ToString(), templateid, "", templateData);
                            }
                            }
                        }
                    }
                } catch(Exception e)
                    {
                    //写日志
                    LogModel logxq = new LogModel();
                    logxq.BusinessName = "开单成功失败";
                    logxq.OperationIp = "";
                    logxq.OperationTime = DateTime.Now;
                    logxq.PrimaryKey = "";
                    logxq.UserId = int.Parse(SysVisitor.Instance.cookiesUserId);
                    logxq.SqlText = "开单发送微信ip";
                    logxq.TableName = "jydOrder";
                    logxq.note = e.Message;
                    logxq.OperationType = (int)OperationType.Other;
                    LogDal.Instance.Insert(logxq);
                    WriteLogs.WriteLogsE("Logs", "Error >> 发送微信", e.Message + " >>> " + e.StackTrace);
                    }
                #endregion
            }
            catch (Exception e)
            {
                ctran.Rollback();
                WriteLogs.WriteLogsE("Logs", "Error >> 结合订单信息写任务清单", e.Message + " >>> " + e.StackTrace);

            }

            //

            return true;
        }
    }
}