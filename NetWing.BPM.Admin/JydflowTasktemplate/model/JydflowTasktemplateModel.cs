using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("jydflowTasktemplate")]
	[Description("任务模板")]
	public class JydflowTasktemplateModel
	{
				/// <summary>
		/// 任务模板ID
		/// </summary>
		[Description("任务模板ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 节点名称
		/// </summary>
		[Description("节点名称")]
		public string nodename { get; set; }		
		/// <summary>
		/// 优先级:0为1,1为2
		/// </summary>
		[Description("优先级:0为1,1为2")]
		public int priority { get; set; }		
		/// <summary>
		/// 执行者ID
		/// </summary>
		[Description("执行者ID")]
		public int operatorID { get; set; }		
		/// <summary>
		/// 执行者姓名
		/// </summary>
		[Description("执行者姓名")]
		public string operatorname { get; set; }
        /// <summary>
        /// 执行者open
        /// </summary>
        [Description("执行者open")]
        public string operatoropen { get; set; }
        /// <summary>
        /// 印刷品类型ID
        /// </summary>
        [Description("印刷品类型ID")]
		public int PrintedmatterID { get; set; }		
		/// <summary>
		/// 印刷品类型
		/// </summary>
		[Description("印刷品类型")]
		public string Printedmattername { get; set; }		
		/// <summary>
		/// 排序
		/// </summary>
		[Description("排序")]
		public int sortid { get; set; }		
		/// <summary>
		/// 软删除:0为正常,1为已删除
		/// </summary>
		[Description("软删除:0为正常,1为已删除")]
		public int softdeletion { get; set; }	
        /// <summary>
        /// 时间戳
        /// </summary>
        /// <returns></returns>
        [Description("时间戳")]
		public string timestamp { get; set; }
        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}