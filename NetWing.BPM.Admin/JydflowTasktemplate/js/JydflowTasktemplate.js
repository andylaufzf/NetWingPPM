var actionURL = '/JydflowTasktemplate/ashx/JydflowTasktemplateHandler.ashx';
var formurl   = '/JydflowTasktemplate/html/JydflowTasktemplate.html';

$(function () {
 	  		 							  		 							  		 							  		 							  		 							  		 							  		 							  		 							  		 						
	//为避免数据字典加载不完整。这段在后面加载
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });

    $('#a_add').click(CRUD.add);
    $('#a_edit').click(CRUD.edit);
    $('#a_delete').click(CRUD.del);
    //高级查询
    $('#a_search').click(function () {
        search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#a_getSearch').click(function () {//自定义搜索框
        mySearch();
    });

    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });

    //批量删除
    $("#a_alldel").click(function () {
        var ids = [];
        var rows = $('#list').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].KeyId);
        }
        var allid = ids.join(',');//所有的id
        var o = {};
        o.action = "alldel";
        o.KeyIds = allid;
        var param = "json=" + JSON.stringify(o);

        if (confirm('确认要执行批量删除操作吗？')) {
            jQuery.ajaxjson(actionURL, param, function (d) {
                if (parseInt(d) > 0) {
                    msg.ok('批量删除成功！');
                    grid.reload();
                } else {
                    MessageOrRedirect(d);
                }
            });
        }


    });

});
//这段后加载，避免数据字典加载不完整的问题
window.onload = function () {
    autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
};


//editor:'datetimebox' 日期及时间选择框  editor:'datebox' 日期选择框    editor:'numberspinner' 数字调节器  editor:{type: 'numberspinner',options:{value:0,required:true}}
//定义一个$JQ为$.　以后在js 中就可以用${JQ}AJAX了在前台这样写(定义)://通用数据字典start
var getDic = {
    jsonData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?categoryId=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
			//alert(newData);
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
    jsonParentData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?action=parent&parentid=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
}

//通用数据字典end
//调用数据字典和使用数据字典 如果不需要请不要打开否则会导致系统速度慢
//getDic.jsonData(9);//取得性别数据字典
//在onLoad:的地方如下使用
//top.$('#txt_user_sex').combobox({ data: eval($('body').data('data9')), valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '请选择性别', disabled: false });





var grid = {
    bind: function (winSize) {
        $('#list').datagrid({
            url: actionURL,
            toolbar: '#toolbar',
            title: "数据列表",
            iconCls: 'icon icon-list',
            width: winSize.width,
            height: winSize.height,
            nowrap: false, //折行
            rownumbers: true, //行号
            striped: true, //隔行变色
            idField: 'KeyId',//主键
            singleSelect: true, //单选
            frozenColumns: [[]],
            columns: [[//应为宽度不是很需要所以注释了宽度
				{ title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                {title:'任务模板ID',field:'KeyId',sortable: true,width:'',hidden: true},
                {title:'节点名称',field:'nodename',sortable: true,width:'',hidden: false},
                {
                    title: '优先级:0为1,1为2', field: 'priority', sortable: true, width: '', hidden: false,
                    formatter: function (value, row, index) {
                        switch (row.priority) {
                            case 0:
                                return "<font color=66ccff>(0:单走)</font>";
                                break;
                            case 1:
                                return "1:同时走";
                                break;
                            default:
                                return "未知"
                                break;
                        }
                    }
                },
                {title:'执行者ID',field:'operatorID',sortable: true,width:'',hidden: true},
                {title:'执行者姓名',field:'operatorname',sortable: true,width:'',hidden: false},
                {title:'执行者open',field:'operatoropen',sortable: true,width:'',hidden: true},
                {title:'印刷品类型ID',field:'PrintedmatterID',sortable: true,width:'',hidden: false},
                {title:'印刷品类型',field:'Printedmattername',sortable: true,width:'',hidden: false},
                {title:'排序',field:'sortid',sortable: true,width:'',hidden: false},
                {
                    title: '软删除:0为正常,1为已删除', field: 'softdeletion', sortable: true, width: '', hidden: true,
                    formatter: function (q, w, e) {
                        switch (w.softdeletion) {
                            case 0:
                                return "正常";
                                break;
                            case 1:
                                return "<font color=ff0000>软删除</font>";
                                break;
                            default:
                                return "未知";
                                break;
                        }
                    }
                },
                { title: '唯一ID', field: 'timestamp', sortable: true, width: '', hidden: false },
													                
            ]],
            onEndEdit: onEndEdit,//结束编辑时函数 这里为了简洁 该函数写在下面
            onUnselect: onUnselect,
			onLoadSuccess:function(data){
				//alert($('body').data('data70'));
				//alert($('body').data('data69'));
			},
            onCancelEdit: onCancelEdit,//在用户取消编辑一行的时候触发
            onSelect: onSelect,//在用户选择一行的时候触发
            onClickRow: onClickRow,//在用户点击一行的时候触发
            //onAfterEdit: onAfterEdit,//在用户完成编辑一行的时候触发
            onDblClickCell: onDblClickCell,//为了程序逻辑清楚函数写在外面
            onHeaderContextMenu: function (e, field) {//列菜单实现动态隐藏列
                e.preventDefault();
                if (!cmenu) {
                    createColumnMenu();
                }
                cmenu.menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            pagination: true,
            pageSize: PAGESIZE,
            sortName: 'KeyId',

            sortOrder: 'asc',
            pageList: [20, 40, 50,100,200]
        });
    },
    getSelectedRow: function () {
        return $('#list').datagrid('getSelected');
    },
    reload: function () {
        $('#list').datagrid('clearSelections').datagrid('reload', { filter: '' });
    }
};

function createParam(action, keyid) {
    var o = {};
    var query = top.$('#uiform').serializeArray();
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}


var CRUD = {
    add: function () {
        var hDialog = top.jQuery.hDialog({
            title: '添加', width: 1000, height: 800, href:formurl, iconCls: 'icon-add', 
            onLoad: function () {

                top.$('#aee').bind('click', function () {
                    console.log('123121321323');
                });

                //初始化执行者
                //top.$("#txt_operatorname").combogrid({
                //    delay: 500, //自动完成功能实现搜索
                //    mode: 'remote',//开启后系统会自动传一个参数q到后台 
                //    panelWidth: 200,
                //    required: true,
                //    //value:'fullname',   
                //    editable: true,
                //    idField: 'UserName',
                //    textField: 'UserName',
                //    url: '/sys/ashx/UserHandler.ashx',
                //    columns: [[
                //        { field: 'KeyId', title: 'KeyId', width: 50 },
                //        { field: 'UserName', title: '客户', width: 130 },

                //    ]],
                //    limitToList: true,//只能从下拉中选择值
                //    //reversed: true,//定义在失去焦点的时候是否恢复原始值。
                //    onHidePanel: function () {
                //        var t = top.$(this).combogrid('getValue');//获取combogrid的值
                //        var g = top.$(this).combogrid('grid');	// 获取数据表格对象
                //        var r = g.datagrid('getSelected');	// 获取选择的行
                //        console.log("选择的行是：" + r + "选择的值是:" + t);
                //        if (r == null || t != r.UserName) {//没有选择或者选项不相等时清除内容
                //            top.$.messager.alert('警告', '请选择，不要直接输入!');
                //            top.$(this).combogrid('setValue', '');
                //        } else {
                //            //do something...
                //        }
                //    },
                //    onSelect: function (rowIndex, rowData) {
                //        console.log("设置的值是：" + rowData.OpenID);
 
                //        top.$("#txt_operatorID").textbox('setValue',rowData.KeyId);//KeyId
                //        top.$("#txt_operatorname").textbox('setValue', rowData.UserName);//执行人
                //        top.$("#txt_operatoropen").textbox('setValue', rowData.OpenID);//微信
                //    }
                //});

                top.$('#txt_operatorname').combobox({
                    url: '/sys/ashx/UserHandler.ashx?action=xxx',//对应的ashx页面的数据源
                    method: 'get',
                    valueField: 'UserName',//绑定字段ID
                    textField: 'UserName', //绑定字段Name
                    panelHeight: 'auto',//自适应
                    multiple: true,
                    formatter: function (row) {
                        console.log("1" + row);
                        var opts = top.$(this).combobox('options');
                        return '<input type="checkbox" class="combobox-checkbox" id="' + row[opts.valueField] + '">' + row[opts.textField]
                    },
                    onShowPanel: function () {
                        var opts = top.$(this).combobox('options');
                        var target = this;
                        var values = top.$(target).combobox('getValues');
                        $.map(values, function (value) {
                            var el = opts.finder.getEl(target, value);
                            el.find('input.combobox-checkbox')._propAttr('checked', true);
                        })
                    },
                    onLoadSuccess: function () {
                        var opts = top.$(this).combobox('options');
                        var target = this;
                        var values = top.$(target).combobox('getValues');
                        $.map(values, function (value) {
                            var el = opts.finder.getEl(target, value);
                            el.find('input.combobox-checkbox')._propAttr('checked', true);
                        })
                    },
                    onSelect: function (row) {
                        var opts = top.$(this).combobox('options');


                        var el = opts.finder.getEl(this, row[opts.valueField]);
                        el.find('input.combobox-checkbox')._propAttr('checked', true);
                    },
                    onUnselect: function (row) {
                        var opts = top.$(this).combobox('options');
                        var el = opts.finder.getEl(this, row[opts.valueField]);
                        el.find('input.combobox-checkbox')._propAttr('checked', false);
                    }
                });

                //初始化印刷品类型
                top.$("#txt_Printedmattername").combogrid({
                    delay: 500, //自动完成功能实现搜索
                    mode: 'remote',//开启后系统会自动传一个参数q到后台 
                    panelWidth: 200,
                    required: true,
                    //value:'fullname',   
                    editable: true,
                    idField: 'Title',
                    textField: 'Title',
                    url: '/sys/ashx/dichandler.ashx?categoryId=85',
                    columns: [[
                        { field: 'KeyId', title: 'KeyId', width: 50 },
                        { field: 'Title', title: '名称', width: 130 },

                    ]],
                    limitToList: true,//只能从下拉中选择值
                    //reversed: true,//定义在失去焦点的时候是否恢复原始值。
                    onHidePanel: function () {
                        var t = top.$(this).combogrid('getValue');//获取combogrid的值
                        var g = top.$(this).combogrid('grid');	// 获取数据表格对象
                        var r = g.datagrid('getSelected');	// 获取选择的行
                        console.log("选择的行是：" + r + "选择的值是:" + t);
                        if (r == null || t != r.Title) {//没有选择或者选项不相等时清除内容
                            top.$.messager.alert('警告', '请选择，不要直接输入!');
                            top.$(this).combogrid('setValue', '');
                        } else {
                            //do something...
                        }
                    },
                    onSelect: function (rowIndex, rowData) {
                        console.log("设置的值是：" + rowData.Title);
                        //$("#contactid").val(rowData.KeyId);//给经手人设置ID
                        //top.$("#txt_operatorname").combogrid('setValue', rowData.comname);//客户
                        top.$("#txt_PrintedmatterID").textbox('setValue',rowData.KeyId);//客户
                        top.$("#txt_Printedmattername").textbox('setValue', rowData.Title);//经手人
                        //top.$("#tel").textbox('setValue', rowData.tell);
                        //alert(rowData.TrueName);
                    }
                });



                top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器   			       																																																																																																			                top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
            },
			submit: function () {
			    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate'))





                {
                    var query = createParam('add', '0');
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (parseInt(d) > 0) {
                            msg.ok('添加成功！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                }
				msg.warning('请填写必填项！');
                return false;
            }
        });
       
        top.$('#uiform').validate();
    },
    edit: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '编辑', width: 1000, height: 700, href: formurl, iconCls: 'icon-save',
                onLoad: function () {
                    top.$('#aee').bind('click', function () {
                        console.log('123121321323');
                    });
                    top.$('#txt_KeyId').numberspinner ('setValue', row.KeyId);
                    top.$('#txt_nodename').textbox ('setValue', row.nodename);
                    top.$('#txt_priority').numberspinner ('setValue', row.priority);
                    top.$('#txt_operatorID').numberspinner('setValue', row.operatorID);
                    //top.$('#txt_operatorname').combogrid({
                    //        delay: 500, //自动完成功能实现搜索
                    //        mode: 'remote',//开启后系统会自动传一个参数q到后台 
                    //        panelWidth: 200,
                    //        required: true,
                    //        //value:'fullname',   
                    //        editable: true,
                    //        idField: 'UserName',
                    //        textField: 'UserName',
                    //        url: '/sys/ashx/UserHandler.ashx',
                    //        columns: [[
                    //            { field: 'KeyId', title: 'KeyId', width: 50 },
                    //            { field: 'UserName', title: '客户', width: 130 },

                    //        ]],
                    //        limitToList: true,//只能从下拉中选择值
                    //        //reversed: true,//定义在失去焦点的时候是否恢复原始值。
                    //        onHidePanel: function () {
                    //            var t = top.$(this).combogrid('getValue');//获取combogrid的值
                    //            var g = top.$(this).combogrid('grid');	// 获取数据表格对象
                    //            var r = g.datagrid('getSelected');	// 获取选择的行
                    //            console.log("选择的行是：" + r + "选择的值是:" + t);
                    //            if (r == null || t != r.UserName) {//没有选择或者选项不相等时清除内容
                    //                top.$.messager.alert('警告', '请选择，不要直接输入!');
                    //                top.$(this).combogrid('setValue', '');
                    //            } else {
                    //                //do something...
                    //            }
                    //        },
                    //        onSelect: function (rowIndex, rowData) {
                    //            console.log("设置的值是：" + rowData.OpenID);

                    //            top.$("#txt_operatorID").textbox('setValue', rowData.KeyId);//KeyId
                    //            top.$("#txt_operatorname").textbox('setValue', rowData.UserName);//执行人
                    //            top.$("#txt_operatoropen").textbox('setValue', rowData.OpenID);//微信
                    //    },
                    //    onLoadSuccess() { top.$('#txt_operatorname').combogrid('setValue', row.operatorname); }
                    //    });

                    top.$('#txt_operatorname').combobox({
                        url: '/sys/ashx/UserHandler.ashx?action=xxx',//对应的ashx页面的数据源
                        method: 'get',
                        valueField: 'UserName',//绑定字段ID
                        textField: 'UserName', //绑定字段Name
                        panelHeight: 'auto',//自适应
                        multiple: true,
                        formatter: function (row) {
                            console.log("1" + row);
                            var opts = top.$(this).combobox('options');
                            return '<input type="checkbox" class="combobox-checkbox" id="' + row[opts.valueField] + '">' + row[opts.textField]
                        },
                        onShowPanel: function () {
                            var opts = top.$(this).combobox('options');
                            var target = this;
                            var values = top.$(target).combobox('getValues');
                            $.map(values, function (value) {
                                var el = opts.finder.getEl(target, value);
                                el.find('input.combobox-checkbox')._propAttr('checked', true);
                            })
                        },
                        onLoadSuccess: function () {
                            var opts = top.$(this).combobox('options');
                            var target = this;
                            var values = top.$(target).combogrid('setValue');
                            $.map(values, function (value) {
                                var el = opts.finder.getEl(target, value);
                                el.find('input.combobox-checkbox')._propAttr('checked', true);
                            })
                            //top.$('#txt_operatorname').combogrid('setValue', row.operatorname);
                        },
                        onSelect: function (row) {
                            var opts = top.$(this).combobox('options');


                            var el = opts.finder.getEl(this, row[opts.valueField]);
                            el.find('input.combobox-checkbox')._propAttr('checked', true);
                        },
                        onUnselect: function (row) {
                            var opts = top.$(this).combobox('options');
                            var el = opts.finder.getEl(this, row[opts.valueField]);
                            el.find('input.combobox-checkbox')._propAttr('checked', false);
                        },
                       
                    });

                    top.$('#txt_operatoropen').textbox('setValue', row.operatoropen);
                    top.$('#txt_PrintedmatterID').numberspinner ('setValue', row.PrintedmatterID);
                    top.$('#txt_Printedmattername').combogrid({
                        delay: 500, //自动完成功能实现搜索
                        mode: 'remote',//开启后系统会自动传一个参数q到后台 
                        panelWidth: 200,
                        required: true,
                        //value:'fullname',   
                        editable: true,
                        idField: 'Title',
                        textField: 'Title',
                        url: '/sys/ashx/dichandler.ashx?categoryId=85',
                        columns: [[
                            { field: 'KeyId', title: 'KeyId', width: 50 },
                            { field: 'Title', title: '名称', width: 130 },

                        ]],
                        limitToList: true,//只能从下拉中选择值
                        //reversed: true,//定义在失去焦点的时候是否恢复原始值。
                        onHidePanel: function () {
                            var t = top.$(this).combogrid('getValue');//获取combogrid的值
                            var g = top.$(this).combogrid('grid');	// 获取数据表格对象
                            var r = g.datagrid('getSelected');	// 获取选择的行
                            console.log("选择的行是：" + r + "选择的值是:" + t);
                            if (r == null || t != r.Title) {//没有选择或者选项不相等时清除内容
                                top.$.messager.alert('警告', '请选择，不要直接输入!');
                                top.$(this).combogrid('setValue', '');
                            } else {
                                //do something...
                            }
                        },
                        onSelect: function (rowIndex, rowData) {
                            console.log("设置的值是：" + rowData.Title);
                            //$("#contactid").val(rowData.KeyId);//给经手人设置ID
                            //top.$("#txt_operatorname").combogrid('setValue', rowData.comname);//客户
                            top.$("#txt_PrintedmatterID").textbox('setValue', rowData.KeyId);//客户
                            top.$("#txt_Printedmattername").textbox('setValue', rowData.Title);//经手人
                            //top.$("#tel").textbox('setValue', rowData.tell);
                            //alert(rowData.TrueName);
                        },
                        onLoadSuccess() { top.$('#txt_Printedmattername').combogrid('setValue', row.Printedmattername); }
                    });
                    top.$('#txt_sortid').numberspinner ('setValue', row.sortid);
                    top.$('#txt_softdeletion').numberspinner('setValue', row.softdeletion); 
                    top.$('#txt_timestamp').textbox('setValue', row.timestamp);
                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
				  //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
				  //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
				  //$('#nn').numberbox('setValue', 206.12);
				  //$('#nn').textbox('setValue',1);
				  //$('#cc').combobox('setValues', ['001','002']);
				  //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');    
            },
                submit: function () {
					//alert(top.$("#uiform").form('enableValidation').form('validate'));
					//alert(top.$("#uiform").form('validate'));
					//原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {
                        var query = createParam('edit', row.KeyId);;
                        jQuery.ajaxjson(actionURL, query, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    }
					msg.warning('请填写必填项！');
                    return false;
                }
            });
           
        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('请注意,在执行的任务删除了将出错,请谨慎删除!!!确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

//实现动态隐藏列
var cmenu = null;
function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#list').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#list').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#list').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#list').datagrid('getColumnOption', field);
        cmenu.menu('appendItem', {
            text: col.title,
            name: field,
            iconCls: 'icon-ok'
        });
    }
}

//实现动态隐藏列结束


//双击编辑表单焦点消失后保存
var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#list').datagrid('validateRow', editIndex)) {
        $('#list').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function onDblClickCell(index, field) {
    if (editIndex != index) {
        if (endEditing()) {
            $('#list').datagrid('selectRow', index)
            $('#list').datagrid('beginEdit', index);
            var ed = $('#list').datagrid('getEditor', { index: index, field: field });
            if (ed) {
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
				//这个写法很有意思
                //为了方便 ,类似字段要同事写两个值 1把部门id写入隐藏字段2把标题换成值写入 显示字段
								  								  								  								  								  								  								  								  								  				
            }
            editIndex = index;
        } else {
            setTimeout(function () {
                $('#list').datagrid('selectRow', editIndex);
            }, 0);
        }
    }
}

function onUnselect(index, row) {
    //alert(index);
}

function onCancelEdit(index, row) {
    //alert(index);
}
function onClickRow(index, row) {
    if (editIndex != undefined) {
		//alert("正在编辑的行是：" + editIndex);
        //alert("验证正在编辑的行：" + $('#list').datagrid('validateRow', editIndex));
        if ($('#list').datagrid('validateRow', editIndex)) {//如果验证正在编辑的行数据有效则
            $("#list").datagrid('endEdit', editIndex);//如果点其他行，结束编辑正在编辑的行
            editIndex = undefined;
        } else {
            msg.warning("还有一行没编辑完啦！！");
        }
    }
    $("#list").datagrid('selectRow', index);


}

function onSelect(index, row) {

}

function onEndEdit(index, row, changes) {
    //alert('结束编辑'+index+JSON.stringify(row));
    var o = {};
    var query = JSON.stringify(row);
    o.jsonEntity = query;
    o.action = 'edit';
    o.keyid = row.KeyId;
    query = "json=" + JSON.stringify(o);
    //alert(query);
    //表格内编辑模式编辑成功不提示信息
    jQuery.ajaxjson(actionURL, query, function (d) {
        //if (parseInt(d) > 0) {
        //    msg.ok('编辑成功！');
        //    hDialog.dialog('close');
        grid.reload();
        // } else {
        //     MessageOrRedirect(d);
        // }
    });

}
function append() {
    if (endEditing()) {
        $('#list').datagrid('appendRow', { status: 'P' });
        editIndex = $('#list').datagrid('getRows').length - 1;
        $('#list').datagrid('selectRow', editIndex)
                .datagrid('beginEdit', editIndex);
    }
}
function removeit() {
    if (editIndex == undefined) { return }
    $('#list').datagrid('cancelEdit', editIndex)
            .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept() {
    if (endEditing()) {
        $('#list').datagrid('acceptChanges');
    }
}
function reject() {
    $('#list').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges() {
    var rows = $('#list').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}
//双击编辑表单焦点消失后保存结束

//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var myunit = $("#myUnit").textbox('getValue');
    var connman = $("#myConnMan").textbox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    var i = 0;
    if (myunit != '' && myunit != undefined) {//假如单位搜索不为空
        o.rules[i] = JSON.parse('{"field":"unit","op":"cn","data":"' + myunit + '"}');
        i = i + 1;
    }
    if (connman != '' & connman != undefined) {//联系人不为空
        o.rules[i] = JSON.parse('{"field":"connman","op":"cn","data":"' + connman + '"}');
    }

    $('#list').datagrid('reload', { filter: JSON.stringify(o) });


}


//自定义搜索结束


//单选多选开关
$('#selectSwitch').switchbutton({
    checked: false,
    onChange: function (checked) {
        if (checked) {
            $('#list').datagrid({ singleSelect: false });//多选
        } else {
            $('#list').datagrid({ singleSelect: true });//单选
        }
    }
})

