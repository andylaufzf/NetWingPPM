using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJWbtcBll
    {
        public static MJWbtcBll Instance
        {
            get { return SingletonProvider<MJWbtcBll>.Instance; }
        }

        public int Add(MJWbtcModel model)
        {
            return MJWbtcDal.Instance.Insert(model);
        }

        public int Update(MJWbtcModel model)
        {
            return MJWbtcDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJWbtcDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJWbtcDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
