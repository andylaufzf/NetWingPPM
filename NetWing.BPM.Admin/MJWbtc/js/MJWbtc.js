var actionURL = '/MJWbtc/ashx/MJWbtcHandler.ashx';
var formurl = '/MJWbtc/html/MJWbtc.html';

$(function () {

    //为避免数据字典加载不完整。这段在后面加载
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });

    $('#a_add').click(CRUD.add);
    $('#a_edit').click(CRUD.edit);
    $('#a_delete').click(CRUD.del);
    //高级查询
    $('#a_search').click(function () {
        search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#a_getSearch').click(function () {//自定义搜索框
        mySearch();
    });

    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });

    //批量删除
    $("#a_alldel").click(function () {
        var ids = [];
        var rows = $('#list').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].KeyId);
        }
        var allid = ids.join(',');//所有的id
        var o = {};
        o.action = "alldel";
        o.KeyIds = allid;
        var param = "json=" + JSON.stringify(o);

        if (confirm('确认要执行批量删除操作吗？')) {
            jQuery.ajaxjson(actionURL, param, function (d) {
                if (parseInt(d) > 0) {
                    msg.ok('批量删除成功！');
                    grid.reload();
                } else {
                    MessageOrRedirect(d);
                }
            });
        }


    });

});
//这段后加载，避免数据字典加载不完整的问题
window.onload = function () {
    autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
};


//editor:'datetimebox' 日期及时间选择框  editor:'datebox' 日期选择框    editor:'numberspinner' 数字调节器  editor:{type: 'numberspinner',options:{value:0,required:true}}

//定义一个$JQ为$.　以后在js 中就可以用${JQ}AJAX了在前台这样写(定义)://通用数据字典start
var getDic = {
    jsonData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?categoryId=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            //alert(newData);
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
    jsonParentData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?action=parent&parentid=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
}

//通用数据字典end
//调用数据字典和使用数据字典 如果不需要请不要打开否则会导致系统速度慢
//getDic.jsonData(9);//取得性别数据字典
//在onLoad:的地方如下使用
//top.$('#txt_user_sex').combobox({ data: eval($('body').data('data9')), valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '请选择性别', disabled: false });





var grid = {
    bind: function (winSize) {
        $('#list').datagrid({
            url: actionURL,
            toolbar: '#toolbar',
            title: "数据列表",
            iconCls: 'icon icon-list',
            width: winSize.width,
            height: winSize.height,
            nowrap: false, //折行
            rownumbers: true, //行号
            striped: true, //隔行变色
            idField: 'KeyId',//主键
            singleSelect: true, //单选
            frozenColumns: [[]],
            columns: [[//应为宽度不是很需要所以注释了宽度
                { title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                { title: '关键ID', field: 'KeyId', sortable: true, editor: 'numberspinner', width: '', hidden: true },//主键自动判断隐藏
                { title: '车型', field: 'carType', sortable: true, width: '', hidden: false },
                { title: '车牌号或车架号', field: 'carNo', sortable: true, width: '', hidden: false },
                { title: '添加时间', field: 'add_time', sortable: true, editor: 'datetimebox', width: '', hidden: true },//主键自动判断隐藏
                { title: '更新时间', field: 'up_time', sortable: true, editor: 'datetimebox', width: '', hidden: true },//主键自动判断隐藏
                { title: '服务开始时间', field: 'order_starttime', sortable: true, width: '', hidden: false },
                { title: '服务结束时间', field: 'order_endtime', sortable: true, width: '', hidden: false },
                { title: '最近续费时间', field: 'lastxftime', sortable: true, width: '', hidden: false },

                { title: '停车费', field: 'tcf', sortable: true, width: '', hidden: false },
                { title: '蓝牙卡押金', field: 'lanya_money', sortable: true, width: '', hidden: false },
                { title: '门禁卡押金', field: 'menjin_money', sortable: true, width: '', hidden: false },
                { title: '收款账号', field: 'account', sortable: true, width: '', hidden: false },
                {
                    title: '收款金额', field: 'moneyall', sortable: false,
                    formatter: function (value, row, index) {
                        return row.tcf + row.lanya_money + row.menjin_money;
                    },

                    width: '', hidden: false
                },
                { title: '部门ID', field: 'depid', sortable: true, editor: 'numberspinner', width: '', hidden: true },//主键自动判断隐藏
                { title: '数据所有者', field: 'ownner', sortable: true, editor: 'numberspinner', width: '', hidden: true },//主键自动判断隐藏
                { title: '联系人', field: 'connman', sortable: true, width: '', hidden: false },
                { title: '联系电话', field: 'mobile', sortable: true, width: '', hidden: false },
                { title: '备注', field: 'note', sortable: true, width: '', hidden: false },
                { title: '操作', field: 'opt', width: '', hidden: false, formatter: formatOper },

            ]],
            onEndEdit: onEndEdit,//结束编辑时函数 这里为了简洁 该函数写在下面
            onUnselect: onUnselect,
            onLoadSuccess: function (data) {
                //alert($('body').data('data70'));
                //alert($('body').data('data69'));
            },
            onCancelEdit: onCancelEdit,//在用户取消编辑一行的时候触发
            onSelect: onSelect,//在用户选择一行的时候触发
            //onClickRow: onClickRow,//在用户点击一行的时候触发
            //onAfterEdit: onAfterEdit,//在用户完成编辑一行的时候触发
            //onDblClickCell: onDblClickCell,//为了程序逻辑清楚函数写在外面
            onHeaderContextMenu: function (e, field) {//列菜单实现动态隐藏列
                e.preventDefault();
                if (!cmenu) {
                    createColumnMenu();
                }
                cmenu.menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            pagination: true,
            pageSize: PAGESIZE,
            pageList: [20, 40, 50, 100, 200]
        });
    },
    getSelectedRow: function () {
        return $('#list').datagrid('getSelected');
    },
    reload: function () {
        $('#list').datagrid('clearSelections').datagrid('reload', { filter: '' });
    }
};


function formatOper(val, row, index) {
    console.log(row);
    var r = row;
    var s = '';



    if (row.tuiedNumber == "" || row.tuiedNumber == null) {//如果还没有退

        if (row.edNumber != "" && row.edNumber != null) {
            s = s + '<input type="button"  style="color: #000000;" onclick="sj(\'' + row.edNumber + '\');" value="停车费收据">';
        }


        if (row.lastedNumber != "" && row.lastedNumber != null) {
            s = s + '<input type="button"  style="color: #000000;" onclick="zhxf(\'' + row.edNumber + '\',\'' + row.lastedNumber + '\');" value="最近续费收据">';
        }

        s = s + '<input type="button"  style="color: #0000ff;" onclick="xf(\'' + row.edNumber + '\',\'' + row.connman + '\',\'' + row.carNo + '\');" value="续费">';
        s = s + '<input type="button"  style="color: #ff0000;" onclick="tyj(\'' + row.edNumber + '\',\'' + row.connman + '\',\'' + row.carNo + '\');" value="退押金">';
    } else {//如果已经退了
        s = s + '<input type="button"  style="color: #000000;" onclick="ptyj(\'' + row.edNumber + '\',\'' + row.tuiedNumber + '\');" value="退押金收据">';
    }


    return s;
}



function tyj(edNumber, connman, carno) {
    var row = grid.getSelectedRow();
    if (row) {

        var hDialog = top.jQuery.hDialog({
            title: connman + '  ' + carno + '  ' + '退押金', width: 1000, height: 600, href: '/MJWbtc/html/tyj.html', iconCls: 'icon-add',
            onLoad: function () {
                otherinit();
                top.$("#uiform").form("load", row);
                console.log(JSON.stringify(row));
                top.$("#txt_carType").combobox("setValue",row.carType);
                
                //top.$("#txt_carType").combobox({ editable: false, readonly: true });
                //top.$("#txt_carNo").textbox({ editable: false, readonly: true });
                top.$.messager.alert('注意', '要退的押金如实填写。如果不退押金请填写0元');

            },
            submit: function () {


                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate')) {
                    var query = createParam('tyj', row.KeyId);;
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (d.status == 1) {
                            msg.ok('退押金业务办理成功！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                } else {
                    msg.warning('请填写必填项！');
                }

                return false;
            }
        });
    } else {
        msg.warning('请选择要续费的行！！！');
    }
}



function xf(edNumber, connman, carno) {
    var row = grid.getSelectedRow();
    if (row) {

        var hDialog = top.jQuery.hDialog({
            title: connman + '  ' + carno + '  ' + '续费', width: 1000, height: 600, href: '/MJWbtc/html/xf.html', iconCls: 'icon-add',
            onLoad: function () {
                top.$.messager.alert('注意', '请认真核对车型、费用。确定后再提交。');
                otherinit();
                top.$("#uiform").form("load", row);
                //

                //top.$("#txt_carType").combobox("setValue",row.carType);
                //top.$("#txt_carType").combobox({ editable: false, readonly: true });
                top.$("#txt_carNo").textbox({ editable: false, readonly: true });

                top.$("#subjectid").val(row.subjectid);//还原科目ID

                //
                

            },
            submit: function () {


                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate')) {
                    var query = createParam('xf', row.KeyId);;
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (d.status == 1) {
                            msg.ok('续费成功，请勿重复续费！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                } else {
                    msg.warning('请填写必填项！');
                }

                return false;
            }
        });
    } else {
        msg.warning('请选择要续费的行！！！');
    }
}



function ptyj(edNumber, tuiedNumber) {
    console.log("系统单号是：" + edNumber);
    //alert(murl);
    var dialog = $.dialog({
        title: '打印退押金收据',
        content: 'url:/mjprint/tingtuiyj.aspx?edNumber=' + edNumber + '&tuiedNumber=' + tuiedNumber + '',
        min: false,
        max: false,
        lock: true,
        width: 850,
        height: 550
    });
}





//打印预约收据
function sj(edNumber) {
    console.log("系统单号是：" + edNumber);
    //alert(murl);
    var dialog = $.dialog({
        title: '打印停车费收据',
        content: 'url:/mjprint/tingsj.aspx?edNumber=' + edNumber + '',
        min: false,
        max: false,
        lock: true,
        width: 850,
        height: 550
    });
}


//打印预约收据
function zhxf(edNumber, lastedNumber) {
    console.log("系统单号是：" + edNumber + "最后续费单号是:" + lastedNumber);
    //alert(murl);
    var dialog = $.dialog({
        title: '打印停车费收据',
        content: 'url:/mjprint/tingzhxfsj.aspx?edNumber=' + edNumber + '&lastedNumber=' + lastedNumber,
        min: false,
        max: false,
        lock: true,
        width: 850,
        height: 550
    });
}

function createParam(action, keyid) {
    var o = {};
    var query = top.$('#uiform').serializeArray();
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}



function otherinit() {

    //银行
    top.$(".accountid").combobox({
        valueField: 'KeyId',//用中文名作为名称
        required: true,
        textField: 'accountName',
        url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJBankAccount&field=accountName',
        onSelect: function (rec) {
            //查找附近的方法
            top.$('#account').textbox('setValue', rec.accountName);//设置隐藏ID
            //top.$("#accountid").textbox("setValue", rec.KeyId);
            console.log("账户名：" + rec.accountName + "账户ID：" + rec.KeyId);
        }
    });


    top.$("#subjectid").css('display', 'none');
    top.$("#txt_carType").combobox({
        onSelect: function (rec) {
            //alert(rec.text);
            switch (rec.text) {
                case "机动车":
                    top.$("#subjectid").val("18");//机动车
                    break;
                case "电瓶车":
                    top.$("#subjectid").val("17");//电瓶车
                    break;
                case "自行车":
                    top.$("#subjectid").val("13");//电瓶车
                    break;
                default:
                    top.$("#subjectid").val("18");//机动车
                    break;
            }
        }
    });

}

var CRUD = {
    add: function () {
        var hDialog = top.jQuery.hDialog({
            title: '增加外部停车', width: 1000, height: 600, href: formurl, iconCls: 'icon-add',
            onLoad: function () {
                top.$.messager.alert('注意', '请认真核对车型、费用。确定后再提交。');
                top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                otherinit();//初始化其他内容
            },
            submit: function () {


                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate')) {
                    var query = createParam('add', '0');
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (d.status == 1) {
                            msg.ok('添加成功！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                } else {
                    msg.warning('请填写必填项！');
                }

                return false;
            }
        });

        top.$('#uiform').validate();
    },
    edit: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '编辑', width: 1000, height: 700, href: formurl, iconCls: 'icon-save',
                onLoad: function () {
                    otherinit();

                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');   
                    top.$("#uiform").form("load", row);
                },
                submit: function () {
                    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                    //alert(top.$("#uiform").form('validate'));
                    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {
                        var query = createParam('edit', row.KeyId);;
                        jQuery.ajaxjson(actionURL, query, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功！仅修改了基本信息。费用、车型、收款账户将不做修改！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    } else {
                        msg.warning('请填写必填项！');
                    }

                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

//实现动态隐藏列
var cmenu = null;
function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#list').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#list').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#list').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#list').datagrid('getColumnOption', field);
        cmenu.menu('appendItem', {
            text: col.title,
            name: field,
            iconCls: 'icon-ok'
        });
    }
}

//实现动态隐藏列结束

//双击编辑表单焦点消失后保存
var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#list').datagrid('validateRow', editIndex)) {
        $('#list').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function onDblClickCell(index, field) {
    if (editIndex != index) {
        if (endEditing()) {
            $('#list').datagrid('selectRow', index)
            $('#list').datagrid('beginEdit', index);
            var ed = $('#list').datagrid('getEditor', { index: index, field: field });
            if (ed) {
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                //这个写法很有意思
                //为了方便 ,类似字段要同事写两个值 1把部门id写入隐藏字段2把标题换成值写入 显示字段

            }
            editIndex = index;
        } else {
            setTimeout(function () {
                $('#list').datagrid('selectRow', editIndex);
            }, 0);
        }
    }
}

function onUnselect(index, row) {
    //alert(index);
}

function onCancelEdit(index, row) {
    //alert(index);
}
function onClickRow(index, row) {
    if (editIndex != undefined) {
        //alert("正在编辑的行是：" + editIndex);
        //alert("验证正在编辑的行：" + $('#list').datagrid('validateRow', editIndex));
        if ($('#list').datagrid('validateRow', editIndex)) {//如果验证正在编辑的行数据有效则
            $("#list").datagrid('endEdit', editIndex);//如果点其他行，结束编辑正在编辑的行
            editIndex = undefined;
        } else {
            msg.warning("还有一行没编辑完啦！！");
        }
    }
    $("#list").datagrid('selectRow', index);


}

function onSelect(index, row) {

}

function onEndEdit(index, row, changes) {
    //alert('结束编辑'+index+JSON.stringify(row));
    var o = {};
    var query = JSON.stringify(row);
    o.jsonEntity = query;
    o.action = 'edit';
    o.keyid = row.KeyId;
    query = "json=" + JSON.stringify(o);
    //alert(query);
    //表格内编辑模式编辑成功不提示信息
    jQuery.ajaxjson(actionURL, query, function (d) {
        //if (parseInt(d) > 0) {
        //    msg.ok('编辑成功！');
        //    hDialog.dialog('close');
        grid.reload();
        // } else {
        //     MessageOrRedirect(d);
        // }
    });

}
function append() {
    if (endEditing()) {
        $('#list').datagrid('appendRow', { status: 'P' });
        editIndex = $('#list').datagrid('getRows').length - 1;
        $('#list').datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
    }
}
function removeit() {
    if (editIndex == undefined) { return }
    $('#list').datagrid('cancelEdit', editIndex)
        .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept() {
    if (endEditing()) {
        $('#list').datagrid('acceptChanges');
    }
}
function reject() {
    $('#list').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges() {
    var rows = $('#list').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}
//双击编辑表单焦点消失后保存结束

//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var myunit = $("#myUnit").textbox('getValue');
    var connman = $("#myConnMan").textbox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    var i = 0;
    if (myunit != '' && myunit != undefined) {//假如单位搜索不为空
        o.rules[i] = JSON.parse('{"field":"unit","op":"cn","data":"' + myunit + '"}');
        i = i + 1;
    }
    if (connman != '' & connman != undefined) {//联系人不为空
        o.rules[i] = JSON.parse('{"field":"connman","op":"cn","data":"' + connman + '"}');
    }

    $('#list').datagrid('reload', { filter: JSON.stringify(o) });


}


//自定义搜索结束


//单选多选开关
$('#selectSwitch').switchbutton({
    checked: false,
    onChange: function (checked) {
        if (checked) {
            $('#list').datagrid({ singleSelect: false });//多选
        } else {
            $('#list').datagrid({ singleSelect: true });//单选
        }
    }
})

