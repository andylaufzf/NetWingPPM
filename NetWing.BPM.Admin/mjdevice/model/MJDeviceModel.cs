using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJDevice")]
	[Description("设备分类管理")]
	public class MJDeviceModel
	{
				/// <summary>
		/// ID
		/// </summary>
		[Description("ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 设备类型
		/// </summary>
		[Description("设备类型")]
		public string d_type { get; set; }		
		/// <summary>
		/// 共有数量
		/// </summary>
		[Description("共有数量")]
		public int d_number { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string remarks { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}