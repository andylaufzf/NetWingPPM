using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("jydflowtasklist")]
	[Description("任务列表")]
	public class JydflowtasklistModel
	{
		/// <summary>
		/// 任务清单ID
		/// </summary>
		[Description("任务清单ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 节点名称
		/// </summary>
		[Description("节点名称")]
		public string nodename { get; set; }		
		/// <summary>
		/// 执行者ID
		/// </summary>
		[Description("执行者ID")]
		public int operatorID { get; set; }		
		/// <summary>
		/// 执行者姓名
		/// </summary>
		[Description("执行者姓名")]
		public string operatorname { get; set; }
        /// <summary>
        /// 订单orderkey
        /// </summary>
        [Description("订单ID")]
        public int orderkey { get; set; }
        /// <summary>
        /// 订单IDorderkey
        /// </summary>
        [Description("订单ID")]
		public string orderID { get; set; }		
		/// <summary>
		/// 订单名称
		/// </summary>
		[Description("订单名称")]
		public string ordername { get; set; }		
		/// <summary>
		/// 印刷品ID
		/// </summary>
		[Description("印刷品ID")]
		public int PrintedmatterID { get; set; }		
		/// <summary>
		/// 印刷品名称
		/// </summary>
		[Description("印刷品名称")]
		public string Printedmattername { get; set; }		
		/// <summary>
		/// 添加时间
		/// </summary>
		[Description("添加时间")]
		public DateTime addtime { get; set; }		
		/// <summary>
		/// 是否完成:0为完成,1为未完成
		/// </summary>
		[Description("是否完成:0为完成,1为未完成")]
		public int isitcomplete { get; set; }		
		/// <summary>
		/// 完成情况备注
		/// </summary>
		[Description("完成情况备注")]
		public string completeofth { get; set; }

        /// <summary>
        /// 时间戳
        /// </summary>
        [Description("时间戳")]
        public string timestamp { get; set; }

        /// <summary>
        /// 流程
        /// </summary>
        /// <returns></returns>
        [Description("流程")]
        public string SchemeContent { get; set; }
        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}