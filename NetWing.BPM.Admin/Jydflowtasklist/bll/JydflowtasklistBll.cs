using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydflowtasklistBll
    {
        public static JydflowtasklistBll Instance
        {
            get { return SingletonProvider<JydflowtasklistBll>.Instance; }
        }

        public int Add(JydflowtasklistModel model)
        {
            return JydflowtasklistDal.Instance.Insert(model);
        }

        public int Update(JydflowtasklistModel model)
        {
            return JydflowtasklistDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydflowtasklistDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydflowtasklistDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
