using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class MJFinanceDetailDal : BaseRepository<MJFinanceDetailModel>
    {
        public static MJFinanceDetailDal Instance
        {
            get { return SingletonProvider<MJFinanceDetailDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(MJFinanceDetailModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}