﻿using NetWing.Bll;
using NetWing.Common.Data.SqlServer;
using NetWing.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NetWing.BPM.Admin.JydInvoicet.ashx
{
    /// <summary>
    /// invoicet 的摘要说明
    /// </summary>
    public class invoicet : IHttpHandler
    {
        public string callbacktype = "";
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");

            string action = context.Request["action"];

            if (string.IsNullOrEmpty(action))
            {
                if (callbacktype == "jsonp")
                {
                    context.Response.Write("jcb({\"code\":\"4000\",\"msg\":\"请求失败，参数有误！\"})");
                }
                else
                {
                    context.Response.Write("{\"code\":\"4000\",\"msg\":\"请求失败，参数有误！\"}");
                }
                return;
            }
            switch (action)
            {
                case "adds"://接件开票
                    adds(context);
                    break;
                case "addpd"://判断是否已经开票了
                    addpd(context);
                    break;

                default:
                    if (callbacktype == "jsonp")
                    {
                        context.Response.Write("jcb({\"code\":\"1000\",\"msg\":\"请求失败，请重新尝试！\"})");
                    }
                    else
                    {
                        context.Response.Write("{\"code\":\"1000\",\"msg\":\"请求失败，请重新尝试！\"}");
                    }
                    break;

            }
        }

        #region 判断是否已经开票了
        private void addpd(HttpContext context)
        {
            string orkeyid = context.Request["orkeyid"];

            #region 判断是否开票了
            string kpxxsql = "select COUNT(order_keyid) as kpnum from jydInvoicet where order_keyid=" + orkeyid + "";
            DataRow kpxxdr = SqlEasy.ExecuteDataRow(kpxxsql);

            int kpnum = int.Parse(kpxxdr["kpnum"].ToString());

            if (kpnum >= 1)
            {
                
                context.Response.Write("{\"status\":\"1\",\"msg\":\"此接件已经开票了！\"}");
            }
            else
            {
                context.Response.Write("{\"status\":\"0\"}");
            }
            

            #endregion
        }
        #endregion


        #region 接件开票
        private void adds(HttpContext context)
        {
            string orkeyid = context.Request["orkeyid"];//接件id
            string Invoice_no = context.Request["Invoice_no"];//发票号

            #region 查询接件信息（根据接件id查询）
            string jjxxsql = "select * from jydOrder where KeyId=" + orkeyid + " ";
            DataRow jjxxdr = SqlEasy.ExecuteDataRow(jjxxsql);

            if (!string.IsNullOrEmpty(jjxxdr.ToString()))
            {
                string comname = jjxxdr["comname"].ToString();
                string orskeyid = jjxxdr["KeyId"].ToString();
                string OrderID = jjxxdr["OrderID"].ToString();
                string money = jjxxdr["jexx"].ToString();

                #region 新增开票信息
                JydInvoicetModel model = new JydInvoicetModel();
                model.OrderID = OrderID;
                model.order_keyid = int.Parse(orskeyid);
                model.money = decimal.Parse(money);
                model.Invoice_no = Invoice_no;
                model.Invoice_time = DateTime.Now;
                model.Corporate_name = comname;
                model.Invoicetype = "开票";

                int addtj = JydInvoicetBll.Instance.Add(model);

                if (addtj > 0)
                {
                    context.Response.Write("{\"status\":\"1\",\"msg\":\"开票成功！\"}");
                }
                else
                {
                    context.Response.Write("{\"status\":\"1\",\"msg\":\"开票失败！\"}");
                }

                #endregion

            }
            else
            {
                context.Response.Write("{\"status\":\"1\",\"msg\":\"接件信息报错！\"}");
            }

            #endregion
        }
        #endregion

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}