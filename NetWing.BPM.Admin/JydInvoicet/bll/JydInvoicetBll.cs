using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydInvoicetBll
    {
        public static JydInvoicetBll Instance
        {
            get { return SingletonProvider<JydInvoicetBll>.Instance; }
        }

        public int Add(JydInvoicetModel model)
        {
            return JydInvoicetDal.Instance.Insert(model);
        }

        public int Update(JydInvoicetModel model)
        {
            return JydInvoicetDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydInvoicetDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydInvoicetDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
