using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("Tpjsontemplate")]
	[Description("模板json")]
	public class TpjsontemplateModel
	{
				/// <summary>
		/// 模板主键
		/// </summary>
		[Description("模板主键")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 执行者
		/// </summary>
		[Description("执行者")]
		public string executor { get; set; }		
		/// <summary>
		/// 节点名称
		/// </summary>
		[Description("节点名称")]
		public string nodename { get; set; }		
		/// <summary>
		/// 节点标识
		/// </summary>
		[Description("节点标识")]
		public string nodefiction { get; set; }		
		/// <summary>
		/// 节点主ID
		/// </summary>
		[Description("节点主ID")]
		public string nodeID { get; set; }		
		/// <summary>
		/// 驳回类型:0:成功,1:不成功
		/// </summary>
		[Description("驳回类型:0:成功,1:不成功")]
		public int rejectedtype { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string remarks { get; set; }		
		/// <summary>
		/// 类型:1:单走,2:双走
		/// </summary>
		[Description("类型:1:单走,2:双走")]
		public int remark { get; set; }		
		/// <summary>
		/// 执行者微信
		/// </summary>
		[Description("执行者微信")]
		public string executorwechar { get; set; }		
		/// <summary>
		/// 模板主键ID
		/// </summary>
		[Description("模板主键ID")]
		public int templateKeyId { get; set; }		
		/// <summary>
		/// 排序
		/// </summary>
		[Description("排序")]
		public int sortid { get; set; }		
		/// <summary>
		/// 添加时间
		/// </summary>
		[Description("添加时间")]
		public DateTime addtime { get; set; }		
		/// <summary>
		/// 修改时间
		/// </summary>
		[Description("修改时间")]
		public DateTime uptime { get; set; }		
		/// <summary>
		/// 模板主名称
		/// </summary>
		[Description("模板主名称")]
		public string templatename { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}