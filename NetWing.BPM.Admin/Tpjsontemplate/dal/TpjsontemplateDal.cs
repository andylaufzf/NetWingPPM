using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class TpjsontemplateDal : BaseRepository<TpjsontemplateModel>
    {
        public static TpjsontemplateDal Instance
        {
            get { return SingletonProvider<TpjsontemplateDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(TpjsontemplateModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}