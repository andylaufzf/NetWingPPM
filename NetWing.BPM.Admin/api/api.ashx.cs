﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NetWing.Model;
using System.Data.Linq;
using NetWing.BPM.Core.Model;
using NetWing.Common.Data.SqlServer;
using NetWing.Common.JSON;
namespace NetWing.BPM.Admin.api
{
    /// <summary>
    /// api 的摘要说明
    /// </summary>
    public class api : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            //context.Response.ContentType = "text/plain";
            //context.Response.Write("Hello World");
            string action = context.Request["action"];
            DataContext db = new DataContext(SqlEasy.connString);
            if (!string.IsNullOrEmpty(action))
            {
                switch (action)
                {
                    case "layui-sysusers":
                        Table<User> User = db.GetTable<User>();
                        var list = from u in User
                                   orderby u.ShortNo ascending
                                   select new { name = u.TrueName, value = u.KeyId };
                        if (!string.IsNullOrEmpty(context.Request["keyword"]))
                        {
                            list = from o in list
                                   where o.name.Contains(context.Request["keyword"])
                                   select o;
                        }
                        context.Response.Write(JSONhelper.ToJson(list));
                        break;

                    default:
                        break;
                }
            }
            db.Dispose();//释放连接
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}