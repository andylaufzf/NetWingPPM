using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class MJDevice_childDal : BaseRepository<MJDevice_childModel>
    {
        public static MJDevice_childDal Instance
        {
            get { return SingletonProvider<MJDevice_childDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(MJDevice_childModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}