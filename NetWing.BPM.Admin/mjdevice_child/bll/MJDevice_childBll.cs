using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJDevice_childBll
    {
        public static MJDevice_childBll Instance
        {
            get { return SingletonProvider<MJDevice_childBll>.Instance; }
        }

        public int Add(MJDevice_childModel model)
        {
            return MJDevice_childDal.Instance.Insert(model);
        }

        public int Update(MJDevice_childModel model)
        {
            return MJDevice_childDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJDevice_childDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJDevice_childDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
