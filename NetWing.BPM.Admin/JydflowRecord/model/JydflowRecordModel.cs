using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("jydflowRecord")]
	[Description("任务操作记录")]
	public class JydflowRecordModel
	{
				/// <summary>
		/// 任务记录ID
		/// </summary>
		[Description("任务记录ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 实例进程ID
		/// </summary>
		[Description("实例进程ID")]
		public int processID { get; set; }		
		/// <summary>
		/// 操作内容
		/// </summary>
		[Description("操作内容")]
		public string Operationalcontent { get; set; }		
		/// <summary>
		/// 操作时间
		/// </summary>
		[Description("操作时间")]
		public DateTime Operationtime { get; set; }		
		/// <summary>
		/// 创建时间
		/// </summary>
		[Description("创建时间")]
		public string addtime { get; set; }		
		/// <summary>
		/// 操作人
		/// </summary>
		[Description("操作人")]
		public string Operctionname { get; set; }	
        /// <summary>
        /// orderID
        /// </summary>
        /// <returns></returns>
		[Description("订单id")]
        public string orderid { get; set; }
        /// <summary>
        ///备注
        /// </summary>
        /// <returns></returns>
        [Description("备注")]
        public string Remarks { get; set; }
        /// <summary>
        /// 关联keyid
        /// </summary>
        /// <returns></returns>
        [Description("关联keyid")]
        public int nikeyid { get; set; }
        /// <summary>
        /// 执行ID
        /// </summary>
        /// <returns></returns>
        [Description("执行ID")]
        public int sortid { get; set; }
        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}