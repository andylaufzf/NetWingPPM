using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class Psi_goodssuanBll
    {
        public static Psi_goodssuanBll Instance
        {
            get { return SingletonProvider<Psi_goodssuanBll>.Instance; }
        }

        public int Add(Psi_goodssuanModel model)
        {
            return Psi_goodssuanDal.Instance.Insert(model);
        }

        public int Update(Psi_goodssuanModel model)
        {
            return Psi_goodssuanDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return Psi_goodssuanDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return Psi_goodssuanDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
