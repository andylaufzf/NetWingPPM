using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class Psi_goodssuanDal : BaseRepository<Psi_goodssuanModel>
    {
        public static Psi_goodssuanDal Instance
        {
            get { return SingletonProvider<Psi_goodssuanDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(Psi_goodssuanModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}