using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("psi_goodssuan")]
	[Description("库存出入表")]
	public class Psi_goodssuanModel
	{
        /// <summary>
		/// 库存子表ID
		/// </summary>
		[Description("库存子表ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 库存表ID
		/// </summary>
		[Description("库存表ID")]
		public int goodsID { get; set; }		
		/// <summary>
		/// 编号
		/// </summary>
		[Description("编号")]
		public string goodsNo { get; set; }		
		/// <summary>
		/// 库存名称
		/// </summary>
		[Description("库存名称")]
		public string goodsName { get; set; }		
		/// <summary>
		/// 库存类别
		/// </summary>
		[Description("库存类别")]
		public string goodsclassname { get; set; }		
		/// <summary>
		/// 规格
		/// </summary>
		[Description("规格")]
		public string spercs { get; set; }		
		/// <summary>
		/// 单位
		/// </summary>
		[Description("单位")]
		public string unit { get; set; }		
		/// <summary>
		/// 库存量
		/// </summary>
		[Description("库存量")]
		public int stock { get; set; }		
		/// <summary>
		/// 进价
		/// </summary>
		[Description("进价")]
		public decimal buyprice { get; set; }		
		/// <summary>
		/// 添加时间
		/// </summary>
		[Description("添加时间")]
		public DateTime? add_time { get; set; }		
		/// <summary>
		/// 修改时间
		/// </summary>
		[Description("修改时间")]
		public DateTime? up_time { get; set; }		
		/// <summary>
		/// 当前状态
		/// </summary>
		[Description("当前状态")]
		public string status { get; set; }		
	    /// <summary>
        /// 实际数量
        /// </summary>
        /// <returns>
        /// </returns>
        [Description("实际数量")]
        public int number { get; set; }
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}