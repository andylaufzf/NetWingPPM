using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("jydDo")]
	[Description("时间流程")]
	public class JydDoModel
	{
				/// <summary>
		/// 
		/// </summary>
		[Description("")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 工艺
		/// </summary>
		[Description("工艺")]
		public string gytype { get; set; }		
		/// <summary>
		/// 主表ID
		/// </summary>
		[Description("主表ID")]
		public int mainKeyid { get; set; }		
		/// <summary>
		/// 明细表ID
		/// </summary>
		[Description("明细表ID")]
		public int detailKeyid { get; set; }		
		/// <summary>
		/// 订单号
		/// </summary>
		[Description("订单号")]
		public string orderid { get; set; }		
		/// <summary>
		/// 负责人
		/// </summary>
		[Description("负责人")]
		public string doman { get; set; }		
		/// <summary>
		/// 负责人ID
		/// </summary>
		[Description("负责人ID")]
		public int domanid { get; set; }		
		/// <summary>
		/// 完成时间
		/// </summary>
		[Description("完成时间")]
		public DateTime finishTime { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string note { get; set; }

        [Description("订单名称")]
        public string myorder { get; set; }

        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}