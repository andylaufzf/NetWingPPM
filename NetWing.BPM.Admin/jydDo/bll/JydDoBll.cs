using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydDoBll
    {
        public static JydDoBll Instance
        {
            get { return SingletonProvider<JydDoBll>.Instance; }
        }

        public int Add(JydDoModel model)
        {
            return JydDoDal.Instance.Insert(model);
        }

        public int Update(JydDoModel model)
        {
            return JydDoDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydDoDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydDoDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
