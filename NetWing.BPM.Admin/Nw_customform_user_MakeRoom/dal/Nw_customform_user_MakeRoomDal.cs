using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class Nw_customform_user_MakeRoomDal : BaseRepository<Nw_customform_user_MakeRoomModel>
    {
        public static Nw_customform_user_MakeRoomDal Instance
        {
            get { return SingletonProvider<Nw_customform_user_MakeRoomDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(Nw_customform_user_MakeRoomModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}