var actionURL = '/Nw_customform_user_MakeRoom/ashx/Nw_customform_user_MakeRoomHandler.ashx';
var formurl = '/Nw_customform_user_MakeRoom/html/Nw_customform_user_MakeRoom.html';
//定义要隐藏的字段用于动态显示（主要是为了方便开发人员调试）
var hiddenFields = new Array("KeyId", "maa_wxpaynum", "maa_shpaynum", "maa_idc", "maa_openid", "maa_addtime", "maa_rzstate", "maa_fjname", "maa_fjid", "maa_patime", "up_time");

$(function () {


    //为避免数据字典加载不完整。这段在后面加载
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });

    $('#a_add').click(CRUD.add);
    $('#a_edit').click(CRUD.edit);
    $('#a_delete').click(CRUD.del);
    //高级查询
    $('#a_search').click(function () {
        search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#a_getSearch').click(function () {//自定义搜索框
        mySearch();
    });

    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });

    //批量删除
    $("#a_alldel").click(function () {
        var ids = [];
        var rows = $('#list').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].KeyId);
        }
        var allid = ids.join(',');//所有的id
        var o = {};
        o.action = "alldel";
        o.KeyIds = allid;
        var param = "json=" + JSON.stringify(o);

        if (confirm('确认要执行批量删除操作吗？')) {
            jQuery.ajaxjson(actionURL, param, function (d) {
                if (parseInt(d) > 0) {
                    msg.ok('批量删除成功！');
                    grid.reload();
                } else {
                    MessageOrRedirect(d);
                }
            });
        }


    });

});
//这段后加载，避免数据字典加载不完整的问题
window.onload = function () {
    autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
};


//editor:'datetimebox' 日期及时间选择框  editor:'datebox' 日期选择框    editor:'numberspinner' 数字调节器  editor:{type: 'numberspinner',options:{value:0,required:true}}

//定义一个$JQ为$.　以后在js 中就可以用${JQ}AJAX了在前台这样写(定义)://通用数据字典start
var getDic = {
    jsonData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?categoryId=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            //alert(newData);
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
    jsonParentData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?action=parent&parentid=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
}

//通用数据字典end
//调用数据字典和使用数据字典 如果不需要请不要打开否则会导致系统速度慢
//getDic.jsonData(9);//取得性别数据字典
//在onLoad:的地方如下使用
//top.$('#txt_user_sex').combobox({ data: eval($('body').data('data9')), valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '请选择性别', disabled: false });





var grid = {
    bind: function (winSize) {
        $('#list').datagrid({
            url: actionURL,
            toolbar: '#toolbar',
            title: "预约列表",
            iconCls: 'icon icon-list',
            width: winSize.width,
            height: winSize.height,
            nowrap: false, //折行
            rownumbers: true, //行号
            striped: true, //隔行变色
            idField: 'KeyId',//主键
            singleSelect: true, //单选
            frozenColumns: [[]],
            columns: [[//应为宽度不是很需要所以注释了宽度
                { title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                { title: '原始编号', field: 'id', sortable: true, editor: 'numberspinner', width: '', hidden: true },//主键自动判断隐藏
                { title: '系统ID', field: 'KeyId', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '表单ID', field: 'form_id', sortable: true, editor: 'numberspinner', width: '', hidden: true },//主键自动判断隐藏
                { title: '排序ID', field: 'sort_id', sortable: true, editor: 'numberspinner', width: '', hidden: true },//主键自动判断隐藏
                { title: '状态', field: 'status', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏
                { title: '是否是消息', field: 'is_msg', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏
                { title: '是否置顶', field: 'is_top', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏
                { title: '是否可读', field: 'is_red', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏
                { title: '是否热门', field: 'is_hot', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏
                { title: '是否系统', field: 'is_sys', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏
                { title: '是否是幻灯片', field: 'is_slide', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏
                { title: '点击量', field: 'click', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏

                { title: '门店', field: 'maa_md', sortable: true, width: '120', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '房型', field: 'maa_fx', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '房号', field: 'maa_fh', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '预约金', field: 'maa_price', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '姓名', field: 'maa_name', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '性别', field: 'maa_sex', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '身份证号码', field: 'maa_idc', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '手机号码', field: 'maa_tel', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '入住人数', field: 'maa_num', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                {
                    title: '预约看房时间', field: 'maa_yykfsj', sortable: true, width: '150',
                    formatter: function (value, row, index) {
                        return new Date(value).Format("yyyy-MM-dd");
                    },
                    hidden: false, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } }
                },
                {
                    title: '预计入住时间', field: 'maa_yyrzsj', sortable: true, width: '150',
                    formatter: function (value, row, index) {
                        return new Date(value).Format("yyyy-MM-dd");
                    },
                    hidden: false, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } }
                },
                {
                    title: '预计退房时间', field: 'maa_yytfsj', sortable: true, width: '150',
                    formatter: function (value, row, index) {
                        return new Date(value).Format("yyyy-MM-dd");
                    },
                    hidden: false, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } }
                },
                { title: '支付状态', field: 'maa_paystate', sortable: true, width: '80', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '微信支付订单号', field: 'maa_wxpaynum', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '商户订单号', field: 'maa_shpaynum', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '预约者微信ID', field: 'maa_openid', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '系统时间', field: 'maa_addtime', sortable: true, width: '', hidden: false, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '入住状态', field: 'maa_rzstate', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '房间名称', field: 'maa_fjname', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '房间ID', field: 'maa_fjid', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '支付时间', field: 'maa_patime', sortable: true, width: '160', hidden: false, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '添加时间', field: 'add_time', sortable: true, width: '160', hidden: false, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '更新时间', field: 'up_time', sortable: true, width: '160', hidden: false, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '备注', field: 'note', sortable: true, width: '160', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '操作', field: 'opt', width: '', hidden: false, formatter: formatOper },

            ]],
            onEndEdit: onEndEdit,//结束编辑时函数 这里为了简洁 该函数写在下面
            onUnselect: onUnselect,
            onLoadSuccess: function (data) {
                console.log("加载完隐藏不需要显示的列");
                for (var i = 0; i < hiddenFields.length; i++) {
                    var field = hiddenFields[i];
                    $("#list").datagrid('hideColumn', field);//加载完吧所有的列都隐藏
                }
            },
            onCancelEdit: onCancelEdit,//在用户取消编辑一行的时候触发
            onSelect: onSelect,//在用户选择一行的时候触发
            //onClickRow: onClickRow,//在用户点击一行的时候触发
            //onAfterEdit: onAfterEdit,//在用户完成编辑一行的时候触发
            //onDblClickCell: onDblClickCell,//为了程序逻辑清楚函数写在外面
            onHeaderContextMenu: function (e, field) {//列菜单实现动态隐藏列
                e.preventDefault();
                if (!cmenu) {
                    createColumnMenu();
                }
                cmenu.menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            pagination: true,
            pageSize: PAGESIZE,
            pageList: [20, 40, 50, 100, 200]
        });
    },
    getSelectedRow: function () {
        return $('#list').datagrid('getSelected');
    },
    reload: function () {
        $('#list').datagrid('clearSelections').datagrid('reload', { filter: '' });
    }
};

function formatOper(val, row, index) {
    console.log(row);
    var r = row;
    var s = '';
    if (row.accountid != 0 && row.accountid != "" && row.edNumber != null) {
        s = s + '<input type="button"  style="color: #000000;" onclick="pyyj(\'' + row.edNumber + '\');" value="预约金收据">';

        if (row.tuiedNumber == "" || row.tuiedNumber == null) {//如果还没有退
            s = s + '<input type="button"  style="color: #ff0000;" onclick="tyyj(\'' + row.edNumber + '\',\'' + row.KeyId + '\',\'' + row.maa_name + '\',\'' + row.maa_price + '\');" value="退预约金">';
        } else {//如果已经退了
            s = s + '<input type="button"  style="color: #000000;" onclick="ptyyj(\'' + row.edNumber + '\',\'' + row.KeyId + '\',\'' + row.maa_name + '\',\'' + row.maa_price + '\');" value="退预约金收据">';
        }

    }
    return s;
}


//打印预约收据
function ptyyj(edNumber, kid, name, price) {
    console.log("系统单号是：" + edNumber);
    //alert(murl);
    var dialog = $.dialog({
        title: '打印退预约金收据',
        content: 'url:/mjprint/tyyjsj.aspx?edNumber=' + edNumber + '',
        min: false,
        max: false,
        lock: true,
        width: 850,
        height: 550
    });
}



//退预约金收据
function tyyj(edNumber,kid,name,price) {
    console.log("系统单号是：" + edNumber);
    var hDialog = top.jQuery.hDialog({
        title: name+'退预约定金'+price+'元', width: 600, height: 400, href: '/Nw_customform_user_MakeRoom/html/tui.html', iconCls: 'icon-add',
        onLoad: function () {
            //银行
            top.$(".accountid").combobox({
                valueField: 'KeyId',//用中文名作为名称
                required: true,
                editable: false,
                textField: 'accountName',
                url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJBankAccount&field=accountName',
                onSelect: function (rec) {
                    $(this).prev('.account').val(rec.accountName);
                    //$("#account").val(rec.accountName);//设置隐藏ID
                }
            });
        },
        submit: function () {
            if (top.$("#uiform").form('validate')) {
                var query = createParam('tuiyyj', kid);
                jQuery.ajaxjson(actionURL, query, function (d) {
                    console.log(JSON.stringify(d));
                    if (d.status == 1) {
                        msg.ok(d.msg);
                        hDialog.dialog('close');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d.msg);
                    }
                });
            } else {
                msg.warning('请填写必填项！');
            }
        }
    });
}


//打印预约收据
function pyyj(edNumber) {
    console.log("系统单号是：" + edNumber);
    //alert(murl);
    var dialog = $.dialog({
        title: '打印预约金收据',
        content: 'url:/mjprint/yyjsj.aspx?edNumber=' + edNumber + '',
        min: false,
        max: false,
        lock: true,
        width: 850,
        height: 550
    });
}


function createParam(action, keyid) {
    var o = {};
    var query = top.$('#uiform').serializeArray();
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}


var CRUD = {
    add: function () {
        var hDialog = top.jQuery.hDialog({
            title: '添加', width: 1000, height: 600, href: formurl, iconCls: 'icon-add',
            onLoad: function () {
                top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器


                console.log("开始检测预约金");
                top.$("#txt_maa_price").numberbox({
                    onChange: function (newValue, oldValue) {
                        if (newValue > 0) {
                            console.log("开始加载银行");
                            //银行
                            top.$(".accountid").combobox({
                                valueField: 'KeyId',//用中文名作为名称
                                required: true,
                                editable: false,
                                textField: 'accountName',
                                url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJBankAccount&field=accountName',
                                onSelect: function (rec) {
                                    $(this).prev('.account').val(rec.accountName);
                                    //$("#account").val(rec.accountName);//设置隐藏ID
                                }
                            });

                            top.$("#showaccount").show();
                        } else {
                            top.$("#showaccount").hide();
                        }
                    }
                });
            },
            submit: function () {
                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate')) {
                    var query = createParam('add', '0');
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (d.status == 1) {
                            msg.ok('添加成功！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d.msg);
                        }
                    });
                } else {
                    msg.warning('请填写必填项！');
                }

                return false;
            }
        });

        top.$('#uiform').validate();
    },
    edit: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '编辑', width: 1000, height: 600, href: formurl, iconCls: 'icon-save',
                onLoad: function () {
                    //top.$('#txt_id').numberspinner('setValue', row.id);
                    //top.$('#txt_KeyId').numberspinner('setValue', row.KeyId);
                    //top.$('#txt_form_id').numberspinner('setValue', row.form_id);
                    //top.$('#txt_sort_id').numberspinner('setValue', row.sort_id);
                    //top.$('#txt_status').textbox('setValue', row.status);
                    //top.$('#txt_is_msg').textbox('setValue', row.is_msg);
                    //top.$('#txt_is_top').textbox('setValue', row.is_top);
                    //top.$('#txt_is_red').textbox('setValue', row.is_red);
                    //top.$('#txt_is_hot').textbox('setValue', row.is_hot);
                    //top.$('#txt_is_sys').textbox('setValue', row.is_sys);
                    //top.$('#txt_is_slide').textbox('setValue', row.is_slide);
                    //top.$('#txt_click').textbox('setValue', row.click);
                    //top.$('#txt_add_time').datetimebox('setValue', row.add_time);
                    //top.$('#txt_up_time').datetimebox('setValue', row.up_time);
                    //top.$('#txt_maa_md').textbox('setValue', row.maa_md);
                    //top.$('#txt_maa_fx').textbox('setValue', row.maa_fx);
                    //top.$('#txt_maa_price').numberbox('setValue', row.maa_price);
                    //top.$('#txt_maa_name').textbox('setValue', row.maa_name);
                    //top.$('#txt_maa_sex').textbox('setValue', row.maa_sex);
                    //top.$('#txt_maa_idc').textbox('setValue', row.maa_idc);
                    //top.$('#txt_maa_tel').textbox('setValue', row.maa_tel);
                    //top.$('#txt_maa_num').textbox('setValue', row.maa_num);
                    //top.$('#txt_maa_yykfsj').datetimebox('setValue', row.maa_yykfsj);
                    //top.$('#txt_maa_yyrzsj').datetimebox('setValue', row.maa_yyrzsj);
                    //top.$('#txt_maa_yytfsj').datetimebox('setValue', row.maa_yytfsj);
                    //top.$('#txt_maa_paystate').textbox('setValue', row.maa_paystate);
                    //top.$('#txt_maa_wxpaynum').textbox('setValue', row.maa_wxpaynum);
                    //top.$('#txt_maa_shpaynum').textbox('setValue', row.maa_shpaynum);
                    //top.$('#txt_maa_openid').textbox('setValue', row.maa_openid);
                    //top.$('#txt_maa_addtime').datetimebox('setValue', row.maa_addtime);
                    //top.$('#txt_maa_rzstate').textbox('setValue', row.maa_rzstate);
                    //top.$('#txt_maa_fjname').textbox('setValue', row.maa_fjname);
                    //top.$('#txt_maa_fjid').textbox('setValue', row.maa_fjid);
                    //top.$('#txt_maa_patime').datetimebox('setValue', row.maa_patime);
                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');   

                    top.$('#uiform').form('load', row);//整个表格加载数据的方法
                    console.log("编辑加载");
                },
                submit: function () {
                    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                    //alert(top.$("#uiform").form('validate'));
                    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {
                        var query = createParam('edit', row.KeyId);;
                        jQuery.ajaxjson(actionURL, query, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    } else {
                        msg.warning('请填写必填项！');
                    }
                    
                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

//实现动态隐藏列
var cmenu = null;
function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#list').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#list').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#list').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#list').datagrid('getColumnOption', field);
        cmenu.menu('appendItem', {
            text: col.title,
            name: field,
            iconCls: 'icon-ok'
        });
    }
}

//实现动态隐藏列结束

//双击编辑表单焦点消失后保存
var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#list').datagrid('validateRow', editIndex)) {
        $('#list').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function onDblClickCell(index, field) {
    if (editIndex != index) {
        if (endEditing()) {
            $('#list').datagrid('selectRow', index)
            $('#list').datagrid('beginEdit', index);
            var ed = $('#list').datagrid('getEditor', { index: index, field: field });
            if (ed) {
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                //这个写法很有意思
                //为了方便 ,类似字段要同事写两个值 1把部门id写入隐藏字段2把标题换成值写入 显示字段

            }
            editIndex = index;
        } else {
            setTimeout(function () {
                $('#list').datagrid('selectRow', editIndex);
            }, 0);
        }
    }
}

function onUnselect(index, row) {
    //alert(index);
}

function onCancelEdit(index, row) {
    //alert(index);
}
function onClickRow(index, row) {
    if (editIndex != undefined) {
        //alert("正在编辑的行是：" + editIndex);
        //alert("验证正在编辑的行：" + $('#list').datagrid('validateRow', editIndex));
        if ($('#list').datagrid('validateRow', editIndex)) {//如果验证正在编辑的行数据有效则
            $("#list").datagrid('endEdit', editIndex);//如果点其他行，结束编辑正在编辑的行
            editIndex = undefined;
        } else {
            msg.warning("还有一行没编辑完啦！！");
        }
    }
    $("#list").datagrid('selectRow', index);


}

function onSelect(index, row) {

}

function onEndEdit(index, row, changes) {
    //alert('结束编辑'+index+JSON.stringify(row));
    var o = {};
    var query = JSON.stringify(row);
    o.jsonEntity = query;
    o.action = 'edit';
    o.keyid = row.KeyId;
    query = "json=" + JSON.stringify(o);
    //alert(query);
    //表格内编辑模式编辑成功不提示信息
    jQuery.ajaxjson(actionURL, query, function (d) {
        //if (parseInt(d) > 0) {
        //    msg.ok('编辑成功！');
        //    hDialog.dialog('close');
        grid.reload();
        // } else {
        //     MessageOrRedirect(d);
        // }
    });

}
function append() {
    if (endEditing()) {
        $('#list').datagrid('appendRow', { status: 'P' });
        editIndex = $('#list').datagrid('getRows').length - 1;
        $('#list').datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
    }
}
function removeit() {
    if (editIndex == undefined) { return }
    $('#list').datagrid('cancelEdit', editIndex)
        .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept() {
    if (endEditing()) {
        $('#list').datagrid('acceptChanges');
    }
}
function reject() {
    $('#list').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges() {
    var rows = $('#list').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}
//双击编辑表单焦点消失后保存结束

//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var myunit = $("#myUnit").textbox('getValue');
    var connman = $("#myConnMan").textbox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    var i = 0;
    if (myunit != '' && myunit != undefined) {//假如单位搜索不为空
        o.rules[i] = JSON.parse('{"field":"unit","op":"cn","data":"' + myunit + '"}');
        i = i + 1;
    }
    if (connman != '' & connman != undefined) {//联系人不为空
        o.rules[i] = JSON.parse('{"field":"connman","op":"cn","data":"' + connman + '"}');
    }

    $('#list').datagrid('reload', { filter: JSON.stringify(o) });


}


//自定义搜索结束


//单选多选开关
$('#selectSwitch').switchbutton({
    checked: false,
    onChange: function (checked) {
        if (checked) {
            $('#list').datagrid({ singleSelect: false });//多选
        } else {
            $('#list').datagrid({ singleSelect: true });//单选
        }
    }
})

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
//调用： 

//var time1 = new Date().Format("yyyy-MM-dd");
//var time2 = new Date().Format("yyyy-MM-dd HH:mm:ss"); 