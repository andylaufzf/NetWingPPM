using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("jydorderhoujiagong")]
	[Description("后加工单管理")]
	public class JydorderhoujiagongModel
	{
				/// <summary>
		/// 序号
		/// </summary>
		[Description("序号")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 订单号
		/// </summary>
		[Description("订单号")]
		public string orderid { get; set; }		
		/// <summary>
		/// 后加工厂
		/// </summary>
		[Description("后加工厂")]
		public string myws { get; set; }		
		/// <summary>
		/// 下单人
		/// </summary>
		[Description("下单人")]
		public string myxd { get; set; }		
		/// <summary>
		/// 开单日期
		/// </summary>
		[Description("开单日期")]
		public DateTime adddate { get; set; }		
		/// <summary>
		/// 后加工实收人
		/// </summary>
		[Description("后加工实收人")]
		public string mysh { get; set; }		
		/// <summary>
		/// 交货日期
		/// </summary>
		[Description("交货日期")]
		public DateTime myjhrq { get; set; }		
		/// <summary>
		/// 印刷品名
		/// </summary>
		[Description("印刷品名")]
		public string yspmc { get; set; }		
		/// <summary>
		/// 加工尺寸
		/// </summary>
		[Description("加工尺寸")]
		public string mycc { get; set; }		
		/// <summary>
		/// 加工数量
		/// </summary>
		[Description("加工数量")]
		public int mynum { get; set; }		
		/// <summary>
		/// 实收数量
		/// </summary>
		[Description("实收数量")]
		public string actual_quantity { get; set; }		
		/// <summary>
		/// 实收日期
		/// </summary>
		[Description("实收日期")]
		public DateTime receiving_time { get; set; }		
		/// <summary>
		/// 单价
		/// </summary>
		[Description("单价")]
		public decimal mydj { get; set; }		
		/// <summary>
		/// 金额
		/// </summary>
		[Description("金额")]
		public decimal myysf { get; set; }		
		/// <summary>
		/// 制作项目
		/// </summary>
		[Description("制作项目")]
		public string myzzxm { get; set; }		
		/// <summary>
		/// 加工备注
		/// </summary>
		[Description("加工备注")]
		public string wsjgbz { get; set; }		
		/// <summary>
		/// 付款方式
		/// </summary>
		[Description("付款方式")]
		public string status_fk { get; set; }		
	    /// <summary>
        /// 主表keyid
        /// </summary>
        /// <returns></returns>
        [Description("主表KeyId")]
        public int orderkeyid { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        /// <returns></returns>
        [Description("图片")]
        public string order_img { get; set; }
        /// <summary>
        /// 已付金额
        /// </summary>
        /// <returns></returns>
        [Description("已付金额")]
        public decimal yfprice { get; set; }
        /// <summary>
        /// 余
        /// </summary>
        /// <returns></returns>
        [Description("余")]
        public decimal af { get; set; }
        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}