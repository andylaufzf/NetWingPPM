﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="Jydorderhoujiagong.aspx.cs" Inherits="NetWing.BPM.Admin.Jydorderhoujiagong.Jydorderhoujiagong" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->



<!-- 工具栏按钮 -->
    <div id="toolbar"><%= base.BuildToolbar()%>
        <%--<a id="print" class="easyui-linkbutton">打印</a>--%>
        <a id="btn" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'">打印</a>
        <a href="#" plain="true" class="easyui-linkbutton" title="后加工厂">后加工厂:<input id="myws" name="myws" class="easyui-textbox" style="width: 80px"/>
        <a href="#" plain="true" class="easyui-linkbutton" title="下单人">下单人:<input id="myxd" name="myxd" class="easyui-textbox" style="width: 80px"/>
        
        日期：<input type="text" id="dtOpstart" style="width: 120px;" class="easyui-datetimebox" />
        至
            <input type="text" id="dtOpend" style="width: 120px;" class="easyui-datetimebox" />
            交货时间:<input type="text" id="jhsjo" style="width: 120px;"  class="easyui-datebox" />
        <a id="a_getSearch" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a  href="#" plain="true" class="easyui-linkbutton" title="模式">模式</a>
        <input id="selectSwitch" class="easyui-switchbutton" data-options="onText:'多选',offText:'单选'"> 

    </div>

    <!-- datagrid 列表 -->
    <table id="list" ></table>  


	<!--Uploader-->
	<!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
	<!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>
    <script src="../scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <!-- 引入js文件 -->
    <script src="js/Jydorderhoujiagong.js"></script>
</asp:Content>



