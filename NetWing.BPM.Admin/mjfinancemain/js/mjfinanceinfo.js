var actionURL = '/MJFinanceMain/ashx/MJFinanceMainHandler.ashx';
var formurl = '/MJFinanceMain/html/MJFinanceMain.html';
//定义要隐藏的字段用于动态显示（主要是为了方便开发人员调试）
var hiddenFields = new Array("KeyId", "unitid", "contactid", "dep", "depid", "accountid", "operateid", "up_time");
$(function () {

    //为避免数据字典加载不完整。这段在后面加载
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });



    $('#a_add').click(CRUD.add);
    $('#a_edit').click(CRUD.edit);
    $('#a_delete').click(CRUD.del);
    //高级查询
    $('#a_search').click(function () {
        search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#a_getSearch').click(function () {//自定义搜索框
        mySearch();
    });

    //打印
    $('#a_printWin').click(function () {
        var row = grid.getSelectedRow();
        console.log(row.edNumber);
        printWin(row.edNumber);
    });


    //标准显示
    $('#a_view').click(function () {
        for (var i = 0; i < hiddenFields.length; i++) {
            var field = hiddenFields[i];
            $("#list").datagrid('hideColumn', field);//加载完吧所有的列都隐藏
        }
    });
    //全部显示
    $('#a_allview').click(function () {
        for (var i = 0; i < hiddenFields.length; i++) {
            var field = hiddenFields[i];
            $("#list").datagrid('showColumn', field);//加载完吧所有的列都隐藏
        }
    });


    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });

    //批量删除
    $("#a_alldel").click(function () {
        var ids = [];
        var rows = $('#list').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].KeyId);
        }
        var allid = ids.join(',');//所有的id
        var o = {};
        o.action = "alldel";
        o.KeyIds = allid;
        var param = "json=" + JSON.stringify(o);

        if (confirm('确认要执行批量删除操作吗？')) {
            jQuery.ajaxjson(actionURL, param, function (d) {
                if (parseInt(d) > 0) {
                    msg.ok('批量删除成功！');
                    grid.reload();
                } else {
                    MessageOrRedirect(d);
                }
            });
        }


    });

});
//这段后加载，避免数据字典加载不完整的问题
window.onload = function () {
    autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
};


//editor:'datetimebox' 日期及时间选择框  editor:'datebox' 日期选择框    editor:'numberspinner' 数字调节器  editor:{type: 'numberspinner',options:{value:0,required:true}}

//定义一个$JQ为$.　以后在js 中就可以用${JQ}AJAX了在前台这样写(定义)://通用数据字典start
var getDic = {
    jsonData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?categoryId=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            //alert(newData);
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
    jsonParentData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?action=parent&parentid=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
}

//通用数据字典end
//调用数据字典和使用数据字典 如果不需要请不要打开否则会导致系统速度慢
//getDic.jsonData(9);//取得性别数据字典
//在onLoad:的地方如下使用
//top.$('#txt_user_sex').combobox({ data: eval($('body').data('data9')), valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '请选择性别', disabled: false });





var grid = {
    bind: function (winSize) {
        $('#list').datagrid({
            url: actionURL,
            toolbar: '#toolbar',
            title: "财务明细单",
            iconCls: 'icon icon-list',
            width: winSize.width,
            height: winSize.height,
            nowrap: false, //折行
            rownumbers: true, //行号
            striped: true, //隔行变色
            idField: 'KeyId',//主键
            singleSelect: true, //单选
            frozenColumns: [[]],
            columns: [[//应为宽度不是很需要所以注释了宽度
                { title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                { title: '系统编号', field: 'KeyId', sortable: true, width: '', hidden: true, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '编号', field: 'edNumber', sortable: true, width: '150', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '客户', field: 'unit', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '客户ID', field: 'unitid', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '经手人', field: 'contact', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '经手人ID', field: 'contactid', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '部门', field: 'dep', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '部门ID', field: 'depid', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },

                { title: '关联账户', field: 'account', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '关联账户ID', field: 'accountid', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '应收/付金额', field: 'total', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '实收/付金额', field: 'payment', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '添加时间', field: 'add_time', sortable: true, width: '150', hidden: false, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '更新时间', field: 'up_time', sortable: true, width: '120', hidden: false, editor: { type: 'datetimebox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '操作人', field: 'operate', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '操作人ID', field: 'operateid', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
                { title: '备注', field: 'note', sortable: true, width: '300', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
                {
                    title: '操作', field: 'opt', sortable: true, width: '', formatter: function (value, row, index) {
                        if (row.edNumber.indexOf("CW") > -1) {
                            return "<input type='button' onclick=printpz('" + row.edNumber + "') value='打印凭证'>";
                        }
                    }, hidden: false,
                },

            ]],
            onEndEdit: onEndEdit,//结束编辑时函数 这里为了简洁 该函数写在下面
            onUnselect: onUnselect,
            onLoadSuccess: function (data) {
                console.log("加载完隐藏不需要显示的列");
                for (var i = 0; i < hiddenFields.length; i++) {
                    var field = hiddenFields[i];
                    $("#list").datagrid('hideColumn', field);//加载完吧所有的列都隐藏
                }
            },
            onCancelEdit: onCancelEdit,//在用户取消编辑一行的时候触发
            onSelect: onSelect,//在用户选择一行的时候触发
            //onClickRow: onClickRow,//在用户点击一行的时候触发
            //onAfterEdit: onAfterEdit,//在用户完成编辑一行的时候触发
            //onDblClickCell: onDblClickCell,//为了程序逻辑清楚函数写在外面
            onHeaderContextMenu: function (e, field) {//列菜单实现动态隐藏列
                e.preventDefault();
                if (!cmenu) {
                    createColumnMenu();
                }
                cmenu.menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            pagination: true,
            pageSize: PAGESIZE,
            pageList: [20, 40, 50, 100, 200],
            view: detailview,
            detailFormatter: function (index, row) {
                return '<div class="ddv" style="padding:5px 0"></div>';
            },
            onExpandRow: function (index, row) {//展开的时候
                $.ajax({
                    type: "POST",
                    //url: "/MJOrderDetail/ashx/MJOrderDetailHandler.ashx?t=list&KeyId=" + row.KeyId + "",
                    url: "/MJFinanceDetail/ashx/MJFinanceDetailHandler.ashx",
                    //data: { "filter": "{\"groupOp\":\"AND\",\"rules\":[{\"field\":\"financeId\",\"op\":\"eq\",\"data\":\"" + row.KeyId + "\"}],\"groups\":[]}" },
                    data: { "filter": "{\"groupOp\": \"AND\", \"rules\": [{\"field\":\"financeId\",\"op\":\"eq\",\"data\": \"" + row.KeyId + "\" }], \"groups\": [] }" },
                    dataType: "json",
                    beforeSend: function () { },
                    complete: function () { },
                    success: function (d) {
                        //alert(JSON.stringify(d));
                        var temptab = "<table><tbody>";
                        temptab = temptab + "<tr>";
                        temptab = temptab + "<td>序号</td>";
                        temptab = temptab + "<td>名称</td>";
                        temptab = temptab + "<td>Name</td>";
                        temptab = temptab + "<td>值</td>";
                        temptab = temptab + "</tr>";



                        //temptab = temptab + "<tr><td colspan=8><center>服&nbsp;&nbsp;务&nbsp;&nbsp;信&nbsp;&nbsp;息</center></td></tr>";
                        //temptab = temptab + "<tr><td>序号</td><td>服务名称</td><td>单价</td><td>数量</td><td>总价</td><td>服务开始时间</td><td>服务到期时间</td><td>备注</td></tr>";
                        //var temptrtd = "";
                        $.each(d.rows, function (i, item) {
                            temptab = temptab + "<tr><td colspan=4><b><center>第" + (parseInt(i + 1)) + "个</center></b><hr/></td></tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>1</td>";
                            temptab = temptab + "<td>编号</td>";
                            temptab = temptab + "<td>KeyId</td>";
                            temptab = temptab + "<td>" + item.KeyId + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>2</td>";
                            temptab = temptab + "<td>主表ID</td>";
                            temptab = temptab + "<td>financeId</td>";
                            temptab = temptab + "<td>" + item.financeId + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>3</td>";
                            temptab = temptab + "<td>收支类型</td>";
                            temptab = temptab + "<td>ftype</td>";
                            temptab = temptab + "<td>" + item.ftype + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>4</td>";
                            temptab = temptab + "<td>编号</td>";
                            temptab = temptab + "<td>edNumber</td>";
                            temptab = temptab + "<td>" + item.edNumber + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>5</td>";
                            temptab = temptab + "<td>科目</td>";
                            temptab = temptab + "<td>subject</td>";
                            temptab = temptab + "<td>" + item.subject + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>6</td>";
                            temptab = temptab + "<td>科目ID</td>";
                            temptab = temptab + "<td>subjectid</td>";
                            temptab = temptab + "<td>" + item.subjectid + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>7</td>";
                            temptab = temptab + "<td>产品或服务数量</td>";
                            temptab = temptab + "<td>num</td>";
                            temptab = temptab + "<td>" + item.num + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>8</td>";
                            temptab = temptab + "<td>单价</td>";
                            temptab = temptab + "<td>sprice</td>";
                            temptab = temptab + "<td>" + item.sprice + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>9</td>";
                            temptab = temptab + "<td>金额</td>";
                            temptab = temptab + "<td>sumMoney</td>";
                            temptab = temptab + "<td>" + item.sumMoney + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>10</td>";
                            temptab = temptab + "<td>服务开始时间</td>";
                            temptab = temptab + "<td>stime</td>";
                            temptab = temptab + "<td>" + item.stime + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>11</td>";
                            temptab = temptab + "<td>服务到期时间</td>";
                            temptab = temptab + "<td>etime</td>";
                            temptab = temptab + "<td>" + item.etime + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>12</td>";
                            temptab = temptab + "<td>部门</td>";
                            temptab = temptab + "<td>dep</td>";
                            temptab = temptab + "<td>" + item.dep + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>13</td>";
                            temptab = temptab + "<td>部门ID</td>";
                            temptab = temptab + "<td>depid</td>";
                            temptab = temptab + "<td>" + item.depid + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>14</td>";
                            temptab = temptab + "<td>备注</td>";
                            temptab = temptab + "<td>note</td>";
                            temptab = temptab + "<td>" + item.note + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>15</td>";
                            temptab = temptab + "<td>添加时间</td>";
                            temptab = temptab + "<td>add_time</td>";
                            temptab = temptab + "<td>" + item.add_time + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>16</td>";
                            temptab = temptab + "<td>更新时间</td>";
                            temptab = temptab + "<td>up_time</td>";
                            temptab = temptab + "<td>" + item.up_time + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>17</td>";
                            temptab = temptab + "<td>数据所有者ID</td>";
                            temptab = temptab + "<td>ownner</td>";
                            temptab = temptab + "<td>" + item.ownner + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>18</td>";
                            temptab = temptab + "<td>支付类型</td>";
                            temptab = temptab + "<td>paytype</td>";
                            temptab = temptab + "<td>" + item.paytype + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>19</td>";
                            temptab = temptab + "<td>下次支付日期</td>";
                            temptab = temptab + "<td>nextpaydate</td>";
                            temptab = temptab + "<td>" + item.nextpaydate + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>20</td>";
                            temptab = temptab + "<td>房间ID</td>";
                            temptab = temptab + "<td>roomid</td>";
                            temptab = temptab + "<td>" + item.roomid + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>21</td>";
                            temptab = temptab + "<td>房间号</td>";
                            temptab = temptab + "<td>roomno</td>";
                            temptab = temptab + "<td>" + item.roomno + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>22</td>";
                            temptab = temptab + "<td>客户ID</td>";
                            temptab = temptab + "<td>userid</td>";
                            temptab = temptab + "<td>" + item.userid + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>23</td>";
                            temptab = temptab + "<td>客户姓名</td>";
                            temptab = temptab + "<td>username</td>";
                            temptab = temptab + "<td>" + item.username + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>24</td>";
                            temptab = temptab + "<td>手机号</td>";
                            temptab = temptab + "<td>mobile</td>";
                            temptab = temptab + "<td>" + item.mobile + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>25</td>";
                            temptab = temptab + "<td>经手人</td>";
                            temptab = temptab + "<td>contact</td>";
                            temptab = temptab + "<td>" + item.contact + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>26</td>";
                            temptab = temptab + "<td>联系人ID</td>";
                            temptab = temptab + "<td>contactid</td>";
                            temptab = temptab + "<td>" + item.contactid + "</td>";
                            temptab = temptab + "</tr>";
                            temptab = temptab + "<tr>";
                            temptab = temptab + "<td>27</td>";
                            temptab = temptab + "<td>订单/服务明细ID</td>";
                            temptab = temptab + "<td>orderdetailid</td>";
                            temptab = temptab + "<td>" + item.orderdetailid + "</td>";
                            temptab = temptab + "</tr>";

                        });
                        temptab = temptab + "</tbody></table>";
                        console.log("第一个ajax加载完");
                        var ddv = $("#list").datagrid('getRowDetail', index).find('div.ddv');
                        ddv.panel({//把这个div 弄成面板                                 border:false,
                            content: "" + temptab + "",
                            onLoad: function () {
                                //$('#financeDetail').datagrid('fixDetailRowHeight',index); 
                            }
                        });
                        $('#list').datagrid('fixDetailRowHeight', index);
                    }
                });
                $('#list').datagrid('fixDetailRowHeight', index);
            },

        });
    },
    getSelectedRow: function () {
        return $('#list').datagrid('getSelected');
    },
    reload: function () {
        $('#list').datagrid('clearSelections').datagrid('reload', { filter: '' });
    }
};

function createParam(action, keyid) {
    var o = {};
    var query = top.$('#uiform').serializeArray();
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}


var CRUD = {
    add: function () {
        var hDialog = top.jQuery.hDialog({
            title: '添加', width: 1000, height: 800, href: formurl, iconCls: 'icon-add',
            onLoad: function () {
                top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
            },
            submit: function () {
                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate')) {
                    var query = createParam('add', '0');
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (parseInt(d) > 0) {
                            msg.ok('添加成功！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                }
                msg.warning('请填写必填项！');
                return false;
            }
        });

        top.$('#uiform').validate();
    },
    edit: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '编辑', width: 1000, height: 700, href: formurl, iconCls: 'icon-save',
                onLoad: function () {
                    top.$('#txt_KeyId').numberspinner('setValue', row.KeyId);
                    top.$('#txt_edNumber').textbox('setValue', row.edNumber);
                    top.$('#txt_unit').textbox('setValue', row.unit);
                    top.$('#txt_unitid').numberspinner('setValue', row.unitid);
                    top.$('#txt_contact').textbox('setValue', row.contact);
                    top.$('#txt_contactid').numberspinner('setValue', row.contactid);
                    top.$('#txt_dep').textbox('setValue', row.dep);
                    top.$('#txt_depid').numberspinner('setValue', row.depid);
                    top.$('#txt_note').textbox('setValue', row.note);
                    top.$('#txt_account').textbox('setValue', row.account);
                    top.$('#txt_accountid').numberspinner('setValue', row.accountid);
                    top.$('#txt_total').textbox('setValue', row.total);
                    top.$('#txt_payment').textbox('setValue', row.payment);
                    top.$('#txt_add_time').datetimebox('setValue', row.add_time);
                    top.$('#txt_up_time').datetimebox('setValue', row.up_time);
                    top.$('#txt_operate').textbox('setValue', row.operate);
                    top.$('#txt_operateid').numberspinner('setValue', row.operateid);
                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');    
                },
                submit: function () {
                    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                    //alert(top.$("#uiform").form('validate'));
                    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {
                        var query = createParam('edit', row.KeyId);;
                        jQuery.ajaxjson(actionURL, query, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    }
                    msg.warning('请填写必填项！');
                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

//实现动态隐藏列
var cmenu = null;
function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#list').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#list').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#list').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#list').datagrid('getColumnOption', field);
        cmenu.menu('appendItem', {
            text: col.title,
            name: field,
            iconCls: 'icon-ok'
        });
    }
}

//实现动态隐藏列结束

//双击编辑表单焦点消失后保存
var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#list').datagrid('validateRow', editIndex)) {
        $('#list').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function onDblClickCell(index, field) {
    if (editIndex != index) {
        if (endEditing()) {
            $('#list').datagrid('selectRow', index)
            $('#list').datagrid('beginEdit', index);
            var ed = $('#list').datagrid('getEditor', { index: index, field: field });
            if (ed) {
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                //这个写法很有意思
                //为了方便 ,类似字段要同事写两个值 1把部门id写入隐藏字段2把标题换成值写入 显示字段

            }
            editIndex = index;
        } else {
            setTimeout(function () {
                $('#list').datagrid('selectRow', editIndex);
            }, 0);
        }
    }
}

function onUnselect(index, row) {
    //alert(index);
}

function onCancelEdit(index, row) {
    //alert(index);
}
function onClickRow(index, row) {
    if (editIndex != undefined) {
        //alert("正在编辑的行是：" + editIndex);
        //alert("验证正在编辑的行：" + $('#list').datagrid('validateRow', editIndex));
        if ($('#list').datagrid('validateRow', editIndex)) {//如果验证正在编辑的行数据有效则
            $("#list").datagrid('endEdit', editIndex);//如果点其他行，结束编辑正在编辑的行
            editIndex = undefined;
        } else {
            msg.warning("还有一行没编辑完啦！！");
        }
    }
    $("#list").datagrid('selectRow', index);


}

function onSelect(index, row) {

}

function onEndEdit(index, row, changes) {
    //alert('结束编辑'+index+JSON.stringify(row));
    var o = {};
    var query = JSON.stringify(row);
    o.jsonEntity = query;
    o.action = 'edit';
    o.keyid = row.KeyId;
    query = "json=" + JSON.stringify(o);
    //alert(query);
    //表格内编辑模式编辑成功不提示信息
    jQuery.ajaxjson(actionURL, query, function (d) {
        //if (parseInt(d) > 0) {
        //    msg.ok('编辑成功！');
        //    hDialog.dialog('close');
        grid.reload();
        // } else {
        //     MessageOrRedirect(d);
        // }
    });

}
function append() {
    if (endEditing()) {
        $('#list').datagrid('appendRow', { status: 'P' });
        editIndex = $('#list').datagrid('getRows').length - 1;
        $('#list').datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
    }
}
function removeit() {
    if (editIndex == undefined) { return }
    $('#list').datagrid('cancelEdit', editIndex)
        .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept() {
    if (endEditing()) {
        $('#list').datagrid('acceptChanges');
    }
}
function reject() {
    $('#list').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges() {
    var rows = $('#list').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}
//双击编辑表单焦点消失后保存结束

//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var myunit = $("#myUnit").textbox('getValue');
    var connman = $("#myConnMan").textbox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    var i = 0;
    if (myunit != '' && myunit != undefined) {//假如单位搜索不为空
        o.rules[i] = JSON.parse('{"field":"unit","op":"cn","data":"' + myunit + '"}');
        i = i + 1;
    }
    if (connman != '' & connman != undefined) {//联系人不为空
        o.rules[i] = JSON.parse('{"field":"connman","op":"cn","data":"' + connman + '"}');
    }

    $('#list').datagrid('reload', { filter: JSON.stringify(o) });


}


//自定义搜索结束


//单选多选开关
$('#selectSwitch').switchbutton({
    checked: false,
    onChange: function (checked) {
        if (checked) {
            $('#list').datagrid({ singleSelect: false });//多选
        } else {
            $('#list').datagrid({ singleSelect: true });//单选
        }
    }
})



//配送打印
function printpz(edNumber) {
    //alert(murl);
    var dialog = $.dialog({
        title: '打印记账凭证',
        content: 'url:' + '/mjprint/printpz.aspx?edNumber=' + edNumber,
        min: false,
        max: false,
        lock: true,
        width: 850,
        height: 550
    });
}



function printWin(edNumber) {
    
    var dialog = $.dialog({
        title: '打印记账凭证明细',
        content: 'url:' + '/mjprint/Detailed.aspx?edNumber=' + edNumber,
        min: false,
        max: false,
        lock: true,
        width: 850,
        height: 550
    });
}


