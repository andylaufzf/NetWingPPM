﻿
$(document).ready(function () {
    $('#dg').datagrid({
        //url: 'datagrid_data.json',
        singleSelect: true,//只能选择一行
        toolbar: [{
            iconCls: 'icon-save',
            text: '保存',
            handler: function () {
                var isValid = $("#form1").form('validate');//验证表单是否符合验证格式
                if (isValid) {

                } else {
                    $.messager.alert('提醒', '表格没有填写完整');
                    return false;//返回false 程序不执行
                }

                //得到正在编辑的所有行
                var k = $('#dg').datagrid('getEditingRowIndexs');
                for (var i = 0; i < k.length; i++) {
                    var olded = $('#dg').datagrid('getEditor', { index: k[i], field: 'ftype' });
                    var newed = $('#dg').datagrid('getEditor', { index: k[i], field: 'sumMoney' });
                    var nv = $(newed.target).textbox('getValue');
                    var ov = $(olded.target).textbox('getValue');//取得支出类型
                    if (ov == '支出') {//如果收支类型是支出
                        if (parseFloat(nv) > 0) {//要判断如果是正数才修改，如果事负数就不管了
                            $(newed.target).textbox('setValue', '-' + nv);//如果是支出，则把值设为负数
                        }

                    }

                }

                //asp.net 自动生成了一个form1 所以这里验证表单时有个#form1
                $('#dg').datagrid('acceptChanges');
                var rows = $('#dg').datagrid('getRows');//获取当前的数据行
                var total = 0//计算sumMoney的总和
                for (var i = 0; i < rows.length; i++) {
                    //alert(rows[i]['sumMoney']);
                    total += parseFloat(rows[i]['sumMoney']);
                }
                total = total.toFixed(2);//保留小数点两位注意是字符哦  
                $('#payment').textbox({ value: total });//加金额给实付金额
                $('#total').textbox({ value: 0 });// 财务记账不管合计金额(也就是应付金额)
            }
        }],
        rowStyler: function (index, row) {
            if (row.ftype == '支出') {
                return 'background-color:#6293BB;color:#fff;';    // rowStyle是一个已经定义了的ClassName(类名)
            }
        },
        rownumbers: true, //行号
        columns: [[
            { title: 'ID', field: 'KeyId', sortable: true, width: '', hidden: true, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '主表ID', field: 'financeId', sortable: true, editor: 'numberspinner', width: '', hidden: true },//主键自动判断隐藏
            { title: '编号', field: 'edNumber', sortable: true, width: '', hidden: true, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
            {
                title: '科目', field: 'subject', sortable: true, width: '150', editor: {
                    type: 'combobox', options: {
                        required: true, validType: 'length[0,100]', url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJaccountingSubjects&field=subjectName&wherestr=caiwu', textField: 'subjectName',
                        valueField: 'subjectName', missingMessage: '',
                        onHidePanel : function () {
                            var _options = $(this).combobox('options');
                            var _data = $(this).combobox('getData');/* 下拉框所有选项 */
                            var _value = $(this).combobox('getValue');/* 用户输入的值 */
                            var _b = false;/* 标识是否在下拉列表中找到了用户输入的字符 */
                            for (var i = 0; i < _data.length; i++) {
                                if (_data[i][_options.valueField] == _value) {
                                    _b = true;
                                    break;
                                }
                            }
                            if (!_b) {
                                $.messager.alert('警告', '只能从下拉列表中选择值！');   
                                $(this).combobox('setValue', '');
                            }
                        },  
                    }
                }
            },
            { title: '收支类型', field: 'ftype', sortable: true, width: '100', editor: { type: 'textbox', options: { required: true, validType: 'length[0,100]', readonly: true, missingMessage: '类型不能为空' } } },
            { title: '科目ID', field: 'subjectid', sortable: true, editor: 'numberspinner', width: '', hidden: true },//主键自动判断隐藏
            {
                title: '金额', field: 'sumMoney', sortable: true, width: '100', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'moneyAll', missingMessage: '金额格式不对', invalidMessage: "金额格式不对" } },

            },
            //{ title: '部门', field: 'dep', sortable: true, width: '200', editor: { type: 'combotree', options: { required: true, validType: 'length[0,100]', missingMessage: '不能为空' } } },
            //{ title: '部门ID', field: 'depid', sortable: true, editor: 'numberspinner', width: '', hidden: true },//主键自动判断隐藏
            { title: '备注', field: 'note', sortable: true, width: '200', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '添加时间', field: 'add_time', sortable: true, editor: 'datetimebox', width: '', hidden: true },//主键自动判断隐藏
            { title: '更新时间', field: 'up_time', sortable: true, editor: 'datetimebox', width: '', hidden: true },//主键自动判断隐藏
        ]],
        data: [//先整10个空表格出来
            { KeyId: '', financeId: '', edNumber: '', ftype: '', subject: '', subjectid: '', sumMoney: 0, dep: '', depid: '', note: '', add_time: '', up_time: '' },
            { KeyId: '', financeId: '', edNumber: '', ftype: '', subject: '', subjectid: '', sumMoney: 0, dep: '', depid: '', note: '', add_time: '', up_time: '' },
            { KeyId: '', financeId: '', edNumber: '', ftype: '', subject: '', subjectid: '', sumMoney: 0, dep: '', depid: '', note: '', add_time: '', up_time: '' },
            { KeyId: '', financeId: '', edNumber: '', ftype: '', subject: '', subjectid: '', sumMoney: 0, dep: '', depid: '', note: '', add_time: '', up_time: '' },
            { KeyId: '', financeId: '', edNumber: '', ftype: '', subject: '', subjectid: '', sumMoney: 0, dep: '', depid: '', note: '', add_time: '', up_time: '' },
            { KeyId: '', financeId: '', edNumber: '', ftype: '', subject: '', subjectid: '', sumMoney: 0, dep: '', depid: '', note: '', add_time: '', up_time: '' },
            { KeyId: '', financeId: '', edNumber: '', ftype: '', subject: '', subjectid: '', sumMoney: 0, dep: '', depid: '', note: '', add_time: '', up_time: '' },
            { KeyId: '', financeId: '', edNumber: '', ftype: '', subject: '', subjectid: '', sumMoney: 0, dep: '', depid: '', note: '', add_time: '', up_time: '' },
            { KeyId: '', financeId: '', edNumber: '', ftype: '', subject: '', subjectid: '', sumMoney: 0, dep: '', depid: '', note: '', add_time: '', up_time: '' },
            { KeyId: '', financeId: '', edNumber: '', ftype: '', subject: '', subjectid: '', sumMoney: 0, dep: '', depid: '', note: '', add_time: '', up_time: '' },
        ],
        onClickCell: function (index, field, value) { //双击可以修改
            //alert($('#dg').form('validate'));
            //$('#dg').datagrid('beginEdit', index);
            //if (field == 'subject') {//如果当前选择的是科目名称
            //    var ed = $('#dg').datagrid('getEditor', { index: index, field: 'subject' });//找到行editor
            //    $(ed.target).combobox({
            //        onSelect: function (rec) {//子节点选择时的动作
            //            var val = $(ed.target).combobox('getText');//得到combogrid的值
            //            var edb = $('#dg').datagrid('getEditor', { index: index, field: 'subjectid' });//找到行editor 找到第一个列文本框编辑器
            //            $(edb.target).textbox('setText', val);//设置文本框
            //            var ft = $('#dg').datagrid('getEditor', { index: index, field: 'ftype' });//得到收支类型
            //            $(ft.target).textbox('setValue', rec.subjectType);//设置收支类型

            //        }
            //    });//获得当前选择的值
            //}



        },
        onDblClickRow: function (index, row) {//单击row
            $('#dg').datagrid('beginEdit', index);
            var ed = $('#dg').datagrid('getEditor', { index: index, field: 'subject' });//找到行editor
            $(ed.target).combobox({
                onSelect: function (rec) {//子节点选择时的动作
                    var val = $(ed.target).combobox('getText');//得到combogrid的值
                    var edb = $('#dg').datagrid('getEditor', { index: index, field: 'subjectid' });//找到行editor 找到第一个列文本框编辑器
                    $(edb.target).textbox('setText', val);//设置文本框
                    var ft = $('#dg').datagrid('getEditor', { index: index, field: 'ftype' });//得到收支类型
                    $(ft.target).textbox('setValue', rec.subjectType);//设置收支类型
                    var subjectided = $('#dg').datagrid('getEditor', { index: index, field: 'subjectid' });//得到科目id编辑器
                    $(subjectided.target).textbox('setValue', rec.KeyId);


                }
            });//获得当前选择的值

            //var deped = $('#dg').datagrid('getEditor', { index: index, field: 'dep' });//得到部门ed
            //$(deped.target).combotree({
            //    required: true,
            //    validType: 'length[0,100]',
            //    url: '/sys/ashx/userhandler.ashx?json={"jsonEntity":"{}","action":"deps"}',
            //    textField: 'Title',
            //    onSelect: function (node) {
            //        var ed = $('#dg').datagrid('getEditor', { index: index, field: 'depid' });
            //        $(ed.target).numberspinner('setValue', node.id); node.id = node.text;
            //    },
            //    missingMessage: ''

            //});


        }

    });

    loadClient();//加载用户信息

});
//提交表单方法s
$("#submit").linkbutton({
    onClick: function () {//这里绑定的是easyui linkbutton事件
        submit();
    }
});

function submit() {
    $('#form1').form('submit', {
        url: '/mjfinancemain/ashx/mjfinance.ashx',
        onSubmit: function (param) {
            var isValid = $("#form1").form('validate');//验证表单是否符合验证格式
            if (isValid == false) {
                $.messager.alert('警告', '表单还有信息没有填完整！');
                return false;
                //$.messager.progress('close');	// 如果表单是无效的则隐藏进度条
            } else {//表单验证通过
                $('#dg').datagrid('acceptChanges');//datagrid接受改变
                if ($('#payment').numberbox('getValue') == '') {
                    //alert("请先保存了再提交");
                    $.messager.alert('警告', '请先保存再提交!');
                    return false;
                }



                $('#dg').datagrid({ singleSelect: false });//设置允许多选
                //循环选择行 只有选择行了之后才能返回选中行
                var rows = $('#dg').datagrid('getRows');//获取当前的数据行
                for (var i = 0; i < rows.length; i++) {//循环所有列
                    //不允许为负数 if (rows[i]['subjectName'] != '' && rows[i]['sumMoney'] != 0) {
                    if (rows[i]['subject'] != '') {
                        $('#dg').datagrid('selectRow', i);
                        //alert(rows[i]['subject']);
                    }
                    else {
                        //alert("有项目等于0");
                    }
                }
                var rows = $('#dg').datagrid('getSelections');//返回选中的行
                if (rows == '') {//判断是否返回空数组 说明没有填写数据
                    //alert("空数组");
                    $.messager.alert('注意', '请填写或保存详细数据!');
                    return false;//返回失败
                } else {
                    //alert(JSON.stringify(rows));//有数据返回数据
                }
                param.main = $("#form1").serializeJSON();  //jquery方法把表单系列化成json后提交
                //以上用法参考https://github.com/macek/jquery-serialize-object
                param.detail = JSON.stringify(rows);//param  是EasyUI格式详细表序列化成json 
                //alert(param.main);
                //return false;
                return true;
            }
        },
        success: function (d) {
            var d = eval('(' + d + ')');  // change the JSON string to javascript object    
            if (d.status == 1) {
                //$.messager.alert('恭喜', d.msg);
                //location = location;//刷新本地网址
                $.messager.confirm('恭喜', d.msg, function (r) {
                    if (r) {//是否确认都关闭进货单录入窗口
                        top.$('#tabs').tabs('close', '财务记账');//进货单录入窗口
                    } else {
                        top.$('#tabs').tabs('close', '财务记账');//进货单录入窗口
                    }
                }); 

            } else {
                $.messager.alert('对不起，提交失败!',d.msg);
            }

            //
            //alert("成功动作");
            //$.messager.progress('close');	// 如果提交成功则隐藏进度条
        }
    });
}

//提交表单方法e

/*
*  datagrid 获取正在编辑状态的行，使用如下：
*  $('#id').datagrid('getEditingRowIndexs'); //获取当前datagrid中在编辑状态的行编号列表
*/
$.extend($.fn.datagrid.methods, {
    getEditingRowIndexs: function (jq) {
        var rows = $.data(jq[0], "datagrid").panel.find('.datagrid-row-editing');
        var indexs = [];
        rows.each(function (i, row) {
            var index = row.sectionRowIndex;
            if (indexs.indexOf(index) == -1) {
                indexs.push(index);
            }
        });
        return indexs;
    }
});


//加载客户信息
function loadClient() {
    //客户信息
    //这里手动填写故此隐藏
    //$("#unit").combogrid({
    //    panelWidth: 350,
    //    required: true,
    //    //value:'fullname',   
    //    idField: 'fullname',
    //    textField: 'fullName',
    //    url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJUser&field=fullname',
    //    columns: [[
    //        { field: 'KeyId', title: 'KeyId', width: 60, hidden: true },
    //        { field: 'fullname', title: '姓名', width: 60 },
    //        { field: 'tel', title: '电话', width: 100 },
    //        { field: 'sex', title: '性别', width: 40 },
    //        { field: 'email', title: 'email', width: 100 }
    //    ]],
    //    onSelect: function (rowIndex, rowData) {
    //        $("#unitid").val(rowData.KeyId);//给隐藏的客户ID赋值
    //    }

    //});
    //经手人
    //经手人手动填写故此隐藏
    $("#contact").combogrid({
        panelWidth: 350,
        required: true,
        //value:'fullname',   
        idField: 'TrueName',
        textField: 'TrueName',
        url: '/sys/ashx/userhandler.ashx?action=mydep',
        columns: [[
            { field: 'TrueName', title: '姓名', width: 60 },
            { field: 'KeyId', title: 'KeyId', width: 100 }
        ]],
        onSelect: function (rowIndex, rowData) {
            $("#contactid").val(rowData.KeyId);//给经手人设置ID
            //alert(rowData.KeyId);
        }
    });
    //部门
    $("#dep").combotree({
        url: '/sys/ashx/userhandler.ashx?action=deps',
        required: true,
        onSelect: function (node) {
            $("#depid").val(node.id);//设置隐藏部门id
            node.id = node.text;//把text赋值给id用

        }
    });

    //银行
    $("#bank").combobox({
        valueField: 'accountName',//用中文名作为名称
        required: true,
        textField: 'accountName',
        url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJBankAccount&field=accountName',
        onSelect: function (rec) {
            $("#bankid").val(rec.KeyId);//设置隐藏ID
        }
    });

}


//往来单位
//初始化后加工工厂
//var gys = '{ "groupOp":"AND", "rules": [{ "field":"fenlei", "op":"cn", "data":"供应商"}], "groups": [] }';
$("#unit").combogrid({
    delay: 500, //自动完成功能实现搜索
    mode: 'remote',//开启后系统会自动传一个参数q到后台 
    panelWidth: 350,
    required: true,
    queryParams: {
        //filter: gys
    },
    //value:'fullname',   
    editable: true,
    idField: 'comname',
    //textField: 'comname',
    url: '/JydUser/ashx/JydUserHandler.ashx',
    columns: [[
        { field: 'KeyId', title: 'KeyId', width: 50 },
        { field: 'comname', title: '客户', width: 120 },
        { field: 'tell', title: '电话', width: 120 }

    ]],
    limitToList: true,//只能从下拉中选择值
    //reversed: true,//定义在失去焦点的时候是否恢复原始值。
    onHidePanel: function () {
        var t = $(this).combogrid('getValue');//获取combogrid的值
        var g = $(this).combogrid('grid');	// 获取数据表格对象
        var r = g.datagrid('getSelected');	// 获取选择的行
        console.log("选择的行是：" + r + "选择的值是:" + t);
        if (r == null || t != r.comname) {//没有选择或者选项不相等时清除内容
            $.messager.alert('警告', '请选择，不要直接输入!');
            $(this).combogrid('setValue', '');
        } else {
            //do something...
        }
    },
    onSelect: function (rowIndex, rowData) {
        //console.log(JSON.stringify(rowData));
        console.log("设置的值是：" + rowData.KeyId);
        //$("#unitid").combogrid("setValue",rowData.KeyId);//给经手人设置ID
        $("#unitid").val(rowData.KeyId);//客户
        $("#unit_man").textbox("setValue", rowData.realname);//设置联系人
        $("#unit_moble").textbox("setValue", rowData.tell);//设置联系联系电话       //top.$("#connman").textbox('setValue', rowData.realname);//经手人
        //top.$("#tel").textbox('setValue', rowData.tell)
        //alert(rowData.TrueName);
    }
});



$.extend($.fn.validatebox.defaults.rules, {
    comboxValidate: {
        validator: function (value, param, missingMessage) {
            if ($('#' + param).combobox('getValue') != '' && $('#' + param).combobox('getValue') != null) {
                return true;
            }
            return false;
        },
        message: "{1}"
    }
});  
//使用方法
//<select class="easyui-combobox" id="cc" validType="comboxValidate['cc','请选择状态']">
//    <option value="">请选择</option>
//   <option value="1">bitem1</option>
//    <option value="2">ditem2</option>
//</select>

//validType = "comboxValidate['cc','请选择状态']" ： 第一个参数（CC）表示要验证的组件ID，第二个参数表示验证的提示信息
//class="easyui-combobox"：标示这是一个easyui combox元素
//以上二个配置缺一不可