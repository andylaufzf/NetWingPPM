﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="mjfinance.aspx.cs" Inherits="NetWing.BPM.Admin.mjfinance.mjfinance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->



    <!-- 工具栏按钮 -->


    <!-- datagrid 列表 -->
    <div id="tt" class="easyui-tabs" style="width: ; height: ;">
        <div title="财务记账" style="padding: 20px; display: none;">
            客户：<select id="unit" class="" name="unit"  style="width: 100px;"></select>
            <input id="unitid" name="unitid"  data-options="" type="hidden" value="" style="width:100px"> 
            经手人：<select id="contact" class="easyui-textbox" name="contact" data-options="required:true,validType:''" style="width: 100px;"></select>
            <input id="contactid" name="contactid"  type="hidden" value="" style="width:100px"> 
           <!--隐藏不需要的东西 部门：<select id="dep" name="dep" class="easyui-combotree" style="width: 150px;"></select>
            <input id="depid" name="depid"  type="hidden" value="" style="width:100px"> 
            单号：<input id="orderid" name="edNumber" class="easyui-textbox" value="<%=edNumber%>" data-options="readonly:true" style="width: 120px">
            -->
            <br />
            <br />
            摘要：<input id="note" name="note" class="easyui-textbox" data-options="" style="width: 300px">
            <br />
            <br />

            <table id="dg"></table>
            <br />
            银行账户：<input id="bank"  style="width: 150px" class="easyui-combobox" name="account"/>
            <input id="bankid" name="accountid"  type="hidden" value="" style="width:100px"> 
           <div style="display:none;">合计金额：<input id="total" name="total" readonly="readonly" class="easyui-textbox" data-options="" style="width:100px"> </div>
            实付金额：<input id="payment" name="payment" class="easyui-textbox" data-options="required: false" style="width:100px"> 
            <a id="submit" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'">提交</a>  
            <br />
            <br />
             小贴士:收入录入“正”数，支出请录入“负”数。财务不支持删除或修改。录入错误，请录一笔“反”的记录冲销。并做详细备注。
            <br />

        </div>

    </div>


    <!--Uploader-->
    <!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
    <!--导出Excel-->
    <script src="../../scripts/Export.js"></script>
    <script src="js/finance.js"></script>
    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>



</asp:Content>



