﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="mjfinanceInfo.aspx.cs" Inherits="NetWing.BPM.Admin.mjfinancemain.mjfinanceInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
        
    </div>-->
    



    <!-- 工具栏按钮 -->
    <div id="toolbar">
        <%= base.BuildToolbar()%>
        <a id="a_printWin" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="打印">打印</a>
        <a href="#" plain="true" class="easyui-linkbutton" title="模式">模式</a><input id="selectSwitch" class="easyui-switchbutton" data-options="onText:'多选',offText:'单选'">
    </div>

    <!-- datagrid 列表 -->
    <table id="list"></table>


    <!--Uploader-->
    <!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
    <!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>
    <!--引入-->
    <script src="../scripts/easyui/datagrid-detailview.js"></script>

    <!-- 引入js文件 -->
    <script src="js/mjfinanceinfo.js"></script>
    <!--引入lhgdialog插件-->
    <script src="../scripts/lhgdialog/lhgdialog.min.js?skin=idialog"></script>
</asp:Content>
