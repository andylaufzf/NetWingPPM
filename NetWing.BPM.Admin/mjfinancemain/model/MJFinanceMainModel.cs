using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
    [TableName("MJFinanceMain")]
    [Description("财务主表")]
    public class MJFinanceMainModel
    {
        /// <summary>
        /// 系统编号
        /// </summary>
        [Description("系统编号")]
        public int KeyId { get; set; }
        /// <summary>
        /// 编号
        /// </summary>
        [Description("编号")]
        public string edNumber { get; set; }
        /// <summary>
        /// 客户
        /// </summary>
        [Description("客户")]
        public string unit { get; set; }
        /// <summary>
        /// 客户ID
        /// </summary>
        [Description("客户ID")]
        public int unitid { get; set; }
        /// <summary>
        /// 经手人
        /// </summary>
        [Description("经手人")]
        public string contact { get; set; }
        /// <summary>
        /// 经手人ID
        /// </summary>
        [Description("经手人ID")]
        public int contactid { get; set; }
        /// <summary>
        /// 部门
        /// </summary>
        [Description("部门")]
        public string dep { get; set; }
        /// <summary>
        /// 部门ID
        /// </summary>
        [Description("部门ID")]
        public int depid { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public string note { get; set; }
        /// <summary>
        /// 关联账户
        /// </summary>
        [Description("关联账户")]
        public string account { get; set; }
        /// <summary>
        /// 关联账户ID
        /// </summary>
        [Description("关联账户ID")]
        public int accountid { get; set; }



        /// <summary>
        /// 关联账户
        /// </summary>
        [Description("关联账户")]
        public string accountb { get; set; }
        /// <summary>
        /// 关联账户ID
        /// </summary>
        [Description("关联账户ID")]
        public int accountidb { get; set; }


        /// <summary>
        /// 关联账户c
        /// </summary>
        [Description("关联账户")]
        public string accountc { get; set; }
        /// <summary>
        /// 关联账户IDc
        /// </summary>
        [Description("关联账户IDc")]
        public int accountidc { get; set; }

        /// <summary>
        /// 关联账户d
        /// </summary>
        [Description("关联账户")]
        public string accountd { get; set; }
        /// <summary>
        /// 关联账户IDc
        /// </summary>
        [Description("关联账户IDc")]
        public int accountidd { get; set; }

        [Description("实付金额b")]
        public decimal shifub { get; set; }
        [Description("实付金额c")]
        public decimal shifuc { get; set; }
        [Description("实付金额d")]
        public decimal shifud { get; set; }


        /// <summary>
        /// 金额合计
        /// </summary>
        [Description("金额合计")]
        public decimal total { get; set; }
        /// <summary>
        ///  实付金额
        /// </summary>
        [Description(" 实付金额")]
        public decimal payment { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        [Description("添加时间")]
        public DateTime add_time { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Description("更新时间")]
        public DateTime up_time { get; set; }
        /// <summary>
        /// 操作人
        /// </summary>
        [Description("操作人")]
        public string operate { get; set; }
        /// <summary>
        /// 操作人ID
        /// </summary>
        [Description("操作人ID")]
        public int operateid { get; set; }
        /// <summary>
        /// 数据所有者ID
        /// </summary>
        [Description("数据所有者ID")]
        public int ownner { get; set; }

        /// <summary>
        /// 付款方式：年付 月付 季付
        /// </summary>
        [Description("付款方式")]
        public string paytype { get; set; }

        [Description("下次付款日期")]
        public DateTime nextpaydate { get; set; }

        [Description("房间id")]
        public int roomid { get; set; }
        [Description("房间号")]
        public string roomno { get; set; }
        [Description("订单编号")]
        public int orderid { get; set; }

        public override string ToString()
        {
            return JSONhelper.ToJson(this);
        }
    }
}