using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;

namespace NetWing.BPM.Admin.mjfinance
{
    public partial class mjfinance : NetWing.BPM.Core.BasePage.BpmBasePage
    {
        public string mytitle = "";//收入单标题
        public string otype = "";//收支单据类型
        public string edNumber = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Request["type"] == "IN")//收入
            {
                mytitle = "网翼收入单";
                otype = "IN";

            }
            else if (Request["type"] == "OUT")
            {
                mytitle = "翼通支出单";
                otype = "OUT";
            }
            else if (string.IsNullOrEmpty(Request["type"]))
            {
                mytitle = "翼通支出单";
                otype = "F";
            }
            edNumber = getedNumber(otype);
        }
        public static string getedNumber(string type)
        {
            string edNumber = "";
            SqlDataReader dr = SqlEasy.ExecuteDataReader("SELECT   edNumber  FROM  MJFinanceMain where CONVERT(varchar(100),add_time, 23)='" + DateTime.Now.ToString("yyyy-MM-dd") + "' and edNumber like '" + type + "%' order by keyid desc");
            if (dr.Read())
            {
                string ovl = dr["edNumber"].ToString();
                string[] ovlArr = ovl.Split('-');
                ovl = ovlArr[2].ToString();
                edNumber = type + "-" + DateTime.Now.ToString("yyyyMMdd") + "-" + (Convert.ToInt16(ovl) + 1).ToString("D3");
            }
            else
            {
                edNumber = type + "-" + DateTime.Now.ToString("yyyyMMdd") + "-" + "001";
            }
            dr.Close();
            return edNumber;
        }
    }
}