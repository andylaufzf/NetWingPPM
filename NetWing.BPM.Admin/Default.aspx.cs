﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Model;
using NetWing.Common;
using NetWing.Utility;

namespace NetWing.BPM.Admin
{
    public partial class Default : NetWing.BPM.Core.BasePage.BpmBasePage
    {
        public string appName=Global.appName;
        public string copyRight = Global.copyRight;//版权信息
        public string mainUrl = Global.mainUrl;//主站连接
        protected string NavContent = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string configData = SysVisitor.Instance.CurrentUser.ConfigJson;

            string themePath = Server.MapPath("theme/navtype/");
            NVelocityHelper vel = new NVelocityHelper(themePath);
            string showTopDepName = NetWingHelper.returnDep(UserName, "s");
            vel.Put("username", UserName + "&nbsp;&nbsp;" + showTopDepName);
           string navHTML = "Accordion.html";
            if (!string.IsNullOrEmpty(configData))
            {
                ConfigModel sysconfig = JSONhelper.ConvertToObject<ConfigModel>(configData);
                if (sysconfig != null)
                {

                    switch (sysconfig.ShowType)
                    {
                        case "menubutton":
                            navHTML = "menubutton.html";
                            break;
                        case "tree":
                            navHTML = "tree.html";
                            break;
                        case "menuAccordion":
                        case "menuAccordion2":
                        case "menuAccordionTree":
                            navHTML = "topandleft.html";
                            break;
                        default:
                            navHTML = "Accordion.html";
                            break;

                    }

                }
            }

            NavContent = vel.FileToString(navHTML);
        }
    }
}