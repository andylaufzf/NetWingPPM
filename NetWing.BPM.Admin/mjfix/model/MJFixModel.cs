using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJFix")]
	[Description("报修管理")]
	public class MJFixModel
	{
				/// <summary>
		/// 系统ID
		/// </summary>
		[Description("系统ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 房间ID
		/// </summary>
		[Description("房间ID")]
		public int roomid { get; set; }		
		/// <summary>
		/// 房间号
		/// </summary>
		[Description("房间号")]
		public string roomno { get; set; }		
		/// <summary>
		/// 用户ID
		/// </summary>
		[Description("用户ID")]
		public int userid { get; set; }		
		/// <summary>
		/// 报修人
		/// </summary>
		[Description("报修人")]
		public string username { get; set; }		
		/// <summary>
		/// 手机号
		/// </summary>
		[Description("手机号")]
		public string mobile { get; set; }		
		/// <summary>
		/// 报修问题
		/// </summary>
		[Description("报修问题")]
		public string wenti { get; set; }		
		/// <summary>
		/// 处理状态
		/// </summary>
		[Description("处理状态")]
		public string status { get; set; }		
		/// <summary>
		/// 报修时间
		/// </summary>
		[Description("报修时间")]
		public DateTime add_time { get; set; }		
		/// <summary>
		/// 更新时间
		/// </summary>
		[Description("更新时间")]
		public DateTime up_time { get; set; }		
		/// <summary>
		/// 完成时间
		/// </summary>
		[Description("完成时间")]
		public DateTime fin_time { get; set; }		
		/// <summary>
		/// 数据权限
		/// </summary>
		[Description("数据权限")]
		public int depid { get; set; }		
		/// <summary>
		/// 数据所有者
		/// </summary>
		[Description("数据所有者")]
		public int ownner { get; set; }
        /// <summary>
        /// 处理人
        /// </summary>
        [Description("处理人")]
        public string clr { get; set; }




        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}