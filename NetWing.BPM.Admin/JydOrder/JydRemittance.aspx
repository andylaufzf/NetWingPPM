﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JydRemittance.aspx.cs" Inherits="NetWing.BPM.Admin.JydOrder.JydRemittance" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>汇总表</title>
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <!-- 引入js -->
    <script src="js/JydRemittance.js"></script>
    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
    <!--导出Excel-->
    <script src="../../scripts/Export.js"></script>
    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>
    <!--打印-->
    <style>
        @page {
            size: landscape;
        }
        /*纵向打印*/
        /*@page { size: portrait; }*/
    </style>
</head>
<body>
    <script type="text/javascript">

        function printdiv(printpage) {

            var status = confirm("打印的时候请注意,已作废的订单是不进行计算的!!!");
            if (!status) {
                return false;
            }
            var headhtml = "<html><head><title></title></head><body>";
            var foothtml = "</body>";



            // 获取div中的html内容
            var newhtml = document.all.item(printpage).innerHTML;
            // 获取div中的html内容，jquery写法如下
            // var newhtml= $("#" + printpage).html();

            // 获取原来的窗口界面body的html内容，并保存起来
            var oldhtml = document.body.innerHTML;

            // 给窗口界面重新赋值，赋自己拼接起来的html内容
            document.body.innerHTML = headhtml + newhtml + foothtml;
            // 调用window.print方法打印新窗口
            window.print();

            // 将原来窗口body的html值回填展示
            document.body.innerHTML = oldhtml;
            return false;
        }

    </script>


    <form action="?" method="get">
        <div id="toolber">

            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>搜索条件</legend>
            </fieldset>

            <div class="layui-form">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">客户名称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="comname" value="<%=Request["comname"] %>" id="comname" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">客户简拼</label>
                        <div class="layui-input-inline">
                            <input type="text" name="jpm" value="<%=Request["jpm"] %>" id="jpm" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">订单号</label>
                        <div class="layui-input-inline">
                            <input type="text" name="orderid" value="<%=Request["orderid"] %>" id="orderid" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">业务员</label>
                        <div class="layui-input-inline">
                            <input type="text" name="ywy" value="<%=Request["ywy"] %>" id="ywy" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">收款情况</label>
                        <div class="layui-input-inline">
                            <select name="skqk" lay-verify="required" lay-search="">
                                <option value="">全部</option>
                                <option value="1" <%if (Request["skqk"] == "1") Response.Write("selected"); %>>全单收款</option>
                                <option value="2" <%if (Request["skqk"] == "2") Response.Write("selected"); %>>部分收款</option>
                                <option value="3" <%if (Request["skqk"] == "3") Response.Write("selected"); %>>未收款</option>

                            </select>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">状态</label>
                        <div class="layui-input-inline">
                            <select name="status" lay-verify="required" lay-search="">
                                <option value="">全部</option>
                                <option value="4" <%if (Request["status"] == "4") Response.Write("selected"); %>>作废</option>
                                <option value="999" <%if (Request["status"] == "999") Response.Write("selected"); %>>正常</option>

                            </select>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">网点</label>
                        <div class="layui-input-inline">
                            <select name="dept" lay-verify="required" lay-search="">
                                <option value="">全部</option>
                                <option value="1" <%if (Request["dept"] == "1") Response.Write("selected"); %>>总公司</option>
                                <option value="5" <%if (Request["dept"] == "5") Response.Write("selected"); %>>昆明盘亘商贸</option>
                            </select>
                        </div>
                    </div>

                    <div class="layui-inline">
                        <label class="layui-form-label">下单日期</label>
                        <div class="layui-input-inline">
                            <input type="text" value="<%=Request["xdrq"] %>" class="layui-input" name="xdrq" id="xdrq" readonly placeholder=" - ">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">交货日期</label>
                        <div class="layui-input-inline">
                            <input type="text" class="layui-input" value="<%=Request["jhrq"] %>" name="jhrq" id="jhrq" readonly placeholder=" - ">
                        </div>
                    </div>




                    <div class="layui-inline">
                        <div class="layui-input-inline">
                            <input type="submit" class="layui-btn" value="搜索" />
                        </div>
                        <div class="layui-input-inline">
                            <button class="layui-btn" onclick="printdiv('print');">生成报表</button>
                        </div>
                    </div>



                </div>
            </div>


        </div>
    </form>
    <form id="form1" runat="server">
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card"></div>
                    <div class="layui-card-body">
                        <%int show = 1;//只显示一次span 
                            int show1 = 1;
                            int tempOldDetKeyId = 0;
                            //新思路，
                            //1只查询一次数据库返回数据表DT
                            //2对dt进行group by 分组放回当前主订单号
                            //3判断主订单号下面有几个子订单 做rowspan判断
                            //4根据返回的DT 查询子订单并循环子订单

                            DataTable alldt = dt.Copy();//定义一个原始的datatable 防止某些查询会改变数据结构
                            DataTable maindt = GetDistinctSelf(dt, "mainkeyid");
                            //完全不明白这个去重是如何执行的,就这样
                            //Response.Write("过滤后的订单："+JSONhelper.FormatJSONForEasyuiDataGrid(maindt.Rows.Count, maindt));
                            // Response.Write("<br/>全部混合订单："+JSONhelper.FormatJSONForEasyuiDataGrid(alldt.Rows.Count, alldt));
                        %>
                        <% Double c, a, b; %>
                        <!--lay-data="{cellMinWidth: 80,size:'sm',toolbar: true,defaultToolbar: ['print', 'exports'],totalRow:true,title:'<%=total %>'}" -->
                        <center><div class="layui-box layui-laypage layui-laypage-default"><span>共<%=total %>记录</span><span>共<%=allpage %>页</span><span>每页<%=pagesize %>条记录</span><a href="?page=1<%=searchUrl %>">第一页</a><a href="?page=<%=firstpage %><%=searchUrl %>">«上一页</a><%=mdpage %><a href="?page=<%=nextpage %><%=searchUrl %>">下一页»</a><a href="?page=<%=allpage %><%=searchUrl %>">最后一页</a></div></center>
                        <table border="1" class="layui-table" id="grid" lay-filter="datatable">
                            <thead>
                                <tr>
                                    <td data-field="序号">序号</td>
                                    <td data-field="客户名称">客户名称</td>
                                    <td data-field="接件单号">接件单号</td>
                                    <td data-field="印刷品名称">印刷品名称</td>
                                    <td data-field="数量">数量</td>
                                    <td data-field="单价">单价</td>
                                    <td data-field="金额">金额</td>
                                    <td data-field="小计">小计</td>
                                    <td data-field="预付款">预付款</td>
                                    <td data-field="欠款">欠款</td>
                                    <td data-field="收款情况">收款情况</td>
                                    <td data-field="订单状态">订单状态</td>
                                </tr>
                            </thead>
                            <!--循环主订单-->
                            <%foreach (DataRow maindr in maindt.Rows)
                                {%>
                            <%DataRow[] drows = alldt.Select(" mainkeyid=" + maindr["mainkeyid"].ToString() + "");%>
                            <%if (drows.Length > 1)//多行的情况
                                { %>
                            <%int nowline = 1;//子订单默认第一个
                                foreach (DataRow dr in drows)
                                {%>
                            <%if (nowline == 1)//当子订单第一个的时候
                                { %>
                            <tr>
                                <td rowspan="<%=drows.Length %>" data-field="<%=show %>"><%=show %></td>
                                <td rowspan="<%=drows.Length %>" data-field="<%=maindr["comname"].ToString() %>"><%=maindr["comname"].ToString() %></td>
                                <td rowspan="<%=drows.Length %>" data-field="<%=maindr["OrderID"].ToString() %>"><%=maindr["OrderID"].ToString() %></td>
                                <td data-field="<%=maindr["yspmc"].ToString() %>"><%=maindr["yspmc"].ToString() %></td>
                                <td data-field="<%=maindr["sl"].ToString() %>"><%=maindr["sl"].ToString() %></td>
                                <td data-field="<%=maindr["dj"].ToString() %>"><%=maindr["dj"].ToString() %></td>
                                <td data-field="<%=maindr["je"].ToString() %>"><%=maindr["je"].ToString() %></td>
                                <td rowspan="<%=drows.Length %>" data-field="<%=maindr["jexx"].ToString() %>"><%=maindr["jexx"].ToString() %></td>

                                <%a = System.Convert.ToDouble(maindr["jexx"].ToString()); %>                                <%b = System.Convert.ToDouble(maindr["yufuk"].ToString()); %>                                <%c = a - b; %>
                                <td rowspan="<%=drows.Length %>" data-field="<%=maindr["yufuk"].ToString() %>"><%=maindr["yufuk"].ToString() %></td>

                                <%if (maindr["mystatus"].ToString() == "3" || maindr["mystatus"].ToString() == "4")
                                    { %>
                                <td rowspan="<%=drows.Length %>"><font color="ff0000" data-field="0">0</font></td>
                                <%}
                                else
                                {%>
                                <td rowspan="<%=drows.Length %>"><font color="ff0000" data-field="<%=c %>"><%=c%></font></td>
                                <%} %>
                                <%-- 款项 --%>
                                <td>
                                    <%string aay = dr["status"].ToString().Replace(",", ""); %>
                                    <%string aai = aay; %>

                                    <%if (aai == "0" || aai == null || aai == "")
                                        {%>

                                    <%if (aai == "3")
                                        { %>
                                    <font color="#0000ff" data-field="<%=c %>">已结清：<%=c %></font>&nbsp;&nbsp;<font color="#FF0000" data-field="<%=maindr["jexx"].ToString() %>">总金额：<%=maindr["jexx"].ToString() %></font>
                                    <%}
                                        else if (aai == "0" || aai == "1" || aai == "2" || aai == "")
                                        { %>
                                    <button type="button" class="layui" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="jieqing('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">结清</button>
                                    <button type="button" class="layui" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="fenqifk('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">分期付款</button>
                                    <%}
                                        else if (aai == "4")
                                        {%>
                                    <font color="red">已作废</font>
                                    <%} %>
                                    <%}
                                        else
                                        {%>
                                    <%string aak = aai.Substring(aai.Length - 1, +1); %>
                                    <%if (aak == "3")
                                        { %>
                                    <font color="#0000ff" data-field="<%=c %>">已结清：<%=c %></font>&nbsp;&nbsp;<font color="#FF0000" data-field="<%=maindr["jexx"].ToString() %>">总金额：<%=maindr["jexx"].ToString() %></font>
                                    <%}
                                        else if (aak == "0" || aak == "1" || aak == "2")
                                        { %>
                                    <button type="button" class="layui" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="jieqing('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">结清</button>
                                    <button type="button" class="layui" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="fenqifk('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">分期付款</button>

                                    <%}
                                        else if (aak == "4")
                                        {%>
                                    <font color="red">已作废</font>
                                    <%} %>
                                    <%} %>
                                </td>
                                <%-- 作废------------------------------------------------------------- --%>
                                <td>

                                    <%string y = dr["status"].ToString().Replace(",", ""); %>
                                    <%string i = y; %>

                                    <%if (i == "0" || i == null || i == "")
                                        {%>

                                    <%if (i != "4")
                                        { %>
                                    <button type="button" class="layui-bg-red" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="clickHandler('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">作废此单</button>
                                    <%}
                                        else if (i == "4")
                                        {%>
                                    <font color="0000ff">已作废</font>
                                    <%} %>
                                    <%}
                                        else
                                        {%>
                                    <%string k = i.Substring(i.Length - 1, +1); %>
                                    <%if (k != "4")
                                        { %>
                                    <button type="button" class="layui-bg-red" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="clickHandler('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">作废此单</button>
                                    <%}
                                        else if (k == "4")
                                        {%>
                                    <font color="0000ff">已作废</font>
                                    <%} %>
                                    <%} %>

                                </td>
                            </tr>
                            <%}
                                else//当子订单第二个以上时，因为我们要合并需要这项，这项就不显示
                                { %>
                            <tr>
                                <td data-field="<%=dr["yspmc"].ToString() %>"><%=dr["yspmc"].ToString() %></td>
                                <td data-field="<%=dr["sl"].ToString() %>"><%=dr["sl"].ToString() %></td>
                                <td data-field="<%=dr["dj"].ToString() %>"><%=dr["dj"].ToString() %></td>
                                <td data-field="<%=dr["je"].ToString() %>"><%=dr["je"].ToString() %></td>
                                <td>
                                    <%string qqy = dr["status"].ToString().Replace(",", ""); %>
                                    <%string eei = qqy; %>

                                    <%if (eei == "0" || eei == null || eei == "")
                                        {%>

                                    <%a = System.Convert.ToDouble(maindr["jexx"].ToString()); %>
                                    <%b = System.Convert.ToDouble(maindr["yufuk"].ToString()); %>
                                    <%c = a - b; %>
                                    <%if (eei == "3")
                                        {%>
                                    <font color="#0000ff" data-field="<%=c %>">已结清：<%=c %></font>&nbsp;&nbsp;<font color="#FF0000" data-field="<%=dr["jexx"].ToString() %>">总金额：<%=dr["jexx"].ToString() %></font>
                                    <%}
                                        else if (eei == "0" || eei == "1" || eei == "2" || eei == "")
                                        { %>
                                    <button type="button" class="layui" color="ff0000" id="<%=dr["OrderID"].ToString() %>" onclick="jieqing('<%=dr["OrderID"].ToString() %>','<%=dr["mystatus"].ToString() %>')">结清</button>
                                    <button type="button" class="layui" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="fenqifk('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">分期付款</button>

                                    <%}
                                        else if (eei == "4")
                                        {%>
                                    <font color="0000ff">已作废</font>
                                    <%} %>
                                    <%}
                                        else
                                        { %>
                                    <%string wwk = eei.Substring(eei.Length - 1, +1); %>
                                    <%a = System.Convert.ToDouble(maindr["jexx"].ToString()); %>
                                    <%b = System.Convert.ToDouble(maindr["yufuk"].ToString()); %>
                                    <%c = a - b; %>
                                    <%if (wwk == "3")
                                        {%>
                                    <font color="#0000ff" data-field="<%=c %>">已结清：<%=c %></font>&nbsp;&nbsp;<font color="#FF0000" data-field="<%=dr["jexx"].ToString() %>">总金额：<%=dr["jexx"].ToString() %></font>
                                    <%}
                                        else if (wwk == "0" || wwk == "1" || wwk == "2")
                                        { %>
                                    <button type="button" class="layui" color="ff0000" id="<%=dr["OrderID"].ToString() %>" onclick="jieqing('<%=dr["OrderID"].ToString() %>','<%=dr["mystatus"].ToString() %>')">结清</button>
                                    <button type="button" class="layui" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="fenqifk('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">分期付款</button>

                                    <%}
                                        else if (wwk == "4")
                                        {%>
                                    <font color="0000ff">已作废</font>
                                    <%} %>


                                    <%} %>

                                </td>
                                <%-- 作废------------------------------------------------------------- --%>
                                <td>
                                    <%string y = dr["status"].ToString().Replace(",", ""); %>
                                    <%string i = y; %>

                                    <%if (i == "0" || i == null || i == "")
                                        {%>
                                    <%if (i != "4")
                                        { %>
                                    <button type="button" class="layui-bg-red" color="ff0000" id="<%=dr["OrderID"].ToString() %>" onclick="clickHandler('<%=dr["OrderID"].ToString() %>','<%=dr["mystatus"].ToString() %>')">作废此单</button>
                                    <%}
                                        else if (i == "4")
                                        {%>
                                    <font color="0000ff">已作废</font>
                                    <%} %>
                                    <%}
                                        else
                                        { %>
                                    <%string k = i.Substring(i.Length - 1, +1); %>
                                    <%if (k != "4")
                                        { %>
                                    <button type="button" class="layui-bg-red" color="ff0000" id="<%=dr["OrderID"].ToString() %>" onclick="clickHandler('<%=dr["OrderID"].ToString() %>','<%=dr["mystatus"].ToString() %>')">作废此单</button>
                                    <%}
                                        else if (k == "4")
                                        {%>
                                    <font color="0000ff">已作废</font>
                                    <%} %>
                                    <%} %>

                                </td>

                            </tr>
                            <%} %>
                            <% nowline = nowline + 1;
                                } %><!----子foreach到这里结束---->
                            <%}//一行的情况
                                else
                                { %>

                            <%a = System.Convert.ToDouble(string.IsNullOrEmpty(maindr["jexx"].ToString()) ? "0" : maindr["jexx"].ToString()); %>

                            <%b = System.Convert.ToDouble(string.IsNullOrEmpty(maindr["yufuk"].ToString()) ? "0" : maindr["yufuk"].ToString()); %>

                            <%c = a - b; %>


                            <tr>
                                <td data-field="<%=show %>"><%=show %></td>
                                <td data-field="<%=maindr["comname"].ToString() %>"><%=maindr["comname"].ToString() %></td>
                                <td data-field="<%=maindr["orderid"].ToString() %>"><%=maindr["orderid"].ToString() %></td>
                                <td data-field="<%=maindr["yspmc"].ToString() %>"><%=maindr["yspmc"].ToString() %></td>
                                <td data-field="<%=maindr["sl"].ToString() %>"><%=maindr["sl"].ToString() %></td>
                                <td data-field="<%=maindr["dj"].ToString() %>"><%=maindr["dj"].ToString() %></td>
                                <td data-field="<%=maindr["je"].ToString() %>"><%=maindr["je"].ToString() %></td>
                                <td data-field="<%=maindr["jexx"].ToString() %>"><%=maindr["jexx"].ToString() %></td>
                                <td data-field="<%=maindr["yufuk"].ToString() %>"><%=maindr["yufuk"].ToString() %></td>
                                <%if (maindr["mystatus"].ToString() == "3" || maindr["mystatus"].ToString() == "4")
                                    {%>
                                <td><font color="ff0000" data-field="<%=c %>">0</font></td>
                                <%}
                                else
                                {%><%-- 款项 --%>
                                <td><font color="ff0000" data-field="<%=c %>"><%=c %></font></td>
                                <%} %>
                                <td>


                                    <%string yyi = maindr["status"].ToString().Replace(",", ""); %>
                                    <%string iiy = yyi; %>
                                    <%if (iiy == "0" || iiy == null || iiy == "")
                                        {%>

                                    <%if (iiy == "0" || iiy == null || iiy == "")
                                        {%>
                                    <button type="button" class="layui" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="jieqing('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">结清</button>
                                    <button type="button" class="layui" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="fenqifk('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">分期付款</button>
                                    <%}
                                        else if (iiy == "3")
                                        {%>
                                    <font color="#0000ff">已结清：<%=c %></font>&nbsp;&nbsp;<font color="#FF0000">总金额：<%=maindr["jexx"].ToString() %></font>
                                    <%}
                                        else if (iiy == "4")
                                        {%>
                                    <font color="red">已作废</font>
                                    <%} %>


                                    <%}
                                        else
                                        {%>
                                    <%string kky = iiy.Substring(iiy.Length - 1, +1); %>
                                    <%if (kky == "0" || kky == "1" || kky == "2")
                                        {%>
                                    <button type="button" class="layui" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="jieqing('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">结清</button>
                                    <button type="button" class="layui" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="fenqifk('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">分期付款</button>

                                    <%}
                                        else if (kky == "3")
                                        {%>
                                    <font color="#0000ff">已结清：<%=c %></font>&nbsp;&nbsp;<font color="#FF0000">总金额：<%=maindr["jexx"].ToString() %></font>
                                    <%}
                                        else if (kky == "4")
                                        {%>
                                    <font color="red">已作废</font>
                                    <%} %>
                                    <%} %>

                                </td>
                                <%-- 作废---------------------------一行的情况---------------------------------- --%>
                                <td>



                                    <%string y = maindr["status"].ToString().Replace(",", ""); %>
                                    <%string i = y; %>
                                    <%if (i == "0" || i == null || i == "")
                                        {%>

                                    <%if (i == "0" || i == null || i == "" || i != "4")
                                        { %>
                                    <button type="button" class="layui-bg-red" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="clickHandler('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">作废本单</button>
                                    <%}
                                        else if (i == "4")
                                        {%>
                                    <font color="red">已作废</font>
                                    <%} %>

                                    <%}
                                        else
                                        {%>
                                    <%string k = i.Substring(i.Length - 1, +1); %>

                                    <%if (k != "4")
                                        { %>
                                    <button type="button" class="layui-bg-red" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="clickHandler('<%=maindr["OrderID"].ToString() %>','<%=maindr["mystatus"].ToString() %>')">作废本单</button>
                                    <%}
                                        else if (k == "4")
                                        {%>
                                    <font color="red">已作废</font>
                                    <%} %>

                                    <%} %>

                                </td>
                            </tr>
                            <%} %>                            <% show = show + 1;//显示行号
                                                                  } %>
                            <tr>
                                <td colspan="4" style="text-align: center"></td>
                                <td style="text-align: center">合计:</td>
                                <td>总金额: </td>
                                <td><%=dt.Compute("Sum(jexx)","") %>元</td>
                                <td style="text-align: center">预付款: </td>
                                <td><%=dt.Compute("Sum(yufuk)","") %>元</td>

                                <td colspan="1">欠款: <%=decimal.Parse(dt.Compute("Sum(jexx)","").ToString()) - decimal.Parse(dt.Compute("Sum(yufuk)", "").ToString()) - decimal.Parse(string.IsNullOrEmpty(dt.Compute("Sum(jexx)", "mystatus=3").ToString())?"0":dt.Compute("Sum(jexx)", "mystatus=3").ToString())  %>元  </td>

                                <td>已结清: <%=dt.Compute("Sum(jexx)","mystatus=3") %>元</td>
                                <td></td>
                            </tr>
                        </table>
                        <%--金额合计:<%=dt.Compute("Sum(jexx)","") %>预付款:<%=dt.Compute("Sum(yufuk)","") %>已经结清(要注意结清后就不能修改订单状态和作废)<%=dt.Compute("Sum(jexx)","mystatus=3") %>
                        总欠款=金额合计-预付款-已结清
                        <%=decimal.Parse(dt.Compute("Sum(jexx)","").ToString())-decimal.Parse(dt.Compute("Sum(yufuk)","").ToString())-decimal.Parse(dt.Compute("Sum(jexx)","mystatus=3").ToString()) %>--%>



                        <%-- 以下是要打印的局部html---------------------------------------------------------------------------------------------------------------------- --%>

                        <div style="display: none" id="print">

                            <table border="1" class="layui-table" id="grid">
                                <!--订单号和简拼搜索出来只能打印一部分,暂时不支持这两种打印-->
                                <%if (mcname == "" || mcname == null)
                                    {%>
                                <div>
                                    <h2 style="text-align: center; font-size: 24px; font-family: 微软雅黑; height: 30px; line-height: 30px; margin: 4px 0 0 0;">昆明聚源达彩印包装有限公司汇总表</h2>
                                </div>
                                <%}
                                    else
                                    {%>
                                <tr>
                                    <h2 style="text-align: center;">昆明聚源达彩印包装有限公司<br />
                                    </h2>
                                    <h3 style="text-align: center;"><%=mcname %> <%=yhsj %>
                                 的订单汇总表</h3>
                                    <%} %>
                                </tr>
                                <thead>
                                    <tr>
                                        <td data-field="xh">序号</td>
                                        <td data-field="jjdh">接件单号</td>
                                        <td data-field="yspmc">印刷品名称</td>
                                        <td data-field="sl">数量</td>
                                        <td data-field="dj">单价</td>
                                        <td data-field="je">金额</td>
                                        <td data-field="xj">小计</td>
                                        <td data-field="yfk">预付款</td>
                                        <td data-field="qk">欠款</td>
                                    </tr>
                                </thead>
                                <!--循环主订单-->
                                <%foreach (DataRow maindr in maindt.Rows)
                                    {%>
                                <%DataRow[] drows = alldt.Select("mainkeyid=" + maindr["mainkeyid"].ToString() + "");%>
                                <%--<%if (ii++ == 9) {%>
                            <tr style="page-break-after:always; text-align: center;"><td>表格内容</td></tr>
                            <%} %>--%>
                                <%if (drows.Length > 1)//多行的情况
                                    { %>
                                <%int nowline = 1;//子订单默认第一个
                                    foreach (DataRow dr in drows)
                                    {%>
                                <%if (nowline == 1)//当子订单第一个的时候
                                    { %>
                                <tr>
                                    <td rowspan="<%=drows.Length %>" data-field="<%=show1 %>"><%=show1 %></td>
                                    <td rowspan="<%=drows.Length %>" data-field="<%=maindr["OrderID"].ToString() %>"><%=maindr["OrderID"].ToString() %></td>
                                    <td data-field="<%=maindr["yspmc"].ToString() %>"><%=maindr["yspmc"].ToString() %></td>
                                    <td data-field="<%=maindr["sl"].ToString() %>"><%=maindr["sl"].ToString() %></td>
                                    <td data-field="<%=maindr["dj"].ToString() %>"><%=maindr["dj"].ToString() %></td>
                                    <td id="zjtd" name="zjtd" data-field="<%=maindr["je"].ToString() %>"><%=maindr["je"].ToString() %></td>
                                    <td rowspan="<%=drows.Length %>" data-field="<%=maindr["jexx"].ToString() %>"><%=maindr["jexx"].ToString() %></td>

                                    <%a = System.Convert.ToDouble(maindr["jexx"].ToString()); %>
                                    <%b = System.Convert.ToDouble(maindr["yufuk"].ToString()); %>
                                    <%c = a - b; %>
                                    <td rowspan="<%=drows.Length %>" data-field="<%=maindr["yufuk"].ToString() %>"><%=maindr["yufuk"].ToString() %></td>
                                    <%if (maindr["mystatus"].ToString() == "3" || maindr["mystatus"].ToString() == "4")
                                        { %>
                                    <td rowspan="<%=drows.Length %>"><font color="ff0000" data-field="0">0</font></td>
                                    <%}
                                    else
                                    {%>
                                    <td rowspan="<%=drows.Length %>"><font color="ff0000" data-field="<%=c %>"><%=c%></font></td>
                                    <%} %>
                                </tr>
                                <%}
                                    else//当子订单第二个以上时，因为我们要合并需要这项，这项就不显示
                                    { %>
                                <tr>
                                    <td data-field="<%=dr["yspmc"].ToString() %>"><%=dr["yspmc"].ToString() %></td>
                                    <td data-field="<%=dr["sl"].ToString() %>"><%=dr["sl"].ToString() %></td>
                                    <td data-field="<%=dr["dj"].ToString() %>"><%=dr["dj"].ToString() %></td>
                                    <td data-field="<%=dr["je"].ToString() %>"><%=dr["je"].ToString() %></td>

                                    <%--<td>
                                    <%if (dr["mystatus"].ToString() == "0" || dr["mystatus"].ToString() == "1" || dr["mystatus"].ToString() == "2" || dr["mystatus"].ToString() == "3")
                                        { %>
                                    <%--<button type="button" class="layui-bg-red" color="ff0000" id="<%=dr["OrderID"].ToString() %>" onclick="clickHandler('<%=dr["OrderID"].ToString() %>','<%=dr["status"].ToString() %>')">作废此单</button>--%>
                                    <%-- <%}
                                        else if (dr["mystatus"].ToString() == "4")
                                        {%>
                                    <font color="0000ff">已作废</font>
                                    <%} %>
                                </td>--%>
                                </tr>
                                <%} %>
                                <% nowline = nowline + 1;
                                    } %><!----子foreach到这里结束---->
                                <%}//一行的情况
                                    else
                                    { %>
                                <%a = System.Convert.ToDouble(string.IsNullOrEmpty(maindr["jexx"].ToString()) ? "0" : maindr["jexx"].ToString()); %>
                                <%b = System.Convert.ToDouble(string.IsNullOrEmpty(maindr["yufuk"].ToString()) ? "0" : maindr["yufuk"].ToString()); %>
                                <%c = a - b; %>


                                <tr>
                                    <td data-field="<%=show %>"><%=show1 %></td>
                                    <td data-field="<%=maindr["orderid"].ToString() %>"><%=maindr["orderid"].ToString() %></td>
                                    <td data-field="<%=maindr["yspmc"].ToString() %>"><%=maindr["yspmc"].ToString() %></td>
                                    <td data-field="<%=maindr["sl"].ToString() %>"><%=maindr["sl"].ToString() %></td>
                                    <td data-field="<%=maindr["dj"].ToString() %>"><%=maindr["dj"].ToString() %></td>
                                    <td data-field="<%=maindr["je"].ToString() %>"><%=maindr["je"].ToString() %></td>
                                    <td data-field="<%=maindr["jexx"].ToString() %>"><%=maindr["jexx"].ToString() %></td>
                                    <td data-field="<%=maindr["yufuk"].ToString() %>"><%=maindr["yufuk"].ToString() %></td>
                                    <%if (maindr["mystatus"].ToString() == "3" || maindr["mystatus"].ToString() == "4")
                                        { %>
                                    <td><font color="ff0000" data-field="0">0</font></td>
                                    <%}
                                    else
                                    {%>
                                    <td><font color="ff0000" data-field="<%=c %>"><%=c%></font></td>
                                    <%} %>

                                    <%-- <td>
                                    <%if (maindr["mystatus"].ToString() == "0" || maindr["mystatus"].ToString() == "1" || maindr["mystatus"].ToString() == "2" || maindr["mystatus"].ToString() == "3"){ %>
                                        <%--<button type="button" class="layui-bg-red" color="ff0000" id="<%=maindr["OrderID"].ToString() %>" onclick="clickHandler('<%=maindr["OrderID"].ToString() %>','<%=maindr["status"].ToString() %>')">作废本单</button>--%>
                                    <%--<%}else if (maindr["mystatus"].ToString() == "4"){%>
                                        <font color="red">已作废</font>
                                    <%} %>
                                </td>--%>
                                </tr>
                                <%} %>

                                <% show1 = show1 + 1;//显示行号

                                    } %>
                                <tr>
                                    <td colspan="3" style="text-align: center">合计:</td>
                                    <td colspan="2">总金额: <%=dt.Compute("Sum(jexx)","") %>元</td>
                                    <td colspan="2">预付款: <%=dt.Compute("Sum(yufuk)","") %>元</td>
                                    <td>已结清: <%=dt.Compute("Sum(jexx)","mystatus=3") %>元</td>
                                    <td colspan="1">欠款: <%=decimal.Parse(dt.Compute("Sum(jexx)","").ToString())-decimal.Parse(dt.Compute("Sum(yufuk)","").ToString())-decimal.Parse(string.IsNullOrEmpty(dt.Compute("Sum(jexx)","mystatus=3").ToString())?"0":dt.Compute("Sum(jexx)","mystatus=3").ToString()) %>元  </td>
                                </tr>
                            </table>

                        </div>

                        <center><div class="layui-box layui-laypage layui-laypage-default"><span>共<%=total %>记录</span><span>共<%=allpage %>页</span><span>每页<%=pagesize %>条记录</span><a href="?page=1<%=searchUrl %>">第一页</a><a href="?page=<%=firstpage %><%=searchUrl %>">«上一页</a><%=mdpage %><a href="?page=<%=nextpage %><%=searchUrl %>">下一页»</a><a href="?page=<%=allpage %><%=searchUrl %>">最后一页</a></div></center>




                    </div>
                </div>
            </div>
        </div>

        <script src="../scripts/layui/layui.js"></script>
        <!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
        <script>
            layui.use(['form', 'layedit', 'laydate'], function () {
                var form = layui.form
                    , layer = layui.layer
                    , layedit = layui.layedit
                    , laydate = layui.laydate;
                //日期时间范围
                laydate.render({
                    elem: '#xdrq'
                    , type: 'datetime'
                    , range: '~' //或 range: '~' 来自定义分割字符
                });
                laydate.render({
                    elem: '#jhrq'
                    , type: 'datetime'
                    , range: '~' //或 range: '~' 来自定义分割字符
                });
            });

        </script>


    </form>

</body>
</html>
