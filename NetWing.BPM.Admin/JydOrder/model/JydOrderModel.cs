using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
    [TableName("jydOrder")]
    [Description("接件单")]
    public class JydOrderModel
    {
        /// <summary>
        /// 系统编号
        /// </summary>
        [Description("系统编号")]
        public int KeyId { get; set; }
        /// <summary>
        /// 服务类型
        /// </summary>
        [Description("服务类型")]
        public int ServiceType { get; set; }
        /// <summary>
        /// 订单年份
        /// </summary>
        [Description("订单年份")]
        public string OrderYear { get; set; }
        /// <summary>
        /// 子订单个数
        /// </summary>
        [Description("子订单个数")]
        public int OrderInt { get; set; }
        /// <summary>
        /// 自定义订单编号
        /// </summary>
        [Description("自定义订单编号")]
        public string OrderID { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [Description("用户名")]
        public string username { get; set; }
        /// <summary>
        /// 单位名称
        /// </summary>
        [Description("单位名称")]
        public string comname { get; set; }
        /// <summary>
        /// 单位ID
        /// </summary>
        [Description("单位ID")]
        public int comnameid { get; set; }
        /// <summary>
        /// 单位简拼码
        /// </summary>
        [Description("单位简拼码")]
        public string jpm { get; set; }
        /// <summary>
        /// 联系人姓名
        /// </summary>
        [Description("联系人姓名")]
        public string name { get; set; }
        /// <summary>
        /// 电话
        /// </summary>
        [Description("电话")]
        public string tel { get; set; }
        /// <summary>
        /// 地址
        /// </summary>
        [Description("地址")]
        public string address { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public string explain { get; set; }
        /// <summary>
        /// 交货时间
        /// </summary>
        [Description("交货时间")]
        public DateTime? deliveryDate { get; set; }
        /// <summary>
        /// 交货
        /// </summary>
        [Description("交货")]
        public string delivery { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        [Description("添加时间")]
        public DateTime? adddate { get; set; }
        /// <summary>
        /// fk
        /// </summary>
        [Description("fk")]
        public int fk { get; set; }
        /// <summary>
        /// yf
        /// </summary>
        [Description("yf")]
        public int yf { get; set; }
        /// <summary>
        /// qt
        /// </summary>
        [Description("qt")]
        public int qt { get; set; }
        /// <summary>
        /// 总金额
        /// </summary>
        [Description("总金额")]
        public int totalprice { get; set; }
        /// <summary>
        /// cfzt
        /// </summary>
        [Description("cfzt")]
        public string cfzt { get; set; }
        /// <summary>
        /// 接件员
        /// </summary>
        [Description("接件员")]
        public string jjy { get; set; }
        /// <summary>
        /// 腹膜A
        /// </summary>
        [Description("腹膜A")]
        public string fumoa { get; set; }
        /// <summary>
        /// 腹膜B
        /// </summary>
        [Description("腹膜B")]
        public string fumob { get; set; }
        /// <summary>
        /// 压A
        /// </summary>
        [Description("压A")]
        public string yaa { get; set; }
        /// <summary>
        /// 压B
        /// </summary>
        [Description("压B")]
        public string yab { get; set; }
        /// <summary>
        /// 烫A
        /// </summary>
        [Description("烫A")]
        public string tana { get; set; }
        /// <summary>
        /// 烫B
        /// </summary>
        [Description("烫B")]
        public string tanb { get; set; }
        /// <summary>
        /// 折页
        /// </summary>
        [Description("折页")]
        public string zheye { get; set; }
        /// <summary>
        /// UVA
        /// </summary>
        [Description("UVA")]
        public string uva { get; set; }
        /// <summary>
        /// UVB
        /// </summary>
        [Description("UVB")]
        public string uvb { get; set; }
        /// <summary>
        /// 装订
        /// </summary>
        [Description("装订")]
        public string zd { get; set; }
        /// <summary>
        /// 刀版
        /// </summary>
        [Description("刀版")]
        public string db { get; set; }
        /// <summary>
        /// 裱盒
        /// </summary>
        [Description("裱盒")]
        public string bh { get; set; }
        /// <summary>
        /// 送货
        /// </summary>
        [Description("送货")]
        public string sh { get; set; }
        /// <summary>
        /// sk
        /// </summary>
        [Description("sk")]
        public string sk { get; set; }
        /// <summary>
        /// 货发
        /// </summary>
        [Description("货发")]
        public string huofa { get; set; }
        /// <summary>
        /// 发货人
        /// </summary>
        [Description("发货人")]
        public string fahuoren { get; set; }
        /// <summary>
        /// ys
        /// </summary>
        [Description("ys")]
        public string ys { get; set; }
        /// <summary>
        /// fy
        /// </summary>
        [Description("fy")]
        public string fy { get; set; }
        /// <summary>
        /// dha
        /// </summary>
        [Description("dha")]
        public string dha { get; set; }
        /// <summary>
        /// dhb
        /// </summary>
        [Description("dhb")]
        public string dhb { get; set; }
        /// <summary>
        /// 状态
        /// </summary>
        [Description("状态")]
        public string status { get; set; }
        /// <summary>
        /// 金额
        /// </summary>
        [Description("金额")]
        public decimal jexx { get; set; }
        /// <summary>
        /// 金额大写
        /// </summary>
        [Description("金额大写")]
        public string jedx { get; set; }
        /// <summary>
        /// 预付款
        /// </summary>
        [Description("预付款")]
        public decimal yufuk { get; set; }
        /// <summary>
        /// 预付款A
        /// </summary>
        [Description("预付款A")]
        public string yufukuana { get; set; }
        /// <summary>
        /// sckd
        /// </summary>
        [Description("sckd")]
        public string sckd { get; set; }
        /// <summary>
        /// sjwcsj
        /// </summary>
        [Description("sjwcsj")]
        public string sjwcsj { get; set; }
        /// <summary>
        /// cpwcsj
        /// </summary>
        [Description("cpwcsj")]
        public string cpwcsj { get; set; }
        /// <summary>
        /// ctpsj
        /// </summary>
        [Description("ctpsj")]
        public string ctpsj { get; set; }
        /// <summary>
        /// sbwcsj
        /// </summary>
        [Description("sbwcsj")]
        public string sbwcsj { get; set; }
        /// <summary>
        /// qslsj
        /// </summary>
        [Description("qslsj")]
        public string qslsj { get; set; }
        /// <summary>
        /// yswcsj
        /// </summary>
        [Description("yswcsj")]
        public string yswcsj { get; set; }
        /// <summary>
        /// hjgkssj
        /// </summary>
        [Description("hjgkssj")]
        public string hjgkssj { get; set; }
        /// <summary>
        /// hjgwcsj
        /// </summary>
        [Description("hjgwcsj")]
        public string hjgwcsj { get; set; }
        /// <summary>
        /// wjgjcsj
        /// </summary>
        [Description("wjgjcsj")]
        public string wjgjcsj { get; set; }
        /// <summary>
        /// qcpsj
        /// </summary>
        [Description("qcpsj")]
        public string qcpsj { get; set; }
        /// <summary>
        /// zdkssj
        /// </summary>
        [Description("zdkssj")]
        public string zdkssj { get; set; }
        /// <summary>
        /// dzwcsj
        /// </summary>
        [Description("dzwcsj")]
        public string dzwcsj { get; set; }
        /// <summary>
        /// shryccsj
        /// </summary>
        [Description("shryccsj")]
        public string shryccsj { get; set; }
        /// <summary>
        /// shryhcsj
        /// </summary>
        [Description("shryhcsj")]
        public string shryhcsj { get; set; }
        /// <summary>
        /// dbsj
        /// </summary>
        [Description("dbsj")]
        public string dbsj { get; set; }
        /// <summary>
        /// szp
        /// </summary>
        [Description("szp")]
        public string szp { get; set; }
        [Description("收支票金额")]
        public decimal szpje { get; set; }

        [Description("收现金金额")]
        public decimal sxjje { get; set; }

        /// <summary>
        /// sxj
        /// </summary>
        [Description("sxj")]
        public string sxj { get; set; }
        /// <summary>
        /// skje
        /// </summary>
        [Description("skje")]
        public int skje { get; set; }
        /// <summary>
        /// zph
        /// </summary>
        [Description("zph")]
        public string zph { get; set; }
        /// <summary>
        /// kkyy
        /// </summary>
        [Description("kkyy")]
        public string kkyy { get; set; }
        /// <summary>
        /// zrr
        /// </summary>
        [Description("zrr")]
        public string zrr { get; set; }
        [Description("责任人ID")]
        public int zrrid { get; set; }




        /// <summary>
        /// rksj
        /// </summary>
        [Description("rksj")]
        public string rksj { get; set; }
        /// <summary>
        /// cksj
        /// </summary>
        [Description("cksj")]
        public string cksj { get; set; }
        /// <summary>
        /// myws
        /// </summary>
        [Description("myws")]
        public string myws { get; set; }
        /// <summary>
        /// myysf
        /// </summary>
        [Description("myysf")]
        public string myysf { get; set; }
        /// <summary>
        /// O_myhjg1
        /// </summary>
        [Description("O_myhjg1")]
        public string O_myhjg1 { get; set; }
        /// <summary>
        /// jgzysx
        /// </summary>
        [Description("jgzysx")]
        public string jgzysx { get; set; }
        /// <summary>
        /// 联系人
        /// </summary>
        [Description("联系人")]
        public string connman { get; set; }
        /// <summary>
        /// 订单状态
        /// </summary>
        [Description("订单状态")]
        public int mystatus { get; set; }
        /// <summary>
        /// kkkje
        /// </summary>
        [Description("kkkje")]
        public int kkkje { get; set; }
        /// <summary>
        /// ds
        /// </summary>
        [Description("ds")]
        public int ds { get; set; }
        /// <summary>
        /// 印刷品名称
        /// </summary>
        [Description("印刷品名称")]
        public string allyspmc { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        [Description("部门名称")]
        public string dept { get; set; }
        /// <summary>
        /// 部门ID
        /// </summary>
        [Description("部门ID")]
        public int deptid { get; set; }
        /// <summary>
        /// 流程单
        /// </summary>
        [Description("流程单")]
        public int liuchendan { get; set; }
        /// <summary>
        /// kaidan
        /// </summary>
        [Description("kaidan")]
        public string kaidan { get; set; }

        /// <summary>
        /// 二维码地址
        /// </summary>
        [Description("order_img")]
        public string order_img { get; set; }

        /// <summary>
        /// 用户下单状态
        /// </summary>
        [Description("FlowStatus")]
        public string FlowStatus { get; set; }

        [Description("业务员")]
        public string ywy { get; set; }
        [Description("业务员ID")]
        public int ywyid{ get; set; }

        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}

	}
}