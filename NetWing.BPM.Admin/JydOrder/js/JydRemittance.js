﻿var actionURL = '/JydOrder/ashx/JydOrderHandler.ashx';
var formurl = '/JydOrder/JydRemittance.aspx';
$(function () {
    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });
});
function fenqifk(q, e) {
    console.log('====' + q);
    var index = layer.open({
        type: 2,
        content: '/JydOrder/fenqifk.aspx?ai='+q+'',
        area: ['1268px', '606px'],
        maxmin: false
    });
}

function clickHandler(id, rows) {
    var mystatus = confirm("您确定要作废此单?");
    if (!mystatus) {
        return false;
    }
    $.ajax({
        url: "/JydOrder/ashx/JydOrderHandler.ashx?action=zuofei",
        type: "POST",
        dataType: "json",
        data: {
            OrderID: id,
            mystatus: rows
        },
        success: function (d) {
            if (d.code === 3) {
                top.$.messager.alert('系统提示', d.msg, 'warning');
            }
            if (d.code === 1) {
                window.location.reload();
                //$.messager.alert('提示', d.msg);
                top.$.messager.alert('系统提示', d.msg, 'warning');
            } else {
                window.location.reload();
                top.$.messager.alert('系统提示', d.msg, 'warning');
            }
        },
        error: function (e) {

        }
    });
}

function jieqing(value, row) {
    var opp = confirm("您确定要结清此单?");
    if (!opp) {
        return false;
    }
    row = 3;
    $.ajax({
        url: "/JydOrder/ashx/JydOrderHandler.ashx?action=jieqing",
        type: "POST",
        dataType: "json",
        data: {
            OrderID: value,
            mystatus: row
        },
        success: function (a) {
            
            if (a.status === 1) {//如果成功则提示修改成功
                window.location.reload();//刷新页面
                top.$.messager.alert('系统提示', a.msg, 'warning');
            } else {
                window.location.reload();
                top.$.messager.alert('系统提示', a.msg, 'warning');
            }
        }
    });
}


$(function () {
    //高级查询
    $('#a_search').click(function () {

        search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#mysearch').click(function () {//自定义搜索框
        mySearch();
    });
});



//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var orderdeptid = $("#orderdeptid").textbox('getValue');
    var orderstatus = $("#orderstatus").textbox('getValue');
    var OrderID = $("#orderid").textbox('getValue');
    var yspmc = $("#allyspmc").textbox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    var i = 0;
    if (orderdeptid !== '' && orderdeptid !== undefined) {//假如部门搜索不为空
        o.rules[i] = JSON.parse('{"field":"orderdeptid","op":"cn","data":"' + orderdeptid + '"}');
        i = i + 1;
    }
    if (orderstatus !== '' && orderstatus !== undefined) {//假如单位搜索不为空
        o.rules[i] = JSON.parse('{"field":"orderstatus","op":"cn","data":"' + orderstatus + '"}');
        i = i + 1;
    }
    if (OrderID !== '' && OrderID !== undefined) {//假如订单号搜索不为空
        o.rules[i] = JSON.parse('{"field":"OrderID","op":"cn","data":"' + OrderID + '"}');
        i = i + 1;
    }
    if (yspmc !== '' & yspmc !== undefined) {//联系人不为空
        o.rules[i] = JSON.parse('{"field":"allyspmc","op":"cn","data":"' + yspmc + '"}');
    }

    $('#list').datagrid('reload', { filter: JSON.stringify(o) });


}
