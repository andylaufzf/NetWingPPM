var actionURL = '/JydOrder/ashx/JydOrderHandler.ashx';
var formurl = '/JydOrder/html/JydOrder.html';

$(function () {

    //为避免数据字典加载不完整。这段在后面加载
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });

    //$('#a_jhsj').click(function () {
    //    toPdf();
    //})
    $('#a_add').click(CRUD.add);
    $('#a_edit').click(CRUD.edit);
    $('#a_send').click(function () {
        send();
    });//送货签收
    //开票信息
    $('#a_invoice').click(function () {
        var row = grid.getSelectedRow();
        //console.log("选中的数据是：" + row.KeyId);
        invoice(row);
    });
    $('#a_allmoney').click(function () {
        allpaymoney();
    });//全单收款
    $("#a_editlcd").click(CRUD.editlcd);//流程单
    $('#a_delete').click(CRUD.del);
    //高级查询
    $('#a_search').click(function () {

        search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#mysearch').click(function () {//自定义搜索框
        mySearch();
    });

    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });

    //批量删除
    $("#a_alldel").click(function () {
        var ids = [];
        var rows = $('#list').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].KeyId);
        }
        var allid = ids.join(',');//所有的id
        var o = {};
        o.action = "alldel";
        o.KeyIds = allid;
        var param = "json=" + JSON.stringify(o);

        if (confirm('确认要执行批量删除操作吗？')) {
            jQuery.ajaxjson(actionURL, param, function (d) {
                if (parseInt(d) > 0) {
                    msg.ok('批量删除成功！');
                    grid.reload();
                } else {
                    MessageOrRedirect(d);
                }
            });
        }


    });

});
//这段后加载，避免数据字典加载不完整的问题
window.onload = function () {
    autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
};



//editor:'datetimebox' 日期及时间选择框  editor:'datebox' 日期选择框    editor:'numberspinner' 数字调节器  editor:{type: 'numberspinner',options:{value:0,required:true}}

//定义一个$JQ为$.　以后在js 中就可以用${JQ}AJAX了在前台这样写(定义)://通用数据字典start
var getDic = {
    jsonData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?categoryId=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
    jsonParentData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?action=parent&parentid=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
}

//通用数据字典end
//调用数据字典和使用数据字典 如果不需要请不要打开否则会导致系统速度慢
//getDic.jsonData(9);//取得性别数据字典
//在onLoad:的地方如下使用
//top.$('#txt_user_sex').combobox({ data: eval($('body').data('data9')), valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '请选择性别', disabled: false });

var grid = {
    bind: function (winSize) {
        $('#list').datagrid({
            url: actionURL,
            toolbar: '#toolbar',
            title: "订单列表",
            iconCls: 'icon icon-list',
            width: winSize.width,
            height: winSize.height,
            nowrap: false, //折行
            rownumbers: true, //行号
            striped: true, //隔行变色
            idField: 'KeyId',//主键
            singleSelect: true, //单选
            frozenColumns: [[]],
            rowStyler: function (value, row, index) {
                if (row.deliveryDate == "" || row.deliveryDate == null || row.deliveryDate == undefined) {//用户手机提交的交货日期为空显示背景为粉红色
                    return 'background-color:pink;color:blue;';
                } else {
                    if (row.FlowStatus == 1) {
                        return 'background-color:#ff9966;color:blue;';
                    }
                }
            },
            columns: [[//应为宽度不是很需要所以注释了宽度
                { title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                //{ title: '系统编号', field: 'KeyId', sortable: true, width: '', hidden: false },
                //{ title: '服务类型', field: 'ServiceType', sortable: true, width: '', hidden: false },
                //{ title: '订单年份', field: 'OrderYear', sortable: true, width: '', hidden: false },
                //{ title: '子订单个数', field: 'OrderInt', sortable: true, width: '', hidden: false },
                { title: '订单号', field: 'OrderID', sortable: true, width: '', hidden: false },
                //{ title: '用户名', field: 'username', sortable: true, width: '', hidden: false },
                { title: '客户名称/印刷品名称', field: 'khmcANDyspmc', sortable: false, width: '300', hidden: false },
                //{ title: '单位ID', field: 'comnameid', sortable: true, width: '', hidden: false },
                //{ title: '单位简拼码', field: 'jpm', sortable: true, width: '', hidden: false },
                //{ title: '联系人姓名', field: 'name', sortable: true, width: '', hidden: false },
                { title: '联系电话', field: 'tel', sortable: true, width: '', hidden: false },
                //{ title: '地址', field: 'address', sortable: true, width: '', hidden: false },
                //{ title: '备注', field: 'explain', sortable: true, width: '', hidden: false },
                { title: '下单日期', field: 'adddate', sortable: true, width: '', hidden: false },
                {
                    title: '交货日期', field: 'deliveryDate', sortable: true,

                    width: '', hidden: false

                },


                //{
                //    title: '交货日期', field: 'deliveryDate', sortable: true,
                //    formatter: function (value, row, index) {
                //        if (row.deliveryDate !== "" && row.deliveryDate !== null) {
                //            return new Date(value).Format("yyyy-MM-dd");

                //        } else if (row.deliveryDate === null) {

                //            return 'background-color:pink;color:blue;font-weight:bold;';
                //        } else {
                //            return value;
                //        }
                //    },
                //    width: '', hidden: false
                //    //条件设置行背景颜色

                //},



                //{ title: '交货', field: 'delivery', sortable: true, width: '', hidden: false },

                //{ title: 'fk', field: 'fk', sortable: true, width: '', hidden: false },
                //{ title: 'yf', field: 'yf', sortable: true, width: '', hidden: false },
                //{ title: 'qt', field: 'qt', sortable: true, width: '', hidden: false },
                //{ title: '总金额', field: 'totalprice', sortable: true, width: '', hidden: false },
                //{ title: 'cfzt', field: 'cfzt', sortable: true, width: '', hidden: false },
                //{ title: '接件员', field: 'jjy', sortable: true, width: '', hidden: false },
                //{ title: '腹膜A', field: 'fumoa', sortable: true, width: '', hidden: false },
                //{ title: '腹膜B', field: 'fumob', sortable: true, width: '', hidden: false },
                //{ title: '压A', field: 'yaa', sortable: true, width: '', hidden: false },
                //{ title: '压B', field: 'yab', sortable: true, width: '', hidden: false },
                //{ title: '烫A', field: 'tana', sortable: true, width: '', hidden: false },
                //{ title: '烫B', field: 'tanb', sortable: true, width: '', hidden: false },
                //{ title: '折页', field: 'zheye', sortable: true, width: '', hidden: false },
                //{ title: 'UVA', field: 'uva', sortable: true, width: '', hidden: false },
                //{ title: 'UVB', field: 'uvb', sortable: true, width: '', hidden: false },
                //{ title: '装订', field: 'zd', sortable: true, width: '', hidden: false },
                //{ title: '刀版', field: 'db', sortable: true, width: '', hidden: false },
                //{ title: '裱盒', field: 'bh', sortable: true, width: '', hidden: false },
                //{ title: '送货', field: 'sh', sortable: true, width: '', hidden: false },
                //{ title: 'sk', field: 'sk', sortable: true, width: '', hidden: false },
                //{ title: '货发', field: 'huofa', sortable: true, width: '', hidden: false },
                //{ title: '发货人', field: 'fahuoren', sortable: true, width: '', hidden: false },
                //{ title: 'ys', field: 'ys', sortable: true, width: '', hidden: false },
                //{ title: 'fy', field: 'fy', sortable: true, width: '', hidden: false },
                //{ title: 'dha', field: 'dha', sortable: true, width: '', hidden: false },
                //{ title: 'dhb', field: 'dhb', sortable: true, width: '', hidden: false },
                //{ title: '状态', field: 'status', sortable: true, width: '', hidden: false },
                //{ title: '金额', field: 'jexx', sortable: true, width: '', hidden: false },
                //{ title: '金额大写', field: 'jedx', sortable: true, width: '', hidden: false },
                //{ title: '预付款', field: 'yufuk', sortable: true, width: '', hidden: false },
                //{ title: '预付款A', field: 'yufukuana', sortable: true, width: '', hidden: false },
                //{ title: 'sckd', field: 'sckd', sortable: true, width: '', hidden: false },
                //{ title: 'sjwcsj', field: 'sjwcsj', sortable: true, width: '', hidden: false },
                //{ title: 'cpwcsj', field: 'cpwcsj', sortable: true, width: '', hidden: false },
                //{ title: 'ctpsj', field: 'ctpsj', sortable: true, width: '', hidden: false },
                //{ title: 'sbwcsj', field: 'sbwcsj', sortable: true, width: '', hidden: false },
                //{ title: 'qslsj', field: 'qslsj', sortable: true, width: '', hidden: false },
                //{ title: 'yswcsj', field: 'yswcsj', sortable: true, width: '', hidden: false },
                //{ title: 'hjgkssj', field: 'hjgkssj', sortable: true, width: '', hidden: false },
                //{ title: 'hjgwcsj', field: 'hjgwcsj', sortable: true, width: '', hidden: false },
                //{ title: 'wjgjcsj', field: 'wjgjcsj', sortable: true, width: '', hidden: false },
                //{ title: 'qcpsj', field: 'qcpsj', sortable: true, width: '', hidden: false },
                //{ title: 'zdkssj', field: 'zdkssj', sortable: true, width: '', hidden: false },
                //{ title: 'dzwcsj', field: 'dzwcsj', sortable: true, width: '', hidden: false },
                //{ title: 'shryccsj', field: 'shryccsj', sortable: true, width: '', hidden: false },
                //{ title: 'shryhcsj', field: 'shryhcsj', sortable: true, width: '', hidden: false },
                //{ title: 'dbsj', field: 'dbsj', sortable: true, width: '', hidden: false },
                //{ title: 'szp', field: 'szp', sortable: true, width: '', hidden: false },
                //{ title: 'sxj', field: 'sxj', sortable: true, width: '', hidden: false },
                //{ title: 'skje', field: 'skje', sortable: true, width: '', hidden: false },
                //{ title: 'zph', field: 'zph', sortable: true, width: '', hidden: false },
                //{ title: 'kkyy', field: 'kkyy', sortable: true, width: '', hidden: false },
                //{ title: 'zrr', field: 'zrr', sortable: true, width: '', hidden: false },
                //{ title: 'rksj', field: 'rksj', sortable: true, width: '', hidden: false },
                //{ title: 'cksj', field: 'cksj', sortable: true, width: '', hidden: false },
                //{ title: 'myws', field: 'myws', sortable: true, width: '', hidden: false },
                //{ title: 'myysf', field: 'myysf', sortable: true, width: '', hidden: false },
                //{ title: 'O_myhjg1', field: 'O_myhjg1', sortable: true, width: '', hidden: false },
                //{ title: 'jgzysx', field: 'jgzysx', sortable: true, width: '', hidden: false },
                {
                    title: '回款状态', field: 'hkzt', sortable: false, width: '',
                    formatter: function (value, row, index) {
                        //console.log(JSON.stringify(row));
                        if (row.status !== "" && row.status !== null) {
                            var s = row.status;
                            if (s.indexOf("3") >= 0) {
                                return "<font color=008800>全单收款</font>";
                            } else {
                                return "回款中";
                            }
                        } else {
                            return "回款中";
                        }

                    },
                    hidden: false
                },
                {
                    title: '订单状态', field: 'mystatus', sortable: true, width: '',
                    formatter: function (value, row, index) {
                        //console.log(JSON.stringify(row));
                        var myztt = row.status;
                        if (myztt == null || myztt == '' || myztt == undefined) {

                        } else {
                            var myzt = myztt.replace(",", "");
                            var zzkj = myzt.substr(myzt.length-1,+1);
                        }

                        
                        //console.log("我的订单我的订单我的订单我的订单我的订单我的订单我的订单我的订单" + zzkj);
                        if (row.mystatus === 0) {
                            return "<font color=0000ff>未走上流程</font>";//开单接件单是0
                        }
                        if (row.mystatus === 1) {
                            return "<font color=CC00CC>正在生产中</font>";//流程单是1
                        }
                        if (row.mystatus === 2) {
                            return "<font color=#green>已送货签单</font>";//送货签收单是2
                        }
                        if (row.status !== "" && row.status !== null) {//作废单是4
                            var s = row.status;
                            //conso.log("值是:" + s);
                            if (s.indexOf("4") >= 0) {
                                return "<font color=ff0000>(已作废)</font>";
                            }
                        } else {
                            return "未知";


                        }

                        //console.log("我的订单我的订单我的订单我的订单我的订单我的订单我的订单我的订单" + zzkj);
                        //if (row.mystatus === 0) {
                        //    return "<font color=0000ff>未走上流程</font>";
                        //}
                        //if (row.mystatus === 1) {
                        //    return "<font color=CC00CC>正在生产中</font>";
                        //}
                        //if (row.mystatus === 2) {
                        //    return "<font color=#green>已送货签单</font>";
                        //}
                        //if (row.status !== "" && row.status !== null) {
                        //    var s = row.status;
                        //    //conso.log("值是:" + s);
                        //    if (s.indexOf("4") >= 0) {
                        //        return "<font color=ff0000>(已作废)</font>";
                        //    }




                        //switch (row.mystatus) {
                        //	case 0:
                        //		return "<font color=0000ff>未走上流程</font>";
                        //		break;
                        //	case 1:
                        //		return "<font color=CC00CC>正在生产中</font>";
                        //		break;
                        //	case 2:
                        //		return "<font color=ff0000>已送货签单</font>";
                        //		break;
                        //	case 3:
                        //		return "<font color=008800>全单收款</font>";
                        //		break;
                        //	case 4:
                        //		return "<font color=ff0000>(已作废)</font>";
                        //		break;
                        //	default:
                        //		return "未知";
                        //		break;
                        //}
                    },
                    hidden: false
                },
                //{ title: 'kkkje', field: 'kkkje', sortable: true, width: '', hidden: false },
                //{ title: 'ds', field: 'ds', sortable: true, width: '', hidden: false },
                //{ title: '印刷品名称', field: 'allyspmc', sortable: true, width: '', hidden: false },
                { title: '订单来源', field: 'dept', sortable: true, width: '', hidden: false },
                { title: '开单员', field: 'jjy', sortable: true, width: '', hidden: false },
                {
                    title: '业务员', field: 'ywy', sortable: true, width: '', hidden: false,
                    formatter: function (value, row, index) {
                        if (row.ywy != null && row.ywy != "") {
                            var btn = '&nbsp;<a class="seaveewm" title="打印' + row.OrderID + '销售单" onclick="showywy(\'' + row.KeyId + '\',\'' + row.OrderID + '\')" href="javascript:void(0)" style="background-color: #9c6194;padding: 3px 14px; color: #fff;font-size: 12px;">' + row.ywy+'</a>';
                            return btn;
                        }
                    }
                },

                {
                    title: '流程二维码', field: 'order_img', sortable: false, width: '120', hidden: false,
                    formatter: function (value, row, index) {
                        var btn = '&nbsp;<a class="seaveewm" onclick="showqrcode(\'' + row.OrderID + '\',\'' + row.KeyId + '\')" href="javascript:void(0)" style="background-color: #45dcd5;padding: 3px 14px; color: #fff;font-size: 12px;">查看二维码</a>';
                        return btn;
                    }
                },
                //<a class="showewm" onclick="imgShow(\'' + row.order_img + '\',\'' + row.KeyId + '\',\'' + row.OrderID + '\')" href="javascript:void(0)" style="background-color: #2ac4ff;padding: 3px 14px; color: #fff;font-size: 12px;">查看</a>
                {
                    title: '催单操作', field: 'FlowStatus', sortable: true, width: '', hidden: false,
                    formatter: function (fd, rhfhfgow, fghfg) {
                        var btnnn = '<button onclick="cuidxg(\'' + rhfhfgow.KeyId + '\')"  href="javascript:void(0)" style="background-color: #FFD700;padding: 3px 14px; color: #fff;font-size: 12px;">已催单</button>&nbsp;';
                        return btnnn;
                    }
                }
            ]],


            onEndEdit: onEndEdit,//结束编辑时函数 这里为了简洁 该函数写在下面
            onUnselect: onUnselect,
            onLoadSuccess: function (data) {
                //alert($('body').data('data70'));
                //alert($('body').data('data69'));
            },
            onCancelEdit: onCancelEdit,//在用户取消编辑一行的时候触发
            onSelect: onSelect,//在用户选择一行的时候触发
            onClickRow: onClickRow,//在用户点击一行的时候触发
            //onAfterEdit: onAfterEdit,//在用户完成编辑一行的时候触发
            onDblClickCell: onDblClickCell,//为了程序逻辑清楚函数写在外面
            onHeaderContextMenu: function (e, field) {//列菜单实现动态隐藏列
                e.preventDefault();
                if (!cmenu) {
                    createColumnMenu();
                }
                cmenu.menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            pagination: true,
            pageSize: PAGESIZE,
            pageList: [20, 40, 50, 100, 200]

        });
    },
    getSelectedRow: function () {
        return $('#list').datagrid('getSelected');
    },
    reload: function () {
        $('#list').datagrid('clearSelections').datagrid('reload', { filter: '' });
    }
};
//管理员修改用户订单催单状态
function cuidxg(KeyId) {
    var mydd = confirm("如此单已通知工厂加工,请点击确定");
    if (!mydd) {
        return false;
    }
    $.ajax({
        url: "/JydOrder/ashx/JydOrderHandler.ashx",
        type: "post",
        dataType: "json",
        data: {
            action: 'cuidxgf',
            KeyId: KeyId
        },

        success: function (d) {
            if (d.code === 0) {
                window.location.reload();
                top.$.messager.alert('系统提示', d.msg, 'warning');
            }
        }
    });
}




//创建一个easyui的弹窗展示二维码
function imgShow(order_img, KeyId, OrderID) {
    top.$('#imgShow').dialog({
        title: OrderID + ' - ' + KeyId,
        width: 400,
        height: 400,
        resizable: true,
        closed: false,
        cache: false,
        modal: true
    });
    $('#img_boox').attr("src", '../' + order_img);
}

//二维码下载
function imgDownload(order_img) {
    if (order_img != '' && order_img != undefined && order_img != null && order_img.length > 10) {
        var imgs = [];
        var arim = order_img.split(';');
        for (var j = 0; j < arim.length; j++) {
            imgs.push('../' + arim[j]);
        }
        imgs.map(function (i) {
            var a = document.createElement('a');
            a.setAttribute('download', '');
            a.href = i;
            document.body.appendChild(a);
            a.click();
        })
    } else {
        msg.warning('暂无图片！');
    }
}

function createParam(action, keyid) {
    var o = {};
    var query = top.$('#uiform').serializeArray();4
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}

//自定义表单名的方法
function createObjParam(action, keyid, obj) {
    var o = {};
    var query = top.$('#' + obj + '').serializeArray();
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}


function allmoney() {
    var je = 0;
    top.$(".mymy").find('.je').each(function () {
        var dje = top.$(this).numberbox("getValue");
        je = je + parseFloat(dje);
        console.log("计算总金额:" + je);
    });

    top.$("#jexx").numberbox("setValue", je);//设置总金额
    top.$("#jedx").textbox("setValue", cmycurd(je));//设置金额大写

}



function initbody() {
    //渲染原理：逐个逐个渲染(因为统一渲染easyui 事件会用不成)
    //初始化预付款
    top.$(".mymy:last").find(".kzcc,.kzccb,.zs,.zsb,.fsb,.fs,.pss,.pssb,.pstb,.pst").textbox({
        required: true
    });//开纸尺寸,正数,放数,色,套

    //top.$(".mymy:last").find(".kzcc,.zs,.fs,.pss,.pst").textbox({
    //	required: true
    //});//开纸尺寸,正数,放数,色,套

    top.$(".mymy:last").find('.jx,.jxb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=101',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=101', 'dic101'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        required: true,
        valueField: 'Title',
        textField: 'Title',
        onLoadSuccess: function () {

        }
    });
    top.$(".mymy:last").find('.fbyq,.fbyqb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=100',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=100', 'dic100'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        required: true,
        valueField: 'Title',
        textField: 'Title',
        onLoadSuccess: function () {

        }
    });

    top.$("#yufuk").numberbox({
        onChange: function (newValue, oldValue) {
            top.$("#yufukuana").textbox("setValue", cmycurd(newValue));
        }
    });

    top.$(".mymy:last").find('.dddd,.ddddb').textbox({
        multiline: true,
    });



    top.$(".mymy:last").find('.yspmc,.danwei_m,.cpcc,.zzf,.ym,.sj,.yspmcb,.danwei_mb,.cpccb,.zzfb,.ymb,.sjb').textbox({});
    //流程单里没有印刷品名称
    top.$(".mymy:last").find(".yspmc").textbox({
        required: true,
        validType: 'length[2,50]'
    });

    top.$(".mymy:last").find('.nysl').numberbox({
        min: 0,
        //precision: 2,
        value: 0,
        required: true,
        //validType: 'email' 
    });

    //初始化数量
    top.$(".mymy:last").find('.sl').numberbox({
        min: 0,
        //precision: 2,
        value: 0,
        required: true,
        //validType: 'email' 
        onChange: function (newValue, oldValue) {
            var dj = top.$(this).closest(".mymy").find(".dj").numberbox("getValue");
            var je = dj * newValue;//得到金额
            top.$(this).closest(".mymy").find(".je").numberbox("setValue", je);
            allmoney();//计算总金额


        }
    });

    //初始化单价
    top.$(".mymy:last").find('.dj').numberbox({
        min: 0,
        precision: 3,
        value: 0,
        required: true,
        //validType: 'money',
        //validType: 'email' 
        onChange: function (newValue, oldValue) {
            var sl = top.$(this).closest(".mymy").find(".sl").numberbox("getValue");
            var je = sl * newValue;//得到金额
            top.$(this).closest(".mymy").find(".je").numberbox("setValue", je);
            allmoney();//计算总金额

        }
    });

    //初始化金额
    top.$(".mymy:last").find('.je').numberbox({
        min: 0,
        precision: 3,
        value: 0,
        required: true,
        //validType: 'money',
        onChange: function (newValue, oldValue) {
            var sl = top.$(this).closest(".mymy").find(".sl").numberbox("getValue");
            var dj = newValue / sl;//得到单价
            top.$(this).closest(".mymy").find(".dj").numberbox("setValue", dj);
            allmoney();//计算总金额

        }
        //validType: 'email' 
    });


    //jquery选择多个元素
    top.$(".mymy:last").find('.uva,.uvab,.tana,.tanab,.fumoa,.fumoab,.yaa,.yaab').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=85',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=85', 'dic85'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
    });

    //腹膜
    top.$(".mymy:last").find('.fumob,.fumobb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=88',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=88', 'dic88'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
    });
    //折页
    top.$(".mymy:last").find('.zheye,.zheyeb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=94',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=94', 'dic94'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
    });

    //装订
    top.$(".mymy:last").find('.zd,.zdb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=95',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=95', 'dic95'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
    });

    //快捷备注1
    top.$(".mymy:last").find('.dda').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=98',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=98', 'dic98'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
        onSelect: function (rec) {
            //var url = 'get_data2.php?id=' + rec.id;
            //$('#cc2').combobox('reload', url);

            var obj = top.$(this).parent().find(".dddd");
            //判断元素是否存在0不存在
            if ($(obj).length === 0) {//说明不存在
                obj = top.$(this).parent().find(".ddddb");
            }
            var ov = $(obj).val();
            top.$(obj).textbox("setValue", ov + "," + rec.Title);//重新赋值
            console.log($(obj).length + "找到备注" + $(obj).val() + "赋值：" + rec.Title);

        }
    });
    //快捷备注2
    top.$(".mymy:last").find('.ddb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=99',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=99', 'dic99'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
        onSelect: function (rec) {
            //var url = 'get_data2.php?id=' + rec.id;
            //$('#cc2').combobox('reload', url);

            var obj = top.$(this).parent().find(".dddd");
            //判断元素是否存在0不存在
            if ($(obj).length === 0) {//说明不存在
                obj = top.$(this).parent().find(".ddddb");
            }
            var ov = $(obj).val();
            top.$(obj).textbox("setValue", ov + "," + rec.Title);//重新赋值
            console.log($(obj).length + "找到备注" + $(obj).val() + "赋值：" + rec.Title);

        }
    });


    //裱盒
    top.$(".mymy:last").find('.bh,.bhb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=96',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=96', 'dic96'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
    });

    //付款要求
    top.$(".mymy:last").find('.sk,.skb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=97',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=97', 'dic97'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
    });


    //烫B
    top.$(".mymy:last").find('.tanb,.tanbb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=89',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=89', 'dic89'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
    });
    //压
    top.$(".mymy:last").find('.yab,.yabb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=93',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=93', 'dic93'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
    });


    //uvb
    top.$(".mymy:last").find('.uvb,.uvbb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=92',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=92', 'dic92'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
    });
    //刀版
    top.$(".mymy:last").find('.db,.dbb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=91',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=91', 'dic91'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
    });
    //送货
    top.$(".mymy:last").find('.sh,.shb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=90',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=90', 'dic90'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title',
    });


    //工艺开关
    top.$(".mymy:last").find('.myswigongyi').switchbutton({
        onText: '显示',
        offText: '隐藏',
        onChange: function (checked) {
            console.log("开关状态是：" + checked);
            if (checked) {
                //原理是先找到父元素.mymy 然后再找子元素.myshowgongyi
                top.$(this).closest(".mymy").find('.myshowgongyi').show();
            } else {
                top.$(this).closest(".mymy").find('.myshowgongyi').hide();
            }
        }
    });


    //工艺开关-内页
    top.$(".mymy:last").find('.myswigongyiny').switchbutton({
        onText: '显示',
        offText: '隐藏',
        onChange: function (checked) {
            console.log("工艺内页开关状态是：" + checked);
            if (checked) {
                //原理是先找到父元素.mymy 然后再找子元素.myshowgongyi
                top.$(this).closest(".mymy").find('.myshowgongyiny').show();
            } else {
                top.$(this).closest(".mymy").find('.myshowgongyiny').hide();
            }
        }
    });

    //只渲染最后一个PrintType
    top.$(".mymy:last").find('.PrintType').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=85',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=85', 'dic85'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        required: true,
        valueField: 'Title',
        textField: 'Title',
        onSelect: function (r) {
            console.log("选择的值是：" + r.Title);
            //printtypea(r.Title);
            if (r.Title === '画册') {
                //alert("渲染画册");
                var f = top.$(this).closest(".mymy");//找到最近的mymy父元素
                console.log("找到的父元素是：" + f);
                var c = top.$(f).children(".myhc");//找到最近的这个子元素
                top.$(c).show();
                //如果是画册,则印刷品名称自动加封面和内页
                var tyspmc = top.$(this).closest(".mymy").find('.yspmc').textbox("getValue");//印刷品名称
                console.log(tyspmc);
                //设置印刷品名称  名称+封面

                if (tyspmc.indexOf("封面") === -1) {//只有不包含封面的时候才加,加过的就不加了  如果要检索的字符串值没有出现，则该方法返回 -1。
                    top.$(this).closest(".mymy").find('.yspmc').textbox("setValue", tyspmc + "封面");
                    top.$(this).closest(".mymy").find('.yspmcb').textbox("setValue", tyspmc + "内页");
                }
                //如果选择了画册 那么画册单位必填
                top.$(this).closest(".mymy").find('.danwei_mb').combobox({ required: true });
                top.$(this).closest(".mymy").find('.jxb').combobox({ required: true });
                top.$(this).closest(".mymy").find('.fbyqb').combobox({ required: true });
                top.$(this).closest(".mymy").find(".kzccb,.zsb,.fsb,.pssb,.pstb").textbox({
                    required: true
                });//开纸尺寸,正数,放数,色,套


            } else {//如果不是画册 则画册单位不必填
                top.$(this).closest(".mymy").find('.danwei_mb').combobox({ required: false });
                top.$(this).closest(".mymy").find('.jxb').combobox({ required: false });
                top.$(this).closest(".mymy").find('.fbyqb').combobox({ required: false });

                top.$(this).closest(".mymy").find(".kzccb,.zsb,.fsb,.pssb,.pstb").textbox({
                    required: false
                });//开纸尺寸,正数,放数,色,套

                var f = top.$(this).closest(".mymy");//找到最近的mymy父元素
                console.log("找到的父元素是：" + f);
                var c = top.$(f).children(".myhc");//找到最近的这个子元素
                top.$(c).hide();
            }

        }
    });
    //渲染单位
    top.$(".mymy:last").find('.danwei_m').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=103',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=103', 'dic103'),
        limitToList: false,
        //required: true,
        valueField: 'Title',
        textField: 'Title',
        editable: false
    });
    //渲染单位 画册内页
    top.$(".mymy:last").find('.danwei_mb').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=103',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=103', 'dic103'),
        limitToList: false,
        required: false,
        valueField: 'Title',
        textField: 'Title',
        editable: false
    });

    //渲染客户自带
    top.$(".mymy:last").find('.zidaihz').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=102',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=102', 'dic102'),
        limitToList: false,
        //required: true,
        valueField: 'Title',
        multiple: true,
        textField: 'Title',
        editable: false
    });

    //渲染客户自带画册内页
    top.$(".mymy:last").find('.zidaihchz').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=102',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=102', 'dic102'),
        limitToList: false,
        //required: true,
        valueField: 'Title',
        multiple: true,
        textField: 'Title',
        editable: false
    });
}
////印刷类型弹框选择流程
//function printtypea(titlea) {
//    console.log("123" + titlea);
//    var query = '{"groupOp":"AND","rules":[],"groups":[]}';//先给一个搜索的空的模板
//    var o = JSON.parse(query);//把文本转换为json obj
//    var i = 0;//这个i用来表示我有几条搜索规则，初始时是0条
//    o.rules[i] = JSON.parse('{ "field":"Printedmattername", "op":"eq", "data":"' + titlea + '"}');
//    console.log("132313132" + JSON.stringify(o));
//    $.ajax({//获取等于当前印刷类型的json模板
//        url: '/JydflowTasktemplate/ashx/JydflowTasktemplateHandler.ashx',
//        type: 'POST',
//        dateType: 'JSON',
//        async: false,    //同步
//        timeout: 5000,    //超时时间
//        data: { filter: JSON.stringify(o) },
//        success: function (q, w, e) {
//            console.log(q);

//        }
//    })
//}
//印刷类型弹框选择流程
//function printtypea(titlea) {
//    console.log("123" + titlea);
//    var gys = '{ "groupOp":"AND", "rules": [{ "field":"Printedmattername", "op":"cn", "data":"' + titlea + '"}], "groups": [] }';
//    top.$("#printflo").combogrid({
//        delay: 500, //自动完成功能实现搜索
//        mode: 'remote',//开启后系统会自动传一个参数q到后台 
//        panelWidth: 350,
//        required: true,
//        //queryparams实现带参数查询
//        queryParams: {
//            filter: gys
//        },
//        //value:'fullname',   
//        editable: true,
//        idField: 'nodename',
//        textField: 'nodename',
//        url: '/JydflowTasktemplate/ashx/JydflowTasktemplateHandler.ashx',
//        columns: [[
//            { field: 'KeyId', title: 'KeyId', width: 50 },
//            { field: 'nodename', title: '客户', width: 130 },

//        ]],
//        limitToList: true,//只能从下拉中选择值
//        //reversed: true,//定义在失去焦点的时候是否恢复原始值。
//        onHidePanel: function () {
//            var t = top.$(this).combogrid('getValue');//获取combogrid的值
//            var g = top.$(this).combogrid('grid');	// 获取数据表格对象
//            var r = g.datagrid('getSelected');	// 获取选择的行
//            console.log("选择的行是：" + r + "选择的值是:" + t);
//            if (r == null || t != r.ondename) {//没有选择或者选项不相等时清除内容
//                top.$.messager.alert('警告', '请选择，不要直接输入!');
//                top.$(this).combogrid('setValue', '');
//            } else {
//                //do something...
//            }
//        },
//        onSelect: function (rowIndex, rowData) {
//            console.log("设置的值是：" + rowData.KeyId);
//            //$("#contactid").val(rowData.KeyId);//给经手人设置ID
//            top.$("#printflo").combogrid('setValue', rowData.nodename);//客户
//            //top.$("#connman").textbox('setValue', rowData.realname);//经手人
//            //top.$("#tel").textbox('setValue', rowData.tell)
//            //alert(rowData.TrueName);
//        }
//    });
//}

function init() {
    //初始化时克隆模板过来
    var t = top.$(".mymytemp").html();//得到不带样式的模板 注意修改时也要修改模板
    //alert(top.$(".mymy:last").html());
    if (top.$(".mymy:last").html() == "") {//如果mymy里面为空则追加进去，否则
        top.$(".mymy").append(t);//把模板追加到mymy
    } else {//如果有多个
        top.$(".mymy:last").after('<tbody class="mymy">' + t + '</tbody>');//把模板追加到mymy
    }


    top.$("#jedx").textbox({});//金额大写




    //获得当前时间 作为接件时间


    //得到接件员名字
    top.$.ajax({
        type: "POST",
        url: '/JydOrder/ashx/JydOrderHandler.ashx?json={"action":"jjy"}',
        data: {},
        dataType: "json",
        beforeSend: function () { },
        complete: function () { },
        success: function (d) {
            console.log("得到的结果是：" + d.jjy);
            top.$("#jjy").textbox("setValue", d.jjy);
            //Bug 系统会吧接件日期修改为当前日期
            //top.$("#adddate").datetimebox("setValue", d.dt);

            top.$("#jjy").textbox({//设置为只读
                readonly: true,
            });
            //alert("发送结果：" + JSON.stringify(d));
        }
    });

    //初始化总金额
    top.$(".jexx").numberbox({
        min: 0,
        precision: 2,
        value: 0,
        required: true,
        validType: 'money',
        onChange: function (newValue, oldValue) {

        }
        //validType: 'email' 
    });


    //初始化客户名称
    top.$("#comname").combogrid({
        delay: 500, //自动完成功能实现搜索
        mode: 'remote',//开启后系统会自动传一个参数q到后台 
        panelWidth: 350,
        required: true,
        //value:'fullname',   
        editable: true,
        idField: 'comname',
        textField: 'comname',
        url: '/JydUser/ashx/JydUserHandler.ashx',
        columns: [[
            { field: 'KeyId', title: 'KeyId', width: 50 },
            { field: 'comname', title: '客户', width: 120 },
            { field: 'tell', title: '电话', width: 120 },

        ]],
        limitToList: true,//只能从下拉中选择值
        //reversed: true,//定义在失去焦点的时候是否恢复原始值。
        onHidePanel: function () {
            var t = top.$(this).combogrid('getValue');//获取combogrid的值
            var g = top.$(this).combogrid('grid');	// 获取数据表格对象
            var r = g.datagrid('getSelected');	// 获取选择的行
            console.log("选择的行是：" + r + "选择的值是:" + t);
            if (r == null || t != r.comname) {//没有选择或者选项不相等时清除内容
                top.$.messager.alert('警告', '请选择，不要直接输入!');
                top.$(this).combogrid('setValue', '');
            } else {
                //do something...
            }
        },
        onSelect: function (rowIndex, rowData) {
            console.log("设置的值是：" + rowData.KeyId);
            //$("#contactid").val(rowData.KeyId);//给经手人设置ID
            top.$("#comname").combogrid('setValue', rowData.comname);//客户
            top.$("#comnameid").val(rowData.KeyId);//客户
            top.$("#connman").textbox('setValue', rowData.realname);//经手人
            top.$("#tel").textbox('setValue', rowData.tell);
            //alert(rowData.TrueName);
        }
    });



    //初始化业务员
    top.$("#ywyid").combogrid({
        delay: 500, //自动完成功能实现搜索
        //mode: 'remote',//开启后系统会自动传一个参数q到后台 
        panelWidth: 350,
        //required: true,
        //value:'fullname',   
        editable: true,
        idField: 'KeyId',
        textField: 'TrueName',
        url: '/sys/ashx/userhandler.ashx?action=xxx',
        columns: [[
            { field: 'KeyId', title: 'KeyId', width: 50 },
            { field: 'TrueName', title: '姓名', width: 120 },
            { field: 'Mobile', title: '电话', width: 120 },

        ]],
        limitToList: true,//只能从下拉中选择值
        //reversed: true,//定义在失去焦点的时候是否恢复原始值。
        onHidePanel: function () {
            var t = top.$(this).combogrid('getValue');//获取combogrid的值
            var g = top.$(this).combogrid('grid');	// 获取数据表格对象
            var r = g.datagrid('getSelected');	// 获取选择的行
            console.log("选择的行是：" + r + "选择的值是:" + t);
            if (r == null || t != r.KeyId) {//没有选择或者选项不相等时清除内容
                top.$.messager.alert('警告', '请选择，不要直接输入!');
                top.$(this).combogrid('setValue', '');
            } else {
                //do something...
            }
        },
        onSelect: function (rowIndex, rowData) {
            console.log("设置的值是：" + rowData.KeyId);
            //$("#contactid").val(rowData.KeyId);//给经手人设置ID
            top.$("#ywy").val(rowData.TrueName);//客户
            top.$("#ywyid").combogrid('setValue', rowData.KeyId);//客户
            //alert(rowData.TrueName);
        }
    });



    top.$("#ds").numberspinner({
        editable: false,
        onSpinUp: function () {//鼠标向上点
            top.$(".mymy:last").after('<tbody class="mymy">' + t + '</tbody>');//把模板追加到mymy

            //insertjzr();//插入居住人
            //在最后一个后面克隆一个元素包含事件
            console.log("克隆页面");
            //mymy 和mymy1有父子关系 mymy1默认不显示
            //var temp = top.$(".mymy:last").html();//保存渲染前的模板
            ////console.log("得到的模板是：" + temp);



            //var mod = top.$(".mymy:last").clone();//克隆得到模板
            //top.$(".mymy:last").after(mod);//克隆后插入
            //top.$(".mymy:last").find(".myPrintType").empty();//找到印刷品类型并清空easyui渲染过的印刷品类型
            //top.$(".mymy:last").find(".myPrintType").append('<input name="PrintType" style="width:80px;" value="" class="PrintType" />');
            ////移除easyui工艺渲染新的easyui工艺
            //top.$(".mymy:last").find('.mygongyi').empty();
            //top.$(".mymy:last").find(".mygongyi").append('<input class="myswigongyi">');
            initbody();//克隆完毕重新渲染-只渲染body里面的内容

        },
        onSpinDown: function () {//鼠标向下点
            //var v = top.$("#jzrsdiv").children("li").length;
            var v = top.$(".mymy").length;
            if (v == 1) {//只有一个时不能删除
                top.$.messager.alert("注意", "至少接一件！！！");
                //$.messager.alert('警告', '表单还有信息没有填完整！');
            } else {
                top.$(".mymy:last").remove();//移除最后一个

            }
        }
    });
    console.log("加载印刷类型");

    //运输
    top.$('#ys').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=86',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=86', 'dic86'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title'
    });
    //好像渲染了两次注销这次
    //画册时客户自带 不验证 
    //top.$('#zidaihz').combobox({
    //	//url: '/sys/ashx/dichandler.ashx?categoryId=102',
    //	data: localData('/sys/ashx/dichandler.ashx?categoryId=102', 'dic102'),
    //	limitToList: false,
    //	//required: true,
    //	valueField: 'Title',
    //	textField: 'Title'
    //});

    //单位
    top.$('#danwei_m').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=103',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=103', 'dic103'),
        limitToList: false,
        //required: true,
        valueField: 'Title',
        textField: 'Title'
    });



    //费用
    top.$('#fy').combobox({
        //url: '/sys/ashx/dichandler.ashx?categoryId=87',
        data: localData('/sys/ashx/dichandler.ashx?categoryId=87', 'dic87'),
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        //required: true,
        valueField: 'Title',
        textField: 'Title'
    });

    initbody();//渲染body里面的内容
}


//时间流程
function initlc() {

    top.$(".sjlc").click(function () {
        var h = top.$(this).closest(".mymy").find(".mytimetable").parent().html();
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({

                title: 'JYD' + row.OrderID + '时间流程表', width: '60%', height: '60%', href: '/JydOrder/datalc.aspx?OrderID=' + row.OrderID + '', iconCls: 'icon-add',

                //title: '时间流程', width: 600, height: 500, iconCls: 'icon-add',
                //href: '/JydUser/datalc.aspx',
                //content: h,
                //onLoad: function () {
                //	top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                //	init();//渲染
                //},
                onSubmit: function () {

                }
            });
        }
    });



    //var row = grid.getSelectedRow();
    //if (row) {

    //    top.$(".sjlc").click(function () {
    //        var h = top.$(this).closest(".mymy").find(".mytimetable").parent().html();
    //        var rid = row.KeyId;
    //        var hDialog = top.jQuery.hDialog({
    //            title: '时间流程', width: 800, height: 500, iconCls: 'icon-add',
    //            href: '/JydOrder/datalc.aspx?orderid=' + rid,
    //            content: h,
    //            //onLoad: function () {
    //            //	top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
    //            //	init();//渲染
    //            //},
    //            onSubmit: function () {
    //                console.log("时间流程:" + rid);
    //            }

    //        });

    //    });

    //}

    //收款情况
    top.$(".skqk").click(function () {//注意一个问题，只有jquery 模式绑定事件才能找到元素
        var h = top.$(this).closest(".mymy").find(".KeyId");
        //alert(top.$(h).val());
        var row = null;
        var query = '{"groupOp":"AND","rules":[],"groups":[]}';//先给一个搜索的空的模板
        var o = JSON.parse(query);//把文本转换为json obj
        var i = 0;//这个i用来表示我有几条搜索规则，初始时是0条
        o.rules[i] = JSON.parse('{"field":"KeyId","op":"eq","data":"' + top.$(h).val() + '"}');//cn 就是包含  eq就是等于
        $.ajax({
            url: '/JydOrderDetail/ashx/JydOrderDetailHandler.ashx',
            type: 'POST', //GET
            async: false,    //同步
            timeout: 5000,    //超时时间
            data: { filter: JSON.stringify(o) },
            dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data, textStatus, jqXHR) {
                row = data.rows;
            },

        });


        var hDialog = top.jQuery.hDialog({
            title: 'JYD' + row[0].orderid + ' 子订单:' + row[0].KeyId + ' ' + row[0].yspmc + '收款', width: '800px', height: '250px', href: '/JydOrder/html/JydAllMoney.html', iconCls: 'icon-save',
            onLoad: function () {
                //init();//初始化
                top.$("#jexx").html(row[0].je + "元");
                top.$("#yufuk").html("子订单无预付款");//预付款
                top.$("#yinshou").html((row[0].je) + "元");
                top.$("#szpck").click(function () {
                    var isChecked = top.$('#szpck').attr('checked');
                    if (isChecked === undefined) {//没选中
                        top.$("#zph").textbox("setValue", "");
                        top.$("#zph").textbox({ readonly: true, required: false });
                        top.$("#szp").textbox("setValue", "0");//设置收支票金额为0且不可以写
                        top.$("#szp").textbox({ readonly: true });
                    } else {//选中了
                        top.$("#zph").textbox({ readonly: false, required: true });
                        top.$("#szp").textbox({ readonly: false, });
                    }
                    console.log("点击了:" + isChecked);
                });
                top.$("#sxjck").click(function () {
                    var isChecked = top.$('#sxjck').attr('checked');
                    if (isChecked === undefined) {//没选中
                        top.$("#sxj").numberbox("setValue", "0");//设置收支票金额为0且不可以写
                        top.$("#sxj").numberbox({ readonly: true });
                    } else {//选中了
                        top.$("#sxj").numberbox({ readonly: false });
                    }
                    console.log("点击了:" + isChecked);
                });
                //收支票改变
                top.$("#szp").numberbox({
                    onChange: function (newV, oldV) {
                        var xj = top.$("#sxj").textbox("getValue");
                        var kkje = top.$("#kkkje").numberbox("getValue");
                        top.$("#skje").textbox("setValue", parseFloat(newV) + parseFloat(xj) - parseFloat(kkje));
                    }
                });
                //收现金改变
                top.$("#sxj").numberbox({
                    onChange: function (newV, oldV) {
                        var zp = top.$("#szp").textbox("getValue");
                        var kkje = top.$("#kkkje").numberbox("getValue");
                        top.$("#skje").textbox("setValue", parseFloat(newV) + parseFloat(zp) - parseFloat(kkje));
                    }
                });


                //扣款金额
                top.$("#kkkje").numberbox({

                    onChange: function (newV, oldV) {
                        console.log("扣款金额变动：" + newV);
                        if (newV !== "0") {//要验证必填
                            top.$("#kkyy").textbox({ required: true, validType: '' });
                            top.$("#kkyy").textbox({ readonly: false });
                            var zp = top.$("#szp").textbox("getValue");
                            var xj = top.$("#sxj").numberbox("getValue");
                            top.$("#skje").textbox("setValue", parseFloat(zp) + parseFloat(xj) - newV);
                        } else {
                            console.log("执行等于0时");
                            //top.$("#kkyy").textbox({ validate: true });
                            top.$("#kkyy").textbox({ readonly: true });
                            top.$("#kkyy").textbox({ required: false, });
                        }
                    }
                });



                //初始化业务员-也就是责任人
                //top.$("#zrrid").combogrid({
                //    delay: 500, //自动完成功能实现搜索
                //    //mode: 'remote',//开启后系统会自动传一个参数q到后台 
                //    panelWidth: 350,
                //    required: true,
                //    //value:'fullname',   
                //    editable: true,
                //    idField: 'KeyId',
                //    textField: 'TrueName',
                //    url: '/sys/ashx/userhandler.ashx',
                //    columns: [[
                //        { field: 'KeyId', title: 'KeyId', width: 50 },
                //        { field: 'TrueName', title: '姓名', width: 120 },
                //        { field: 'Mobile', title: '电话', width: 120 },

                //    ]],
                //    limitToList: true,//只能从下拉中选择值
                //    //reversed: true,//定义在失去焦点的时候是否恢复原始值。
                //    onHidePanel: function () {
                //        var t = top.$(this).combogrid('getValue');//获取combogrid的值
                //        var g = top.$(this).combogrid('grid');	// 获取数据表格对象
                //        var r = g.datagrid('getSelected');	// 获取选择的行
                //        console.log("选择的行是：" + r + "选择的值是:" + t);
                //        if (r == null || t != r.KeyId) {//没有选择或者选项不相等时清除内容
                //            top.$.messager.alert('警告', '请选择，不要直接输入!');
                //            top.$(this).combogrid('setValue', '');
                //        } else {
                //            //do something...
                //        }
                //    },
                //    onSelect: function (rowIndex, rowData) {
                //        console.log("设置的值是：" + rowData.KeyId);
                //        //$("#contactid").val(rowData.KeyId);//给经手人设置ID
                //        top.$("#zrr").val(rowData.TrueName);//客户
                //        top.$("#zrrid").combogrid('setValue', rowData.KeyId);//客户


                //        //alert(rowData.TrueName);
                //    }
                //});


                top.$.ajax({
                    type: "POST",
                    url: '/JydOrder/ashx/JydOrderHandler.ashx?json={"action":"zrrid"}',
                    data: {},
                    dataType: "json",
                    beforeSend: function () { },
                    complete: function () { },
                    success: function (d) {
                        //console.log("这里得到的ID结果是：" + d.zrrid + "与" + d.zrr);
                        top.$("#zrrid").textbox("setValue", d.zrrid);
                        top.$("#zrr").textbox("setValue", d.zrr);
                        //Bug 系统会吧接件日期修改为当前日期
                        //top.$("#adddate").datetimebox("setValue", d.dt);

                        top.$("#zrr").textbox({//设置为只读
                            readonly: true
                        });
                        top.$("#zrrid").textbox({//设置为只读
                            readonly: true
                        });
                        //alert("发送结果：" + JSON.stringify(d));
                    }
                });


            },
            buttons: [
                //{
                //	text: '确定',
                //	iconCls: 'icon-ok',
                //	handler: function () {
                //		//liuchengdan(hDialog, row);//流程单模块化 方便重用
                //		//alert(d);
                //	}
                //},
                //开完单不能打印,点接件单才能打印
                {
                    text: '确定收款',
                    iconCls: 'icon-ok',
                    handler: function () {
                        //alert(top.$("#uiform").form('enableValidation').form('validate'));
                        //alert(top.$("#uiform").form('validate'));
                        //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                        if (top.top.$("#moneyuiform").form('validate')) {

                            var query = createObjParam('zddsk', row[0].KeyId, 'moneyuiform');
                            console.log("子订单收款query:" + query);
                            //alert(query);

                            jQuery.ajaxjson('/JydOrder/ashx/JydOrderHandler.ashx', query, function (d) {
                                if (parseInt(d) > 0) {
                                    msg.ok('修改成功！');
                                    hDialog.dialog('close');
                                    grid.reload();
                                } else {
                                    MessageOrRedirect(d);
                                }
                            });
                        } else {
                            msg.warning('请填写必填项！');
                        }
                        //

                    }
                },

                {
                    text: '关闭',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        hDialog.dialog("close");
                    }
                }],
            submit: function () {



                return false;
            }
        });




    });
}





//外送加工 后加工单
function waisong() {
    top.$(".wsjg").click(function () {
        var h = top.$(this).closest(".mymy").find(".KeyId");
        var row = null;
        var query = '{"groupOp":"AND","rules":[],"groups":[]}';//先给一个搜索的空的模板
        var o = JSON.parse(query);//把文本转换为json obj
        var i = 0;//这个i用来表示我有几条搜索规则，初始时是0条
        o.rules[i] = JSON.parse('{"field":"KeyId","op":"eq","data":"' + top.$(h).val() + '"}');//cn 就是包含  eq就是等于
        console.log("得到的json:" + JSON.stringify(o));
        $.ajax({
            url: '/JydOrderDetail/ashx/JydOrderDetailHandler.ashx',
            type: 'POST', //GET
            async: false,    //同步
            timeout: 5000,    //超时时间
            data: { filter: JSON.stringify(o) },
            dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data, textStatus, jqXHR) {
                row = data.rows;

            }

        });
        console.log("返回的数据是：" + JSON.stringify(row[0]));
        //打开一个弹窗新增客户
        var editDialog = top.jQuery.hDialog({
            title: 'JYD' + row[0].orderid + '子订单:' + row[0].KeyId + row[0].yspmc + '', width: '70%', height: '400px', href: '/JydOrder/html/waisjg.html', iconCls: 'icon-add',
            onLoad: function () {
                top.$("#yspmc").html(row[0].yspmc);
                top.$("#orderid").html(row[0].orderid);
                top.$("#mynum").numberbox({//加工数量
                    value: row[0].mynum,
                    required: true,
                    onChange: function (nv, ov) {
                        var mydj = top.$("#mydj").numberbox("getValue");
                        top.$("#myysf").numberbox("setValue", parseFloat(nv) * parseFloat(mydj));
                        top.$("#total").html(parseFloat(nv) * parseFloat(mydj) + "元");
                    }
                });
                //top.$("#mynum").numberbox("setValue",row[0].mynum);
                top.$("#mydj").numberbox({//加工单价
                    value: row[0].mydj,
                    min: 0,
                    precision: 2,
                    required: true,
                    onChange: function (nv, ov) {
                        var mynum = top.$("#mynum").numberbox("getValue");
                        top.$("#myysf").numberbox("setValue", parseFloat(nv) * parseFloat(mynum));
                        top.$("#total").html(parseFloat(nv) * parseFloat(mynum) + "元");
                    }
                });
                //top.$("#myje").numberbox({//加工金额
                //	value: 0.00,
                //	min: 0,
                //	precision: 2, 
                //	required: true,
                //});
                top.$("#myysf").numberbox({//加工金额
                    value: row[0].myysf,
                    min: 0,
                    precision: 2,
                    required: true,
                });
                //更改为大写
                top.$("#total").html(cmycurd(row[0].myysf));
                top.$("#mycc").textbox("setValue", row[0].mycc);
                top.$("#myzzxm").textbox("setValue", row[0].myzzxm);
                top.$("#wsjgbz").textbox("setValue", row[0].wsjgbz);
                top.$("#myxdrq").textbox("setValue", row[0].myxdrq);
                top.$("#myjhrq").textbox("setValue", row[0].myjhrq);

                //初始化后加工工厂
                var gys = '{ "groupOp":"AND", "rules": [{ "field":"fenlei", "op":"cn", "data":"供应商"}], "groups": [] }';
                top.$("#myws").combogrid({
                    delay: 500, //自动完成功能实现搜索
                    mode: 'remote',//开启后系统会自动传一个参数q到后台 
                    panelWidth: 350,
                    required: true,
                    //queryparams实现带参数查询
                    queryParams: {
                        filter: gys
                    },
                    //value:'fullname',   
                    editable: true,
                    idField: 'comname',
                    textField: 'comname',
                    url: '/JydUser/ashx/JydUserHandler.ashx',
                    columns: [[
                        { field: 'KeyId', title: 'KeyId', width: 50 },
                        { field: 'comname', title: '客户', width: 120 },
                        { field: 'tell', title: '电话', width: 120 },

                    ]],
                    limitToList: true,//只能从下拉中选择值
                    //reversed: true,//定义在失去焦点的时候是否恢复原始值。
                    onHidePanel: function () {
                        var t = top.$(this).combogrid('getValue');//获取combogrid的值
                        var g = top.$(this).combogrid('grid');	// 获取数据表格对象
                        var r = g.datagrid('getSelected');	// 获取选择的行
                        console.log("选择的行是：" + r + "选择的值是:" + t);
                        if (r == null || t != r.comname) {//没有选择或者选项不相等时清除内容
                            top.$.messager.alert('警告', '请选择，不要直接输入!');
                            top.$(this).combogrid('setValue', '');
                        } else {
                            //do something...
                        }
                    },
                    onSelect: function (rowIndex, rowData) {
                        console.log("设置的值是：" + rowData.KeyId);
                        //$("#contactid").val(rowData.KeyId);//给经手人设置ID
                        top.$("#myws").combogrid('setValue', rowData.comname);//客户
                        //top.$("#connman").textbox('setValue', rowData.realname);//经手人
                        //top.$("#tel").textbox('setValue', rowData.tell)
                        //alert(rowData.TrueName);
                    }
                });


                

                top.$("#myws").combogrid("setValue", row[0].myws);//后加工工厂赋值
                //初始后加工下单人
                top.$("#myxdid").combogrid({
                    delay: 500, //自动完成功能实现搜索
                    //mode: 'remote',//开启后系统会自动传一个参数q到后台 
                    panelWidth: 350,
                    required: true,
                    //value:'fullname',   
                    editable: true,
                    idField: 'KeyId',
                    textField: 'TrueName',
                    url: '/sys/ashx/userhandler.ashx',
                    columns: [[
                        { field: 'KeyId', title: 'KeyId', width: 50 },
                        { field: 'TrueName', title: '姓名', width: 120 },
                        { field: 'Mobile', title: '电话', width: 120 },

                    ]],
                    limitToList: true,//只能从下拉中选择值
                    //reversed: true,//定义在失去焦点的时候是否恢复原始值。
                    onHidePanel: function () {
                        var t = top.$(this).combogrid('getValue');//获取combogrid的值
                        var g = top.$(this).combogrid('grid');	// 获取数据表格对象
                        var r = g.datagrid('getSelected');	// 获取选择的行
                        console.log("选择的行是：" + r + "选择的值是:" + t);
                        if (r == null || t != r.KeyId) {//没有选择或者选项不相等时清除内容
                            top.$.messager.alert('警告', '请选择，不要直接输入!');
                            top.$(this).combogrid('setValue', '');
                        } else {
                            //do something...
                        }
                    },
                    onSelect: function (rowIndex, rowData) {
                        console.log("设置的值是：" + rowData.KeyId);
                        //$("#contactid").val(rowData.KeyId);//给经手人设置ID
                        top.$("#myxd").val(rowData.TrueName);//客户
                        top.$("#myxdid").combogrid('setValue', rowData.KeyId);//客户


                        //alert(rowData.TrueName);
                    }
                });



                top.$("#myxdid").combogrid("setValue", row[0].myxdid);//后加工下单人赋值
                //初始化后加工收货人
                top.$("#myshid").combogrid({
                    delay: 500, //自动完成功能实现搜索
                    //mode: 'remote',//开启后系统会自动传一个参数q到后台 
                    panelWidth: 350,
                    required: true,
                    //value:'fullname',   
                    editable: true,
                    idField: 'KeyId',
                    textField: 'TrueName',
                    url: '/sys/ashx/userhandler.ashx',
                    columns: [[
                        { field: 'KeyId', title: 'KeyId', width: 50 },
                        { field: 'TrueName', title: '姓名', width: 120 },
                        { field: 'Mobile', title: '电话', width: 120 },

                    ]],
                    limitToList: true,//只能从下拉中选择值
                    //reversed: true,//定义在失去焦点的时候是否恢复原始值。
                    onHidePanel: function () {
                        var t = top.$(this).combogrid('getValue');//获取combogrid的值
                        var g = top.$(this).combogrid('grid');	// 获取数据表格对象
                        var r = g.datagrid('getSelected');	// 获取选择的行
                        console.log("选择的行是：" + r + "选择的值是:" + t);
                        if (r == null || t != r.KeyId) {//没有选择或者选项不相等时清除内容
                            top.$.messager.alert('警告', '请选择，不要直接输入!');
                            top.$(this).combogrid('setValue', '');
                        } else {
                            //do something...
                        }
                    },
                    onSelect: function (rowIndex, rowData) {
                        console.log("设置的值是：" + rowData.KeyId);
                        //$("#contactid").val(rowData.KeyId);//给经手人设置ID
                        top.$("#mysh").val(rowData.TrueName);//客户
                        top.$("#myshid").combogrid('setValue', rowData.KeyId);//客户


                        //alert(rowData.TrueName);
                    }
                });
                top.$("#myshid").combogrid("setValue", row[0].myshid);//后加工收货人赋值
                //后加工付款方式
                top.$('#status_fk').combobox({
                    //url: '/sys/ashx/dichandler.ashx?categoryId=87',
                    data: localData('/sys/ashx/dichandler.ashx?categoryId=106', 'dic106'),
                    limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
                    //required: true,
                    valueField: 'Title',
                    textField: 'Title'
                });
            },
            buttons: [
                {
                    text: '确定',
                    iconCls: 'icon-ok',
                    handler: function () {
                        //alert(top.$("#uiform").form('enableValidation').form('validate'));
                        //alert(top.$("#uiform").form('validate'));
                        //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                        if (top.top.$("#waisjg").form('validate')) {

                            var query = createObjParam('waisongjg', row[0].KeyId, 'waisjg');
                            console.log("外送加工query:" + query);
                            //alert(query);

                            jQuery.ajaxjson('/JydOrder/ashx/JydOrderHandler.ashx', query, function (d) {
                                if (parseInt(d) > 0) {
                                    msg.ok('修改成功！');
                                    grid.reload();
                                } else {
                                    MessageOrRedirect(d);
                                }
                            });
                        } else {
                            msg.warning('请填写必填项！');
                        }
                        //
                    }
                },
                //开完单不能打印,点接件单才能打印
                {
                    text: '打印',
                    iconCls: 'icon-printer_start',
                    handler: function () {
                        print_printdy('打印后加工单JYD' + row[0].orderid + ',' + row[0].yspmc + '', '/JydWodrmb/print_printhjg.aspx?KeyId=' + row[0].KeyId + '&OrderID=' + row[0].orderid + '');

                    }
                },

                {
                    text: '关闭',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        editDialog.dialog("close");
                    }
                }],
            submit: function () {//提交时
                return false;

            }
        });

    });
}

//开单,模块化是因为以后可以重用
function kaidan(dialog) {
    //alert(top.$("#uiform").form('enableValidation').form('validate'));
    //alert(top.$("#uiform").form('validate'));
    //原来用的是这种方法 alert(top.$('#uiform').validate().form());

    if (top.$("#uiform").form('validate')) {
        var query = createParam('add', '0');
        //console.log(query);


        var temp = top.$(".mymy :input").serializeArray();//serializeArray 要么选择form 要么 $(".mymy :input") 这样选择
        //先循环mymy下所有html元素
        var mymylenth = top.$(".mymy").length;//得到有几个.mymy
        // alert(mymylenth);
        var detailjson = "[";
        var j = 1;

        top.$(".mymy").each(function () {
            var tempzd = top.$(this).find(".zidaihz").combobox("getValues");//得到原始客户自带内容 xx,xx格式
            tempzd = tempzd.toString();
            top.$(this).find(".zidaihz").combobox("setValue", tempzd);//反设置回去 这里非常重要,把获得的1,2,3 当做字符看待,注意是setValue 不是setValues
            //处理画册内页自带汇总


            var tempzdhc = top.$(this).find(".zidaihchz").combobox("getValues");//得到原始客户自带内容 xx,xx格式
            tempzdhc = tempzdhc.toString();
            top.$(this).find(".zidaihchz").combobox("setValue", tempzdhc);//反设置回去 这里非常重要,把获得的1,2,3 当做字符看待,注意是setValue 不是setValues

            console.log("tempzd" + tempzd);
            var temp2 = top.$(this).find(":input").serializeArray();
            //var temp3 = top.$(this).find(":input").serialize();

            var jsonstr = JSON.stringify(convertArrayNew(temp2));
            console.log("jsonstr:" + jsonstr);



            //console.log("序列化后得到的jsonstr：" + jsonstr);
            //var jsono = JSON.parse(jsonstr);//把json转化为组件
            //console.log(tempzd);
            //jsono.zidaihz = tempzd;//重新给客户自带赋值(因为值x,x,x json以为是数组,所以先替换为# 然后去数据库里处理)
            //jsonstr = JSON.stringify(jsono);//重新解析为json字符


            if (j < mymylenth) {
                detailjson = detailjson + '{"d":' + jsonstr + '},';
            } else {
                detailjson = detailjson + '{"d":' + jsonstr + '}';

            }


            //alert(top.$(this).html())
            j = j + 1;
            //alert("转化后是："+JSON.stringify(convertArray(temp2)));
        });
        detailjson = detailjson + ']';

        console.log("得到的json数据是：" + detailjson);


        //console.log("没有经过转化前：" + temp);
        //alert(temp);
        //console.log("序列化后是：" + JSON.stringify(convertArray(temp)));//转化
        //alert(JSON.stringify(convertArray(temp)));
        //return false;
        jQuery.ajaxjson(actionURL, query + "&d=" + detailjson + "", function (d) {
            if (parseInt(d) > 0) {
                msg.ok('添加成功！');
                dialog.dialog('close');
                grid.reload();
            } else {
                MessageOrRedirect(d);
            }
        });
    } else {
        msg.warning('请填写必填项！');
        console.log("表单name是：" + top.$("#uiform").find(".validatebox-invalid"));
    }
    //msg.warning('请填写必填项！');
}

//function toPdf() {
//    console.log("好好好");
//    var body = $('#list').datagrid('toArray');
//    var docDefinition = {
//        content: [{
//            table: {
//                headerRows: 1,
//                widths: ['*', '*', '*', '*', 'auto', '*'],
//                body: body
//            }
//        }]
//    };
//    pdfMake.createPdf(docDefinition).open();
//}

//流程单模块化
function liuchengdan(dialog, row) {
    //alert(top.$("#uiform").form('enableValidation').form('validate'));
    //alert(top.$("#uiform").form('validate'));
    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
    if (top.$("#uiform").form('validate')) {

        var query = createParam('editlcd', row.KeyId);
        //console.log("编辑时query:" + query);
        //alert(query);


        var temp = top.$(".mymy :input").serializeArray();//serializeArray 要么选择form 要么 $(".mymy :input") 这样选择
        //先循环mymy下所有html元素
        var mymylenth = top.$(".mymy").length;//得到有几个.mymy
        //alert(mymylenth);
        var detailjson = "[";
        var j = 1;

        top.$(".mymy").each(function () {
            var temp2 = top.$(this).find(":input").serializeArray();
            var jsonstr = JSON.stringify(convertArrayNew(temp2));


            if (j < mymylenth) {
                detailjson = detailjson + '{"d":' + jsonstr + '},';
            } else {
                detailjson = detailjson + '{"d":' + jsonstr + '}';
            }


            //alert(top.$(this).html())
            j = j + 1;
            //alert("转化后是："+JSON.stringify(convertArray(temp2)));
        });
        detailjson = detailjson + ']';

        console.log("得到的json数据是：" + detailjson);


        jQuery.ajaxjson(actionURL, query + "&d=" + detailjson, function (d) {
            if (d.status > 0) {
                msg.ok('修改成功！');
                //hDialog.dialog('close');
                grid.reload();
            } else {
                MessageOrRedirect(d);
            }
        });
    } else {
        msg.warning('请填写必填项！');
        console.log("表单是：" + top.$("#uiform").find(".validatebox-invalid") + "组件html是:" + top.$("#uiform").find(".validatebox-invalid").html());
    }
    //msg.warning('请填写必填项！');

}



var CRUD = {
    add: function () {
        var hDialogadd = top.jQuery.hDialog({
            title: '接件开单', width: '80%', height: '80%', href: formurl, iconCls: 'icon-add',
            toolbar: [{
                text: '新增客户',
                iconCls: 'icon-edit',
                handler: function () {

                    //逻辑写这里
                    //打开一个弹窗新增客户
                    var editDialog = top.jQuery.hDialog({
                        title: '新增客户', width: '400px', height: '400px', href: '/JydUser/html/JydUserminiAdd.html', iconCls: 'icon-add',
                        submit: function () {//提交时
                            var tempclient = top.$("#miniclient :input").serializeArray();//serializeArray 要么选择form 要么 $(".mymy :input") 这样选择
                            //console.log("序列化客户结果:"+JSON.stringify(tempclient));
                            var tempo = {};
                            var tempquery = top.$('#miniclient').serializeArray();//把表单序列化成json格式
                            tempquery = convertArray(tempquery);//把以上json转化成数组
                            tempo.jsonEntity = JSON.stringify(tempquery);
                            tempo.action = "add";
                            tempo.keyid = "0";
                            console.log("得到的json:" + JSON.stringify(tempo));

                            jQuery.ajaxjson('/JydUser/ashx/JydUserHandler.ashx', "json=" + JSON.stringify(tempo) + "", function (d) {
                                if (parseInt(d) > 0) {
                                    msg.ok('客户添加成功！');
                                    editDialog.dialog('close');//关闭当前窗口
                                } else {
                                    top.$.messager.alert('警告', '添加失败,客户名称重复!!!!请仔细检查客户名称!!!');

                                }
                            });

                        }
                    });

                }
            }],

            buttons: [{
                text: '确定',
                iconCls: 'icon-ok',
                handler: function () {
                    kaidan(hDialogadd);//模块化开单,目的是可以重用
                    //alert(d);
                }
            },
            //开完单不能打印,点接件单才能打印
            //{
            //text: '打印',
            //    iconCls: 'icon-printer_start',
            //handler: function () {

            //}
            //},

            {
                text: '关闭',
                iconCls: 'icon-cancel',
                handler: function () {
                    hDialogadd.dialog("close");
                }
            }],
            onLoad: function () {
                top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器

                init();//接件开单渲染
            },
            submit: function () {

                return false;
            },
            tijiao: function () {
                alert("提交");
            }
        });

        top.$('#uiform').validate();
    },
    edit: function () {//修改接件单
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: 'JYD' + row.OrderID + '接件单', width: '70%', height: '80%', href: formurl, iconCls: 'icon-save',

                buttons: [{
                    text: '确定',
                    iconCls: 'icon-ok',
                    handler: function () {
                        console.log("点击了确定");
                        //alert(top.$("#uiform").form('enableValidation').form('validate'));
                        //alert(top.$("#uiform").form('validate'));
                        //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                        if (top.$("#uiform").form('validate')) {

                            var query = createParam('edit', row.KeyId);
                            console.log("编辑时query:" + query);
                            //alert(query);


                            var temp = top.$(".mymy :input").serializeArray();//serializeArray 要么选择form 要么 $(".mymy :input") 这样选择
                            //先循环mymy下所有html元素
                            var mymylenth = top.$(".mymy").length;//得到有几个.mymy
                            //alert(mymylenth);
                            var detailjson = "[";
                            var j = 1;


                            top.$(".mymy").each(function () {//循环解析mymy
                                //alert($(this).html());
                                var tempzd = top.$(this).find(".zidaihz").combobox("getValues");//得到原始客户自带内容 xx,xx格式
                                tempzd = tempzd.toString();
                                //alert(tempzd);
                                top.$(this).find(".zidaihz").combobox("setValue", tempzd);//反设置回去 这里非常重要,把获得的1,2,3 当做字符看待,注意是setValue 不是setValues
                                //处理画册内页自带汇总


                                var tempzdhc = top.$(this).find(".zidaihchz").combobox("getValues");//得到原始客户自带内容 xx,xx格式
                                tempzdhc = tempzdhc.toString();
                                //alert("画册内页" + tempzdhc);
                                top.$(this).find(".zidaihchz").combobox("setValue", tempzdhc);//反设置回去 这里非常重要,把获得的1,2,3 当做字符看待,注意是setValue 不是setValues


                                var temp2 = top.$(this).find(":input").serializeArray();
                                var jsonstr = JSON.stringify(convertArrayNew(temp2));
                                
                                if (j < mymylenth) {
                                    detailjson = detailjson + '{"d":' + jsonstr + '},';
                                } else {
                                    detailjson = detailjson + '{"d":' + jsonstr + '}';

                                }
                                
                                //alert(top.$(this).html())
                                j = j + 1;
                                //alert("转化后是："+JSON.stringify(convertArray(temp2)));
                            });
                            detailjson = detailjson + ']';

                            console.log("得到的json数据是：" + detailjson);


                            jQuery.ajaxjson(actionURL, query + "&d=" + detailjson, function (d) {
                                if (d.status > 0) {
                                    msg.ok('修改成功！');
                                    //hDialog.dialog('close');
                                    grid.reload();
                                } else {
                                    MessageOrRedirect(d);
                                    //top.$.messager.alert('注意', d.msg);
                                }
                            });
                        } else {
                            console.log("表单验证未通过");
                            console.log("表单name是：" + top.$("#uiform").find(".validatebox-invalid").prop("name"));//此样式为未校验通过标签组)
                        }
                        //msg.warning('请填写必填项！');
                        return false;
                    }
                },

                {
                    text: '打印',
                    iconCls: 'icon-printer_start',
                    handler: function () {
                        //print_printdy(row.KeyId, row.OrderID);//打印订单 
                        print_printdy('打印接件单JYD' + row.OrderID + '', '/JydWodrmb/print_printdy.aspx?KeyId=' + row.KeyId + '&OrderID=' + row.OrderID + '');
                    }
                },

                {
                    text: '关闭',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        hDialog.dialog("close");
                    }
                }],
                onLoad: function () {
                    //init();//初始化


                    top.$.ajax({
                        type: "POST",
                        url: '/JydOrderDetail/ashx/JydOrderDetailHandler.ashx?filter={"groupOp":"AND","rules":[{"field":"orderid","op":"eq","data":"' + row.OrderID + '"}],"groups":[]}',
                        data: {},
                        dataType: "json",
                        async: true,    //或false,是否异步
                        beforeSend: function () { },
                        complete: function () { },
                        success: function (d) {
                            console.log("加载字表数据：" + JSON.stringify(d));
                            console.log("字表个数：" + d.rows.length);

                            for (var i = 1; i <= d.rows.length; i++) {
                                init();//渲染完成
                            }

                            console.log(top.$(".KeyId").length + "keyid个数");

                            for (var i = 0; i < d.rows.length; i++) {


                                //修改接件单检测到画册时自动显示内页
                                if (d.rows[i].PrintType === '画册') {
                                    console.log("检测到画册");
                                    var f = top.$('.mymy').eq(i);//找到最近的mymy父元素
                                    console.log("找到的父元素是：" + f);
                                    var c = top.$(f).children(".myhc");//找到最近的这个子元素
                                    top.$(c).show();
                                }

                                //top.$(".mymy").eq(i).find('.fumoa').textbox('setValue', d.rows[i].fumoa);//这种写法是标准的

                                top.$('.KeyId').eq(i).val(d.rows[i].KeyId);
                                top.$('.orderid').eq(i).textbox('setValue', d.rows[i].orderid);
                                top.$('.omainid').eq(i).textbox('setValue', d.rows[i].omainid);
                                top.$('.yspmc').eq(i).textbox('setValue', d.rows[i].yspmc);
                                top.$('.PrintType').eq(i).textbox('setValue', d.rows[i].PrintType);
                                top.$('.cpcc').eq(i).textbox('setValue', d.rows[i].cpcc);
                                top.$('.zzf').eq(i).textbox('setValue', d.rows[i].zzf);
                                top.$('.zzn').eq(i).textbox('setValue', d.rows[i].zzn);
                                top.$('.ym').eq(i).textbox('setValue', d.rows[i].ym);
                                top.$('.sj').eq(i).textbox('setValue', d.rows[i].sj);
                                //top.$('.rj').eq(i).textbox('setValue', d.rows[i].rj);
                                //top.$('.jp').eq(i).textbox('setValue', d.rows[i].jp);
                                top.$('.zz').eq(i).val(d.rows[i].zz);
                                if (d.rows[i].zz == "自带") {
                                    top.$('.zz').eq(i).attr("checked", true);
                                    //console.log("判断纸张自带！");
                                }
                                if (d.rows[i].jp == "自带") {
                                    top.$('.jp').eq(i).attr("checked", true);
                                    //console.log("判断纸张自带！");
                                }
                                if (d.rows[i].rj == "自带") {
                                    top.$('.rj').eq(i).attr("checked", true);
                                    //console.log("判断纸张自带！");
                                }

                                //top.$('.zidaihz').eq(i).combobox('setValues', d.rows[i].zidaihz.split(','));

                                top.$('.sl').eq(i).textbox('setValue', d.rows[i].sl);
                                top.$('.cc').eq(i).textbox('setValue', d.rows[i].cc);
                                top.$('.dj').eq(i).textbox('setValue', d.rows[i].dj);
                                top.$('.je').eq(i).textbox('setValue', d.rows[i].je);
                                top.$('.note').eq(i).textbox('setValue', d.rows[i].note);
                                top.$('.kzcc').eq(i).textbox('setValue', d.rows[i].kzcc);
                                top.$('.zs').eq(i).textbox('setValue', d.rows[i].zs);
                                top.$('.fs').eq(i).textbox('setValue', d.rows[i].fs);
                                top.$('.pss').eq(i).textbox('setValue', d.rows[i].pss);
                                top.$('.pst').eq(i).textbox('setValue', d.rows[i].pst);
                                top.$('.fbyq').eq(i).textbox('setValue', d.rows[i].fbyq);
                                top.$('.jx').eq(i).textbox('setValue', d.rows[i].jx);
                                top.$('.fumoa').eq(i).textbox('setValue', d.rows[i].fumoa);
                                top.$('.fumob').eq(i).textbox('setValue', d.rows[i].fumob);
                                top.$('.yaa').eq(i).textbox('setValue', d.rows[i].yaa);
                                top.$('.yab').eq(i).textbox('setValue', d.rows[i].yab);
                                top.$('.tana').eq(i).textbox('setValue', d.rows[i].tana);
                                top.$('.tanb').eq(i).textbox('setValue', d.rows[i].tanb);
                                top.$('.zheye').eq(i).textbox('setValue', d.rows[i].zheye);
                                top.$('.uva').eq(i).textbox('setValue', d.rows[i].uva);
                                top.$('.uvb').eq(i).textbox('setValue', d.rows[i].uvb);
                                top.$('.zd').eq(i).textbox('setValue', d.rows[i].zd);
                                top.$('.db').eq(i).textbox('setValue', d.rows[i].db);
                                top.$('.bh').eq(i).textbox('setValue', d.rows[i].bh);
                                top.$('.sh').eq(i).textbox('setValue', d.rows[i].sh);
                                top.$('.sk').eq(i).textbox('setValue', d.rows[i].sk);
                                top.$('.sjwcsj').eq(i).textbox('setValue', d.rows[i].sjwcsj);
                                top.$('.cpwcsj').eq(i).textbox('setValue', d.rows[i].cpwcsj);
                                top.$('.ctpsj').eq(i).textbox('setValue', d.rows[i].ctpsj);
                                top.$('.sbwcsj').eq(i).textbox('setValue', d.rows[i].sbwcsj);
                                top.$('.qslsj').eq(i).textbox('setValue', d.rows[i].qslsj);
                                top.$('.yswcsj').eq(i).textbox('setValue', d.rows[i].yswcsj);
                                top.$('.hjgkssj').eq(i).textbox('setValue', d.rows[i].hjgkssj);
                                top.$('.hjgwcsj').eq(i).textbox('setValue', d.rows[i].hjgwcsj);
                                top.$('.wjgjcsj').eq(i).textbox('setValue', d.rows[i].wjgjcsj);
                                top.$('.qcpsj').eq(i).textbox('setValue', d.rows[i].qcpsj);
                                top.$('.zdkssj').eq(i).textbox('setValue', d.rows[i].zdkssj);
                                top.$('.dzwcsj').eq(i).textbox('setValue', d.rows[i].dzwcsj);
                                top.$('.shryccsj').eq(i).textbox('setValue', d.rows[i].shryccsj);
                                top.$('.shryhcsj').eq(i).textbox('setValue', d.rows[i].shryhcsj);
                                top.$('.dbsj').eq(i).textbox('setValue', d.rows[i].dbsj);
                                top.$('.rksj').eq(i).textbox('setValue', d.rows[i].rksj);
                                top.$('.cksj').eq(i).textbox('setValue', d.rows[i].cksj);
                                top.$('.zrr').eq(i).textbox('setValue', d.rows[i].zrr);
                                top.$('.kkyy').eq(i).textbox('setValue', d.rows[i].kkyy);
                                top.$('.sxj').eq(i).textbox('setValue', d.rows[i].sxj);
                                top.$('.szp').eq(i).textbox('setValue', d.rows[i].szp);
                                top.$('.zph').eq(i).textbox('setValue', d.rows[i].zph);
                                top.$('.myws').eq(i).textbox('setValue', d.rows[i].myws);
                                top.$('.myysf').eq(i).textbox('setValue', d.rows[i].myysf);
                                top.$('.O_myhjg').eq(i).textbox('setValue', d.rows[i].O_myhjg);
                                top.$('.qianshou').eq(i).textbox('setValue', d.rows[i].qianshou);
                                top.$('.skje').eq(i).textbox('setValue', d.rows[i].skje);
                                top.$('.wsjgbz').eq(i).textbox('setValue', d.rows[i].wsjgbz);
                                top.$('.dddd').eq(i).textbox('setValue', d.rows[i].dddd);
                                top.$('.kkkje').eq(i).textbox('setValue', d.rows[i].kkkje);
                                top.$('.yspmcb').eq(i).textbox('setValue', d.rows[i].yspmcb);
                                top.$('.FileTypeb').eq(i).textbox('setValue', d.rows[i].FileTypeb);
                                top.$('.mianb').eq(i).textbox('setValue', d.rows[i].mianb);
                                top.$('.munb').eq(i).textbox('setValue', d.rows[i].munb);
                                top.$('.colorb').eq(i).textbox('setValue', d.rows[i].colorb);
                                top.$('.pagerb').eq(i).textbox('setValue', d.rows[i].pagerb);
                                top.$('.O_my_jpb').eq(i).textbox('setValue', d.rows[i].O_my_jpb);
                                top.$('.O_my_zzb').eq(i).textbox('setValue', d.rows[i].O_my_zzb);
                                top.$('.ddddb').eq(i).textbox('setValue', d.rows[i].ddddb);
                                top.$('.zznb').eq(i).textbox('setValue', d.rows[i].zznb);
                                top.$('.ymb').eq(i).textbox('setValue', d.rows[i].ymb);
                                top.$('.sjb').eq(i).textbox('setValue', d.rows[i].sjb);
                                top.$('.rjb').eq(i).textbox('setValue', d.rows[i].rjb);
                                top.$('.jpb').eq(i).textbox('setValue', d.rows[i].jpb);
                                top.$('.zzb').eq(i).textbox('setValue', d.rows[i].zzb);
                                //alert("印刷品类型"+d.rows[i].PrintType + "+印刷品名称+"+d.rows[i].yspmc);
                                //var s = "abc,abcd,aaa";
                                //var ss = s.split(",");// 在每个逗号(,)处进行分解  ["abc", "abcd", "aaa"]
                                //console.log("ssssss::::"+ss);
                                //alert(d.rows[i].zidaihz);
                                if (d.rows[i].zidaihz !== '' && d.rows[i].zidaihz !== null) {
                                    if (d.rows[i].zidaihz.indexOf(',') === -1) {//没有包含,号
                                        top.$('.zidaihz').eq(i).combobox('setValue', d.rows[i].zidaihz);
                                    } else {//如果包含,号转换成数组
                                        top.$('.zidaihz').eq(i).combobox('setValues', d.rows[i].zidaihz.split(","));
                                    }
                                }






                                if (d.rows[i].PrintType === '画册') {//只有画册的时候才显示画册内页工艺
                                    //没渲染，可能会报错
                                    //alert("灌进：内页："+d.rows[i].zidaihchz);
                                    //top.$('.zidaihchz').eq(i).combobox('setValue', d.rows[i].zidaihchz);//客户自带汇总 内页
                                    if (d.rows[i].zidaihchz !== '' && d.rows[i].zidaihchz !== null) {
                                        if (d.rows[i].zidaihchz.indexOf(',') === -1) {//没有包含,号
                                            top.$('.zidaihchz').eq(i).combobox('setValue', d.rows[i].zidaihchz);
                                        } else {//如果包含,号转换成数组
                                            top.$('.zidaihchz').eq(i).combobox('setValues', d.rows[i].zidaihchz.split(","));
                                        }
                                    }


                                    top.$('.fumoab').eq(i).combobox('setValue', d.rows[i].fumoab);
                                    top.$('.fumobb').eq(i).combobox('setValue', d.rows[i].fumobb);
                                    top.$('.yaab').eq(i).combobox('setValue', d.rows[i].yaab);
                                    top.$('.yabb').eq(i).combobox('setValue', d.rows[i].yabb);
                                    top.$('.tanab').eq(i).combobox('setValue', d.rows[i].tanab);
                                    top.$('.tanbb').eq(i).combobox('setValue', d.rows[i].tanbb);
                                    top.$('.zheyeb').eq(i).combobox('setValue', d.rows[i].zheyeb);
                                    top.$('.uvab').eq(i).combobox('setValue', d.rows[i].uvab);
                                    top.$('.uvbb').eq(i).combobox('setValue', d.rows[i].uvbb);
                                    top.$('.zdb').eq(i).combobox('setValue', d.rows[i].zdb);
                                    top.$('.dbb').eq(i).combobox('setValue', d.rows[i].dbb);
                                    top.$('.bhb').eq(i).combobox('setValue', d.rows[i].bhb);
                                    top.$('.shb').eq(i).combobox('setValue', d.rows[i].shb);
                                    top.$('.skb').eq(i).combobox('setValue', d.rows[i].skb);
                                    top.$('.PrintTypeb').eq(i).textbox('setValue', d.rows[i].PrintTypeb);
                                    top.$('.zzfb').eq(i).textbox('setValue', d.rows[i].zzfb);
                                    top.$('.noteb').eq(i).textbox('setValue', d.rows[i].noteb);
                                    top.$('.cpccb').eq(i).textbox('setValue', d.rows[i].cpccb);
                                }

                                top.$('.printyes').eq(i).textbox('setValue', d.rows[i].printyes);
                                top.$('.kzccb').eq(i).textbox('setValue', d.rows[i].kzccb);
                                top.$('.zsb').eq(i).textbox('setValue', d.rows[i].zsb);
                                top.$('.fsb').eq(i).textbox('setValue', d.rows[i].fsb);
                                top.$('.pssb').eq(i).textbox('setValue', d.rows[i].pssb);
                                top.$('.pstb').eq(i).textbox('setValue', d.rows[i].pstb);
                                top.$('.fbyqb').eq(i).textbox('setValue', d.rows[i].fbyqb);
                                top.$('.jxb').eq(i).textbox('setValue', d.rows[i].jxb);
                                top.$('.kkkjeb').eq(i).textbox('setValue', d.rows[i].kkkjeb);
                                top.$('.nysl').eq(i).textbox('setValue', d.rows[i].nysl);
                                top.$('.ctpa').eq(i).textbox('setValue', d.rows[i].ctpa);
                                top.$('.ctpb').eq(i).textbox('setValue', d.rows[i].ctpb);
                                top.$('.zhizhangbb').eq(i).textbox('setValue', d.rows[i].zhizhangbb);
                                top.$('.songhuo').eq(i).textbox('setValue', d.rows[i].songhuo);
                                top.$('.xiaoyang').eq(i).textbox('setValue', d.rows[i].xiaoyang);
                                top.$('.danwei_m').eq(i).combobox('setValue', d.rows[i].danwei_m);
                                top.$('.danwei_mb').eq(i).combobox('setValue', d.rows[i].danwei_mb);
                                top.$('.bz_hx').eq(i).textbox('setValue', d.rows[i].bz_hx);
                                top.$('.bz_wbz_cc').eq(i).textbox('setValue', d.rows[i].bz_wbz_cc);
                                top.$('.bz_wbz_gy').eq(i).textbox('setValue', d.rows[i].bz_wbz_gy);
                                top.$('.bz_nbz_zz').eq(i).textbox('setValue', d.rows[i].bz_nbz_zz);
                                top.$('.bz_nbz_cc').eq(i).textbox('setValue', d.rows[i].bz_nbz_cc);
                                top.$('.bz_nbz_gy').eq(i).textbox('setValue', d.rows[i].bz_nbz_gy);
                                top.$('.bz_wkz_zz').eq(i).textbox('setValue', d.rows[i].bz_wkz_zz);
                                top.$('.bz_wkz_cc').eq(i).textbox('setValue', d.rows[i].bz_wkz_cc);
                                top.$('.bz_wkz_gy').eq(i).textbox('setValue', d.rows[i].bz_wkz_gy);
                                top.$('.bz_bc_yl').eq(i).textbox('setValue', d.rows[i].bz_bc_yl);
                                top.$('.bz_bc_sl').eq(i).textbox('setValue', d.rows[i].bz_bc_sl);
                                top.$('.bz_bc_mx').eq(i).textbox('setValue', d.rows[i].bz_bc_mx);
                                top.$('.bz_nt').eq(i).textbox('setValue', d.rows[i].bz_nt);
                                top.$('.bz_nt_cc').eq(i).textbox('setValue', d.rows[i].bz_nt_cc);
                                top.$('.bz_nt_mx').eq(i).textbox('setValue', d.rows[i].bz_nt_mx);
                                top.$('.bz_wbz').eq(i).textbox('setValue', d.rows[i].bz_wbz);
                            }


                        }
                    });



                    top.$('#uiform').form('load', row);
                    console.log("接件单数：" + row.ds);
                    //alert(row.ds);
                    top.$("#ds").numberspinner({ value: row.ds, disabled: true });

                },
                submit: function () {

                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    editlcd: function () {//修改流程单
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: 'JYD' + row.OrderID + '流程单', width: '80%', height: '80%', href: '/JydOrder/html/JydOrderlcd.html', iconCls: 'icon-save',
                onLoad: function () {
                    //init();//初始化
                    console.log("主表row数据" + JSON.stringify(row));

                    top.$.ajax({
                        type: "POST",
                        url: '/JydOrderDetail/ashx/JydOrderDetailHandler.ashx?filter={"groupOp":"AND","rules":[{"field":"orderid","op":"eq","data":"' + row.OrderID + '"}],"groups":[]}',
                        data: {},
                        dataType: "json",
                        beforeSend: function () { },
                        complete: function () { },
                        success: function (d) {
                            //console.log("加载字表数据：" + JSON.stringify(d));
                            console.log("子元素个数：" + d.rows.length);

                            for (var i = 1; i <= d.rows.length; i++) {
                                //console.log("渲染次数：" + i + "值是:" + JSON.stringify(d.rows[i]));
                                //top.$('.mymy').form('load', d.rows[i]);
                                init();//渲染完成

                                //top.$('.mymy').eq(i).form('load', d.rows[i]);

                            }

                            for (var i = 0; i < d.rows.length; i++) {
                                //修改流程单检测到画册时自动显示内页
                                if (d.rows[i].PrintType === '画册') {
                                    console.log("检测到画册");
                                    var f = top.$('.mymy').eq(i);//找到最近的mymy父元素
                                    console.log("找到的父元素是：" + f);
                                    var c = top.$(f).children(".myhc");//找到最近的这个子元素
                                    top.$(c).show();
                                }


                                top.$('.KeyId').eq(i).val(d.rows[i].KeyId);
                                top.$('.orderid').eq(i).textbox('setValue', d.rows[i].orderid);
                                top.$('.omainid').eq(i).textbox('setValue', d.rows[i].omainid);
                                //top.$('.yspmc').eq(i).textbox('setValue', d.rows[i].yspmc);
                                top.$('.yspmcshow').eq(i).text(d.rows[i].yspmc);
                                //top.$('.PrintType').eq(i).textbox('setValue', d.rows[i].PrintType);
                                top.$('.PrintTypeshow').eq(i).text(d.rows[i].PrintType);//印刷品类型

                                //top.$('.cpcc').eq(i).textbox('setValue', d.rows[i].cpcc);
                                top.$('.cpccshow').eq(i).text(d.rows[i].cpcc);//成品尺寸
                                //top.$('.zzf').eq(i).textbox('setValue', d.rows[i].zzf);
                                top.$('.zzfshow').eq(i).text(d.rows[i].zzf);//纸张
                                top.$('.zzn').eq(i).textbox('setValue', d.rows[i].zzn);
                                //top.$('.ym').eq(i).textbox('setValue', d.rows[i].ym);
                                top.$('.ymshow').eq(i).text(d.rows[i].ym);//页码
                                //top.$('.sj').eq(i).textbox('setValue', d.rows[i].sj);
                                top.$('.sjshow').eq(i).text(d.rows[i].sj);//颜色
                                //top.$('.rj').eq(i).textbox('setValue', d.rows[i].rj);
                                //top.$('.jp').eq(i).textbox('setValue', d.rows[i].jp);
                                //top.$('.zz').eq(i).val(d.rows[i].zz);
                                if (d.rows[i].zz == "自带") {
                                    //top.$('.zz').eq(i).attr("checked", true);
                                    //console.log("判断纸张自带！");
                                    var pt = top.$('.khzdshow').eq(i).text();
                                    top.$('.khzdshow').eq(i).text(pt + "纸张");
                                }
                                if (d.rows[i].jp == "自带") {
                                    //top.$('.jp').eq(i).attr("checked", true);
                                    //console.log("判断纸张自带！");
                                    var pt = top.$('.khzdshow').eq(i).text();
                                    top.$('.khzdshow').eq(i).text(pt + "CPT");
                                }
                                if (d.rows[i].rj == "自带") {
                                    //top.$('.rj').eq(i).attr("checked", true);
                                    var pt = top.$('.khzdshow').eq(i).text();
                                    top.$('.khzdshow').eq(i).text(pt + "软件");
                                    //console.log("判断纸张自带！");
                                }
                                //兼容老系统，如果没有则吧客户自带汇总加上去
                                var pt = top.$('.khzdshow').eq(i).text();
                                top.$('.khzdshow').eq(i).text(pt + d.rows[i].zidaihz);

                                //top.$('.sl').eq(i).textbox('setValue', d.rows[i].sl);
                                top.$('.slshow').eq(i).text(d.rows[i].sl);//数量
                                top.$('.cc').eq(i).textbox('setValue', d.rows[i].cc);
                                top.$('.dj').eq(i).textbox('setValue', d.rows[i].dj);
                                top.$('.je').eq(i).textbox('setValue', d.rows[i].je);
                                top.$('.note').eq(i).textbox('setValue', d.rows[i].note);
                                //处理画册内页
                                top.$('.yspmcbshow').eq(i).text(d.rows[i].yspmcb);
                                top.$('.danwei_mbshow').eq(i).text(d.rows[i].danwei_mb);
                                top.$('.zzfbshow').eq(i).text(d.rows[i].zzfb);
                                top.$('.nyslshow').eq(i).text(d.rows[i].nysl);
                                top.$('.cpccbshow').eq(i).text(d.rows[i].cpccb);
                                top.$('.sjbshow').eq(i).text(d.rows[i].sjb);
                                top.$('.ymbshow').eq(i).text(d.rows[i].ymb);
                                top.$('.zidaihchzshow').eq(i).text(d.rows[i].zidaihchz);


                                top.$('.kzcc').eq(i).textbox('setValue', d.rows[i].kzcc);
                                top.$('.zs').eq(i).textbox('setValue', d.rows[i].zs);
                                top.$('.fs').eq(i).textbox('setValue', d.rows[i].fs);
                                top.$('.pss').eq(i).textbox('setValue', d.rows[i].pss);
                                top.$('.pst').eq(i).textbox('setValue', d.rows[i].pst);


                                top.$('.fbyq').eq(i).combobox('setValue', d.rows[i].fbyq);

                                //渲染机型已经移到统一接口
                                top.$('.jx').eq(i).combobox('setValue', d.rows[i].jx);


                                //top.$('.jx').eq(i).textbox('setValue', d.rows[i].jx);
                                top.$('.fumoa').eq(i).textbox('setValue', d.rows[i].fumoa);
                                top.$('.fumob').eq(i).textbox('setValue', d.rows[i].fumob);
                                top.$('.yaa').eq(i).textbox('setValue', d.rows[i].yaa);
                                top.$('.yab').eq(i).textbox('setValue', d.rows[i].yab);
                                top.$('.tana').eq(i).textbox('setValue', d.rows[i].tana);
                                top.$('.tanb').eq(i).textbox('setValue', d.rows[i].tanb);
                                top.$('.zheye').eq(i).textbox('setValue', d.rows[i].zheye);
                                top.$('.uva').eq(i).textbox('setValue', d.rows[i].uva);
                                top.$('.uvb').eq(i).textbox('setValue', d.rows[i].uvb);
                                top.$('.zd').eq(i).textbox('setValue', d.rows[i].zd);
                                top.$('.db').eq(i).textbox('setValue', d.rows[i].db);
                                top.$('.bh').eq(i).textbox('setValue', d.rows[i].bh);
                                top.$('.sh').eq(i).textbox('setValue', d.rows[i].sh);
                                top.$('.sk').eq(i).textbox('setValue', d.rows[i].sk);
                                top.$('.sjwcsj').eq(i).textbox('setValue', d.rows[i].sjwcsj);
                                top.$('.cpwcsj').eq(i).textbox('setValue', d.rows[i].cpwcsj);
                                top.$('.ctpsj').eq(i).textbox('setValue', d.rows[i].ctpsj);
                                top.$('.sbwcsj').eq(i).textbox('setValue', d.rows[i].sbwcsj);
                                top.$('.qslsj').eq(i).textbox('setValue', d.rows[i].qslsj);
                                top.$('.yswcsj').eq(i).textbox('setValue', d.rows[i].yswcsj);
                                top.$('.hjgkssj').eq(i).textbox('setValue', d.rows[i].hjgkssj);
                                top.$('.hjgwcsj').eq(i).textbox('setValue', d.rows[i].hjgwcsj);
                                top.$('.wjgjcsj').eq(i).textbox('setValue', d.rows[i].wjgjcsj);
                                top.$('.qcpsj').eq(i).textbox('setValue', d.rows[i].qcpsj);
                                top.$('.zdkssj').eq(i).textbox('setValue', d.rows[i].zdkssj);
                                top.$('.dzwcsj').eq(i).textbox('setValue', d.rows[i].dzwcsj);
                                top.$('.shryccsj').eq(i).textbox('setValue', d.rows[i].shryccsj);
                                top.$('.shryhcsj').eq(i).textbox('setValue', d.rows[i].shryhcsj);
                                top.$('.dbsj').eq(i).textbox('setValue', d.rows[i].dbsj);
                                top.$('.rksj').eq(i).textbox('setValue', d.rows[i].rksj);
                                top.$('.cksj').eq(i).textbox('setValue', d.rows[i].cksj);
                                top.$('.zrr').eq(i).textbox('setValue', d.rows[i].zrr);
                                top.$('.kkyy').eq(i).textbox('setValue', d.rows[i].kkyy);
                                top.$('.sxj').eq(i).textbox('setValue', d.rows[i].sxj);
                                top.$('.szp').eq(i).textbox('setValue', d.rows[i].szp);
                                top.$('.zph').eq(i).textbox('setValue', d.rows[i].zph);
                                top.$('.myws').eq(i).textbox('setValue', d.rows[i].myws);
                                top.$('.myysf').eq(i).textbox('setValue', d.rows[i].myysf);
                                top.$('.O_myhjg').eq(i).textbox('setValue', d.rows[i].O_myhjg);
                                top.$('.qianshou').eq(i).textbox('setValue', d.rows[i].qianshou);
                                top.$('.skje').eq(i).textbox('setValue', d.rows[i].skje);
                                top.$('.wsjgbz').eq(i).textbox('setValue', d.rows[i].wsjgbz);
                                top.$('.dddd').eq(i).textbox('setValue', d.rows[i].dddd);
                                top.$('.kkkje').eq(i).textbox('setValue', d.rows[i].kkkje);
                                top.$('.yspmcb').eq(i).textbox('setValue', d.rows[i].yspmcb);
                                top.$('.FileTypeb').eq(i).textbox('setValue', d.rows[i].FileTypeb);
                                top.$('.mianb').eq(i).textbox('setValue', d.rows[i].mianb);
                                top.$('.munb').eq(i).textbox('setValue', d.rows[i].munb);
                                top.$('.colorb').eq(i).textbox('setValue', d.rows[i].colorb);
                                top.$('.pagerb').eq(i).textbox('setValue', d.rows[i].pagerb);
                                top.$('.O_my_jpb').eq(i).textbox('setValue', d.rows[i].O_my_jpb);
                                top.$('.O_my_zzb').eq(i).textbox('setValue', d.rows[i].O_my_zzb);
                                top.$('.ddddb').eq(i).textbox('setValue', d.rows[i].ddddb);
                                top.$('.zznb').eq(i).textbox('setValue', d.rows[i].zznb);
                                top.$('.ymb').eq(i).textbox('setValue', d.rows[i].ymb);
                                top.$('.sjb').eq(i).textbox('setValue', d.rows[i].sjb);
                                top.$('.rjb').eq(i).textbox('setValue', d.rows[i].rjb);
                                top.$('.jpb').eq(i).textbox('setValue', d.rows[i].jpb);
                                top.$('.zzb').eq(i).textbox('setValue', d.rows[i].zzb);
                                top.$('.fumoab').eq(i).textbox('setValue', d.rows[i].fumoab);
                                top.$('.fumobb').eq(i).textbox('setValue', d.rows[i].fumobb);
                                top.$('.yaab').eq(i).textbox('setValue', d.rows[i].yaab);
                                top.$('.yabb').eq(i).textbox('setValue', d.rows[i].yabb);
                                top.$('.tanab').eq(i).textbox('setValue', d.rows[i].tanab);
                                top.$('.tanbb').eq(i).textbox('setValue', d.rows[i].tanbb);
                                top.$('.zheyeb').eq(i).textbox('setValue', d.rows[i].zheyeb);
                                top.$('.uvab').eq(i).textbox('setValue', d.rows[i].uvab);
                                top.$('.uvbb').eq(i).textbox('setValue', d.rows[i].uvbb);
                                top.$('.zdb').eq(i).textbox('setValue', d.rows[i].zdb);
                                top.$('.dbb').eq(i).textbox('setValue', d.rows[i].dbb);
                                top.$('.bhb').eq(i).textbox('setValue', d.rows[i].bhb);
                                top.$('.shb').eq(i).textbox('setValue', d.rows[i].shb);
                                top.$('.skb').eq(i).textbox('setValue', d.rows[i].skb);
                                top.$('.PrintTypeb').eq(i).textbox('setValue', d.rows[i].PrintTypeb);
                                top.$('.zzfb').eq(i).textbox('setValue', d.rows[i].zzfb);
                                top.$('.noteb').eq(i).textbox('setValue', d.rows[i].noteb);
                                top.$('.cpccb').eq(i).textbox('setValue', d.rows[i].cpccb);
                                top.$('.printyes').eq(i).textbox('setValue', d.rows[i].printyes);
                                top.$('.kzccb').eq(i).textbox('setValue', d.rows[i].kzccb);
                                top.$('.zsb').eq(i).textbox('setValue', d.rows[i].zsb);
                                top.$('.fsb').eq(i).textbox('setValue', d.rows[i].fsb);
                                top.$('.pssb').eq(i).textbox('setValue', d.rows[i].pssb);
                                top.$('.pstb').eq(i).textbox('setValue', d.rows[i].pstb);
                                top.$('.fbyqb').eq(i).textbox('setValue', d.rows[i].fbyqb);
                                top.$('.jxb').eq(i).textbox('setValue', d.rows[i].jxb);
                                top.$('.kkkjeb').eq(i).textbox('setValue', d.rows[i].kkkjeb);
                                top.$('.nysl').eq(i).textbox('setValue', d.rows[i].nysl);
                                top.$('.ctpa').eq(i).textbox('setValue', d.rows[i].ctpa);
                                top.$('.ctpb').eq(i).textbox('setValue', d.rows[i].ctpb);
                                top.$('.zhizhangbb').eq(i).textbox('setValue', d.rows[i].zhizhangbb);
                                top.$('.songhuo').eq(i).textbox('setValue', d.rows[i].songhuo);
                                //top.$('.xiaoyang').eq(i).textbox('setValue', d.rows[i].xiaoyang);//小样
                                //top.$('.danwei_m').eq(i).textbox('setValue', d.rows[i].danwei_m);
                                top.$('.danwei_mshow').eq(i).text(d.rows[i].danwei_m); //单位

                                top.$('.danwei_mb').eq(i).textbox('setValue', d.rows[i].danwei_mb);
                                top.$('.bz_hx').eq(i).textbox('setValue', d.rows[i].bz_hx);
                                top.$('.bz_wbz_cc').eq(i).textbox('setValue', d.rows[i].bz_wbz_cc);
                                top.$('.bz_wbz_gy').eq(i).textbox('setValue', d.rows[i].bz_wbz_gy);
                                top.$('.bz_nbz_zz').eq(i).textbox('setValue', d.rows[i].bz_nbz_zz);
                                top.$('.bz_nbz_cc').eq(i).textbox('setValue', d.rows[i].bz_nbz_cc);
                                top.$('.bz_nbz_gy').eq(i).textbox('setValue', d.rows[i].bz_nbz_gy);
                                top.$('.bz_wkz_zz').eq(i).textbox('setValue', d.rows[i].bz_wkz_zz);
                                top.$('.bz_wkz_cc').eq(i).textbox('setValue', d.rows[i].bz_wkz_cc);
                                top.$('.bz_wkz_gy').eq(i).textbox('setValue', d.rows[i].bz_wkz_gy);
                                top.$('.bz_bc_yl').eq(i).textbox('setValue', d.rows[i].bz_bc_yl);
                                top.$('.bz_bc_sl').eq(i).textbox('setValue', d.rows[i].bz_bc_sl);
                                top.$('.bz_bc_mx').eq(i).textbox('setValue', d.rows[i].bz_bc_mx);
                                top.$('.bz_nt').eq(i).textbox('setValue', d.rows[i].bz_nt);
                                top.$('.bz_nt_cc').eq(i).textbox('setValue', d.rows[i].bz_nt_cc);
                                top.$('.bz_nt_mx').eq(i).textbox('setValue', d.rows[i].bz_nt_mx);
                                top.$('.bz_wbz').eq(i).textbox('setValue', d.rows[i].bz_wbz);
                                //装填
                                top.$('.kzcc').eq(i).textbox('setValue', d.rows[i].kzcc);

                                //如果是画册则不必填
                                if (d.rows[i].PrintType !== "画册") {
                                    //alert("不必填");

                                    top.$('.jxb').eq(i).combobox({ required: false });
                                    top.$('.fbyqb').eq(i).combobox({ required: false });

                                    top.$(".kzccb").eq(i).textbox({
                                        required: false
                                    });//开纸尺寸,正数,放数,色,套
                                    top.$(".zsb").eq(i).textbox({
                                        required: false
                                    });//开纸尺寸,正数,放数,色,套
                                    top.$(".fsb").eq(i).textbox({
                                        required: false
                                    });//开纸尺寸,正数,放数,色,套
                                    top.$(".pssb").eq(i).textbox({
                                        required: false
                                    });//开纸尺寸,正数,放数,色,套
                                    top.$(".pstb").eq(i).textbox({
                                        required: false
                                    });//开纸尺寸,正数,放数,色,套
                                } else {

                                }


                                //加载完后渲染

                            }

                            initlc();//加载流程--完了之后加载流程
                            waisong();




                        }
                    });

                    //top.$('.mymy').form('load', {comname:'北京'});

                    //top.$('#txt_KeyId').numberspinner('setValue', row.KeyId);
                    //top.$('#txt_ServiceType').numberspinner('setValue', row.ServiceType);
                    //top.$('#txt_OrderYear').textbox('setValue', row.OrderYear);
                    //top.$('#txt_OrderInt').numberspinner('setValue', row.OrderInt);
                    //top.$('#txt_OrderID').textbox('setValue', row.OrderID);
                    //top.$('#txt_username').textbox('setValue', row.username);
                    //top.$('#txt_comname').textbox('setValue', row.comname);
                    //top.$('#txt_comnameid').numberspinner('setValue', row.comnameid);
                    //top.$('#txt_jpm').textbox('setValue', row.jpm);
                    //top.$('#txt_name').textbox('setValue', row.name);
                    //top.$('#txt_tel').textbox('setValue', row.tel);
                    //top.$('#txt_address').textbox('setValue', row.address);
                    //top.$('#txt_explain').textbox('setValue', row.explain);
                    //top.$('#txt_deliveryDate').datetimebox('setValue', row.deliveryDate);
                    //top.$('#txt_delivery').textbox('setValue', row.delivery);
                    //top.$('#txt_adddate').datetimebox('setValue', row.adddate);
                    //top.$('#txt_fk').numberspinner('setValue', row.fk);
                    //top.$('#txt_yf').numberspinner('setValue', row.yf);
                    //top.$('#txt_qt').numberspinner('setValue', row.qt);
                    //top.$('#txt_totalprice').numberspinner('setValue', row.totalprice);
                    //top.$('#txt_cfzt').textbox('setValue', row.cfzt);
                    //top.$('#txt_jjy').textbox('setValue', row.jjy);
                    //top.$('#txt_fumoa').textbox('setValue', row.fumoa);
                    //top.$('#txt_fumob').textbox('setValue', row.fumob);
                    //top.$('#txt_yaa').textbox('setValue', row.yaa);
                    //top.$('#txt_yab').textbox('setValue', row.yab);
                    //top.$('#txt_tana').textbox('setValue', row.tana);
                    //top.$('#txt_tanb').textbox('setValue', row.tanb);
                    //top.$('#txt_zheye').textbox('setValue', row.zheye);
                    //top.$('#txt_uva').textbox('setValue', row.uva);
                    //top.$('#txt_uvb').textbox('setValue', row.uvb);
                    //top.$('#txt_zd').textbox('setValue', row.zd);
                    //top.$('#txt_db').textbox('setValue', row.db);
                    //top.$('#txt_bh').textbox('setValue', row.bh);
                    //top.$('#txt_sh').textbox('setValue', row.sh);
                    //top.$('#txt_sk').textbox('setValue', row.sk);
                    //top.$('#txt_huofa').textbox('setValue', row.huofa);
                    //top.$('#txt_fahuoren').textbox('setValue', row.fahuoren);
                    //top.$('#txt_ys').textbox('setValue', row.ys);
                    //top.$('#txt_fy').textbox('setValue', row.fy);
                    //top.$('#txt_dha').textbox('setValue', row.dha);
                    //top.$('#txt_dhb').textbox('setValue', row.dhb);
                    //top.$('#txt_status').textbox('setValue', row.status);
                    //top.$('#txt_jexx').textbox('setValue', row.jexx);
                    //top.$('#txt_jedx').textbox('setValue', row.jedx);
                    //top.$('#txt_yufuk').numberspinner('setValue', row.yufuk);
                    //top.$('#txt_yufukuana').textbox('setValue', row.yufukuana);
                    //top.$('#txt_sckd').textbox('setValue', row.sckd);
                    //top.$('#txt_sjwcsj').textbox('setValue', row.sjwcsj);
                    //top.$('#txt_cpwcsj').textbox('setValue', row.cpwcsj);
                    //top.$('#txt_ctpsj').textbox('setValue', row.ctpsj);
                    //top.$('#txt_sbwcsj').textbox('setValue', row.sbwcsj);
                    //top.$('#txt_qslsj').textbox('setValue', row.qslsj);
                    //top.$('#txt_yswcsj').textbox('setValue', row.yswcsj);
                    //top.$('#txt_hjgkssj').textbox('setValue', row.hjgkssj);
                    //top.$('#txt_hjgwcsj').textbox('setValue', row.hjgwcsj);
                    //top.$('#txt_wjgjcsj').textbox('setValue', row.wjgjcsj);
                    //top.$('#txt_qcpsj').textbox('setValue', row.qcpsj);
                    //top.$('#txt_zdkssj').textbox('setValue', row.zdkssj);
                    //top.$('#txt_dzwcsj').textbox('setValue', row.dzwcsj);
                    //top.$('#txt_shryccsj').textbox('setValue', row.shryccsj);
                    //top.$('#txt_shryhcsj').textbox('setValue', row.shryhcsj);
                    //top.$('#txt_dbsj').textbox('setValue', row.dbsj);
                    //top.$('#txt_szp').textbox('setValue', row.szp);
                    //top.$('#txt_sxj').textbox('setValue', row.sxj);
                    //top.$('#txt_skje').numberspinner('setValue', row.skje);
                    //top.$('#txt_zph').textbox('setValue', row.zph);
                    //top.$('#txt_kkyy').textbox('setValue', row.kkyy);
                    //top.$('#txt_zrr').textbox('setValue', row.zrr);
                    //top.$('#txt_rksj').textbox('setValue', row.rksj);
                    //top.$('#txt_cksj').textbox('setValue', row.cksj);
                    //top.$('#txt_myws').textbox('setValue', row.myws);
                    //top.$('#txt_myysf').textbox('setValue', row.myysf);
                    //top.$('#txt_O_myhjg1').textbox('setValue', row.O_myhjg1);
                    //top.$('#txt_jgzysx').textbox('setValue', row.jgzysx);
                    //top.$('#txt_connman').textbox('setValue', row.connman);
                    //top.$('#txt_mystatus').numberspinner('setValue', row.mystatus);
                    //top.$('#txt_kkkje').numberspinner('setValue', row.kkkje);
                    //top.$('#txt_ds').numberspinner('setValue', row.ds);
                    //top.$('#txt_allyspmc').textbox('setValue', row.allyspmc);
                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');  

                    top.$('#uiform').form('load', row);

                    //流程单:接件数、客户名称、联系人、电话、接件日期、交货日期、接件员、业务员 不能修改
                    top.$("#ds").numberspinner({ value: row.ds, disabled: true });
                    top.$("#comname").combogrid({ disabled: true });
                    top.$("#connman").textbox({ disabled: true });
                    top.$("#tel").textbox({ disabled: true });
                    top.$("#deliveryDate").datetimebox({ disabled: true });
                    top.$("#adddate").datetimebox({
                        disabled: true,
                        value: row.adddate
                    });
                    top.$("#ywyid").textbox({ disabled: true });
                    top.$("#jjy").textbox({ disabled: true });


                },
                buttons: [{
                    text: '确定',
                    iconCls: 'icon-ok',
                    handler: function () {
                        liuchengdan(hDialog, row);//流程单模块化 方便重用
                        //alert(d);
                    }
                },
                //开完单不能打印,点接件单才能打印
                {
                    text: '打印',
                    iconCls: 'icon-printer_start',
                    handler: function () {
                        //打印前保存 liuchengdan(hDialog, row);//流程单模块化 方便重用
                        print_printdy('打印流程单JYD' + row.OrderID + '', '/JydWodrmb/print_pringbylcd.aspx?KeyId=' + row.KeyId + '&OrderID=' + row.OrderID + '');
                    }
                },

                {
                    text: '关闭',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        hDialog.dialog("close");
                    }
                }],
                submit: function () {

                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

//实现动态隐藏列
var cmenu = null;
function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#list').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#list').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#list').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#list').datagrid('getColumnOption', field);
        cmenu.menu('appendItem', {
            text: col.title,
            name: field,
            iconCls: 'icon-ok'
        });
    }
}

//实现动态隐藏列结束

//双击编辑表单焦点消失后保存
var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#list').datagrid('validateRow', editIndex)) {
        $('#list').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function onDblClickCell(index, field) {
    if (editIndex != index) {
        if (endEditing()) {
            $('#list').datagrid('selectRow', index)
            $('#list').datagrid('beginEdit', index);
            var ed = $('#list').datagrid('getEditor', { index: index, field: field });
            if (ed) {
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                //这个写法很有意思
                //为了方便 ,类似字段要同事写两个值 1把部门id写入隐藏字段2把标题换成值写入 显示字段

            }
            editIndex = index;
        } else {
            setTimeout(function () {
                $('#list').datagrid('selectRow', editIndex);
            }, 0);
        }
    }
}

function onUnselect(index, row) {
    //alert(index);
}

function onCancelEdit(index, row) {
    //alert(index);
}
function onClickRow(index, row) {
    if (editIndex != undefined) {
        //alert("正在编辑的行是：" + editIndex);
        //alert("验证正在编辑的行：" + $('#list').datagrid('validateRow', editIndex));
        if ($('#list').datagrid('validateRow', editIndex)) {//如果验证正在编辑的行数据有效则
            $("#list").datagrid('endEdit', editIndex);//如果点其他行，结束编辑正在编辑的行
            editIndex = undefined;
        } else {
            msg.warning("还有一行没编辑完啦！！");
        }
    }
    $("#list").datagrid('selectRow', index);


}

function onSelect(index, row) {

}

function onEndEdit(index, row, changes) {
    //alert('结束编辑'+index+JSON.stringify(row));
    var o = {};
    var query = JSON.stringify(row);
    o.jsonEntity = query;
    o.action = 'edit';
    o.keyid = row.KeyId;
    query = "json=" + JSON.stringify(o);
    //alert(query);
    //表格内编辑模式编辑成功不提示信息
    jQuery.ajaxjson(actionURL, query, function (d) {
        //if (parseInt(d) > 0) {
        //    msg.ok('编辑成功！');
        //    hDialog.dialog('close');
        grid.reload();
        // } else {
        //     MessageOrRedirect(d);
        // }
    });

}
function append() {
    if (endEditing()) {
        $('#list').datagrid('appendRow', { status: 'P' });
        editIndex = $('#list').datagrid('getRows').length - 1;
        $('#list').datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
    }
}
function removeit() {
    if (editIndex == undefined) { return }
    $('#list').datagrid('cancelEdit', editIndex)
        .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept() {
    if (endEditing()) {
        $('#list').datagrid('acceptChanges');
    }
}
function reject() {
    $('#list').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges() {
    var rows = $('#list').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}
//双击编辑表单焦点消失后保存结束

//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var OrderID = $("#OrderID").textbox('getValue');
    var yspmc = $("#allyspmc").textbox('getValue');
    var dtOpstart = $('#dtOpstart').datebox('getValue');
    var dtOpend = $('#dtOpend').datebox('getValue');
    var fenei = $('#fenei').datebox('getValue');
    var ordertype = $('#ordertype').datebox('getValue');
    //var sk = $('#sk').datebox('getValue');
    var jhsjo = $('#jhsjo').datebox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    var i = 0;
    if (OrderID != '' && OrderID != undefined) {//假如单位搜索不为空
        o.rules[i++] = JSON.parse('{"field":"OrderID","op":"cn","data":"' + OrderID + '"}');
    }
    if (yspmc != '' && yspmc != undefined) {//联系人不为空
        o.rules[i++] = JSON.parse('{"field":"allyspmc","op":"cn","data":"' + yspmc + '"}');
    }
    if (dtOpstart !== '' && dtOpstart !== undefined) {
        o.rules[i++] = JSON.parse('{"field":"adddate","op":"ge","data":"' + dtOpstart + '"}');//大于等于开始时间
    }
    if (dtOpend !== '' && dtOpend !== undefined) {
        o.rules[i++] = JSON.parse('{"field":"adddate","op":"le","data":"' + dtOpend + '" }');//小于等于结束时间
    }
    if (fenei !== '' && fenei !== undefined) {
        o.rules[i++] = JSON.parse('{"field":"deptid","op":"cn","data":"' + fenei + '" }');//部门
    }
    if (ordertype !== '' && ordertype !== undefined & ordertype != 999) {
        o.rules[i++] = JSON.parse('{"field":"mystatus","op":"cn","data":"' + ordertype + '" }');//类型
    } else {
        o.rules[i++] = JSON.parse('{"field":"mystatus","op":"ne","data":"999" }');//类型
    }
    //if (sk !== '' && sk !== undefined) {
    //    o.rules[i++] = JSON.parse('{"field":"status","op":"cn","data":"' + sk + '" }');//类型
    //}

    if (jhsjo !== '' && jhsjo !== undefined) {
        o.rules[i++] = JSON.parse('{"field":"deliveryDate","op":"eq","data":"' + jhsjo + '"}');//大于等于开始时间
    }

    $('#list').datagrid('reload', { filter: JSON.stringify(o) });


}


//自定义搜索结束


//单选多选开关
$('#selectSwitch').switchbutton({
    checked: false,
    onChange: function (checked) {
        if (checked) {
            $('#list').datagrid({ singleSelect: false });//多选
        } else {
            $('#list').datagrid({ singleSelect: true });//单选
        }
    }
})



//回款状态
//$("#sk").combobox({
//    valueField: 'id',
//    textField: 'value',
//    data: [{
//        id: '999',
//        value: '全部',
//    }, {
//        id: '0',
//        value: '全单收款',
//    }],
//    onSelect: function (rec) {
//        //alert(rec.id+"k");
//        if (rec.id == 0) {//正常订单
//            $('#list').datagrid('reload', { filter: '{"groupOp":"AND","rules":[{"field":"status","op":"cn","data":"3"}],"groups":[]}' });
//        }
//        if (rec.id == 999) {
//            $('#list').datagrid('reload', { filter: '' });
//        }

//    }
//});



//订单状态筛选
//$("#orderstatus").combobox({
//    valueField: 'id',
//    textField: 'value',
//    data: [{
//        id: '999',
//        value: '全部',
//    },{
//        id: '0',
//        value: '接件单',
//    }, {
//        id: '1',
//        value: '流程单'
//    }, {
//        id: '2',
//        value: '送货签收单'
//    }, {
//        id: '4',
//        value: '作废单'
//    }
//    ],
//    onSelect: function (rec) {
//        //alert(rec.id+"k");  筛选显示条件
//        if (rec.id == 0) {//正常订单
//            $('#list').datagrid('reload', { filter: '{"groupOp":"AND","rules":[{"field":"mystatus","op":"cn","data":"0"}],"groups":[]}' });
//        }


//        if (rec.id == 1) {//接件单
//            $('#list').datagrid('reload', { filter: '{"groupOp":"AND","rules":[{"field":"mystatus","op":"eq","data":"1"}],"groups":[]}' });
            
//        }
//        if (rec.id == 2) {//送货签收单
//            $('#list').datagrid('reload', { filter: '{"groupOp":"AND","rules":[{"field":"mystatus","op":"eq","data":"2"}],"groups":[]}' });
//        }
//        if (rec.id == 4) {//作废单
//            $('#list').datagrid('reload', { filter: '{"groupOp":"AND","rules":[{"field":"mystatus","op":"eq","data":"4"}],"groups":[]}' });
//        }
//        if (rec.id == 999) {
//            $('#list').datagrid('reload', { filter: '' });
//        }

//    }
//});

//订单部门筛选
//$("#orderdeptid").combobox({
//    valueField: 'id',
//    textField: 'value',
//    data: [{
//        id: '1',
//        value: '总公司',
//    }, {
//        id: '5',
//        value: '昆明盘亘商贸'
//    }],
//    onSelect: function (rec) {
//        //alert(rec.id+"k");
//        if (rec.id == 1) {//正常订单
//            $('#list').datagrid('reload', { filter: '{"groupOp":"AND","rules":[{"field":"deptid","op":"eq","data":"1"}],"groups":[]}' });
//        }
//        if (rec.id == 5) {
//            $('#list').datagrid('reload', { filter: '{"groupOp":"AND","rules":[{"field":"deptid","op":"eq","data":"5"}],"groups":[]}' });
//        }


//    }
//});



//小写转大写
function cmycurd(num) {  //转成人民币大写金额形式
    var str1 = '零壹贰叁肆伍陆柒捌玖';  //0-9所对应的汉字
    var str2 = '万仟佰拾亿仟佰拾万仟佰拾元角分'; //数字位所对应的汉字
    var str3;    //从原num值中取出的值
    var str4;    //数字的字符串形式
    var str5 = '';  //人民币大写金额形式
    var i;    //循环变量
    var j;    //num的值乘以100的字符串长度
    var ch1;    //数字的汉语读法
    var ch2;    //数字位的汉字读法
    var nzero = 0;  //用来计算连续的零值是几个

    num = Math.abs(num).toFixed(2);  //将num取绝对值并四舍五入取2位小数
    str4 = (num * 100).toFixed(0).toString();  //将num乘100并转换成字符串形式
    j = str4.length;      //找出最高位
    if (j > 15) { return '溢出'; }
    str2 = str2.substr(15 - j);    //取出对应位数的str2的值。如：200.55,j为5所以str2=佰拾元角分

    //循环取出每一位需要转换的值
    for (i = 0; i < j; i++) {
        str3 = str4.substr(i, 1);   //取出需转换的某一位的值
        if (i != (j - 3) && i != (j - 7) && i != (j - 11) && i != (j - 15)) {    //当所取位数不为元、万、亿、万亿上的数字时
            if (str3 == '0') {
                ch1 = '';
                ch2 = '';
                nzero = nzero + 1;
            }
            else {
                if (str3 != '0' && nzero != 0) {
                    ch1 = '零' + str1.substr(str3 * 1, 1);
                    ch2 = str2.substr(i, 1);
                    nzero = 0;
                }
                else {
                    ch1 = str1.substr(str3 * 1, 1);
                    ch2 = str2.substr(i, 1);
                    nzero = 0;
                }
            }
        }
        else { //该位是万亿，亿，万，元位等关键位
            if (str3 != '0' && nzero != 0) {
                ch1 = "零" + str1.substr(str3 * 1, 1);
                ch2 = str2.substr(i, 1);
                nzero = 0;
            }
            else {
                if (str3 != '0' && nzero == 0) {
                    ch1 = str1.substr(str3 * 1, 1);
                    ch2 = str2.substr(i, 1);
                    nzero = 0;
                }
                else {
                    if (str3 == '0' && nzero >= 3) {
                        ch1 = '';
                        ch2 = '';
                        nzero = nzero + 1;
                    }
                    else {
                        if (j >= 11) {
                            ch1 = '';
                            nzero = nzero + 1;
                        }
                        else {
                            ch1 = '';
                            ch2 = str2.substr(i, 1);
                            nzero = nzero + 1;
                        }
                    }
                }
            }
        }
        if (i == (j - 11) || i == (j - 3)) {  //如果该位是亿位或元位，则必须写上
            ch2 = str2.substr(i, 1);
        }
        str5 = str5 + ch1 + ch2;

        if (i == j - 1 && str3 == '0') {   //最后一位（分）为0时，加上"整"
            str5 = str5 + '整';
        }
    }
    if (num == 0) {
        str5 = '零元整';
    }
    return str5;
}
//小写转大写//主要是推荐这个函数。它将jquery系列化后的值转为name:value的形式。
function convertArrayNew(o) {
    var v = {};
    for (var i in o) {
        if (o[i].name != '__VIEWSTATE') {
            if (typeof (v[o[i].name]) == 'undefined')
                v[o[i].name] = o[i].value;
            else
                //记住注释了这行
                //v[o[i].name] += "," + o[i].value;
                v[o[i].name] += o[i].value;
        }
    }
    return v;
}

//打印接件单
function print_printdy(title, url) {
    var dialog = $.dialog({
        title: title,
        content: 'url:' + url,
        min: false,
        max: false,
        lock: true,
        width: 850,
        height: 650,
        zIndex: 9999
    });
}
//全单收款
function allpaymoney() {
    console.log("全单收款");
    var row = grid.getSelectedRow();
    if (row) {
        var hDialog = top.jQuery.hDialog({
            title: 'JYD' + row.OrderID + ',客户:' + row.comname + '全单收款', width: '800px', height: '250px', href: '/JydOrder/html/JydAllMoney.html', iconCls: 'icon-save',
            onLoad: function () {
                //init();//初始化
                top.$("#jexx").html(row.jexx + "元");
                top.$("#yufuk").html(row.yufuk + "元");//预付款
                top.$("#yinshou").html((row.jexx - row.yufuk) + "元");
                top.$("#szpck").click(function () {
                    var isChecked = top.$('#szpck').attr('checked');
                    if (isChecked === undefined) {//没选中
                        top.$("#zph").textbox("setValue", "");
                        top.$("#zph").textbox({ readonly: true, required: false });
                        top.$("#szp").textbox("setValue", "0");//设置收支票金额为0且不可以写
                        top.$("#szp").textbox({ readonly: true });
                    } else {//选中了
                        top.$("#zph").textbox({ readonly: false, required: true });
                        top.$("#szp").textbox({ readonly: false, });
                    }
                    console.log("点击了:" + isChecked);
                });
                top.$("#sxjck").click(function () {
                    var isChecked = top.$('#sxjck').attr('checked');
                    if (isChecked === undefined) {//没选中
                        top.$("#sxj").numberbox("setValue", "0");//设置收支票金额为0且不可以写
                        top.$("#sxj").numberbox({ readonly: true });
                    } else {//选中了
                        top.$("#sxj").numberbox({ readonly: false });
                    }
                    console.log("点击了:" + isChecked);
                });
                //收支票改变
                top.$("#szp").numberbox({
                    onChange: function (newV, oldV) {
                        var xj = top.$("#sxj").textbox("getValue");
                        var kkje = top.$("#kkkje").numberbox("getValue");
                        top.$("#skje").textbox("setValue", parseFloat(newV) + parseFloat(xj) - parseFloat(kkje));
                    }
                });
                //收现金改变
                top.$("#sxj").numberbox({
                    onChange: function (newV, oldV) {
                        var zp = top.$("#szp").textbox("getValue");
                        var kkje = top.$("#kkkje").numberbox("getValue");
                        top.$("#skje").textbox("setValue", parseFloat(newV) + parseFloat(zp) - parseFloat(kkje));
                    }
                });


                //扣款金额
                top.$("#kkkje").numberbox({

                    onChange: function (newV, oldV) {
                        console.log("扣款金额变动：" + newV);
                        if (newV !== "0") {//要验证必填
                            top.$("#kkyy").textbox({ required: true, validType: '' });
                            top.$("#kkyy").textbox({ readonly: false });
                            var zp = top.$("#szp").textbox("getValue");
                            var xj = top.$("#sxj").numberbox("getValue");
                            top.$("#skje").textbox("setValue", parseFloat(zp) + parseFloat(xj) - newV);
                        } else {
                            console.log("执行等于0时");
                            //top.$("#kkyy").textbox({ validate: true });
                            top.$("#kkyy").textbox({ readonly: true });
                            top.$("#kkyy").textbox({ required: false, });
                        }
                    }
                });



                //初始化业务员-也就是责任人
                //top.$("#zrrid").combogrid({
                //    delay: 500, //自动完成功能实现搜索
                //    //mode: 'remote',//开启后系统会自动传一个参数q到后台 
                //    panelWidth: 350,
                //    required: true,
                //    //value:'fullname',   
                //    editable: true,
                //    idField: 'KeyId',
                //    textField: 'TrueName',
                //    url: '/sys/ashx/userhandler.ashx',
                //    columns: [[
                //        { field: 'KeyId', title: 'KeyId', width: 50 },
                //        { field: 'TrueName', title: '姓名', width: 120 },
                //        { field: 'Mobile', title: '电话', width: 120 },

                //    ]],
                //    limitToList: true,//只能从下拉中选择值
                //    //reversed: true,//定义在失去焦点的时候是否恢复原始值。
                //    onHidePanel: function () {
                //        var t = top.$(this).combogrid('getValue');//获取combogrid的值
                //        var g = top.$(this).combogrid('grid');	// 获取数据表格对象
                //        var r = g.datagrid('getSelected');	// 获取选择的行
                //        console.log("选择的行是：" + r + "选择的值是:" + t);
                //        if (r == null || t != r.KeyId) {//没有选择或者选项不相等时清除内容
                //            top.$.messager.alert('警告', '请选择，不要直接输入!');
                //            top.$(this).combogrid('setValue', '');
                //        } else {
                //            //do something...
                //        }
                //    },
                //    onSelect: function (rowIndex, rowData) {
                //        console.log("设置的值是：" + rowData.KeyId);
                //        //$("#contactid").val(rowData.KeyId);//给经手人设置ID
                //        top.$("#zrr").val(rowData.TrueName);//客户
                //        top.$("#zrrid").combogrid('setValue', rowData.KeyId);//客户


                //        //alert(rowData.TrueName);
                //    }
                //});

                top.$.ajax({
                    type: "POST",
                    url: '/JydOrder/ashx/JydOrderHandler.ashx?json={"action":"zrrid"}',
                    data: {},
                    dataType: "json",
                    beforeSend: function () { },
                    complete: function () { },
                    success: function (d) {
                        //console.log("这里得到的ID结果是：" + d.zrrid + "与" + d.zrr);
                        top.$("#zrrid").textbox("setValue", d.zrrid);
                        top.$("#zrr").textbox("setValue", d.zrr);
                        //Bug 系统会吧接件日期修改为当前日期
                        //top.$("#adddate").datetimebox("setValue", d.dt);

                        top.$("#zrr").textbox({//设置为只读
                            readonly: true
                        });
                        top.$("#zrrid").textbox({//设置为只读
                            readonly: true
                        });
                        //alert("发送结果：" + JSON.stringify(d));
                    }
                });




            },
            buttons: [
                //{
                //	text: '确定',
                //	iconCls: 'icon-ok',
                //	handler: function () {
                //		//liuchengdan(hDialog, row);//流程单模块化 方便重用
                //		//alert(d);
                //	}
                //},
                //开完单不能打印,点接件单才能打印
                {
                    text: '确定收款',
                    iconCls: 'icon-ok',
                    handler: function () {
                        //alert(top.$("#uiform").form('enableValidation').form('validate'));
                        //alert(top.$("#uiform").form('validate'));
                        //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                        if (top.$("#moneyuiform").form('validate')) {

                            var query = createObjParam('quandansk', row.KeyId, 'moneyuiform');
                            console.log("全单收款query:" + query);
                            //alert(query);

                            jQuery.ajaxjson('/JydOrder/ashx/JydOrderHandler.ashx', query, function (d) {
                                if (parseInt(d) > 0) {
                                    msg.ok('修改成功！');
                                    hDialog.dialog('close');
                                    grid.reload();
                                } else {
                                    MessageOrRedirect(d);
                                }
                            });
                        } else {
                            msg.warning('请填写必填项！');
                        }
                        //

                    }
                },

                {
                    text: '关闭',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        hDialog.dialog("close");
                    }
                }],
            submit: function () {



                return false;
            }
        });

    } else {
        msg.warning('请选择要修改的行。');
    }
}




//送货签收单
function send() {
    var row = grid.getSelectedRow();
    var editIndex = undefined;
    if (row) {
        var hDialog = top.jQuery.hDialog({
            title: 'JYD' + row.OrderID + '送货签收单', width: '800px', height: '600px', href: '/JydOrder/html/send.html', iconCls: 'icon-save',
            onLoad: function () {
                //init();//初始化
                var query = '{"groupOp":"AND","rules":[],"groups":[]}';//先给一个搜索的空的模板
                var o = JSON.parse(query);//把文本转换为json obj
                var i = 0;//这个i用来表示我有几条搜索规则，初始时是0条
                o.rules[i] = JSON.parse('{"field":"orderid","op":"eq","data":"' + row.OrderID + '"}');//cn 就是包含  eq就是等于

                top.$('#send').datagrid({
                    url: '/JydOrderDetail/ashx/JydOrderDetailHandler.ashx?filter=' + JSON.stringify(o) + '',
                    height: '200px',
                    nowrap: false, //折行
                    rownumbers: true, //行号
                    striped: true, //隔行变色
                    idField: 'KeyId',//主键

                    //singleSelect: true, //单选
                    columns: [[
                        { title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                        { field: 'KeyId', title: 'KeyId', },
                        { field: 'yspmc', title: '印刷品名称', },
                        { field: 'PrintType', title: '类型', },
                        { field: 'sl', title: '数量', align: 'right' },
                        { field: 'dj', title: '单价', align: 'right' },
                        { field: 'je', title: '金额', align: 'right' },
                        {
                            field: 'songhuo', title: '送货状态',
                            formatter: function (value, row, index) {
                                if (row.songhuo == "" || row.songhuo == null) {
                                    return "未送货";
                                } else {
                                    return "送货时间:" + value;
                                }
                            },
                        
                            align: 'right'
                        },
                        {
                            field: 'aaaaa', title: '送货数量', align: 'right',
                            formatter: function (v, r, i) {
                                return '<input id="aaaaa' + r.KeyId + '" /><script>$(\'#aaaaa' + r.KeyId + '\').textbox();</script>';
                            }
                        },
                    ]]
                });

                top.$('#songhuo').datagrid({
                    url: '/JydOrderDetailSH/ashx/JydOrderDetailSHHandler.ashx?filter=' + JSON.stringify(o) + '',
                    height: '200px',
                    nowrap: false, //折行
                    rownumbers: true, //行号
                    striped: true, //隔行变色
                    idField: 'KeyId',//主键

                    //singleSelect: true, //单选
                    columns: [[
                        { title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                        { field: 'KeyId', title: 'KeyId', },
                        { field: 'yspmc', title: '印刷品名称', },
                        { field: 'PrintType', title: '类型', },
                        { field: 'sl', title: '数量', align: 'right' },
                        { field: 'dj', title: '单价', align: 'right' },
                        { field: 'je', title: '金额', align: 'right' },
                        {
                            field: 'songhuo', title: '送货状态',
                            formatter: function (value, row, index) {
                                if (row.songhuo == "" || row.songhuo == null) {
                                    return "未送货";
                                } else {
                                    return "送货时间:" + value;
                                }
                            },

                            align: 'right'
                        },
                        { field: 'songhuos', title: '送货数量', align: 'right' },

                    ]]
                });


            },
            buttons: [
                //{
                //	text: '确定',
                //	iconCls: 'icon-ok',
                //	handler: function () {
                //		liuchengdan(hDialog, row);//流程单模块化 方便重用
                //        alert("h");
                //	}
                //},
                //开完单不能打印,点接件单才能打印
                {
                    text: '打印',
                    iconCls: 'icon-printer_start',
                    handler: function () {
                        
                        var srow = top.$('#send').datagrid("getChecked");
                        var keyids = "0";
                        var aaaaa = 0;
                        $.each(srow, function (i, row) {
                            keyids = keyids + "," + row.KeyId;
                            console.log(row.KeyId);
                            aaaaa = top.$('#aaaaa' + row.KeyId).textbox('getValue');
                        });
                        if (keyids == "0") {
                            msg.warning('请选择要修改的行。');
                        } else {
                            //alert(keyids);
                            print_printdy('打印送货签收单JYD' + row.OrderID + '', '/JydWodrmb/print_printshqsd.aspx?KeyId=' + row.KeyId + '&OrderID=' + row.OrderID + '&dids=' + keyids + '&aaaaa=' + aaaaa + '');
                        }
                        var srow = top.$('#songhuo').datagrid("getChecked");
                        var keyids = "0";  
                        $.each(srow, function (i, row) {
                            keyids = keyids + "," + row.KeyId;
                        });
                        if (keyids == "0") {
                            msg.warning('请选择要修改的行。');
                        } else {
                            ////alert(keyids);
                            //print_printdy('打印送货签收单JYD' + row.OrderID + '', '/JydWodrmb/print_printshqsd.aspx?KeyId=' + row.KeyId + '&OrderID=' + row.OrderID + '&dids=' + keyids + '&songhuos=' + row.songhuos + '');
                            msg.warning('此处不打印,请送货的时候打印');
                        }
                        //打印前保存 liuchengdan(hDialog, row);//流程单模块化 方便重用
                        

                    }
                },

                {
                    text: '关闭',
                    iconCls: 'icon-cancel',
                    handler: function () {
                        hDialog.dialog("close");
                    }
                }],
            submit: function () {



                return false;
            }
        });

    } else {
        msg.warning('请选择要修改的行。');
    }
}



//搜索订单号


//把字典存入本地存储

//var k = localData('/sys/ashx/dichandler.ashx?categoryId=103', 'pp');
//alert(k);

function localData(url, name) {
    var localData = localStorage.getItem(name); // 读取字符串数据
    if (localData === null) {
        $.ajax({
            url: url,
            type: 'POST', //GET
            async: false,    //或false,是否异步 高水平小马哥 
            timeout: 5000,    //超时时间
            dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
            success: function (data, textStatus, jqXHR) {
                localStorage.setItem(name, JSON.stringify(data)); // 存储字符串数据到本地
                localData = localStorage.getItem(name); // 读取字符串数据
            }
        });
        return JSON.parse(localData);
    } else {
        return JSON.parse(localData);
    }
}



// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
//调用： 

//var time1 = new Date().Format("yyyy-MM-dd");
//var time2 = new Date().Format("yyyy-MM-dd HH:mm:ss"); 
//点击显示二维码
function showqrcode(orderid, KeyId) {
    var b64 = "";
    $.ajax({
        url: '/jydorder/ashx/JydOrderHandler.ashx?action=qrcode&mainkeyid=' + KeyId + '',
        type: 'GET', //GET
        async: false,    //或false,是否异步
        data: {
            
        },
        timeout: 5000,    //超时时间
        dataType: 'text',    //返回的数据格式：json/xml/html/script/jsonp/text
        beforeSend: function (xhr) {
        },
        success: function (data, textStatus, jqXHR) {
            b64 = data;
        },
    });
    //console.log("得到的json是:"+JSON.stringify(row));
    var hDialog = top.jQuery.hDialog({
        title: 'JYD' + orderid + ',生成流程二维码', width: 600, height: 500, iconCls: 'icon-add',
        content: "<img src='data:image/jpeg;base64," + b64 + "'>",
        width: 320,
        height: 390,
        onLoad: function () {
            //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
            //init();//渲染

        },
        onSubmit: function () {

        }
    });

}

//合同
function showywy(orderid, userkeyid) {
    
    var dialog = $.dialog({
        title: "JYD" + orderid+"合同",
        content: 'url:' + "/JydWodrmb/print_printdysale.aspx?KeyId=" + orderid + "&OrderID=" + userkeyid + "",
        min: false,
        max: false,
        lock: true,
        width: 850,
        height: 650,
        zIndex: 9999
    });


        }

//开票
function invoice(row) {
    var s = row.status;

    if (s.indexOf("3") >= 0) {

        $.ajax({
            type: 'POST',
            url: '/JydInvoicet/ashx/invoicet.ashx',
            data: {
                action: 'addpd',
                orkeyid: row.KeyId
            },
            dataType: 'json',
            beforeSend: function () { },
            complete: function () { },
            success: function (d) {
                if (d.status == 0) {
                    var dialog = $.dialog({
                        title: "接件" + row.KeyId + "开票",
                        content: 'url:' + "/JydWodrmb/print_invoice.aspx?KeyId=" + row.KeyId + "&OrderID=" + row.OrderID + "&jexx=" + row.jexx + "&comname=" + row.comname + "",
                        min: false,
                        max: false,
                        lock: true,
                        width: 850,
                        height: 220,
                        zIndex: 9999
                    });
                } else {
                    msg.warning(d.msg);
                }

            }
        });



    }
    else {
        msg.warning('回款中，不能开票');
    }


}



