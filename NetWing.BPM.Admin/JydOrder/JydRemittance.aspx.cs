﻿using NetWing.Common.Data.SqlServer;
using System;
using System.Data;
using System.Data.SqlClient;

namespace NetWing.BPM.Admin.JydOrder
{
    public partial class JydRemittance : System.Web.UI.Page
    {
        public string ydatajson;
        public DataTable dt = null;
        public int pagesize = 28;
        public int page = 1;
        public int start = 1;
        public int nextpage = 1;
        public int firstpage = 1;
        public int end = 28;
        public int total = 0;
        public int allpage = 0;
        public string mdpage = "";//中间的页码
        public string searchSql = "";
        public string searchUrl = "";
        public string mcname = "";
        public string yhsj = "";
        //public int sum = 0;
        //public int yufkx = 0; 
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            #region Sql查询语句
            mcname = Request["comname"];
            yhsj = Request["xdrq"];

            if (!string.IsNullOrEmpty(Request["comname"]))//企业名称不为空
            {
                searchSql = searchSql + " and o.comname like '%"+ Request["comname"] + "%'";
                searchUrl=searchUrl+ "&comname=" + Request["comname"] +"";
            }
            if (!string.IsNullOrEmpty(Request["jpm"]))//简拼不为空
            {
                searchSql = searchSql + " and o.jpm like '%" + Request["jpm"] + "%'";
                searchUrl = searchUrl + "&jpm=" + Request["jpm"] + "";
            }
            if (!string.IsNullOrEmpty(Request["orderid"]))//订单号
            {
                searchSql = searchSql + " and o.orderid='"+Request["orderid"]+"'";
                searchUrl = searchUrl + "&orderid=" + Request["orderid"] + "";
            }
            
            if (!string.IsNullOrEmpty(Request["xdrq"]))//下单日期
            {
                string xd = Request["xdrq"];
                string[] xdarr = xd.Split('~');
                searchSql = searchSql + " and o.adddate between '" + xdarr[0] + "' and '" + xdarr[1] + "'";
                searchUrl = searchUrl + "&xdrq=" + Request["xdrq"] + "";
            }
            if (!string.IsNullOrEmpty(Request["jhrq"]))//交货日期
            {
                string xd = Request["jhrq"];
                string[] xdarr = xd.Split('~');
                searchSql = searchSql + " and o.deliveryDate between '" + xdarr[0] + "' and '" + xdarr[1] + "'";
                searchUrl = searchUrl + "&jhrq=" + Request["jhrq"] + "";
            }
            if (!string.IsNullOrEmpty(Request["skqk"]))//收款情况
            {
                switch (Request["skqk"])
                {
                    case "1"://全单收款
                        searchSql = searchSql + " and o.skje>=o.jexx";
                        searchUrl = searchUrl + "&skqk=" + Request["skqk"] + "";
                        break;
                    case "2"://部分收款
                        searchSql = searchSql + " and o.skje<o.jexx";
                        searchUrl = searchUrl + "&skqk=" + Request["skqk"] + "";
                        break;
                    case "3"://未收款
                        searchSql = searchSql + " and o.szp is null and o.sxj is null";
                        searchUrl = searchUrl + "&skqk=" + Request["skqk"] + "";
                        break;
                    default:
                        break;
                }

            }//收款情况
            if (!string.IsNullOrEmpty(Request["status"]))//状态
            {
                switch (Request["status"])
                {
                    case "4"://作废
                        searchSql = searchSql + " and o.status like '%4%'";
                        searchUrl = searchUrl + "&status=" + Request["status"] + "";
                        break;
                    case "999":
                        searchSql = searchSql + " and CHARINDEX('4',o.status)=0";
                        searchUrl = searchUrl + "&status=" + Request["status"] + "";
                        break;
                    default:
                        break;
                }

            }
            if (!string.IsNullOrEmpty(Request["dept"]))//网点
            {
                searchSql = searchSql + " and o.deptid="+Request["dept"]+"";
                searchUrl = searchUrl + "&dept=" + Request["dept"] + "";
            }

            if (!string.IsNullOrEmpty(Request["ywy"]))//业务员
            {
                searchSql = searchSql + " and o.ywy like '%" + Request["ywy"] + "%'";
                searchUrl = searchUrl + "&ywy=" + Request["ywy"] + "";
            }

            #endregion
            string sql = "o.KeyId mainKeyid,od.KeyId detKeyId,o.comname,o.jpm,o.ywy,o.orderid,od.yspmc,od.sl,od.dj,od.je,o.jexx,o.yufuk,o.dept,o.status,o.mystatus";//需要显示的字段
            total = (int)SqlEasy.ExecuteScalar("select count(o.keyid) from jydOrder as o,jydOrderDetail as od where od.orderid=o.OrderID "+searchSql+"");

            //sum = Convert.ToInt32(SqlEasy.ExecuteScalar("Select SUM(jexx) FROM (SELECT ROW_NUMBER()Over(order by o.KeyId desc) as rowId," + sql + " from jydOrder as o,jydOrderDetail as od where od.orderid=o.OrderID " + searchSql + ") as mytable WHERE rowId between " + start + " and " + end + ""));
            //yufkx = Convert.ToInt32(SqlEasy.ExecuteScalar("Select SUM(yufuk) FROM (SELECT ROW_NUMBER()Over(order by o.KeyId desc) as rowId," + sql + " from jydOrder as o,jydOrderDetail as od where od.orderid=o.OrderID " + searchSql + ") as mytable WHERE rowId between " + start + " and " + end + ""));

            #region 自由分页

            int k = total % pagesize;//取模 也就是是否除得尽
            if (k>0)//除不尽
            {
                allpage = (int)(total / pagesize)+1;//除不尽+1
            }
            else
            {
                allpage = (int)(total / pagesize);
            }
            
            if (!string.IsNullOrEmpty(Request["page"]))
            {
                page = int.Parse(Request["page"]);
                if (page < allpage)
                {
                    nextpage = page + 1;
                }
                else
                {
                    nextpage = page;
                }


                if (page != 1)
                {
                    firstpage = page - 1;
                }
                start = pagesize * (page - 1) + 1;
                end = pagesize * page;
            }

            //显示中间的页码s


            //页数很少的时候
            if (allpage <8)
            {
                for (int i = 1; i <= allpage; i++)
                {
                    if (i == page)
                    {
                        mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                    }
                    else
                    {
                        mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                    }


                }


            }
            else
            {
                //小于9页也就是8页的时候
                if (page <= 9)
                {
                    for (int i = 1; i <= 9; i++)
                    {
                        if (i == page)
                        {
                            mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                        }
                        else
                        {
                            mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                        }


                    }


                }
                //在末尾的情况
                if ((page + 9) >= allpage)
                {
                    for (int i = (allpage - 8); i <= allpage; i++)
                    {
                        if (i == page)
                        {
                            mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                        }
                        else
                        {
                            mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                        }


                    }


                }
                //在末尾的情况

                //中间的情况s
                if (((page + 9) < allpage) && page > 9)
                {
                    for (int i = (page - 4); i <= (page + 4); i++)
                    {
                        if (i == page)
                        {
                            mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                        }
                        else
                        {
                            mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                        }
                    }


                }
            }

            //显示中间的页码e
            #endregion
            dt = SqlEasy.ExecuteDataTable("Select * FROM (SELECT ROW_NUMBER()Over(order by o.KeyId desc) as rowId," + sql + " from jydOrder as o,jydOrderDetail as od where od.orderid=o.OrderID "+searchSql+") as mytable WHERE rowId between " + start + " and " + end + "");

            //}

        }



        public DataTable ToDataTable(DataRow[] rows)
        {
            if (rows == null || rows.Length == 0) return null;
            DataTable tmp = rows[0].Table.Clone(); // 复制DataRow的表结构
            foreach (DataRow row in rows)
            {
                tmp.ImportRow(row); // 将DataRow添加到DataTable中
            }
            return tmp;
        }
        /// <summary>
        /// DataTable 去重复数据
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="filedNames"></param>
        /// <returns></returns>
        public static DataTable Distinct(DataTable dt, string[] filedNames)
        {
            DataView dv = dt.DefaultView;
            DataTable DistTable = dv.ToTable("Dist", true, filedNames);
            return DistTable;
        }
        /// <summary>
        /// DataTable去重的另一种方法
        /// </summary>
        /// <param name="SourceDt"></param>
        /// <param name="filedName"></param>
        /// <returns></returns>
        public DataTable GetDistinctSelf(DataTable SourceDt, string filedName)
        {
            for (int i = SourceDt.Rows.Count - 2; i > 0; i--)
            {
                DataRow[] rows = SourceDt.Select(string.Format("{0}='{1}'", filedName, SourceDt.Rows[i][filedName]));
                if (rows.Length > 1)
                {
                    SourceDt.Rows.RemoveAt(i);
                }
            }
            return SourceDt;
        }





       


    }
}