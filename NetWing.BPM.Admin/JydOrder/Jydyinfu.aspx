﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Jydyinfu.aspx.cs" Inherits="NetWing.BPM.Admin.JydOrder.Jydyinfu" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form action="?" method="get">
        <div id="toolber">

            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
                <legend>搜索条件</legend>
            </fieldset>

            <div class="layui-form">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label">客户名称</label>
                        <div class="layui-input-inline">
                            <input type="text" name="comname" value="<%=Request["comname"] %>" id="comname" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">客户简拼</label>
                        <div class="layui-input-inline">
                            <input type="text" name="jpm" value="<%=Request["jpm"] %>" id="jpm" autocomplete="off" class="layui-input">
                        </div>
                    </div>

                    
                    
                    

                    <div class="layui-inline">
                        <label class="layui-form-label">日期</label>
                        <div class="layui-input-inline">
                            <input type="text" value="<%=Request["xdrq"] %>" class="layui-input" name="xdrq" id="xdrq" readonly placeholder=" - ">
                        </div>
                    </div>
                    




                    <div class="layui-inline">
                        <div class="layui-input-inline">
                            <input type="submit" class="layui-btn" value="搜索" />
                        </div>
                        
                    </div>



                </div>
            </div>


        </div>
    </form>

    <form id="form1" runat="server">
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card"></div>
                    <div class="layui-card-body">
                        <table border="1" class="layui-table" id="grid" lay-filter="datatable">
                            <thead>
                                <tr>
                                    <td data-field="序号">序号</td>
                                    <td data-field="往来单位">往来单位</td>
                                    <td data-field="单号">单号</td>
                                    <td data-field="经手人">经手人</td>
                                    <td data-field="银行账户">银行账户</td>
                                    <td data-field="合计金额">合计金额</td>
                                    <td data-field="实际金额">实际金额</td>
                                    <td data-field="时间">时间</td>
                                    <td data-field="备注">备注</td>
                                </tr>
                            </thead>
                            <%  int i = 1;
                                foreach (DataRow dr in dt.Rows)
                                {%>
                            <tr>
                                <td data-field="序号"><%=dr["rowid"].ToString() %></td>
                                <td data-field="往来单位"><%=dr["unit"].ToString() %></td>
                                <td data-field="单号"><%=dr["edNumber"].ToString() %></td>
                                <td data-field="经手人"><%=dr["contact"].ToString() %></td>
                                <td data-field="银行账户"><%=dr["account"].ToString() %></td>
                                <td data-field="应付"><%=decimal.Parse(dr["total"].ToString()).ToString("0.00") %>(<%= decimal.Parse(dr["total"].ToString())==0||decimal.Parse(dr["total"].ToString())==decimal.Parse(dr["payment"].ToString())?"记账":decimal.Parse(dr["total"].ToString())<0?"应付":"应收" %>)</td>
                                <td data-field="实付"><%=decimal.Parse(dr["payment"].ToString()).ToString("0.00") %></td>
                                <td data-field="时间"><%=DateTime.Parse(dr["add_time"].ToString()).ToString("yyyy-MM-dd HH:mm") %></td>
                                <td data-field="备注"><%=dr["note"].ToString() %></td>
                            </tr>
                            

                            <% 
                                    i = i + 1;
                                } %>
                            <%decimal a = (decimal)dt.Compute("Sum(total)", "") - (decimal)dt.Compute("Sum(payment)", ""); %>
                            <tr><td colspan="9"><%=a>0?"应收:"+a.ToString("0.00")+"":"应付:"+a.ToString("0.00") %></td></tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <center><div class="layui-box layui-laypage layui-laypage-default"><span>共<%=total %>记录</span><span>共<%=allpage %>页</span><span>每页<%=pagesize %>条记录</span><a href="?page=1<%=searchUrl %>">第一页</a><a href="?page=<%=firstpage %><%=searchUrl %>">«上一页</a><%=mdpage %><a href="?page=<%=nextpage %><%=searchUrl %>">下一页»</a><a href="?page=<%=allpage %><%=searchUrl %>">最后一页</a></div></center>
    </form>
    
            <script src="../scripts/layui/layui.js"></script>
        <!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
        <script>
            layui.use(['form', 'layedit', 'laydate'], function () {
                var form = layui.form
                    , layer = layui.layer
                    , layedit = layui.layedit
                    , laydate = layui.laydate;
                //日期时间范围
                laydate.render({
                    elem: '#xdrq'
                    , type: 'datetime'
                    , range: '~' //或 range: '~' 来自定义分割字符
                });
                laydate.render({
                    elem: '#jhrq'
                    , type: 'datetime'
                    , range: '~' //或 range: '~' 来自定义分割字符
                });
            });

        </script>

</body>
</html>
