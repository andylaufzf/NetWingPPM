﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="JydOrder.aspx.cs" Inherits="NetWing.BPM.Admin.JydOrder.JydOrder" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->


    <!-- 工具栏按钮 -->
    <div id="toolbar">
        <%= base.BuildToolbar()%>
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add" title="开单">开单</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit" title="接件单">接件单</a>
        <a id="a_editlcd" href="#" plain="true" class="easyui-linkbutton" icon="icon-clock" title="流程单">流程单</a>
        <a id="a_send" href="#" plain="true" class="easyui-linkbutton" icon="icon-car" title="送货签收单">送货签收单</a>
        <a id="a_invoice" href="#" plain="true" class="easyui-linkbutton" icon="icon-add" title="开票">开票</a>
        <a id="a_jhsj"  href="#" onclick="$('#list').datagrid('print','DataGrid')" plain="true" class="easyui-linkbutton" icon="icon-time" title="今天交货">今天交货</a>
        <!--onclick="$('#list').datagrid('print','DataGrid')"-->
        <%--<a href="#" plain="true" class="easyui-linkbutton" title="部门">部门</a><input id="orderdeptid" name="orderdeptid" style="width: 80px;" />--%>
        部门:<select id="fenei" name="fenei" class="easyui-combobox" style="width: 80px;">
                <option value="1">总公司</option>
                <option value="5">昆明盘亘商贸</option>
           </select>
        订单类型:<select id="ordertype" name="ordertype" class="easyui-combobox" style="width: 80px;">
                <option value="999">全部</option>
                <option value="0">接件单</option>
                <option value="1">流程单</option>
                <option value="2">送货签收单</option>
                <option value="4">作废单</option>
           </select>
        <%--<a href="#" plain="true" class="easyui-linkbutton" title="订单类型">类型</a><input id="orderstatus" name="orderstatus" style="width: 80px;" />--%>
        <%--收款情况:<select id="sk" name="sk" class="easyui-combobox" style="width: 80px;">
                        <option value="999">全部</option>
                        <option value="3">全单收款</option>
                   </select>--%>
        <%--<a href="#" plain="true" class="easyui-linkbutton" title="收款情况">收款情况</a><input id="sk" name="sk" style="width: 80px;" />--%>
        <a href="#" plain="true" class="easyui-linkbutton" title="单号">单号</a><input id="OrderID" name="OrderID" class="easyui-textbox" style="width: 80px" />
        <a href="#" plain="true" class="easyui-linkbutton" title="印刷品名称">印刷品名称</a><input id="allyspmc" name="allyspmc" class="easyui-textbox" style="width: 80px" />
        <%--<a href="#" plain="true" class="easyui-linkbutton" title="下单日期从">从</a><input id="datestar" class="easyui-datebox" style="width: 100px"/>
        <a href="#" plain="true" class="easyui-linkbutton" title="下单日期到">到</a><input id="dateend" class="easyui-datebox" style="width: 100px"/>--%>
        日期：<input type="text" id="dtOpstart" style="width: 120px;" class="easyui-datetimebox" />
        至
            <input type="text" id="dtOpend" style="width: 120px;" class="easyui-datetimebox" />
        交货时间:<input type="text" id="jhsjo" style="width: 120px;"  class="easyui-datebox" />
        <a id="mysearch" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
    </div>

    <!-- datagrid 列表 -->
    <table id="list"></table>


    <div id="imgShow">
        <img id="img_boox" width="100%" height="100%" />
    </div>
    <!--Uploader-->
    <!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
    <!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>
    <!-- 引入js文件 -->
    <script src="js/JydOrder.js"></script>
    <!--打印-->
    <script src="../scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <script src="js/datagrid-export.js"></script>



<%--<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>--%>

</asp:Content>



