﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="datalc.aspx.cs" Inherits="NetWing.BPM.Admin.JydOrder.datalc" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>时间流程</title>
    <link href="../scripts/easyui/themes/default/easyui.css" rel="stylesheet" />
    <link href="../scripts/easyui/themes/icon.css" rel="stylesheet" />
    <link href="../scripts/easyui/demo/demo.css" rel="stylesheet" />
    <script src="../scripts/easyui/jquery.min.js"></script>
    <script src="../scripts/easyui/jquery.easyui.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    
        <table class="easyui-datagrid">
            <thead>
                <tr>
                    <th data-options="field:'itemid',width:80">工艺</th>
                    <th data-options="field:'productid',width:100">印刷品名称</th>
                    <th data-options="field:'listprice',width:130,align:'right'">订单号</th>
                    <th data-options="field:'unitcost',width:80,align:'right'">负责人</th>
                    <th data-options="field:'attr1',width:200">完成时间</th>
                    <th data-options="field:'status',width:293,align:'center'">备注</th>
                </tr>
            </thead>
            <tbody>
            <%foreach (DataRow yy in dt.Rows) {%>
			<tr>

				<td><%=yy["gytype"].ToString() %></td>
                
                <td><%=yy["myorder"].ToString() %></td><td><%=yy["orderid"].ToString() %></td>
                <td><%=yy["doman"].ToString() %></td><td><%=yy["finishTime"].ToString() %></td><td><%=yy["note"].ToString() %></td>
			</tr>
			<%} %>
		</tbody>



        </table>
    </form>
</body>
</html>
