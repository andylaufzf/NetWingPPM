﻿using NetWing.Common.Data.SqlServer;
using System;
using System.Data;
using System.Data.SqlClient;
using NetWing.Common.Data;

namespace NetWing.BPM.Admin.JydOrder
{

    public partial class Jydyinfu : System.Web.UI.Page
    {
        public DataTable dt = null;
        public int pagesize = 50;
        public int page = 1;
        public int start = 1;
        public int nextpage = 1;
        public int firstpage = 1;
        public int end = 20;
        public int total = 0;
        public int allpage = 0;
        public string mdpage = "";//中间的页码
        public string searchSql = "";
        public string searchUrl = "";
        public string mcname = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            string sql = " * ";

            #region Sql查询语句

            if (!string.IsNullOrEmpty(Request["comname"]))//企业名称不为空
            {
                searchSql = searchSql + " and unit like '%" + Request["comname"] + "%'";
                searchUrl = searchUrl + "&comname=" + Request["comname"] + "";
            }
            //if (!string.IsNullOrEmpty(Request["jpm"]))//简拼不为空
            //{
            //    searchSql = searchSql + " and o.jpm like '%" + Request["jpm"] + "%'";
            //    searchUrl = searchUrl + "&jpm=" + Request["jpm"] + "";
            //}
 

            if (!string.IsNullOrEmpty(Request["xdrq"]))//下单日期
            {
                string xd = Request["xdrq"];
                string[] xdarr = xd.Split('~');
                searchSql = searchSql + " and add_time between '" + xdarr[0] + "' and '" + xdarr[1] + "'";
                searchUrl = searchUrl + "&xdrq=" + Request["xdrq"] + "";
            }




            #endregion

            total = (int)SqlEasy.ExecuteScalar("select count(keyid) from MJFinanceMain where 1=1 " + searchSql + "");
            #region 自由分页
            allpage = DbUtils.GetPageCount(pagesize, total);

            if (!string.IsNullOrEmpty(Request["page"]))
            {
                page = int.Parse(Request["page"]);
                if (page < allpage)
                {
                    nextpage = page + 1;
                }
                else
                {
                    nextpage = page;
                }


                if (page != 1)
                {
                    firstpage = page - 1;
                }
                start = pagesize * (page - 1) + 1;
                end = pagesize * page;
            }

            //显示中间的页码s


            //页数很少的时候
            if (allpage < 8)
            {
                for (int i = 1; i <= allpage; i++)
                {
                    if (i == page)
                    {
                        mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                    }
                    else
                    {
                        mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                    }


                }


            }
            else
            {
                //小于9页也就是8页的时候
                if (page <= 9)
                {
                    for (int i = 1; i <= 9; i++)
                    {
                        if (i == page)
                        {
                            mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                        }
                        else
                        {
                            mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                        }


                    }


                }
                //在末尾的情况
                if ((page + 9) >= allpage)
                {
                    for (int i = (allpage - 8); i <= allpage; i++)
                    {
                        if (i == page)
                        {
                            mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                        }
                        else
                        {
                            mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                        }


                    }


                }
                //在末尾的情况

                //中间的情况s
                if (((page + 9) < allpage) && page > 9)
                {
                    for (int i = (page - 4); i <= (page + 4); i++)
                    {
                        if (i == page)
                        {
                            mdpage = mdpage + "<span class='layui-laypage-curr'><em class='layui-laypage-em'></em><em>" + page + "</em></span>";
                        }
                        else
                        {
                            mdpage = mdpage + "<a href='?page=" + i + "" + searchUrl + "'>" + i + "</a>";
                        }
                    }


                }
            }

            //显示中间的页码e
            #endregion
            dt = SqlEasy.ExecuteDataTable("Select * FROM (SELECT ROW_NUMBER()Over(order by KeyId desc) as rowId," + sql + " from MJFinanceMain where 1=1 " + searchSql + ") as mytable WHERE rowId between " + start + " and " + end + "");
        }
    }
}