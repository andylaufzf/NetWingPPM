using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("sparepartszhu")]
	[Description("备料单")]
	public class SparepartszhuModel
	{
				/// <summary>
		/// 备料单sparepartszhu
		/// </summary>
		[Description("备料单sparepartszhu")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 客户名称
		/// </summary>
		[Description("客户名称")]
		public string comname { get; set; }		
		/// <summary>
		/// 业务员
		/// </summary>
		[Description("业务员")]
		public string salesman { get; set; }		
		/// <summary>
		/// 单号
		/// </summary>
		[Description("单号")]
		public string order_sn { get; set; }		
		/// <summary>
		/// 备料时间
		/// </summary>
		[Description("备料时间")]
		public DateTime add_time { get; set; }		
		/// <summary>
		/// 交货时间
		/// </summary>
		[Description("交货时间")]
		public DateTime jh_time { get; set; }		
		/// <summary>
		/// 总经理同意
		/// </summary>
		[Description("总经理同意")]
		public int manager { get; set; }	
        
        [Description("开单员")]
        public string kdy { get; set; }

        [Description("备注")]
        public string unit_man { get; set; }
        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}