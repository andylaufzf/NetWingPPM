using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("spareparts")]
	[Description("备料单子表")]
	public class SparepartsModel
	{
				/// <summary>
		/// 备料单Spareparts
		/// </summary>
		[Description("备料单Spareparts")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 单号
		/// </summary>
		[Description("单号")]
		public string order_sn { get; set; }		
		/// <summary>
		/// 生产产品名称
		/// </summary>
		[Description("生产产品名称")]
		public string productname { get; set; }		
		/// <summary>
		/// 材料
		/// </summary>
		[Description("材料")]
		public string material { get; set; }		
		/// <summary>
		/// 规格
		/// </summary>
		[Description("规格")]
		public string specifications { get; set; }		
		/// <summary>
		/// 数量
		/// </summary>
		[Description("数量")]
		public string number { get; set; }		
		/// <summary>
		/// 开料规格
		/// </summary>
		[Description("开料规格")]
		public string materialopening { get; set; }		
		/// <summary>
		/// 工艺要求
		/// </summary>
		[Description("工艺要求")]
		public string technology { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string note { get; set; }		
		/// <summary>
		/// 备料员
		/// </summary>
		[Description("备料员")]
		public string billingclerk { get; set; }		
		/// <summary>
		/// 备料员手机号
		/// </summary>
		[Description("备料员手机号")]
		public string tell { get; set; }		
		/// <summary>
		/// 备料员openid
		/// </summary>
		[Description("备料员openid")]
		public string wechareopen { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}