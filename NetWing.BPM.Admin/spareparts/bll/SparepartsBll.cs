using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class SparepartsBll
    {
        public static SparepartsBll Instance
        {
            get { return SingletonProvider<SparepartsBll>.Instance; }
        }

        public int Add(SparepartsModel model)
        {
            return SparepartsDal.Instance.Insert(model);
        }

        public int Update(SparepartsModel model)
        {
            return SparepartsDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return SparepartsDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return SparepartsDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
