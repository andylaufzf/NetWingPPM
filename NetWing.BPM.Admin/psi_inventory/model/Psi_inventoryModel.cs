using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("psi_inventory")]
	[Description("库存盘点表")]
	public class Psi_inventoryModel
	{
				/// <summary>
		/// 库存盘
		/// </summary>
		[Description("库存盘")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 商品ID
		/// </summary>
		[Description("商品ID")]
		public int goods_id { get; set; }		
		/// <summary>
		/// 商品名称
		/// </summary>
		[Description("商品名称")]
		public string goods_name { get; set; }		
		/// <summary>
		/// 盘日期
		/// </summary>
		[Description("盘日期")]
		public DateTime inventorytime { get; set; }		
		/// <summary>
		/// 盘数量
		/// </summary>
		[Description("盘数量")]
		public int discl { get; set; }		
		/// <summary>
		/// 实际数量
		/// </summary>
		[Description("实际数量")]
		public int actuall { get; set; }		
		/// <summary>
		/// 差异量
		/// </summary>
		[Description("差异量")]
		public int quantity { get; set; }		
		/// <summary>
		/// 盘数人id
		/// </summary>
		[Description("盘数人id")]
		public int discname_id { get; set; }		
		/// <summary>
		/// 盘点人
		/// </summary>
		[Description("盘点人")]
		public string discname { get; set; }		
		/// <summary>
		/// 审核人id
		/// </summary>
		[Description("审核人id")]
		public int toexamine_id { get; set; }		
		/// <summary>
		/// 审核人
		/// </summary>
		[Description("审核人")]
		public string toexamine_name { get; set; }		
		/// <summary>
		/// 审核时间
		/// </summary>
		[Description("审核时间")]
		public DateTime toexamine_time { get; set; }		
	    
        /// <summary>
        /// 审核状态
        /// </summary>
        /// <returns></returns>
        [Description("审核状态")]
        public int toexamine_status { get; set; }
        /// <summary>
        [Description("核算单价")]
        public string Accouprice { get; set; }


        /// <summary>
        /// 审核状态decimal(
        /// </summary>
        /// <returns></returns>
        [Description("盈亏金额")]
        public string Profitandamount { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        /// <returns></returns>
        [Description("备注")]
        public string note { get; set; }

        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}