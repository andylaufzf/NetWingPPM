using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class Psi_inventoryDal : BaseRepository<Psi_inventoryModel>
    {
        public static Psi_inventoryDal Instance
        {
            get { return SingletonProvider<Psi_inventoryDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(Psi_inventoryModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}