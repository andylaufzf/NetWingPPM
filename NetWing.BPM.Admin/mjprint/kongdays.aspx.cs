﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using NetWing.Common.Data.SqlServer;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Bll;

namespace NetWing.BPM.Admin.mjprint
{
    public partial class kongdays : NetWing.BPM.Core.BasePage.BpmBasePage
    {
        public string roomid = "";
        public string year = "";

        public int depid = 0;
        public DataTable roomdt = null;

        public DataTable odt = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            roomid = Request["roomid"];
            depid = int.Parse(SysVisitor.Instance.cookiesUserDepId);
            roomdt = SqlEasy.ExecuteDataTable("select * from MJRooms where depid=" + depid + " order by roomnumber  asc");
            odt = SqlEasy.ExecuteDataTable("select * from mjorder where depid=" + depid + "");

        }

        public static DataTable getOdt(string roomid)
        {

            DataTable odt = null;
            string sql = "select * from MJOrder  where roomid=" + roomid + "";
            odt = SqlEasy.ExecuteDataTable(sql);
            return odt;

        }
    }
}