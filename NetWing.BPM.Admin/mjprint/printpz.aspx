﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="printpz.aspx.cs" Inherits="NetWing.BPM.Admin.mjprint.printpz" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
        <script type="text/javascript">
        //窗口API
        var api = frameElement.api, W = api.opener;
        api.button(
            {
                name: '打印',
                focus: true,
                callback: function () {
                    printWin();
                    //var r = tijiao();//提交动作
                    //return false;
                    //返回false 的时候不会关闭当前窗口 return false;
                }
            },
            {
                name: '关闭'
            });
        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
      <div id="content" style="text-align:center;">
<style>
        
                * {
                    margin: 0px;
                    padding: 0px;
                }

                html {
                    overflow-x: hidden;
                }

                body, div, dl, dt, dd, ul, ol, li, pre, code, form, fieldset, legend, input, button, textarea, blockquote {
                    font-family: '微软雅黑';
                }

                input, button, textarea, select, optgroup, option {
                    font-family: 微软雅黑;
	     font-size:100%;
                    font-style: inherit;
                    font-weight: inherit;
                }

                ul, li, dl, dt, dd, ol {
                    display: block;
                    list-style: none;
                }

                img {
                    border: 0;
                }

                .clear {
                    clear: both;
                    height: 0;
                }

                .clear-fix:after {
                    content: ".";
                    display: block;
                    height: 0;
                    font-size: 0;
                    clear: both;
                    visibility: hidden;
                }

                a {
                    text-decoration: none;
                    outline: 0;
                }

                .big_box {
                    width: 100%;
                    cellSpacing =0 cellPadding=0 width=600 border=0
                }

                .center_box {
                    overflow: auto;
                    height: 900px;
                    margin: 0 auto;
                    background: #fff;
                    border: #e5e5e5 1px solid;
                    margin-top: 10px;
                    padding: 0 97px 0 97px;
                    max-height: 1123px;
                }
.table tr{height:40px;font-size:14px !important;text-align:center;}

                  </style>
                  <br />
                  <br />
                  <br />
                  <br />
                  
            <center><h1 style="height:50px;text-align:center;">昆明聚源达彩印包装有限公司财务记账凭证</h1></center>
            <table width="100%" border="1" cellspacing="0" cellpadding="0" bgcolor="#000000" class="table">
          
                <tr>
                    <td colspan="6" bgcolor="#FFFFFF" style="text-align:center; ">原始单据请粘贴在背面(手写无效)</td>
                </tr>
                <tr style="text-align:center;">
                    <td colspan="6" bgcolor="#FFFFFF">
                        

                    系统单号：<%=maindr["ednumber"].ToString() %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;使用单位：<%=maindr["unit"].ToString() %> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;日期：<%=DateTime.Parse(maindr["add_time"].ToString()).ToString("yyyy年MM月dd日 HH:mm:ss") %>

                           
                    </td>
                </tr>
                <tr style="text-align:center;">
                    <td bgcolor="#FFFFFF">费用科目</td>
                    <td bgcolor="#FFFFFF">收支类型</td>
                    <td bgcolor="#FFFFFF">单据张数</td>
                    <td bgcolor="#FFFFFF">合计<br />
                        （元）</td>

                    <td bgcolor="#FFFFFF">备注或用途</td>
                </tr>
                <%foreach (DataRow  dr in detaildt.Rows)
                    {%>

                    
                <tr style="text-align:center;">
                    <td bgcolor="#FFFFFF"><%=dr["subject"].ToString() %></td>
                    <td bgcolor="#FFFFFF"><%=getFtype(int.Parse(dr["subjectid"].ToString())) %></td>
                    <td bgcolor="#FFFFFF"></td>
                    <td bgcolor="#FFFFFF"><%=decimal.Round(decimal.Parse(dr["summoney"].ToString()),2) %></td>

                    <td bgcolor="#FFFFFF"><%=dr["note"].ToString() %></td>
                </tr>
                <%} %>
                <tr style="text-align:center; font-weight:normal !important;">
                    <td bgcolor="#FFFFFF">总计</td>
    
                    <td bgcolor="#FFFFFF" colspan="4">小写:<%=decimal.Round(decimal.Parse(maindr["total"].ToString()),2) %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;大写:<%=ConvertToChinese(decimal.Parse(maindr["total"].ToString())) %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;关联账户:<%=maindr["account"].ToString() %></td>

                </tr>
                <tr style="text-align:left !important;height:70px;">
                    <td colspan="5" bgcolor="#FFFFFF" align="left">单位负责人：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;经手人：<%=maindr["contact"].ToString() %></td>
                </tr>
            </table>
        </div>
    </form>
    </form>
</body>
</html>
