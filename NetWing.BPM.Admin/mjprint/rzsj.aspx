﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="rzsj.aspx.cs" Inherits="NetWing.BPM.Admin.mjprint.rzsj" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <script src="../scripts/lhgdialog/lhgdialog.min.js"></script>
    <script type="text/javascript">
        //窗口API
        var api = frameElement.api, W = api.opener;
        api.button({
            name: '打印',
            focus: true,
            callback: function () {
                printWin();
            }
        }, {
                name: '取消'
            });
        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }
    </script>
    <style type="text/css">
        .auto-style1 {
            height: 19px;
        }
       body,h1,table,tr,td,th{ font-family:"微软雅黑" !important;}

    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="content" style="text-align:center !important;">
<style>
                
                * {
                    margin: 0px;
                    padding: 0px;
                }

                html {
                    overflow-x: hidden;
                }

                body, div, dl, dt, dd, ul, ol, li, pre, code, form, fieldset, legend, input, button, textarea, blockquote {
                    font-family: '微软雅黑';
                }

                input, button, textarea, select, optgroup, option {
                    font-family: 微软雅黑;
	     font-size:100%;
                    font-style: inherit;
                    font-weight: inherit;
                }

                ul, li, dl, dt, dd, ol {
                    display: block;
                    list-style: none;
                }

                img {
                    border: 0;
                }

                .clear {
                    clear: both;
                    height: 0;
                }

                .clear-fix:after {
                    content: ".";
                    display: block;
                    height: 0;
                    font-size: 0;
                    clear: both;
                    visibility: hidden;
                }

                a {
                    text-decoration: none;
                    outline: 0;
                }

                .big_box {
                    width: 100%;
                    cellSpacing =0 cellPadding=0 width=600 border=0
                }

                .center_box {
                    overflow: auto;
                    height: 900px;
                    margin: 0 auto;
                    background: #fff;
                    border: #e5e5e5 1px solid;
                    margin-top: 10px;
                    padding: 0 97px 0 97px;
                    max-height: 1123px;
                }
#form1 table td{ text-align:center;}
.table tr{height:40px;font-size:14px !important;}
            </style>

            <br />
            <br />
            <br />
            <br />
            <h1 style="height:50px;text-align:center;">Me+国际青年公寓(<%=depDr["DepartmentName"].ToString() %>)入住收据</h1>
            <table width="100%" border="1" cellspacing="0" cellpadding="0" bgcolor="#000000"  class="table">
          
                <tr>
                    <td colspan="6" bgcolor="#FFFFFF" style="text-align:center;height:40px;">定金、押金、入住、补办卡等收据(手写无效)&nbsp;&nbsp;&nbsp;&nbsp;合同编号：<%=hth %></td>
                </tr>

                <tr>
                    <td colspan="6" bgcolor="#FFFFFF" style="text-align:center;height:40px;">
                        <%foreach (DataRow dr in dtmain.Rows){%>

                       日期：<%=DateTime.Parse(dr["add_time"].ToString()).ToString("yyyy年MM月dd日") %>&nbsp;&nbsp;&nbsp;&nbsp;房号：<%=dr["roomno"].ToString() %>&nbsp;&nbsp;&nbsp;&nbsp;姓名：<%=dr["users"].ToString() %>

                            <%} %>
                    </td>
                </tr>
                <tr style="text-align:center;">
                    <td bgcolor="#FFFFFF">品名</td>
                    <td bgcolor="#FFFFFF">单价<br />
                        （元）</td>
                    <td bgcolor="#FFFFFF">月数<br />
                        或数量</td>
                    <td bgcolor="#FFFFFF">合计<br />
                        （元）</td>
                    <td bgcolor="#FFFFFF">车牌或车架号<br />
                        房型(期限）</td>
                    <td bgcolor="#FFFFFF">备注</td>
                </tr>
                <%foreach (DataRow  dr in dtdetail.Rows)
                    {%>

                    
                <tr style="text-align:center;">
                    <td bgcolor="#FFFFFF"><%=dr["serviceName"].ToString() %></td>
                    <td bgcolor="#FFFFFF"><%=decimal.Round(decimal.Parse(dr["sprice"].ToString()),2) %></td>
                    <td bgcolor="#FFFFFF"><%=dr["num"].ToString() %></td>
                    <td bgcolor="#FFFFFF"><%=decimal.Round(decimal.Parse(dr["allprice"].ToString()),2) %></td>
                    <td bgcolor="#FFFFFF"><%= DateTime.Parse(dr["add_time"].ToString()).ToString("yyyy年MM月dd") %>-<%if (dr["serviceId"].ToString() == "11")
                                                                                                                      {%> <%}
    else
    { %><%= DateTime.Parse(dr["exp_time"].ToString()).ToString("yyyy年MM月dd") %><%} %></td>
                    <td bgcolor="#FFFFFF"><%=dr["note"].ToString() %></td>
                </tr>
                <%} %>
                <tr style="text-align:center;height:40px;">
                    <td bgcolor="#FFFFFF">总计</td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                    <td bgcolor="#FFFFFF"><%foreach (DataRow dr in dtmain.Rows){%> <%=decimal.Round(decimal.Parse(dr["total"].ToString()),2) %><%} %></td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr style="text-align:left !important;height:70px;">
                    <td colspan="6" bgcolor="#FFFFFF" align="left">Me+国际青年公寓签字（章）：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;业主签字：</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
