﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="zcht.aspx.cs" Inherits="NetWing.BPM.Admin.mjprint.zcht" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <script src="../scripts/lhgdialog/lhgdialog.min.js"></script>
    <script type="text/javascript">
        //窗口API
        var api = frameElement.api, W = api.opener;
        api.button({
            name: '打印',
            focus: true,
            callback: function () {
                printWin();
            }
        }, {
                name: '取消'
            });
        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="content">

            <style>
                * {
                    margin: 0px;
                    padding: 0px;
                }

                html {
                    overflow-x: hidden;
                }

                body, div, dl, dt, dd, ul, ol, li, pre, code, form, fieldset, legend, input, button, textarea, blockquote {
                    font-family: '微软雅黑';
                }

                input, button, textarea, select, optgroup, option {
                    font-family: inherit;
                    font-size: 100%;
                    font-style: inherit;
                    font-weight: inherit;
                }

                ul, li, dl, dt, dd, ol {
                    display: block;
                    list-style: none;
                }

                img {
                    border: 0;
                }

                .clear {
                    clear: both;
                    height: 0;
                }

                .clear-fix:after {
                    content: ".";
                    display: block;
                    height: 0;
                    font-size: 0;
                    clear: both;
                    visibility: hidden;
                }

                a {
                    text-decoration: none;
                    outline: 0;
                }

                .big_box {
                    width: 100%;
                    float: left;
                    cellSpacing=0 cellPadding=0 width=600 border=0
                }

                .center_box {
                    width: 600px;
                    overflow: auto;
                    height: 900px;
                    margin: 0 auto;
                    background: #fff;
                    border: #e5e5e5 1px solid;
                    margin-top: 10px;
                    padding: 0 97px 0 97px;
                    max-height: 1123px;
                }

                .title {
                    text-align: center;
                    font-size: 46px;
                    letter-spacing: 50px;
                    font-family: "宋体";
                    margin-top: 50px;
                }

                .center {
                    margin: 0 auto;
                    margin-top: 60px;
                }

                    .center h1 {
                        font-size: 30px;
                        font-weight: normal;
                    }

                    .center p {
                        text-indent: 2em;
                        line-height: 46px;
                        display: block;
                        font-size: 24px;
                    }

                    .center span {
                        text-align: center;
                        display: block;
                        margin-top: 100px;
                        font-size: 34px;
                        letter-spacing: 10px;
                    }

                .last_text {
                    text-align: center;
                    display: block;
                    margin-top: 150px;
                }

                    .last_text span {
                        font-size: 30px;
                    }

                    .last_text p {
                        font-size: 24px;
                        margin-top: 10px;
                        line-height: 34px;
                    }

                .center_box h2 {
                    text-align: center;
                    font-size: 46px;
                    font-family: "宋体";
                    margin-top: 30px;
                }

                .center_box h3 {
                    text-align: center;
                    font-size: 26px;
                    font-family: "宋体";
                    margin-top: 10px;
                }

                .name {
                    text-align: left;
                    display: block;
                    line-height: 30px;
                    margin-top: 15px;
                    font-size: 18px;
                }

                .text_center {
                    width: 100%;
                    margin: 0 auto;
                    text-align: left;
                }

                    .text_center b {
                        height: 34px;
                        line-height: 34px;
                        display: block;
                    }

                    .text_center p {
                        font-size: 14px;
                        line-height: 24px;
                    }

                .he_footer {
                    width: 100%;
                    float: left;
                }

                    .he_footer span {
                        float: left;
                        font-weight: 900;
                    }

                    .he_footer em {
                        float: right;
                        font-weight: 900;
                        font-style: normal;margin-right:100px;
                    }

                .header {
                    width: 100%;
                    display: block;
                    margin: 0 auto;
                    text-align: center;
                }

                    .header img {
                        width: 80%;
                        display: block;
                        text-align: center;
                        margin: 0 auto;
                    }

                .center_box h4 {
                    text-align: center;
                    font-size: 42px;
                }

                    .center_box h4 span {
                        color: #ea7d31;
                    }

                .text_center01 {
                    width: 100%;
                    margin: 0 auto;
                    text-align: left;
                    margin-top: 10px;
                }

                    .text_center01 b {
                        height: 34px;
                        line-height: 34px;
                        display: block;
                    }

                    .text_center01 p {
                        font-size: 14px;
                        line-height: 24px;
                    }

                    .text_center01 b span {
                        color: #ea7d31;
                    }

                    .text_center01 h2 {
                        font-size: 28px;
                        font-weight: normal;
                        text-align: left;
                    }

                .table {
                    width: 100%;
                    margin: 0 auto;
                }

                    .table tr {
                        width100 %;
                        height: 30px;
                        line-height: 30px;
                        margin-top: -1px;
                        display: block;
                    }

                        .table tr td {
                            border: #333 1px solid;
                            float: left;
                            height: 30px;
                            line-height: 30px;
                            text-align: center;
                            margin-left: -1px;
                        }

                .lin1 {
                    width: 5%
                }

                .lin2 {
                    width: 39%;
                }

                .lin3 {
                    width: 25%
                }

                .lin4 {
                    width: 15%
                }

                .lin5 {
                    width: 15%;
                }

                .table01 {
                    width: 100%;
                }

                    .table01 tr {
                        width: 100%;
                    }

                        .table01 tr td {
                            width: 25%;
                        }

                .text_footer {
                    text-align: center;
                    margin-top: 15px;
                    font-weight: 600;
                }

                .big_box02 {
                    width: 100%;
                    float: left;
                    cellSpacing =0 cellPadding=0 width=600 border=0
                }

                .center_box02 {
                    width: 700px;
                    overflow: auto;
                    height: 900px;
                    margin: 0 auto;
                    background: #fff;
                    border: #e5e5e5 1px solid;
                    margin-top: 10px;
                    padding: 0 47px 0 47px;
                    max-height: 1123px;
                }

                .table02 {
                    width: 100%;
                    text-align: center;
                    margin-top: 20px;
                }

                    .table02 tr {
                        width: 100%;
                        float: left;
                    }

                        .table02 tr td {
                            border: #333 1px solid;
                            float: left;
                            border-right: none;
                            height: inherit;
                        }

                        .table02 tr th {
                            border: #333 1px solid;
                            width: 99.6%;
                            float: left;
                            border-bottom: none !important;
                        }

                .ling1 {
                    width: 9%;
                }

                .ling2 {
                    width: 10%;
                }

                .ling3 {
                    width: 11%;
                }

                .ling4 {
                    width: 9%;
                }

                .ling5 {
                    width: 10%;
                }

                .ling6 {
                    width: 10%;
                }

                .ling7 {
                    width: 10%;
                }

                .ling8 {
                    width: 10%;
                }

                .ling9 {
                    width: 9.8%;
                }

                .ling10 {
                    width: 9.5%;
                }

                .ling11 {
                    width: 30%;
                }

                .ling12 {
                    width: 29.9%;
                }

                .ling13 {
                    width: 33%;
                }

                .ling14 {
                    width: 33%;
                }

                .ling15 {
                    width: 33.3%;
                }

                .ling16 {
                    width: 17%;
                }

                .ling17 {
                    width: 10%;
                }

                .ling18 {
                    width: 10%;
                }

                .ling19 {
                    width: 22%;
                }

                .ling20 {
                    width: 11.6%;
                }

                .ling21 {
                    width: 8%;
                }

                .ling22 {
                    width: 10%;
                }

                .ling23 {
                    width: 10%;
                }

                .ling24 {
                    width: 28.3%;
                }

                .list01 {
                    height: 60px;
                    float: left;
                    line-height: 60px;
                    font-size: 30px;
                }

                .list02 {
                    height: 40px;
                    line-height: 40px;
                    float: left;
                    font-size: 14px;
                }

                .list020 {
                    height: 40px;
                    line-height: 40px;
                    float: left;
                    font-size: 20px;
                }

                .list030 {
                    height: 40px;
                    line-height: 40px;
                    float: left;
                    font-size: 16px;
                }

                .list03 {
                    height: 60px;
                    line-height: 30px;
                    float: left;
                    font-size: 16px;
                    letter-spacing: -1px
                }

                .list04 {
                    height: 50px;
                    line-height: 24px;
                    float: left;
                    font-size: 14px;
                }

                .list05 {
                    height: 40px;
                    line-height: 40px;
                    float: left;
                    color: #f00;
                    font-size: 14px;
                }

                .list06 {
                    height: 50px;
                    line-height: 50px;
                    float: left;
                    font-weight: 600;
                    font-size: 18px;
                }

                .list07 {
                    height: 50px;
                    line-height: 25px;
                    float: left;
                    font-size: 14px;
                    font-weight: normal;
                }

                .list08 th {
                    height: 70px;
                    line-height: 24px;
                    float: left;
                    font-size: 14px;
                    font-weight: normal;
                    display: block;
                    text-align: left;
                }

                .ling1, .ling2, .ling3, .ling4, .ling12, .ling11, .ling5, .ling6, .ling7, .ling8, .ling9, .ling10, .ling13, .ling14, .ling15, .ling16, .ling17, .ling18, .ling19, .ling20, .ling21, .ling22, .ling23, .ling24, .ling25 {
                    border-bottom: none !important;
                }
            </style>

            <div class="big_box">
            <br />
            <br />
            <br />
            <br />
                <h2>Me+国际青年公寓</h2>
                <h3>电动车租赁协议</h3>
                <div class="name">
                    出租方（甲方）：云南米佳公寓酒店管理有限公司（Me+国际青年公寓   ）<br>
                    承租方（乙方）：<%foreach (DataRow dr in dtdetail.Rows)
                                                {%><%=dr["users"].ToString() %><br>
                    租赁时间：<%=DateTime.Parse(dr["add_time"].ToString()).ToString("yyyy-MM-dd") %>至<%=DateTime.Parse(dr["exp_time"].ToString()).ToString("yyyy-MM-dd") %>
                <%} %>
                </div>
                <div class="text_center">
                    <b>一、双方的责任义务</b>
                    <p>（一）甲方的责任义务</p>
                    <p>1、甲方向乙方交付设备齐全的租赁车辆，并对承租人是否具有相应的行为能力进行合理审查。</p>
                    <p>2、车辆租赁期间若发生交通事故,甲方不承担任何责任,需由乙方自行负责。 </p>
                    <p>3、甲方不承担乙方在骑行过程中产生的一切安全及车辆外部零件损坏任何责任。</p>
                    <p>4、甲方免费提供租赁车辆充电、公寓园区内停放管理、保养及年检、合理使用中出现的故障维修。</p>
                    <p>5、甲方不承担由乙方使用租赁车辆引发的连带责任及任何经济责任。 </p>
                    <p>6、甲方拒绝接受租赁车辆在合同期间发生交通违章的处罚,甲方有权将合同中登记的骑车人员作为责任人提交公安交通管理部门处理。</p>
                    <p>8、甲方对电动自行车租赁申请者﹙乙方﹚的信用信息有知情权。 </p>
                    <p>9、乙方在合同协议时间内退还租赁电动自行车,终止协议合同,</p>
                    <p>（二）乙方的责任义务</p>
                    <p>1、安全使用租赁车辆，自觉承担租赁后车辆不合理使用造成的相关费用支出（例如：车辆外观损坏、车架﹑轮圈碰撞变型等）。 </p>
                    <p>2、承担车辆租赁期间发生的交通事故处理费用及事故车辆损失、车辆盗抢、 第三者责任的风险。 </p>
                    <p>3、按照本协议约定及时足额支付租赁费用，合理使用租赁车辆﹙除另有约定外﹚，租赁车辆的合理使用指以满足乙方使用需要的骑车使用。</p>
                    <p>4、在租赁合同规定的期间拥有租赁车辆的合法使用权并享有甲方为保障租赁车辆使用功能所提供的服务。 </p>
                    <p>5、使用租赁车辆应遵守相关法律、法规，如乙方违反交通规则自行承担交通处罚。</p>
                    <p>6、妥善保管租赁车辆,配合甲方保障车辆性能的各项工作。 </p>
                    <p>7、不得买卖、抵押、质押、转租、转借租赁车辆；不得擅自变动、修理、添改租赁车辆任何部位或部件。 </p>
                    <p>8、保证租赁车辆为合同登记的驾驶员驾驶。在合同期内，如乙方登记的信息发生变化，应及时通知甲方，否则发生任何事务责任，将由乙方承担。</p>
                    <p>9、承担因使用不合理造成租赁车辆修理、停驶的损失（见店内公告《收费维修项目明细表》）,承担丢失租赁车辆费用及停驶期间租金。</p>
                    <p>10、乙方如要求延长租期，须在协议到期前3日当面提出续租申请,甲方有权决定是否接续租。</p>
                    <b>二、保证金</b>
                    <p>1、乙方应于租赁合同书签署之日一次性足额交付3000押金。合同履行完毕后,甲方检查车辆各配件，确认完好后返还乙方押金。</p>
                    <b>三、 意外风险及交通事故处理</b>
                    <p>1、乙方承担租赁车辆交通事故车辆损失、车辆盗抢、第三者责任的意外风险。</p>
                    <p>2、乙方自愿向保险公司投保或其它方式承担意外风险，甲方不得干涉乙方意愿。</p>
                    <p>3、上述风险损失的计算、赔付依照保险及赔付程序进行均有乙方自行处理，偿付甲方车辆损失。</p>
                    <p>4、在租赁期间若发生租赁车辆被盗、报废或其它形式的丢失，立即报警处理并在24小时内通知甲方，车辆损失将由乙方赔付。</p>
                    <b>四、本协议最终解释权归甲方所有。</b>
                    <div class="he_footer">
                        <span>甲方（盖章）：Me+国际青年公寓<br>
                            电  话：13759411030<br>
                            QQ  号：3273461427</span> <em>乙方（签字）：<br>
                                电话： </em>
                    </div>
                </div>

            </div>
        </div>
    </form>
</body>
</html>
