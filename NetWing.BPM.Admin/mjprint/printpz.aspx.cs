﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using NetWing.Common.Data.SqlServer;

namespace NetWing.BPM.Admin.mjprint
{
    public partial class printpz : System.Web.UI.Page
    {
        public string edNumber = "";
        public DataRow maindr = null;
        public DataTable detaildt = null;
        public DataRow depDr = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            edNumber = Request["edNumber"];
            if (!string.IsNullOrEmpty(edNumber))
            {
                maindr = SqlEasy.ExecuteDataRow("select top 1 * from MJFinanceMain where edNumber='" + edNumber + "'");
                detaildt = SqlEasy.ExecuteDataTable("select * from  MJFinanceDetail  where edNumber='" + edNumber + "'");
                depDr = SqlEasy.ExecuteDataRow("select * from Sys_Departments where keyid=" + maindr["depid"].ToString() + "");
            }
        }

        public static string getFtype(int fid)
        {
            string ftype = "";
            string sql = "select top 1 subjectType from MJaccountingSubjects where keyid=" + fid + "";
            DataRow fdr = SqlEasy.ExecuteDataRow(sql);



            return fdr["subjectType"].ToString();
        }

        public static String ConvertToChinese(Decimal number)
        {
            var s = number.ToString("#L#E#D#C#K#E#D#C#J#E#D#C#I#E#D#C#H#E#D#C#G#E#D#C#F#E#D#C#.0B0A");
            var d = Regex.Replace(s, @"((?<=-|^)[^1-9]*)|((?'z'0)[0A-E]*((?=[1-9])|(?'-z'(?=[F-L\.]|$))))|((?'b'[F-L])(?'z'0)[0A-L]*((?=[1-9])|(?'-z'(?=[\.]|$))))", "${b}${z}");
            var r = Regex.Replace(d, ".", m => "负元空零壹贰叁肆伍陆柒捌玖空空空空空空空分角拾佰仟万亿兆京垓秭穰"[m.Value[0] - '-'].ToString());
            return r;
        }

    }
}