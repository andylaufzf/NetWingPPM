﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using NetWing.Common.Data.SqlServer;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Bll;

namespace NetWing.BPM.Admin.mjprint
{
    public partial class dayrpt : NetWing.BPM.Core.BasePage.BpmBasePage
    {
        public int depid = 0;
        public DataTable rdt = null;
        public DataTable mdt = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            depid = int.Parse(SysVisitor.Instance.cookiesUserDepId);
            rdt = SqlEasy.ExecuteDataTable("SELECT COUNT(*) as tuinum, CONVERT(varchar(100), istuidate, 23) as thedate FROM MJOrder where depid="+depid+" and  istuidate is not null  group by CONVERT(varchar(100), istuidate, 23)");

            mdt = SqlEasy.ExecuteDataTable("SELECT COUNT(*) as tuinum, year(istuidate) as theyear, month(istuidate) as themonth  FROM MJOrder where depid=" + depid + " and  istuidate is not null  group by year(istuidate), MONTH(istuidate)");

        }
    }
}