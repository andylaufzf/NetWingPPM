﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tyyjsj.aspx.cs" Inherits="NetWing.BPM.Admin.mjprint.tyyjsj" %>


<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script type="text/javascript">
        //窗口API
        var api = frameElement.api, W = api.opener;
        api.button(
            {
                name: '打印',
                focus: true,
                callback: function () {
                    printWin();
                    //var r = tijiao();//提交动作
                    //return false;
                    //返回false 的时候不会关闭当前窗口 return false;
                }
            },
            {
                name: '关闭'
            });
        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="content" style="text-align: center;">
            <br />
            <br />
            <br />
            <br />
            <h1 style="height:50px;text-align:center;">Me+国际青年公寓(<%=depDr["DepartmentName"].ToString() %>)退预约定金收据</h1>
<style>
                
                * {
                    margin: 0px;
                    padding: 0px;
                }

                html {
                    overflow-x: hidden;
                }

                body, div, dl, dt, dd, ul, ol, li, pre, code, form, fieldset, legend, input, button, textarea, blockquote {
                    font-family: '微软雅黑';
                }

                input, button, textarea, select, optgroup, option {
                    font-family: 微软雅黑;
	     font-size:100%;
                    font-style: inherit;
                    font-weight: inherit;
                }

                ul, li, dl, dt, dd, ol {
                    display: block;
                    list-style: none;
                }

                img {
                    border: 0;
                }

                .clear {
                    clear: both;
                    height: 0;
                }

                .clear-fix:after {
                    content: ".";
                    display: block;
                    height: 0;
                    font-size: 0;
                    clear: both;
                    visibility: hidden;
                }

                a {
                    text-decoration: none;
                    outline: 0;
                }

                .big_box {
                    width: 100%;
                    cellSpacing =0 cellPadding=0 width=600 border=0
                }

                .center_box {
                    overflow: auto;
                    height: 900px;
                    margin: 0 auto;
                    background: #fff;
                    border: #e5e5e5 1px solid;
                    margin-top: 10px;
                    padding: 0 97px 0 97px;
                    max-height: 1123px;
                }
#form1 table td{ text-align:center !important;}
.table tr{height:40px;font-size:14px !important;text-align:center;}
            </style>
<table width="100%" border="1" cellspacing="0" cellpadding="0" bgcolor="#000000" style="margin:-5px 0 0 0;  background:#fff; z-index:9999999;" class="table">
                <tr>
                    <td colspan="5" bgcolor="#FFFFFF">特别提示：退房宽带费不退，房租、垃圾费、停车费、等按月计费项目不足一个月按一个月计算，就餐卡及电费卡剩余费用及押金项目全额退还，房间设施按入住清单检查验收，丢失损坏按价赔偿</td>
                </tr>
                <tr>
                    <td colspan="5" bgcolor="#FFFFFF">
                 

                       日期：<%=DateTime.Now.ToString("yyyy年MM月dd日 HH:mm:ss") %><%//=DateTime.Parse(dr["add_time"].ToString()).ToString("yyyy年MM月dd日") %>&nbsp;&nbsp;&nbsp;&nbsp;姓名：<%=dr["username"].ToString() %>

                        
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF">品名</td>
                    <td bgcolor="#FFFFFF">单价<br />
                        （元）</td>
                    <td bgcolor="#FFFFFF">月数<br />
                        或数量</td>
                    <td bgcolor="#FFFFFF">合计<br />
                        （元）</td>

                    <td bgcolor="#FFFFFF">备注</td>
                </tr>




                <tr>
                    <td bgcolor="#FFFFFF"><%=dr["subject"].ToString() %></td>
                    <td bgcolor="#FFFFFF"><%=decimal.Round(decimal.Parse(dr["sprice"].ToString()),2) %></td>
                    <td bgcolor="#FFFFFF"><%=dr["num"].ToString() %></td>
                    <td bgcolor="#FFFFFF"><%=decimal.Round(decimal.Parse(dr["summoney"].ToString()),2).ToString().Replace("-","") %></td>
                    <td bgcolor="#FFFFFF"><%=yyDr["note"].ToString() %></td>
                </tr>

                <tr>
                    <td bgcolor="#FFFFFF">总计</td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                    <td bgcolor="#FFFFFF"><%=decimal.Round(decimal.Parse(dr["summoney"].ToString()),2).ToString().Replace("-","") %></td>

                    <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
                <tr style="text-align:left !important;height:70px;">
                    <td colspan="5" bgcolor="#FFFFFF" align="left">Me+国际青年公寓签字（章）：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;业主签字：</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

