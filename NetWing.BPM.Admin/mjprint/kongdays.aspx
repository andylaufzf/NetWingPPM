﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="kongdays.aspx.cs" Inherits="NetWing.BPM.Admin.mjprint.kongdays" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        table {
            border: #95b8e7 1px solid;
            border-collapse: collapse;
            border-spacing: 0;
            text-align: center;
            font: normal 12px "\5FAE\8F6F\96C5\9ED1";
            color: #444;
        }

            table th {
                height: 28px;
                line-height: 28px;
                background: -webkit-linear-gradient(#eff5fe,#e0ecff);
                background: -moz-linear-gradient(#eff5fe,#e0ecff);
                background: -o-linear-gradient(#eff5fe,#e0ecff);
                background: linear-gradient(#eff5fe,#e0ecff);
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#eff5fe', endColorstr='#e0ecff', GradientType=0);
                border-bottom: #95b8e7 1px solid;
                border-right: #95b8e7 1px dotted;
            }

            table td {
                height: 28px;
                line-height: 28px;
                border: #95b8e7 1px dotted;
                padding: 0px;
            }

            table tr:nth-child(odd) {
                background: #F4F4F4;
            }

            table tr:hover {
                background: #e2edff;
            }
    </style>
    <div id="tt" class="easyui-tabs" style="width: 100%; height: 760px;">
        <div title="空房天数" style="padding: 20px; display: none;">
            <%
                DateTime dt = DateTime.Today;
                //int y = DateTime.IsLeapYear(dt.Year) ? 366 : 365;//计算该年有多少天

                int y = DateTime.Now.DayOfYear;//今天是一年中第几天
                int allkongDays = 0;

                foreach (DataRow roomdr in roomdt.Rows)
                {%>
            <table border="1">
                <tr>
                    <td colspan="8" style=""><%=roomdr["roomnumber"].ToString() %>空房天数</td>

                </tr>
                <tr>
                    <td style="width: 80px;">序号</td>
                    <td style="width: 80px;">姓名</td>
                    <td style="width: 120px;">年度天数</td>
                    <td style="width: 120px;">入住日期</td>
                    <td style="width: 120px;">到期日期</td>
                    <td style="width: 80px;">合同入住天数</td>
                    <td style="width: 80px;">空房天数</td>
                    <td style="width: 80px;">状态</td>
                </tr>
                <%
                    DataTable odt = getOdt(roomdr["keyid"].ToString());
                    int i = 1;
                    int alluseDays = 0;//全部使用天数
                    int kongdays = y;//先初始化空房天数为年度天数
                    foreach (DataRow dr in odt.Rows)
                    {
                        DateTime stime = DateTime.Parse(dr["orderstart_time"].ToString());//入住日期
                        DateTime etime = DateTime.Parse(dr["orderend_time"].ToString());//到期日期
                        TimeSpan ht = etime.Subtract(stime);//合同天数差
                        int useDays = ht.Days;
                        alluseDays = alluseDays + useDays;
                        kongdays = y - alluseDays;
                        if (kongdays < 0)
                        {
                            kongdays = 0;
                        }
                %>
                <tr>
                    <td style="width: 80px;"><%=i %></td>
                    <!--序号-->
                    <td style="width: 80px;"><%=dr["users"].ToString() %></td>
                    <td style="width: 120px;"><%=y %></td>
                    <td style="width: 120px;"><%=DateTime.Parse(dr["orderstart_time"].ToString()).ToString("yyyy-MM-dd") %></td>
                    <td style="width: 120px;"><%=DateTime.Parse(dr["orderend_time"].ToString()).ToString("yyyy-MM-dd") %></td>
                    <td style="width: 80px;"><%=useDays %></td>
                    <td style="width: 80px;"><%=kongdays %></td>
                    <td style="width: 80px;">状态</td>
                </tr>


                <%
                        i = i + 1;
                    } %>
            </table>

            <%
                    allkongDays = allkongDays + kongdays;

                } %>

            <h1>整店空房天数为：<%=allkongDays %></h1>
        </div>
        <div title="签合同情况" style="padding: 20px; display: none;">
            <%
                DateTime now = DateTime.Now;
                DateTime daystart = new DateTime(now.Year, now.Month, 1);//本月第一天
                DateTime dayend = daystart.AddMonths(1).AddDays(-1);//本月最后一天


            %>
            <table border="1">
                <tr><td>日期</td><td>月合同数</td><td>季合同数</td><td>年合同数</td><td>季合同数</td></tr>
            <%for (int i = daystart.Day; i <= dayend.Day; i++)
            {%>


            <%
             } %>
             </table>
    
        </div>
    </div>
</asp:Content>
