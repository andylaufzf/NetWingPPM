﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="zfht.aspx.cs" Inherits="NetWing.BPM.Admin.mjprint.zfht" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <script src="../scripts/lhgdialog/lhgdialog.min.js"></script>
    <script type="text/javascript">
        //窗口API
        var api = frameElement.api, W = api.opener;
        api.button(
         <%if (showprint)
        {%>
            {
                name: '打印',
                focus: true,
                callback: function () {
                    printWin();
                }
            },
        <%}%>

            {
                name: '取消'
            });
        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="content">

            <style>
                
                * {
                    margin: 0px;
                    padding: 0px;
                }

                html {
                    overflow-x: hidden;
                }

                body, div, dl, dt, dd, ul, ol, li, pre, code, form, fieldset, legend, input, button, textarea, blockquote {
                    font-family: '微软雅黑';
                }

                input, button, textarea, select, optgroup, option {
                    font-family: inherit;
                    font-size: 100%;
                    font-style: inherit;
                    font-weight: inherit;
                }

                ul, li, dl, dt, dd, ol {
                    display: block;
                    list-style: none;
                }

                img {
                    border: 0;
                }

                .clear {
                    clear: both;
                    height: 0;
                }

                .clear-fix:after {
                    content: ".";
                    display: block;
                    height: 0;
                    font-size: 0;
                    clear: both;
                    visibility: hidden;
                }

                a {
                    text-decoration: none;
                    outline: 0;
                }

                .big_box {
                    width: 100%;
                    float: left;
                    cellSpacing =0 cellPadding=0 width=600 border=0
                }

                .center_box {
                    overflow: auto;
                    height: 900px;
                    margin: 0 auto;
                    background: #fff;
                    border: #e5e5e5 1px solid;
                    margin-top: 10px;
                    padding: 0 97px 0 97px;
                    max-height: 1123px;
                }
            </style>
            <br />
          
            <div class="big_box02">

                <table width="100%" style="font-size:13px;" cellspacing="0" cellpadding="0" >
                    <tbody>
                        <tr style="text-align: right;font-size: 14px;font-weight:bolder; ">
                            <td colspan="8">合同编号:<%=oDr["ednumber"].ToString() %></td>
                        </tr>
                        <tr style="text-align: center; font-size: 30px;font-weight:bolder;">
                            <td colspan="8">Me+国际青年公寓</td>
                        </tr>
                        <tr style="text-align: center; font-size: 24px;">
                            <td colspan="8">房屋租赁合同</td>
                        </tr>
	        <tr style="text-align: center; font-size: 16px;">
                            <td colspan="8">（请业主认真阅读本合同条款，后期发生一切争议按本合同条款处理）</td>
                        </tr>
                        <tr style="text-align: left; ">
                            <td colspan="8">出租房(甲方):<%=depDr["title"].ToString() %>（Me+国际青年公寓）</td>
                        </tr>
                        <tr style="text-align: left; ">
                            <td colspan="2">承租方（乙方）：</td>
                            <td width="17%"><%foreach (DataRow dr in dtdetail.Rows)
                                                {%><%=dr["users"].ToString() %><%} %></td>
                            <td colspan="5">（身份证已复印）</td>
                        </tr>
                        <tr style="text-align: justify;">
                            <td colspan="8">根据《中华人民共和国经纪合同法》及有关规定，为明确出租方和承租方的权利和义务关系，经双方友好协商一致，就乙方租赁甲方Me+国际青年公寓作为住宿费用，签订本合同条款如下：</td>
                        </tr>
                        <%foreach (DataRow dr in dtdetail.Rows)
                            {%>
                        <tr style="text-align: center;">
                            <td width="22%" style="border:#000 1px solid;border-bottom:none; ">房号</td>
                            <td width="9%" style="border:#000 1px solid; border-bottom:none; border-right:none; border-left:none;"><%=dr["roomno"].ToString() %></td>
                            <td style="border:#000 1px solid;border-bottom:none;border-right:none; ">租期</td>
                            <td width="13%" style="border:#000 1px solid;border-bottom:none;border-right:none;"><%=DateTime.Parse(oDr["orderstart_time"].ToString()).ToString("yyyy-MM-dd") %>至<%=DateTime.Parse(oDr["orderend_time"].ToString()).ToString("yyyy-MM-dd") %></td>
                            <td colspan="2" width="20%" style="border:#000 1px solid;border-bottom:none;border-right:none;">房租月单价:<%=decimal.Round(decimal.Parse(dr["sprice"].ToString()),2) %></td>
                            <td colspan="2" style="border:#000 1px solid;border-bottom:none;">总价:<%=decimal.Round(decimal.Parse(dr["sprice"].ToString())*int.Parse(oDr["zuqi"].ToString()),2) %></td>
                        </tr>
                        <tr style="text-align: center;height:40px;">
                            <td width="22%" style="border:#000 1px solid;border-right:none; ">履约保证金和押金</td>
                            <td style="border:#000 1px solid;border-right:none; "><%foreach (DataRow dryj in dtyj.Rows)
                                    {%> <%=decimal.Round(decimal.Parse(dryj["allprice"].ToString()),2) %> <%}%></td>
                            <td style="border:#000 1px solid;border-right:none; ">付款方式</td>
                            <td style="border:#000 1px solid;border-right:none; "><%switch (dr["num"].ToString())
                                    {
                                        case "3":
                                            Response.Write("季付");
                                            break;
                                        case "6":
                                            Response.Write("半年付");
                                            break;
                                        case "12":
                                            Response.Write("年付");
                                            break;
                                        case "24":
                                            Response.Write("年付");
                                            break;
                                        default:
                                            Response.Write("月付");
                                            break;
                                    }/**/%></td>
                            <td colspan="2" width="20%" style="border:#000 1px solid;border-right:none; ">合同期</td>
                            <td colspan="3" style="border:#000 1px solid;"><%=oDr["zuqi"].ToString() %>个月</td>
                        </tr>
                        <%} %>
                        <tr>
                            <td colspan="8">一、甲方将位于<%=depDr["address"].ToString() %> Me+国际青年公寓的部分房屋出租给乙方作为住宿使用。</td>
                        </tr>
                        <tr>
                            <td colspan="8">二、签订房屋租赁合同时，乙方应向甲方交纳履约保证金或押金，上述保证金及押金合同期满后退还乙方，合同期间双方违约 均扣除对方保证金或押金，因违约造成损失由违约方承担。</td>
                        </tr>
                        <tr>
                            <td colspan="8">三、租赁期间房屋的房产税、营业税及附加税、个人所得税土地使用费、出租管理费由甲方负责交纳；宽带费、水、电费、垃圾清运费由乙方负责交纳。（特别约定除外）本公寓不收取物业费、中介费。</td>
                        </tr>
                        <tr>
                            <td colspan="8">四、甲方应承担房屋工程质量及维修的责任，乙方也不得擅自改变房屋结构，乙方因故意或过失造成租用房屋及其配套设施、室内外财物毁坏，应恢复房屋原状或根据实际价值赔偿经济损失，正常磨损除外。</td>
                        </tr>
                        <tr>
                            <td colspan="8">五、租赁期间，房间设施均按入住须知要求使用保管完好，如有损坏按价赔。</td>
                        </tr>
                        <tr>
                            <td colspan="8">六、租赁期间，本公寓不允许饲养任何大小型宠物，否则甲方有权按乙方违约处理并立即解除合同。</td>
                        </tr>
                        <tr>
                            <td colspan="8">七、租赁期间，乙方必须办理相应IC卡，蓝牙卡、就餐卡等，业主进出本公寓及就餐等必须刷卡，处于安全考虑本公寓不负责开门服务，来访人员在没有业主陪同下，必须在门卫进行身份登记。</td>
                        </tr>
                        <tr>
                            <td colspan="8">八、租赁期间，乙方不得在本公寓范围任何地方丢弃垃圾，乙方所有生活垃圾必须自行带到停车场垃圾桶，生活垃圾不允许摆放房间门口、楼层过道及楼层垃圾桶（楼层垃圾桶仅仅为丢弃零碎小东西设立），绝对不允许将方便面及油污杂物丢弃在楼层垃圾桶，否则甲方有权按乙方违约处理并立即解除合同。</td>
                        </tr>
                        <tr>
                            <td colspan="8">九、租赁期间，乙方必须遵守国家法律法规，不得从事任何违法犯罪活动，例如传销、吸毒、贩毒、藏毒、网络直播、色情聊天卖淫、嫖宿等。</td>
                        </tr>
                        <tr>
                            <td colspan="8">十、租赁期间，乙方不得在本公寓开设任何性质公司及进行任何商务活动，例如开网店、设立厂商办事处等。</td>
                        </tr>
                        <tr>
                            <td colspan="8">十一、租赁期间，若甲方要收回房屋，必须提前15天通知乙方，同时退还乙方双倍的履约保证金；若乙方需要退租，也必须提前10天通知甲方，同时赔付甲方双倍履约保证金。</td>
                        </tr>
                        <tr>
                            <td colspan="8">十二、租赁期间，乙方未经甲方同意，不得将房屋转租给第三方，租赁期满或解除合同时，乙方需结清费用后退还房屋给甲方；</td>
                        </tr>
                        <tr>
                            <td colspan="8">十三、乙方每月在房租到期前7天以电话、短信或者微信的形式与甲方工作人员确认是否续租，房租到期前7天为最后确认时间，确认后又不再续租的业主，除正常收取房屋日租金以外还将以每天100元的违约金扣除违约费用；乙方每月房租须提前五天预交，每延迟一天按照100元/天扣除违约金。（退房时间：到期之日中午12：30以前办理退房手续）</td>
                        </tr>
                        <tr>
                            <td colspan="8">十四、本合同如在履行中发生纠纷，双方应通过协商解决，协商不成可起诉至中华人民共和国人民法院进行处理</td>
                        </tr>
                        <tr>
                            <td colspan="8">十五、本合同经双方签字盖章后即时生效，本合同一式贰份，甲乙双方各执一份，如有未尽事宜，可经双方协商作出补充规定，均具同等法律效力。</td>
                        </tr>
                        <tr>
                            <td colspan="8">十六、乙方签<%=oDr["zuqi"] %>个月合同，房租付费方式按照<%=oDr["paytype"] %>，乙方中途如有违约，乙方房租需按照 <%foreach (DataRow dr in dtdetail.Rows)
                                                                                        {%><%=roomDr["oprice"].ToString() %><%//=decimal.Round(decimal.Parse(dr["sprice"].ToString()), 2) %><%} %>元/月的房租补齐差价，并且扣除违约金1000元。</td>
                        </tr>
                        <tr>
                            <td colspan="3">甲方（盖章）：<%=depDr["title"].ToString() %>
                                <br>
                                电  话：<%=depDr["tel"].ToString() %>                       
                                <br>
                                微信号：<%=depDr["weixin"].ToString() %>                        
                                <br>
                                QQ  号：<%=depDr["qq"].ToString() %>  </td>
                            <td colspan="1">&nbsp;</td>
                            <td colspan="3">
                                <%foreach (DataRow dr in dtmain.Rows)
                                    {%>
              

              
          乙方（签字）：
                                <br>
                                电  话：<%=dr["mobile"].ToString() %>
                                <br>
                                微信号：                      
                                <br>
                                QQ  号： 
          <%} %>
                            </td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>

        <div id="opt" runat="server">
            <asp:Button ID="cussure" runat="server"  Text="客户已确认" OnClick="cussure_Click" />
        </div>
    </form>
</body>
</html>
