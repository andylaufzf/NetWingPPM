﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tfsj.aspx.cs" Inherits="NetWing.BPM.Admin.mjprint.tfsj" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <link href="../scripts/easyui/themes/default/easyui.css" rel="stylesheet" />
    <script src="../scripts/easyui/jquery.easyui.min.js"></script>
    <script src="../scripts/lhgdialog/lhgdialog.min.js"></script>
    <script type="text/javascript">
        //窗口API
        var api = frameElement.api, W = api.opener;
        api.button(
        <%if (showprint)
        {%>
            {
                name: '打印',
                focus: true,
                callback: function () {
                    printWin();
                }
            },
        <%}%>
            {
                name: '取消'
            });
        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }
    </script>
<style>
#content{font-family:"微软雅黑;"}
#content table{font-family:"微软雅黑";}
#content h1{font-family:"微软雅黑";}
#opt{font-family:"微软雅黑";}
#opt input{font-family:"微软雅黑";}
#customsure{ margin:10px 0 10px 0; background: #0f65b4; color: #fff; width:80px; height:
30px; line-height:30px; text-align:center; border:none; border-radius:4px;}
</style>

</head>
<body>
    <form id="form1" runat="server">
        <div id="content" style="text-align: center;">
            <h1 style="height:50px;text-align:center;">Me+国际青年公寓(学府路店)</h1>
<style>
                
                * {
                    margin: 0px;
                    padding: 0px;
                }

                html {
                    overflow-x: hidden;
                }

                body, div, dl, dt, dd, ul, ol, li, pre, code, form, fieldset, legend, input, button, textarea, blockquote {
                    font-family: '微软雅黑';
                }

                input, button, textarea, select, optgroup, option {
                    font-family: 微软雅黑;
	     font-size:100%;
                    font-style: inherit;
                    font-weight: inherit;
                }

                ul, li, dl, dt, dd, ol {
                    display: block;
                    list-style: none;
                }

                img {
                    border: 0;
                }

                .clear {
                    clear: both;
                    height: 0;
                }

                .clear-fix:after {
                    content: ".";
                    display: block;
                    height: 0;
                    font-size: 0;
                    clear: both;
                    visibility: hidden;
                }

                a {
                    text-decoration: none;
                    outline: 0;
                }

                .big_box {
                    width: 100%;
                    cellSpacing =0 cellPadding=0 width=600 border=0
                }

                .center_box {
                    overflow: auto;
                    height: 900px;
                    margin: 0 auto;
                    background: #fff;
                    border: #e5e5e5 1px solid;
                    margin-top: 10px;
                    padding: 0 97px 0 97px;
                    max-height: 1123px;
                }
#form1 table td{ text-align:center !important;}
.table tr{height:40px;font-size:14px !important;}
            </style>
            <table width="100%" border=1" cellspacing="0" cellpadding="0" class="table">
                <tr>
                    <td colspan="6" bgcolor="#FFFFFF" style="text-align:center; font-size:24px;">定金、押金、入住、补办卡等收据(手写无效)</td>
                </tr>
                <tr>
                    <td colspan="6" bgcolor="#FFFFFF">特别提示：退房宽带费不退，房租、垃圾费、停车费、等按月计费项目不足一个月按一个月计算，就餐卡及电费卡剩余费用及押金项目全额退还，房间设施按入住清单检查验收，丢失损坏按价赔偿</td>
                </tr>
                <tr>
                    <td colspan="6" bgcolor="#FFFFFF">
                        <%foreach (DataRow dr in dtmain.Rows)
                            {%>

                       日期：<%=DateTime.Parse(dr["add_time"].ToString()).ToString("yyyy年MM月dd日") %>房号：<%=dr["roomno"].ToString() %>姓名：<%=dr["users"].ToString() %>

                        <%} %>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FFFFFF">品名</td>
                    <td bgcolor="#FFFFFF">单价<br />
                        （元）</td>
                    <td bgcolor="#FFFFFF">月数<br />
                        或数量</td>
                    <td bgcolor="#FFFFFF">合计<br />
                        （元）</td>
                    <td bgcolor="#FFFFFF">车牌或车架号<br />
                        房型(期限）</td>
                    <td bgcolor="#FFFFFF">备注</td>
                </tr>
                <%int total = 0; %>
                <%foreach (DataRow dr in dtdetail.Rows)
                    {%>


                <tr>
                    <td bgcolor="#FFFFFF"><%=dr["servicename"].ToString()%></td>
                    <td bgcolor="#FFFFFF"><%=decimal.Round(decimal.Parse(dr["sprice"].ToString()),2) %></td>
                    <td bgcolor="#FFFFFF"><%=dr["num"].ToString() %></td>
                    <td bgcolor="#FFFFFF"><%=decimal.Round(decimal.Parse(dr["allprice"].ToString()),2) %></td>
                    <td bgcolor="#FFFFFF"><%= DateTime.Parse(dr["add_time"].ToString()).ToString("yyyy年MM月dd") %>-<%= DateTime.Parse(dr["exp_time"].ToString()).ToString("yyyy年MM月dd") %></td>
                    <td bgcolor="#FFFFFF">
                        <%
                            DateTime ndt = DateTime.Now;
                            DateTime adt = DateTime.Parse(dr["add_time"].ToString());//得到开始时间
                            DateTime expdt = DateTime.Parse(dr["exp_time"].ToString());
                            int day = (ndt - expdt).Days;//计算出相差天数 如果小于0说明还剩下多少天 如果大于0说明超出多少天
                            int Month = (ndt.Year - expdt.Year) * 12 + (ndt.Month - expdt.Month);//算出实际相差几个月/m
                            int newMonth = 0;
                            newMonth = Month;

                            //如果是当月退，则不退房租
                            if (ndt.Year==adt.Year&&ndt.Month==adt.Month)//当年当月
                            {
                                newMonth = 0;
                            }

                            string type = "结清";
                            if (newMonth > 0)
                            {
                                type = "补";
                            }
                            if (newMonth < 0)
                            {
                                type = "退";
                            }


                            int[] theonce = getSubjectType(dr["serviceid"].ToString());
                            if (theonce[1] == 1)//表明可以退的科目
                            {
                                if (theonce[0] == 0)//持续性收费
                                {
                                    string n = "" + type + "" + newMonth + "月" + dr["servicename"].ToString() + decimal.Round(newMonth * decimal.Parse(dr["sprice"].ToString()), 2);
                                    note = note + n;
                                    paytotal = paytotal + decimal.Round(newMonth * decimal.Parse(dr["sprice"].ToString()), 2);
                                    %>
                                    <input type="hidden"  name="tuimoney" value="<%=decimal.Round(newMonth * decimal.Parse(dr["sprice"].ToString()), 2) %>" /><!--退-->
                                    <input type="hidden"  name="tuiserviceId" value="<%=dr["serviceId"].ToString() %>" /><!--退-->
                                    <input type="hidden"  name="tuinote" value="<%=n %>" /><!--退-->
                                    <%
                                    Response.Write(n);
                                }
                                if (theonce[0] == 1)//一次性收费都是必须要退的
                                {
                                    string j = "退" + dr["servicename"].ToString() + "：-" + decimal.Round(decimal.Parse(dr["sprice"].ToString()), 2);
                                    note = note + j;
                                    paytotal = paytotal + decimal.Round(decimal.Parse("-" + dr["sprice"].ToString()), 2);
                                    %>
                                    <input type="hidden"  name="tuimoney" value="<%="-" + decimal.Round(decimal.Parse(dr["sprice"].ToString()), 2) %>" /><!--退-->
                                    <input type="hidden"  name="tuiserviceId" value="<%=dr["serviceId"].ToString() %>" /><!--退-->
                                    <input type="hidden"  name="tuinote" value="<%=j %>" /><!--退-->
                                    <%
                                    Response.Write(j);
                                }
                            }


                        %>



                    </td>
                </tr>
                <%} %>
                <tr>
                    <td bgcolor="#FFFFFF">总计</td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                    <td bgcolor="#FFFFFF"><%foreach (DataRow dr in dtmain.Rows)
                                              {%> <%=decimal.Round(decimal.Parse(dr["total"].ToString()),2) %><%} %></td>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                    <td bgcolor="#FFFFFF"><%
                                              if (paytotal > 0)
                                              {
                                                  Response.Write("补：" + paytotal + "元");
                                              }
                                              else
                                              {
                                                  Response.Write("退：" + paytotal + "元");
                                              }
                    %></td>
                </tr>
                <tr style="text-align:left !important;height:70px;">
                    <td colspan="6" bgcolor="#FFFFFF" align="left">Me+国际青年公寓签字（章）：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;业主签字：</td>
                </tr>
            </table>
        </div>
        <div id="opt" runat="server">
            <input type="hidden" name="paytotal" value="<%=paytotal %>" />
            <input type="hidden" name="note" value="<%=note %>" />
            
            <br />
            经手人：<select id="contact" class="easyui-combogrid" name="contact" style="width: 100px;"></select>
            <input id="contactid" name="contactid" type="hidden" value="" style="width: 100px">
            银行账银行账户：<input id="bank" style="width: 150px" class="easyui-combobox" name="account" />
&nbsp;<input id="bankid" name="accountid" type="hidden" value="" style="width: 100px">合计金额：<input id="total" name="total" readonly="readonly" class="easyui-textbox" data-options="" style="width: 100px">
            实付金额：<input id="payment" name="payment" class="easyui-textbox" data-options="required: false" style="width: 100px">
            <asp:Button ID="customsure" runat="server" Text="客户已确认" Enabled="false" OnClick="customsure_Click" /><br />
           <span style="color:#f00;">*</span> 备注：确认金额后方可打印
        </div>



    </form>

    <script>
        $(document).ready(function () {
            $("#total").textbox("setValue",<%=paytotal%>);
            $("#payment").textbox("setValue",<%=paytotal%>);

            //加载银行
            $("#bank").combobox({
                valueField: 'accountName',//用中文名作为名称
                required: true,
                textField: 'accountName',
                url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJBankAccount&field=accountName',
                onSelect: function (rec) {
                    $("#bankid").val(rec.KeyId);//设置隐藏ID
                    $("#customsure").removeAttr("disabled");
                }
            });
            //经手人
            $("#contact").combogrid({
                panelWidth: 350,
                required: true,
                //value:'fullname',   
                idField: 'TrueName',
                textField: 'TrueName',
                url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=Sys_Users&field=TrueName',
                columns: [[
                    { field: 'TrueName', title: '姓名', width: 60 },
                    { field: 'KeyId', title: 'KeyId', width: 100 }
                ]],
                onSelect: function (rowIndex, rowData) {
                    $("#contactid").val(rowData.KeyId);//给经手人设置ID
                    //alert(rowData.KeyId);
                }
            });
        });
    </script>
</body>
</html>
