using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class TpflowProhandBll
    {
        public static TpflowProhandBll Instance
        {
            get { return SingletonProvider<TpflowProhandBll>.Instance; }
        }

        public int Add(TpflowProhandModel model)
        {
            return TpflowProhandDal.Instance.Insert(model);
        }

        public int Update(TpflowProhandModel model)
        {
            return TpflowProhandDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return TpflowProhandDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return TpflowProhandDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
