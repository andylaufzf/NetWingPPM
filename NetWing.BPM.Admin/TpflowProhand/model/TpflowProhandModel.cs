using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("TpflowProhand")]
	[Description("流程处理")]
	public class TpflowProhandModel
	{
				/// <summary>
		/// 流程处理ID
		/// </summary>
		[Description("流程处理ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 流程名称
		/// </summary>
		[Description("流程名称")]
		public string flowname { get; set; }		
		/// <summary>
		/// 流程编号
		/// </summary>
		[Description("流程编号")]
		public string flownumber { get; set; }		
		/// <summary>
		/// 实例编号
		/// </summary>
		[Description("实例编号")]
		public string jsonexamplnumber { get; set; }		
		/// <summary>
		/// 当前节点
		/// </summary>
		[Description("当前节点")]
		public int currentjsonnode { get; set; }		
		/// <summary>
		/// 当前节点名称
		/// </summary>
		[Description("当前节点名称")]
		public string Currentnodename { get; set; }		
		/// <summary>
		/// 创建时间
		/// </summary>
		[Description("创建时间")]
		public DateTime addtime { get; set; }		
		/// <summary>
		/// 完成时间
		/// </summary>
		[Description("完成时间")]
		public DateTime uptime { get; set; }		
		/// <summary>
		/// 创建用户
		/// </summary>
		[Description("创建用户")]
		public string establishname { get; set; }		
		/// <summary>
		/// 是否完成
		/// </summary>
		[Description("是否完成")]
		public int Isitfinished { get; set; }		
		/// <summary>
		/// 执行者
		/// </summary>
		[Description("执行者")]
		public string executor { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}