using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJFoundBll
    {
        public static MJFoundBll Instance
        {
            get { return SingletonProvider<MJFoundBll>.Instance; }
        }

        public int Add(MJFoundModel model)
        {
            return MJFoundDal.Instance.Insert(model);
        }

        public int Update(MJFoundModel model)
        {
            return MJFoundDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJFoundDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJFoundDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
