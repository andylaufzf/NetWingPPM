﻿<%@ Page Title="" Language="C#" MasterPageFile="../Site1.Master" AutoEventWireup="true" CodeBehind="SysConfig.aspx.cs" Inherits="NetWing.BPM.Admin.sys.SysConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div id="sysconfig" style="margin: 10px;">

        <h1>基本设置</h1>
        <div class="c">
            <ul>
                <li>
                    <div>皮肤：</div>
                    <input type="text" id="txt_theme" name="theme" /></li>

            </ul>
        </div>
        <h1>菜单设置</h1>
        <div class="c">
            <ul>
                <li>
                    <div>表现方式：</div>
                    <input type="text" id="txt_nav_showtype" name="navshowtype" /></li>
                <li>
                    <div>&nbsp;</div>
                    <img id="imgPreview" title="点击看大图" src="/images/menuStyles/Accordion.png" style="width: 200px; margin-top: 3px; padding: 2px; border: 1px solid #ccc;" alt="" /></li>
            </ul>
        </div>
        <h1>数据表格设置</h1>
        <div class="c">
            <ul>
                <li>
                    <div>每页记录数：</div>
                    <input type="text" id="txt_grid_rows" name="gridrows" /></li>
            </ul>
        </div>
        <style>
            .c ul li .picker {
                margin-left:5px;
            }
           .c ul li .picker .webuploader-pick {
                text-align: center;
                width: 100px !important;
                height: 22px;
                line-height: 22px;
                overflow: hidden;
                padding: 2px 5px;
            }
        </style>
        <h1>系统设置</h1>
        <div class="c">
            <ul>
                <li>
                    <div>系统名称：</div>
                    <input type="text" id="systemName" class="easyui-textbox" name="systemName" />
                </li>
                <li>
                    <div>版权所有：</div>
                    <input type="text" id="copyRight" class="easyui-textbox" name="systemName" />
                </li>
                <li>
                    <div>官方网址：</div>
                    <input type="text" id="website" class="easyui-textbox" name="website" />
                </li>
                <li><font color="red">注意！！！上传会覆盖原有图片，请谨慎操作！！！</font></li>
                <li>
                    <div>企业Logo：</div>
                    <input type="text" value="" id="syslogo" class="easyui-textbox" name="syslogo" />
                    <div class="picker" style="float:right;" fname="syslogo.png">选择logo文件</div>
                </li>
                <li>
                    <div>登录页背景图：</div>
                    <input type="text" id="loginpic" class="easyui-textbox" name="loginpic" />
                    <div class="picker" style="float:right;" fname="loginpic.png">选择图片文件文件</div>
                </li>
                <li>
                    <div>欢迎页背景图：</div>
                    <input type="text" id="welcomepic" class="easyui-textbox" name="welcomepic" />
                    <div class="picker" style="float:right;" fname="welcomepic.png">选择图片文件文件</div>
                </li>

            </ul>
        </div>




    </div>

    <div style="margin: 140px; width: 160px; margin-top: 40px; font-family: 'Microsoft YaHei'">

        <a id="btnok" href="javascript:;" class="alertify-button alertify-button-ok">保存设置</a>

    </div>
    <!--Uploader-->
    <!--上传组件皮肤已经在公共样式里-->
    <script type="text/javascript" src="/scripts/webuploader/webuploader.min.js"></script>
    <script src="ashx/ConfigHandler.ashx?action=js" type="text/javascript"></script>
    <script src="js/config.js?nguid=7" type="text/javascript"></script>

</asp:Content>
