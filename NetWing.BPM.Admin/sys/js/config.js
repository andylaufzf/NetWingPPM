﻿//系统全局设置
var _data = {
    theme: [{ "title": "默认皮肤", "name": "default" },
    { "title": "流行灰", "name": "gray" },
    { "title": "黑色", "name": "black" },
    { "title": "Bootstrap", "name": "bootstrap" },
    { "title": "Metro", "name": "metro" },
    { "title": "Metro-Blue", "name": "metro-blue" },
    { "title": "Metro-Gray", "name": "metro-gray" },
    { "title": "Metro-Green", "name": "metro-green" },
    { "title": "Metro-Orange", "name": "metro-orange" },
    { "title": "Metro-Red", "name": "metro-red" }
    ],
    navType: [{ "id": "menubutton", "text": "横向菜单" }, { "id": "Accordion", "text": "手风琴(2级)", "selected": true }, { "id": "Accordion2", "text": "手风琴大图标(2级)" }, { "id": "tree", "text": "树形结构" },
    { "id": "menuAccordion", "text": "菜单+手风琴（小图标-3级）" }, { "id": "menuAccordion2", "text": "菜单+手风琴（大图标-3级）" },
    { "id": "AccordionTree", "text": "手风琴+树形目录(2级+)" }]
};

function initCtrl() {
    $('#txt_theme').combobox({
        data: _data.theme, panelHeight: 'auto', editable: false, valueField: 'name', textField: 'title'
    });

    $('#txt_nav_showtype').combobox({
        data: _data.navType, panelHeight: 'auto', editable: false, valueField: 'id', textField: 'text', width: 180,
        onSelect: function (item) {
            $('#imgPreview').attr('src', urlRoot + '/images/menustyles/' + item.id + '.png');
        }
    });

    $('#imgPreview').click(function () {
        var src = $(this).attr('src');
        top.$.hDialog({
            content: '<img src="' + urlRoot + src + '" />',
            width: 665,
            height: 655,
            title: '效果图预览',
            showBtns: false
        });
    });

    $('#txt_grid_rows').val(20).numberspinner({ min: 10, max: 500, increment: 10 });

    if (sys_config) {
        $('#txt_theme').combobox('setValue', sys_config.theme.name);
        $('#txt_nav_showtype').combobox('setValue', sys_config.showType);
        $('#txt_grid_rows').numberspinner('setValue', sys_config.gridRows);
        $('#imgPreview').attr('src', urlRoot + '/images/menustyles/' + sys_config.showType + '.png');
        $('#showValidateCode').attr('checked', sys_config.showValidateCode);
    }
}

$(function () {
    initCtrl();
    $('#btnok').click(saveConfig);

    $('body').css('overflow', 'auto');



});

function saveConfig() {
    var systemName = $("#systemName").textbox('getValue');//系统名称
    var website = $("#website").textbox('getValue');//网址
    var syslogo = $("#syslogo").textbox('getValue');//企业Logo
    var copyRight = $("#copyRight").textbox('getValue');//版权方
    var theme = $('#txt_theme').combobox('getValue');
    var navtype = $('#txt_nav_showtype').combobox('getValue');
    var gridrows = $('#txt_grid_rows').numberspinner('getValue');

    var findThemeObj = function () {
        var obj = null;
        $.each(_data.theme, function (i, n) {
            if (n.name == theme)
                obj = n;
        });
        return obj;
    };
    var configObj = { theme: findThemeObj(), showType: navtype, gridRows: gridrows, showValidateCode: $('#showValidateCode').is(':checked') };

    var str = JSON.stringify(configObj);
    //alert(str);

    $.ajaxtext('ashx/ConfigHandler.ashx?systemName=' + systemName + '&website=' + website + '&copyRight=' + copyRight + '', 'json=' + str, function (d) {
        if (d == 1) {
            msg.ok('恭喜，全局设置保存成功,按F5看效果');
            location.reload();
        }
        else {
            alert(d);
        }
    });



}


//ajax获取站点配置
$.ajax({
    type: "POST",
    url: '/sys/ashx/ConfigHandler.ashx?action=siteconfig',//top=n 是控制条数
    dataType: "json",
    data: {},
    timeout: 20000,
    success: function (data, textStatus) {
        $("#systemName").textbox("setValue", data.systemName);
        $("#copyRight").textbox("setValue", data.copyRight);
        $("#syslogo").textbox("setValue", data.syslogo);
        $("#website").textbox("setValue", data.website);

    },
    error: function (XMLHttpRequest, textStatus, errorThrown) {
        // alert("获取任务信息失败" + textStatus);
        alert("获取预约数据失败，请检查数据库连接！");
    }

});

//上传控件
var upurl = "/sys/ashx/uploadhandler.ashx"
var uploader = WebUploader.create({
    //初始化Web Uploader
    // swf文件路径 这里用html5方式上传因此注释
    //swf: BASE_URL + '/js/Uploader.swf',

    // 文件接收服务端。
    server: upurl,

    // 选择文件的按钮。可选。
    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
    pick: { id: $('.picker'), multiple: false },// //只能选择一个文件上传
    // 选完文件后，是否自动上传。
    auto: true,
    //限制只能上传一个文件
    //fileNumLimit: 1,
    // 不压缩image, 默认如果是jpeg，文件上传前会压缩一把再上传！
    //fileVal {Object }[可选][默认值：'file'] 设置文件上传域的name。
    resize: false,
    accept: {
        title: 'PNG文件',
        extensions: 'jpg,png,gif',
        mimeTypes: 'image/png'
    }
});

// 文件上传成功，给item添加成功class, 用样式标记上传成功。
uploader.on('uploadSuccess', function (file, response) {
    var o = response._raw;
    o = JSON.parse(o);//因为这里是字符串，所以要格式化一下json
    var uploaderId = '#rt_' + file.source.ruid;//每个实例的唯一ID自己写的时候这是个突破口，时间主要花在这个上面了.... 下面可以通过这个自由发挥了）
    var pobj = $(uploaderId).parent();
    var bobj = $(pobj).siblings('input'); //找到兄弟元素input
    //alert(bobj);
    var bid = $(bobj).attr("id");//得到input 的id
    $("#" + bid).textbox("setValue", o.filename);//设置easyui控件的值 好绕啊！！！
    uploader.reset();//上传完成后重置组件

});

// 文件上传失败，显示上传出错。
uploader.on('uploadError', function (file) {

});

// 完成上传完了，成功或者失败，先删除进度条。
uploader.on('uploadComplete', function (file) {

});
// 当有文件被添加进队列的时候
uploader.on('fileQueued', function (file) {
    //alert(uploader.options.pick.id);得到当前上传控件返回的是个obj
    var uploaderId = '#rt_' + file.source.ruid;//每个实例的唯一ID自己写的时候这是个突破口，时间主要花在这个上面了.... 下面可以通过这个自由发挥了）
    var pobj = $(uploaderId).parent();
    var fname = $(pobj).attr("fname");//控件里自定义上传的文件名
    uploader.options.server = upurl + '?fname=' + fname;
    //得到自定义控件的自定义文件名ok
    //alert($(pobj).attr("fname"));
    //return false;
});
uploader.on('uploadBeforeSend', function (object, data, headers) {
    //alert($(object).html())
})

