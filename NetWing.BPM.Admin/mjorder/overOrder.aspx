﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="overOrder.aspx.cs" Inherits="NetWing.BPM.Admin.mjorder.overOrder" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->



<!-- 工具栏按钮 -->
    <div id="tt" class="easyui-tabs" style="width: ; height: ;">
        <div title="入住收据" style="padding: 20px; display: none;">
            特别提示：退房宽带费不退，房租、垃圾费、停车费等按月计费项目不足一个月按一个月计算，就餐卡及电费卡剩余费用及押金项目全额退还，房间设施按入住清单检查验收，丢失损坏按价赔。<br /><br />
            <!--请修改项目顺序和修改样式-->
            <!--系统标识：<input id="KeyId" class="easyui-textbox" name="KeyId" style="width: 100px;">-->
            客户姓名：<input id="users"  name="users" value="" style="width: 100px;display:none;">
            <input id="usersid"  name="usersid" class="easyui-combobox" value="" style="width: 100px;display:;">
            <input id="roomno" value=""  name="roomno" style="width: 100px;display:none;">
            房号：<input id="roomid"  name="roomid" class="easyui-combobox"  style="width: 100px;display:;">
    
            手机号：<input id="mobile" class="easyui-textbox" name="mobile" style="width: 100px;">
            金额合计：<input id="total"  class="easyui-textbox" name="total" style="width: 100px;">
            入住时间：<input id="add_time" class="easyui-datetimebox" name="add_time" style="width: 100px;">
           <!-- 更新时间：<input id="up_time" class="easyui-datetimebox" name="up_time" style="width: 100px;">-->
            单号：<input id="orderid" name="edNumber" class="easyui-textbox" value="<%//=edNumber%>" data-options="readonly:true" style="width: 120px">
            <br />
            <br />
            
            <br /><br />
                    <input id="account" name="account" value=""  style="display:none;"/>
            账户:<input id="accountid" name="accountid" value="" />

            经手人:<input id="contact" name="contact"  type="" value="" style="width:100px;display:none;"> 
            <input id="contactid" name="contactid"  type="" value="" style="width:100px;display:none;"> 

            <input id="dep" name="dep"  type="text" value="" style="width:100px;display:none;"> 
            部门:<input id="depid" name="depid"  type="" value="" style="width:100px"> 
            <br />
            <br />
            <a id="submit" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'">提交</a>
            <a id="btnprint" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-printer'">打印</a>

        </div>

    </div>


	<!--Uploader-->
	<!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
	<!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>


    <!-- 引入js文件 -->
      <script src="js/MJOrder.js"></script>
    <!--引入-->
    <script src="../scripts/easyui/datagrid-detailview.js"></script>
    <!--引入lhgdialog插件-->
    <script src="../scripts/lhgdialog/lhgdialog.min.js?skin=idialog"></script>
    
</asp:Content>



