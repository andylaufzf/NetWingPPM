using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJOrderBll
    {
        public static MJOrderBll Instance
        {
            get { return SingletonProvider<MJOrderBll>.Instance; }
        }

        public int Add(MJOrderModel model)
        {
            return MJOrderDal.Instance.Insert(model);
        }

        public int Update(MJOrderModel model)
        {
            return MJOrderDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJOrderDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJOrderDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
