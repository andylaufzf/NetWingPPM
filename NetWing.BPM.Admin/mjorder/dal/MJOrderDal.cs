using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class MJOrderDal : BaseRepository<MJOrderModel>
    {
        public static MJOrderDal Instance
        {
            get { return SingletonProvider<MJOrderDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(MJOrderModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}