using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
    [TableName("MJOrder")]
    [Description("入住手续主表")]
    public class MJOrderModel
    {
        /// <summary>
        /// 系统标识
        /// </summary>
        [Description("系统标识")]
        public int KeyId { get; set; }
        /// <summary>
        /// 客户姓名
        /// </summary>
        [Description("客户姓名")]
        public string users { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        [Description("用户ID")]
        public int usersid { get; set; }
        /// <summary>
        /// 房号
        /// </summary>
        [Description("房号")]
        public string roomno { get; set; }
        /// <summary>
        /// 房间ID
        /// </summary>
        [Description("房间ID")]
        public int roomid { get; set; }
        /// <summary>
        /// 手机号
        /// </summary>
        [Description("手机号")]
        public string mobile { get; set; }
        /// <summary>
        /// 金额合计
        /// </summary>
        [Description("金额合计")]
        public decimal total { get; set; }
        /// <summary>
        /// 入住时间
        /// </summary>
        [Description("入住时间")]
        public DateTime add_time { get; set; }
        /// <summary>
        /// 更新时间
        /// </summary>
        [Description("更新时间")]
        public DateTime up_time { get; set; }
        /// <summary>
        /// 单号
        /// </summary>
        [Description("单号")]
        public string edNumber { get; set; }
        /// <summary>
        /// 经手人
        /// </summary>
        [Description("经手人")]
        public string contact { get; set; }
        /// <summary>
        /// 经手人ID
        /// </summary>
        [Description("经手人ID")]
        public int contactid { get; set; }
        /// <summary>
        /// 部门
        /// </summary>
        [Description("部门")]
        public string dep { get; set; }
        /// <summary>
        /// 部门ID
        /// </summary>
        [Description("部门ID")]
        public int depid { get; set; }
        /// <summary>
        /// 银行账户
        /// </summary>
        [Description("银行账户")]
        public string account { get; set; }
        /// <summary>
        /// 银行账户ID
        /// </summary>
        [Description("银行账户ID")]
        public int accountid { get; set; }


        /// <summary>
        /// 银行账户b
        /// </summary>
        [Description("银行账户b")]
        public string accountb { get; set; }
        /// <summary>
        /// 银行账户IDb
        /// </summary>
        [Description("银行账户IDb")]
        public int accountidb { get; set; }

        /// <summary>
        /// 银行账户c
        /// </summary>
        [Description("银行账户c")]
        public string accountc { get; set; }
        /// <summary>
        /// 银行账户IDb
        /// </summary>
        [Description("银行账户IDb")]
        public int accountidc { get; set; }


        /// <summary>
        /// 银行账户d
        /// </summary>
        [Description("银行账户d")]
        public string accountd { get; set; }
        /// <summary>
        /// 银行账户IDb
        /// </summary>
        [Description("银行账户IDd")]
        public int accountidd { get; set; }




        /// <summary>
        /// 数据所有者ID
        /// </summary>
        [Description("数据所有者ID")]
        public int ownner { get; set; }

        /// <summary>
        /// 入住收据
        /// </summary>
        [Description("入住收据")]
        public int printsj { get; set; }


        /// <summary>
        /// 租房合同
        /// </summary>
        [Description("租房合同")]
        public int printzfht { get; set; }

        /// <summary>
        /// 租车合同
        /// </summary>
        [Description("租车合同")]
        public int printzcht { get; set; }

        /// <summary>
        /// 温馨提示
        /// </summary>
        [Description("温馨提示")]
        public int printwxts { get; set; }

        /// <summary>
        /// 业主手册
        /// </summary>
        [Description("业主手册")]
        public int printyzsc { get; set; }

        /// <summary>
        /// 退房收据
        /// </summary>
        [Description("退房收据")]
        public int printtfsx { get; set; }

        ///订单状态0正常-1结单
        [Description("订单状态")]
        public int orderstatus { get; set; }

        /// <summary>
        /// 合同开始时间
        /// </summary>
        [Description("合同开始时间")]
        public DateTime orderstart_time { get; set; }

        /// <summary>
        /// 合同结束时间
        /// </summary>
        [Description("合同结束时间")]
        public DateTime orderend_time { get; set; }

        /// <summary>
        /// 租期
        /// </summary>
        [Description("租期")]
        public int zuqi { get; set; }
        /// <summary>
        /// 居住人数
        /// </summary>
        [Description("居住人数")]
        public int jzrs { get; set; }


        [Description("居住人id号")]
        public string userids { get; set; }
        [Description("居住人姓名")]
        public string usernames { get; set; }

        [Description("居住人身份证")]
        public string useridcards { get; set; }
        [Description("居住人手机号")]
        public string usermobiles { get; set; }
        [Description("入住率")]
        public decimal rzl { get; set; }
        [Description("合同入住天数")]
        public int htts { get; set; }
        [Description("实际入住天数")]
        public int sjts { get; set; }
        [Description("截止当日本年度天数")]
        public int yeardaysnow { get; set; }
        [Description("全年入住天数差")]

        
        public int yeardayscha { get; set; }

        [Description("年度开始日期/也叫入住率计算日期")]
        public DateTime yearstartday { get; set; }
        /// <summary>
        /// 付款方式：年付 月付 季付
        /// </summary>
        [Description("付款方式")]
        public string paytype { get; set; }

        [Description("下次付款日期")]
        public DateTime nextpaydate { get; set; }
        [Description("实付金额")]
        public decimal shifu { get; set; }



        [Description("实付金额b")]
        public decimal shifub { get; set; }
        [Description("实付金额c")]
        public decimal shifuc { get; set; }
        [Description("实付金额d")]
        public decimal shifud{ get; set; }

        [Description("是否决定退款")]
        public string istui { get; set; }
        [Description("是否决定退款备注")]
        public string istuinote { get; set; }

        [Description("欠费标记")]
        public string isqian { get; set; }
        public override string ToString()
        {
            return JSONhelper.ToJson(this);
        }
    }
}