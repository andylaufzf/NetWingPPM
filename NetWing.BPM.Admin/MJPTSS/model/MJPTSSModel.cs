using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJPTSS")]
	[Description("配套设施管理")]
	public class MJPTSSModel
	{
				/// <summary>
		/// 关键ID
		/// </summary>
		[Description("关键ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 名称
		/// </summary>
		[Description("名称")]
		public string title { get; set; }		
		/// <summary>
		/// 单位
		/// </summary>
		[Description("单位")]
		public string unit { get; set; }



        [Description("排序")]
        public int sort { get; set; }


		/// <summary>
		/// 价值
		/// </summary>
		[Description("价值")]
		public decimal cost { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string note { get; set; }		
		/// <summary>
		/// 部门所有者
		/// </summary>
		[Description("部门所有者")]
		public int depid { get; set; }		
		/// <summary>
		/// 数据所有者
		/// </summary>
		[Description("数据所有者")]
		public int ownner { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}