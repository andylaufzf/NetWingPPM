using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
    [TableName("MJOrderDetail")]
    [Description("入住手续子表")]
    public class MJOrderDetailModel
    {
        /// <summary>
        /// 系统编号
        /// </summary>
        [Description("系统编号")]
        public int KeyId { get; set; }
        /// <summary>
        /// 订单ID
        /// </summary>
        [Description("订单ID")]
        public int MJOrderid { get; set; }
        /// <summary>
        /// 服务名称
        /// </summary>
        [Description("服务名称")]
        public string serviceName { get; set; }
        /// <summary>
        /// 银行科目ID
        /// </summary>
        [Description("银行科目ID")]
        public int serviceId { get; set; }
        /// <summary>
        /// 单价
        /// </summary>
        [Description("单价")]
        public decimal sprice { get; set; }
        /// <summary>
        /// 数量
        /// </summary>
        [Description("数量")]
        public int num { get; set; }
        /// <summary>
        /// 总价
        /// </summary>
        [Description("总价")]
        public decimal allprice { get; set; }
        /// <summary>
        /// 部门名称
        /// </summary>
        [Description("部门名称")]
        public string dep { get; set; }
        /// <summary>
        /// 部门id
        /// </summary>
        [Description("部门id")]
        public int depid { get; set; }
        /// <summary>
        /// 服务开始时间
        /// </summary>
        [Description("服务开始时间")]
        public DateTime add_time { get; set; }
        /// <summary>
        /// 到期时间
        /// </summary>
        [Description("到期时间")]
        public DateTime exp_time { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public string note { get; set; }
        /// <summary>
        /// 数据所有者ID
        /// </summary>
        [Description("数据所有者ID")]
        public int ownner { get; set; }
        /// <summary>
        /// 经手人
        /// </summary>
        [Description("经手人")]
        public string contact { get; set; }
        /// <summary>
        /// 经手人ID
        /// </summary>
        [Description("经手人ID")]
        public int contactid { get; set; }
        /// <summary>
        /// 客户
        /// </summary>
        [Description("客户")]
        public string users { get; set; }
        /// <summary>
        /// 用户ID
        /// </summary>
        [Description("用户ID")]
        public int usersid { get; set; }
        /// <summary>
        /// 房间号
        /// </summary>
        [Description("房间号")]
        public string roomno { get; set; }
        /// <summary>
        /// 房间ID
        /// </summary>
        [Description("房间ID")]
        public int roomid { get; set; }
        /// <summary>
        /// 系统生成的订单ID
        /// </summary>
        [Description("系统生成的订单ID")]
        public string edNumber { get; set; }
        /// <summary>
        /// 付款方式：年付 月付 季付
        /// </summary>
        [Description("付款方式")]
        public string paytype { get; set; }

  





        [Description("下次付款日期")]
        public DateTime nextpaydate { get; set; }
        public override string ToString()
        {
            return JSONhelper.ToJson(this);
        }
    }
}