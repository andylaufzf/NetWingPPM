$(document).ready(function () {
    //jquery初始化时加载空表以便填入 所有datagrid用list

    $("#total").textbox({ readonly: true });
    $("#total").textbox("setValue", 0);

    $('#list').datagrid({
        url: '/MJOrderDetail/ashx/MJOrderDetailHandler.ashx',
        singleSelect: true,//只能选择一行
        queryParams: {
            filter: '{"groupOp":"AND","rules":[{"field":"MJOrderid","op":"eq","data":"' + $("#MJOrderid").html() + '"},{"field":"edNumber","op":"eq","data":"' + $("#edNumber").html() + '"}],"groups":[]}',

        },
        toolbar: [{
            iconCls: 'icon-save',
            text: '保存',
            handler: function () {
                var isValid = $("#form1").form('validate');//验证表单是否符合验证格式
                if (isValid) {

                } else {
                    $.messager.alert('提醒', '表格没有填写完整');
                    return false;//返回false 程序不执行
                }

                //得到正在编辑的所有行 以下商业逻辑用来做参考
                var k = $('#list').datagrid('getEditingRowIndexs');
                for (var i = 0; i < k.length; i++) {
                    var sped = $('#list').datagrid('getEditor', { index: k[i], field: 'sprice' });
                    var numed = $('#list').datagrid('getEditor', { index: k[i], field: 'num' });
                    var allpriceed = $('#list').datagrid('getEditor', { index: k[i], field: 'allprice' });
                    var spv = $(sped.target).textbox('getValue');
                    var numv = $(numed.target).textbox('getValue');//取得数量和单价的值
                    $(allpriceed.target).textbox('setValue', spv * numv);//设置总价
                }

                //asp.net 自动生成了一个form1 所以这里验证表单时有个#form1
                $('#list').datagrid('acceptChanges');
                var rows = $('#list').datagrid('getRows');//获取当前的数据行
                var total = 0//计算sumMoney的总和
                for (var i = 0; i < rows.length; i++) {
                    //alert(rows[i]['sumMoney']);
                    total += parseFloat(rows[i]['allprice']);
                }
                total = total.toFixed(2);//保留小数点两位注意是字符哦  
                //$('#payment').textbox({ value: total });//加金额给实付金额
                $('#total').textbox({ value: total });//加金额给实付金额
                $('.shifu').eq(0).numberbox('setValue', total);//实付金额第一个给初始值
            }
        },
            //编辑时不需要新增
            //{
            //    iconCls: 'icon-add',
            //    text: '新增',
            //    handler: function () {
            //        $('#list').datagrid('appendRow', { KeyId: 0, MJOrderid: 0, serviceName: '', sprice: 0, num: 1, allprice: 0, add_time: $("#orderstart_time").textbox("getValue"), exp_time: $("#orderend_time").textbox("getValue"), note: '', dep: '', depid: 0, });

            //    }

            //}
        ],
        rowStyler: function (index, row) {
            //格式化行 这里也只用来做参考
            //if (row.ftype == '支出') {
            //    return 'background-color:#6293BB;color:#fff;';    // rowStyle是一个已经定义了的ClassName(类名)
            //}
        },
        rownumbers: true, //行号
        columns: [[
            { title: '系统编号', field: 'KeyId', sortable: true, width: '', hidden: true, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '订单ID', field: 'MJOrderid', sortable: true, width: '', hidden: true, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
            {
                title: '项目(品名)', field: 'serviceName', sortable: true, width: '150', editor: {
                    type: 'combogrid', options: {
                        required: true, panelWidth: 150, validType: 'length[1,1000]', url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJaccountingSubjects&field=subjectType&q=收入&wherefield=usetype&wherestr=room', columns: [[
                            { field: 'KeyId', title: 'ID', width: 20, hidden: false },
                            { field: 'subjectType', title: '类型', width: 30, hidden: false },
                            { field: 'subjectName', title: '项目', width: 100 },
                            { field: 'note', title: '备注', hidden: true, width: 100 },
                        ]], textField: 'subjectName', idField: 'subjectName', missingMessage: '服务名称不能为空！',
                        limitToList: true,//只能从下拉中选择值
                        reversed: true,//定义在失去焦点的时候是否恢复原始值。//这样就必须进行选择
                        onHidePanel: function () {
                            var t = $(this).combogrid('getValue');//获取combogrid的值
                            var g = $(this).combogrid('grid');	// 获取数据表格对象
                            var r = g.datagrid('getSelected');	// 获取选择的行
                            console.log(r);
                            if (r == null || t != r.subjectName) {//没有选择或者选项不相等时清除内容
                                $.messager.alert('警告', '请选择，不要直接输入!');
                                $(this).combogrid('setValue', '');
                            } else {
                                //do something...
                            }
                        },

                    }
                }
            },
            { title: '项目ID', field: 'serviceId', sortable: true, width: '', hidden: true, editor: { type: 'textbox', options: { required: true, validType: 'number', missingMessage: '请输入数字' } } },
            { title: '单价', field: 'sprice', sortable: true, width: '', hidden: false, editor: { type: 'numberbox', options: { required: true, validType: 'number', missingMessage: '请输入数字' } } },
            { title: '数量', field: 'num', sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: true, validType: 'number', missingMessage: '请输入数字' } } },
            { title: '总价', field: 'allprice', sortable: true, width: '', hidden: false, editor: { type: 'numberbox', options: { required: true, validType: 'money', readonly: true, missingMessage: '请输入正确的金额' } } },
            { title: '服务开始时间', field: 'add_time', sortable: true, width: '120', hidden: false, editor: { type: 'datebox', options: { required: true, validType: 'date', missingMessage: '请输入合法的日期' } } },
            { title: '到期时间', field: 'exp_time', sortable: true, width: '120', hidden: false, editor: { type: 'datebox', options: { required: true, validType: 'date', missingMessage: '请输入合法的日期' } } },
            { title: '备注', field: 'note', sortable: true, width: '400', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '系统备注', field: 'setno', sortable: true, width: '150', hidden: false, editor: { type: 'combobox', options: { required: false, validType: '', missingMessage: '', readonly: true, disabled: true } } },
            { title: '部门名称', field: 'dep', sortable: true, width: '', hidden: true, editor: { type: 'combotree', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '部门id', field: 'depid', sortable: true, width: '', hidden: true, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },

        ]],
        data: [//先整10个空表格出来
            { KeyId: 0, MJOrderid: 0, serviceName: '', serviceId: 0, sprice: 0, num: 1, allprice: 0, add_time: $("#orderstart_time").textbox("getValue"), exp_time: $("#orderend_time").textbox("getValue"), note: '', setno: '', dep: '', depid: 0, },


        ],
        onClickCell: function (index, field, value) { //双击可以修改
            //alert($('#list').form('validate'));
            //$('#list').datagrid('beginEdit', index);
            //if (field == 'subject') {//如果当前选择的是科目名称
            //    var ed = $('#list').datagrid('getEditor', { index: index, field: 'subject' });//找到行editor
            //    $(ed.target).combobox({
            //        onSelect: function (rec) {//子节点选择时的动作
            //            var val = $(ed.target).combobox('getText');//得到combogrid的值
            //            var edb = $('#list').datagrid('getEditor', { index: index, field: 'subjectid' });//找到行editor 找到第一个列文本框编辑器
            //            $(edb.target).textbox('setText', val);//设置文本框
            //            var ft = $('#list').datagrid('getEditor', { index: index, field: 'ftype' });//得到收支类型
            //            $(ft.target).textbox('setValue', rec.subjectType);//设置收支类型

            //        }
            //    });//获得当前选择的值
            //}



        },
        onAfterEdit: onEndEdit, //绑定datagrid结束编辑时触发事件
        onDblClickRow: function (index, row) {//单击row
            $('#list').datagrid('beginEdit', index);
            //以下代码主要是用于行内编辑文本框做一些判断留着做一些参考
            var ed = $('#list').datagrid('getEditor', { index: index, field: 'serviceName' });//找到行editor
            $(ed.target).combogrid({
                onSelect: function (rowIndex, rowData) {//子节点选择时的动作
                    var noteed = $('#list').datagrid('getEditor', { index: index, field: 'note' });//找到行editor 找到第一个列文本框编辑器
                    var sided = $('#list').datagrid('getEditor', { index: index, field: 'serviceId' });//科目ID
                    $(noteed.target).textbox('setValue', rowData.note);//设置备注文本框
                    $(sided.target).textbox('setValue', rowData.KeyId);//给隐藏科目ID设置值
                    //alert(rowData.KeyId);//自行车停车费
                    if (rowData.KeyId == 14) {
                        var seted = $('#list').datagrid('getEditor', { index: index, field: 'setno' });
                        $(seted.target).combobox({////启用编辑器
                            url: '/MJDevice_child/ashx/MJDevice_childHandler.ashx',
                            valueField: 'KeyId',
                            textField: 'dcname',
                            disabled: false,
                            readonly: false,
                            required: true,
                            missingMessage: '请选择电瓶车',
                            loadFilter: function (data) {//过滤显示
                                return data.rows;
                            }
                        });
                        //alert("d");
                    } else {
                        var seted = $('#list').datagrid('getEditor', { index: index, field: 'setno' });
                        $(seted.target).combobox({////启用编辑器
                            disabled: true,
                            readonly: true
                        });
                    }

                }
            });//获得当前选择的值


            var allpriceed = $('#list').datagrid('getEditor', { index: index, field: 'allprice' });
            var numed = $('#list').datagrid('getEditor', { index: index, field: 'num' });//数量ed
            var sled = $('#list').datagrid('getEditor', { index: index, field: 'sprice' });//数量ed
            $(sled.target).numberbox({//改变数量时动作
                min: 0,
                //precision: 2,
                onChange: function (newValue, oldValue) {
                    //alert("o" + oldValue + "n" + newValue);
                    //计算单价
                    var num = $(numed.target).numberspinner('getValue');
                    $(allpriceed.target).numberbox('setValue', (num * newValue));
                    //alert(num);
                    $("#total").numberbox('setValue', compute('allprice'));//设置总价

                }
            });
            $(numed.target).numberspinner({//数量改变时动作
                min: 1,
                max: 99999999,
                value: 1,
                onChange: function (newValue, oldValue) {
                    var sl = $(sled.target).numberbox('getValue');
                    $(allpriceed.target).numberbox('setValue', (sl * newValue));//总价=数量*单价
                    //alert(compute('allprice'));
                    //设置总价
                    $("#total").numberbox('setValue', compute('allprice'));
                }
                //editable: false
            });

            // var deped = $('#list').datagrid('getEditor', { index: index, field: 'dep' });//得到部门ed
            // $(deped.target).combotree({
            //     required: true,
            //     validType: 'length[0,100]',
            //     url: '/sys/ashx/userhandler.ashx?json={"jsonEntity":"{}","action":"deps"}',
            //     textField: 'Title',
            //     onSelect: function (node) {
            //         var ed = $('#list').datagrid('getEditor', { index: index, field: 'depid' });
            //         $(ed.target).numberspinner('setValue', node.id); node.id = node.text;
            //     },
            //     missingMessage: ''
            // });


        }

    });

    loadMainInfo();//加载主表数据源字段信息
    //修改时加载订单主表数据
    //loadOrderMain();

});

//列求和
function compute(colName) {
    var rows = $('#list').datagrid('getRows');
    //alert("一共" + rows.length);
    var total = 0;
    for (var i = 0; i < rows.length; i++) {
        //total += parseFloat(rows[i][colName]);
        var ap = $('#list').datagrid('getEditor', { index: i, field: 'allprice' });
        total += parseFloat($(ap.target).numberbox('getValue'));
    }
    return total;
}



function onEndEdit(index, row, changes) {
    //alert(index);
    //row.allprice = row.sprice * row.num;//计算总价=单价*数量
    //alert(row.allprice);
    //alert(row);
    //var ed = $('#list').datagrid('getEditor', { index: index, field: 'allprice' });//找到行editor
    //$(ed.target).textbox('setValue', row.allprice);
    //$('#list').datagrid('updateRow', { index: index, row:row });//写入行数据
    //$('#list').datagrid('endEdit', index);//记录编辑的数据
    //alert();
};

//提交表单方法s
$("#submit").linkbutton({
    onClick: function () {//这里绑定的是easyui linkbutton事件
        submit();
    }
});

function submit() {

    $('#form1').form('submit', {
        url: '/MJOrderDetail/ashx/MJOrderMainEdit.ashx?MJOrderid=' + $("#MJOrderid").html() + '&edNumber=' + $("#edNumber").html() + '',
        onSubmit: function (param) {
            var isValid = $("#form1").form('validate');//验证表单是否符合验证格式
            if (isValid == false) {
                $.messager.alert('警告', '表单还有信息没有填完整！');
                return false;
                //$.messager.progress('close');	// 如果表单是无效的则隐藏进度条
            } else {//表单验证通过
                //验证实收金额不能为0
                //|| $("[name='shifu']").val() == "0" || $("[name='shifu']").val() == "0.00"
                if ($("[name='shifu']").val() == undefined || $("[name='shifu']").val() == "") {
                    $.messager.alert("实付金额错误!","请仔细检查金额！");
                    console.log("实付金额错误" + $("[name='shifu']").val());
                    return false;
                }

                $('#list').datagrid('acceptChanges');//datagrid接受改变
                //以下逻辑提供参考
                //if ($('#payment').numberbox('getValue') == '') {
                //alert("请先保存了再提交");
                //    $.messager.alert('警告', '请先保存再提交!');
                //    return false;
                //}
                //$('#list').datagrid({ singleSelect: false });//设置允许多选
                //循环选择行 只有选择行了之后才能返回选中行
                var rows = $('#list').datagrid('getRows');//获取当前的数据行
                console.log("获取所有行" + JSON.stringify(rows));
                //for (var i = 0; i < rows.length; i++) {//循环所有列
                //    //不允许为负数 if (rows[i]['subjectName'] != '' && rows[i]['sumMoney'] != 0) {
                //    if (rows[i]['subject'] != '') {
                //        $('#list').datagrid('selectRow', i);
                //        //alert(rows[i]['subject']);
                //    }
                //    else {
                //        //alert("有项目等于0");
                //    }
                //}


                var sf = 0;//0次水费
                for (var i = 0; i < rows.length; i++) {//循环所有列
                    //不允许为负数 if (rows[i]['subjectName'] != '' && rows[i]['sumMoney'] != 0) {
                    if (rows[i]['serviceName'] != '') {
                        $('#list').datagrid('selectRow', i);
                        //alert(rows[i]['subject']);

                        //如果所有信息都不为空，验证水费份数 serviceId=9
                        if (rows[i]['serviceId'] == '9') {
                            //sf = sf + 1;//水费+1次
                            sf = rows[i]['num'];
                        }

                    }
                    else {
                        console.log("有项目信息等于0");
                        //alert("有项目等于0");
                        $.messager.alert('注意', '请把信息填完整再提交!项目或品名请选择！！！');
                        return false;
                    }
                }

                var jzrsnum = $("#jzrs").numberspinner("getValue");
                //水费判断与实际不符。故此取消
                //if (jzrsnum != sf) {
                //    console.log("居住人数" + jzrsnum + "水份数" + sf);
                //    $.messager.alert('注意', '居住人数和水费数量不符。有多少人，需要交多少份水费！');
                //    return false;
                //}



                //var rows = $('#list').datagrid('getSelections');//返回选中的行
                if (rows == '') {//判断是否返回空数组 说明没有填写数据
                    //alert("空数组");
                    $.messager.alert('注意', '请填写或保存详细数据!');
                    return false;//返回失败
                } else {
                    //alert(JSON.stringify(rows));//有数据返回数据
                }
                param.main = $("#form1").serializeJSON();  //jquery方法把表单系列化成json后提交
                //以上用法参考https://github.com/macek/jquery-serialize-object
                param.detail = JSON.stringify(rows);//param  是EasyUI格式详细表序列化成json 
                console.log("提交前的明细参数："+param.detail);
                //alert(param.detail);
                console.log("前台验证完毕");
                return true;
                //return true;
            }
        },
        success: function (d) {
            d = JSON.parse(d);
            console.log("系统返回："+d);
            if (d == "" && d == undefined) {
                $.messager.alert("未知错误，请与管理员联系！");
                return false;
            }
            //alert(d);

            //var d = eval('(' + d + ')');  // change the JSON string to javascript object    
            if (d.status == 1) {
                //返回1标识成功！返回0失败
                //如果成功，关闭当前窗口
                // 添加一个未选中状态的选项卡面板

                //var hasTab = top.$('#tabs').tabs('exists', '入住手续主表');

                //if (!hasTab) {
                //    top.$('#tabs').tabs('add', {
                //        title: '入住手续主表',
                //        content: createFrame('/MJOrder/MJOrder.aspx?navid=109'),

                //        icon: '/css/icon/32/note.png'
                //    });
                //} else {
                //    top.$('#tabs').tabs('select', '入住手续主表');
                //    var tab = top.$('#tabs').tabs('getSelected');  // 获取选择的面板
                //    //刷新当前面板
                //    tab.panel('refresh', '/MJOrder/MJOrder.aspx?navid=109');
                //}
                //top.$('#tabs').tabs('close', '办理入住手续');//关闭办理入住手续

                $.messager.confirm('恭喜', d.msg, function (r) {
                    if (r) {
                        //因为要操作父窗口所以用top.
                        var tab = top.$('#tabs').tabs('getSelected');//获取当前选中tabs  
                        var index = top.$('#tabs').tabs('getTabIndex', tab);//获取当前选中tabs的index  
                        top.$('#tabs').tabs('close', index);//关闭对应index的tabs
                    } else {
                        var tab = top.$('#tabs').tabs('getSelected');//获取当前选中tabs  
                        var index = top.$('#tabs').tabs('getTabIndex', tab);//获取当前选中tabs的index  
                        top.$('#tabs').tabs('close', index);//关闭对应index的tabs
                    }
                });

                //location = '/MJOrder/MJOrder.aspx';//刷新本地网址
            } else {
                $.messager.alert('对不起!', d.msg);
            }

            //
            //alert("成功动作");
            //$.messager.progress('close');	// 如果提交成功则隐藏进度条
        }
    });

}

//提交表单方法e

//创建tab面板
function createFrame(url) {
    var s = '<iframe scrolling="auto" frameborder="0"  style="width:100%;height:100%;" src="' + url + '" ></iframe>';
    return s;
}


/*
*  datagrid 获取正在编辑状态的行，使用如下：
*  $('#id').datagrid('getEditingRowIndexs'); //获取当前datagrid中在编辑状态的行编号列表
*/
$.extend($.fn.datagrid.methods, {
    getEditingRowIndexs: function (jq) {
        var rows = $.data(jq[0], "datagrid").panel.find('.datagrid-row-editing');
        var indexs = [];
        rows.each(function (i, row) {
            var index = row.sectionRowIndex;
            if (indexs.indexOf(index) == -1) {
                indexs.push(index);
            }
        });
        return indexs;
    }
});


//加载客户信息
function loadMainInfo() {

    //为了好看，编辑前先整理代码
    //初始化 客户姓名 
    $("#usersid").combogrid({
        panelWidth: 200,
        required: true,
        validType: '',
        //value:'fullname',   
        idField: 'KeyId',
        textField: 'fullname',
        missingMessage: '',
        url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJUser&field=fullname',
        //如果是datagrid 或者combogrid时打开
        columns: [[
            { field: 'KeyId', title: 'KeyId', width: 60, hidden: false },
            { field: 'fullname', title: '客户姓名', width: 60 },
            { field: 'tel', title: '手机号', width: 100 },
        ]],
        onSelect: function (rowIndex, rowData) {
            $("#jzrsdiv").html("");
            //可以在这里写点什么逻辑
            $("#usersid").val(rowData.KeyId);//给隐藏的客户ID赋值
            $("#mobile").textbox("setValue", rowData.tel);//自定设置手机号
            $("#users").val(rowData.fullname);
            //添加第一个入住人也就是签合同人信息
            $("#jzrsdiv").append("<li>姓名：<input type='text' name='usernames' class='easyui-textbox usernames' value='" + rowData.fullname + "'>身份证号：<input type='text' name='useridcards' class='easyui-textbox useridcards' value='" + rowData.cardid + "'>手机号码<input type='text' name='usermobiles' class='easyui-textbox usermobiles' value='" + rowData.tel + "'></li>");
            //绑定样式
            $("#jzrsdiv .usernames").textbox({//姓名
                required: true,
                validType: 'CHS'
            });
            $("#jzrsdiv  .useridcards").textbox({//身份证号码
                required: true,
                validType: 'idcard'
            });
            $("#jzrsdiv  .usermobiles").textbox({//手机
                required: true,
                validType: 'mobile'
            });

        }
    });

    //为了好看，编辑前先整理代码
    //初始化 房号 
    $("#roomid").combogrid({
        panelWidth: 250,
        required: true,
        editable: false,//不允许编辑
        validType: '',
        idField: 'KeyId',
        textField: 'roomnumber',
        missingMessage: '',
        url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJRooms&field=state&q=空闲',
        columns: [[
            { field: 'KeyId', title: 'KeyId', width: 60, hidden: false },
            { field: 'roomnumber', title: '房号', width: 60 },
            { field: 'vest', title: '所属', width: 60 },
            { field: 'state', title: '状态', width: 60 },
        ]],
        onSelect: function (rowIndex, rowData) {
            //可以在这里写点什么逻辑
            $("#roomno").val(rowData.roomnumber);//给隐藏的房间ID赋值
        }
    });






    //以下逻辑作为借鉴
    ////客户信息
    //$("#unit").combogrid({
    //    panelWidth: 350,
    //    required: true,
    //    //value:'fullname',   
    //    idField: 'fullname',
    //    textField: 'fullName',
    //    url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJUser&field=fullname',
    //    columns: [[
    //        { field: 'KeyId', title: 'KeyId', width: 60, hidden: true },
    //        { field: 'fullname', title: '姓名', width: 60 },
    //        { field: 'tel', title: '电话', width: 100 },
    //        { field: 'sex', title: '性别', width: 40 },
    //        { field: 'email', title: 'email', width: 100 }
    //    ]],
    //    onSelect: function (rowIndex, rowData) {
    //        $("#unitid").val(rowData.KeyId);//给隐藏的客户ID赋值
    //    }

    //});
    //经手人
    $("#contactid").combogrid({
        panelWidth: 350,
        required: true,
        //value:'fullname',   
        editable: false,
        idField: 'KeyId',
        textField: 'TrueName',
        url: '/sys/ashx/userhandler.ashx?action=mydep',
        columns: [[
            { field: 'TrueName', title: '姓名', width: 60 },
            { field: 'KeyId', title: 'KeyId', width: 100 }
        ]],
        onSelect: function (rowIndex, rowData) {
            $("#contactid").val(rowData.KeyId);//给经手人设置ID
            $("#contact").val(rowData.TrueName);//经手人
            //alert(rowData.TrueName);
        }
    });
    //部门
    $("#depid").combotree({
        url: '/sys/ashx/userhandler.ashx?action=deps',
        required: true,
        onSelect: function (node) {
            $("#dep").val(node.text);//设置部门
            //alert(node.text);

        }
    });

    //银行
    $(".accountid").combobox({
        valueField: 'KeyId',//用中文名作为名称
        required: true,
        editable: false,
        textField: 'accountName',
        url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJBankAccount&field=accountName',
        onSelect: function (rec) {
            $(this).prev('.account').val(rec.accountName);
            //$("#account").val(rec.accountName);//设置隐藏ID
        }
    });

}


function initBank() {
    $(".easyui-linkbutton").linkbutton({});//初始化easyui按钮
    $(".shifu").numberbox({//初始化实付金额
        min: 0,
        precision: 2
    });
    //银行
    $(".accountid").combobox({
        valueField: 'KeyId',//用中文名作为名称
        required: true,
        textField: 'accountName',
        url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJBankAccount&field=accountName',
        onSelect: function (rec) {
            $(this).prev('.account').val(rec.accountName);
            //$("#account").val(rec.accountName);//设置隐藏ID
        }
    });

    $(".paytypea").click(function (e) {
        $(this).closest("li").remove();
    });
}



//付款方式
function addpaytype() {
    var c = $("#paytypediv").children('li').length;
    if (c == 3) {//付款方式最多三个
        $.messager.alert('警告', '付款方式最多三个');
        return false;
    }
    var temp = '<li class="paytypeli"><input  name="account" class="account" value="" style="display: none;" />付款方式: <input  name="accountid" class="accountid" value="" />实收金额: <input type="text" name="shifu" class="easyui-numberbox shifu" value="0" style="width: 100px;" data-options="min:0,precision:2"></input><a  href="#"  class="easyui-linkbutton paytypea" data-options="iconCls:\'icon-cancel\'">删除</a></li> ';
    //alert(temp);
    $("#paytypediv").append(temp);
    initBank();//初始化银行
}



function getRowIndex(target) {
    var tr = $(target).closest("tr.datagrid-row");
    return tr.attr("datagrid-row-index");
}

function createFrame(url) {
    var s = '<iframe scrolling="auto" frameborder="0"  style="width:100%;height:100%;" src="' + url + '" ></iframe>';
    return s;
}

//时间选择事件
$("#orderstart_time").datebox({
    onSelect: function (date) {
        //alert(date.getFullYear() + ":" + (date.getMonth() + 1) + ":" + date.getDate());
        var k = $("#zuqi").val();//得到输入框选中值一年还是一个月
        //alert(k);
        var newDate = new Date(date.dateAdd("m", k));
        //alert("新日期" + newDate);
        //alert(newDate.getFullYear());
        //var expDay = newDate.getFullYear() + "-" + newDate.getMonth() + "-" + newDate.getDate();
        //alert("到期" + expDay);
        $("#orderend_time").datebox("setValue", newDate.Format("yyyy-MM-dd"));
        //alert(newDate);  

    }
});
//选择年份事件
$("#zuqi").change(function () {
    var k = $("#zuqi").val();//得到输入框选中值一年还是一个月
    var v = $("#orderstart_time").datebox("getValue");
    if (v != "") {//如果是空说明日期还没有选择
        var sdate = new Date(v);
        var newDate = new Date(sdate.dateAdd("m", k));
        var expDate = newDate.dateAdd("d", -1);//租期应该减一天
        $("#orderend_time").datebox("setValue", expDate.Format("yyyy-MM-dd"));
    }

});

//下次付款时间事件
$('#paytype').combobox({
    valueField: 'value',
    textField: 'label',
    editable: false,
    required: true,
    data: [{
        label: '1月付',
        value: '1月付'
    }, {
        label: '2月付',
        value: '2月付'
    }, {
        label: '3月付',
        value: '3月付'
    }, {
        label: '4月付',
        value: '4月付'
    }, {
        label: '5月付',
        value: '5月付'
    }, {
        label: '6月付',
        value: '6月付'
    }, {
        label: '7月付',
        value: '7月付'
    }, {
        label: '8月付',
        value: '8月付'
    }, {
        label: '9月付',
        value: '9月付'
    }, {
        label: '10月付',
        value: '10月付'
    }, {
        label: '11月付',
        value: '11月付'
    }, {
        label: '12月付',
        value: '12月付'
    }],
    onSelect: function (rec) {

        var orderstart_time = new Date($('#orderstart_time').datebox('getValue'));
        //alert(orderstart_time);
        var paytypev = new Date();
        switch (rec.value) {
            case '1月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 1));
                break;
            case '2月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 2));
                break;
            case '3月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 3));
                break;
            case '4月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 4));
                break;
            case '5月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 5));
                break;
            case '6月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 6));
                break;
            case '7月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 7));
                break;
            case '8月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 8));
                break;
            case '9月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 9));
                break;
            case '10月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 10));
                break;
            case '11月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 11));
                break;
            case '12月付':
                paytypev = new Date(orderstart_time.dateAdd("m", 12));
                break;

            case '季付':
                paytypev = new Date(orderstart_time.dateAdd("m", 3));
                break;
            case '半年付':
                paytypev = new Date(orderstart_time.dateAdd("m", 6));
                break;
            case '年付':
                paytypev = new Date(orderstart_time.dateAdd("m", 12));
                break;
            default:
                break;
        }
        paytypev = paytypev.dateAdd("d", -5);//付款时间要提前5天
        $('#nextpay').datebox('setValue', paytypev.Format("yyyy-MM-dd"));
    }
});



//页面加载完执行
window.onload = function () {
    insertjzr();//加载时插入居住人
};


$("#jzrs").numberspinner({
    editable: false,
    onSpinUp: function () {//鼠标向上点
        insertjzr();//插入居住人
    },
    onSpinDown: function () {//鼠标向下点
        var v = $("#jzrsdiv").children("li").length;
        if (v == 1) {//只有一个时不能删除
            $.messager.alert("注意", "至少要有一个入住人！！！");
            //$.messager.alert('警告', '表单还有信息没有填完整！');
        } else {
            $("#jzrsdiv li:last").remove();
        }




    }
});


//插入居住人
function insertjzr() {
    $("#jzrsdiv").append("<li style='margin-top:5px;'>姓名：<input type='text' name='usernames' class='easyui-textbox usernames' value=''>身份证号：<input type='text' name='useridcards' class='easyui-textbox useridcards' value=''>手机号码：<input type='text' name='usermobiles' class='easyui-textbox usermobiles' value=''></li>");

    //绑定样式
    $("#jzrsdiv .usernames").textbox({//姓名
        required: true,
        validType: 'CHS'
    });
    $("#jzrsdiv  .useridcards").textbox({//身份证号码
        required: true,
        validType: 'idcard'
    });
    $("#jzrsdiv  .usermobiles").textbox({//手机
        required: true,
        validType: 'mobile'
    });
    //alert("p");

}



//DateAdd函数
function DateAdd(interval, number, date) {
    switch (interval.toLowerCase()) {
        case "y": return new Date(date.setFullYear(date.getFullYear() + number));
        case "m": return new Date(date.setMonth(date.getMonth() + number));
        case "d": return new Date(date.setDate(date.getDate() + number));
        case "w": return new Date(date.setDate(date.getDate() + 7 * number));
        case "h": return new Date(date.setHours(date.getHours() + number));
        case "n": return new Date(date.setMinutes(date.getMinutes() + number));
        case "s": return new Date(date.setSeconds(date.getSeconds() + number));
        case "l": return new Date(date.setMilliseconds(date.getMilliseconds() + number));
    }
}

//
Date.prototype.dateAdd = function (interval, number) {
    var d = this;
    var k = { 'y': 'FullYear', 'q': 'Month', 'm': 'Month', 'w': 'Date', 'd': 'Date', 'h': 'Hours', 'n': 'Minutes', 's': 'Seconds', 'ms': 'MilliSeconds' };
    var n = { 'q': 3, 'w': 7 };
    eval('d.set' + k[interval] + '(d.get' + k[interval] + '()+' + ((n[interval] || 1) * number) + ')');
    return d;
}
Date.prototype.dateDiff = function (interval, objDate2) {
    var d = this, i = {}, t = d.getTime(), t2 = objDate2.getTime();
    i['y'] = objDate2.getFullYear() - d.getFullYear();
    i['q'] = i['y'] * 4 + Math.floor(objDate2.getMonth() / 4) - Math.floor(d.getMonth() / 4);
    i['m'] = i['y'] * 12 + objDate2.getMonth() - d.getMonth();
    i['ms'] = objDate2.getTime() - d.getTime();
    i['w'] = Math.floor((t2 + 345600000) / (604800000)) - Math.floor((t + 345600000) / (604800000));
    i['d'] = Math.floor(t2 / 86400000) - Math.floor(t / 86400000);
    i['h'] = Math.floor(t2 / 3600000) - Math.floor(t / 3600000);
    i['n'] = Math.floor(t2 / 60000) - Math.floor(t / 60000);
    i['s'] = Math.floor(t2 / 1000) - Math.floor(t / 1000);
    return i[interval];
}

// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
//调用： 

//var time1 = new Date().Format("yyyy-MM-dd");
//var time2 = new Date().Format("yyyy-MM-dd HH:mm:ss"); 



//$.get("submit.php", { id: '123', name: '小王', }, function (data, state) {

//    //这里显示从服务器返回的数据            
//    alert(data);
//    //这里显示返回的状态                
//    if (state == 'ok') {
//        alert("返回数据成功");
//    } else {
//        alert("返回数据失败");
//    }
//});      

//全部加在完ajax 取值
//MJOrder/ashx/MJOrderHandler.ashx
//filter:{"groupOp":"AND","rules":[{"field":"KeyId","op":"eq","data":"111"},{"field":"KeyId","op":"eq","data":"RZ"}],"groups":[]}


window.onload = function () {
    insertjzr();//加载居住人
    loadOrderMain();
}



function loadOrderMain() {
    var searchstr = '{"groupOp":"AND","rules":[{"field":"KeyId","op":"eq","data":"' + $("#MJOrderid").html() + '"},{"field":"edNumber","op":"eq","data":"' + $("#edNumber").html() + '"}],"groups":[]}';
    console.log("searchstr:" + searchstr);
    $.get("/MJOrder/ashx/MJOrderHandler.ashx", { filter: searchstr }, function (data, state) {

        //这里显示从服务器返回的数据  
        console.log("状态" + state);
        //alert(data);
        //这里显示返回的状态                
        if (state == 'success') {
            //alert("返回数据成功");
            console.log("数据加载成功！" + data);
            var jsondata = $.parseJSON(data);
            console.log(jsondata);
            $("#roomid").combogrid("setValue", jsondata.rows[0].roomno);
            //$("#roomid").combogrid('setValue', { idField: jsondata.rows[0].roomid, textField: jsondata.rows[0].roomno });
            

            $("#roomid").combogrid('readonly', true);//只读模式
            
            $("[name='roomid']").val(jsondata.rows[0].roomid);//给房间id赋值 显示的text是一样。实际ID又是另外一个
 
            $("#zuqi").val(jsondata.rows[0].zuqi);//租期
            $("#roomno").val(jsondata.rows[0].roomno);//给隐藏的房间ID赋值
            $("#orderstart_time").datebox('setValue', jsondata.rows[0].orderstart_time);
            $("#orderend_time").datebox('setValue', jsondata.rows[0].orderend_time);
            $("#jzrs").numberspinner('setValue', jsondata.rows[0].jzrs);
            $("#paytype").combobox("setValue", jsondata.rows[0].paytype);
            $("#nextpay").datebox("setValue", jsondata.rows[0].nextpaydate);

            //处理居住人
            if (jsondata.rows[0].jzrs > 1) {
                for (var i = 0; i < jsondata.rows[0].jzrs-1; i++) {
                    insertjzr();//插入居住人 有几个插入几次-1
                }
                //拆分字符
                var usernamesarr = new Array(); //定义一数组 
                usernamesarr = jsondata.rows[0].usernames.split(","); //字符分割 
                var useridcardsarr = new Array();//身份证数组
                useridcardsarr = jsondata.rows[0].useridcards.split(",");
                var usermobilesarr = new Array();//手机号数组
                usermobilesarr = jsondata.rows[0].usermobiles.split(",");
                for (i = 0; i < usernamesarr.length; i++) {
                    //document.write(strs[i] + "<br/>"); //分割后的字符输出 
                    $("#jzrsdiv li .usernames").eq(i).textbox('setValue', usernamesarr[i]);
                    $("#jzrsdiv li .useridcards").eq(i).textbox('setValue', useridcardsarr[i]);
                    $("#jzrsdiv li .usermobiles").eq(i).textbox('setValue', usermobilesarr[i]);
                } 
                console.log("居住人数大于1");
            } else {//如果只有一个，直接处理

                $("#jzrsdiv li .usernames").textbox('setValue', jsondata.rows[0].usernames);
                $("#jzrsdiv li .usernames").textbox('readonly', true);
                $("#jzrsdiv li .useridcards").textbox('setValue', jsondata.rows[0].useridcards);
                $("#jzrsdiv li .useridcards").textbox('readonly', true);
                $("#jzrsdiv li .usermobiles").textbox('setValue', jsondata.rows[0].usermobiles);
                $("#jzrsdiv li .usermobiles").textbox('readonly', true);
                console.log("居住人数为1");
            }




            $("#contact").val(jsondata.rows[0].contact);
            $("#contactid").combogrid('setValue', jsondata.rows[0].contactid);


            $("#total").numberbox('setValue', jsondata.rows[0].total);
            console.log("居住人" + jsondata.rows[0].usernames);
            console.log("房间ID" + jsondata.rows[0].roomid + "房间号" + jsondata.rows[0].roomno);


            //alert(jsondata.rows);


        } else {
            alert("返回数据失败");
        }
    });
}





