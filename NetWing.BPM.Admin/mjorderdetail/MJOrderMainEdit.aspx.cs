﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace NetWing.BPM.Admin.mjorderdetail
{
    public partial class MJOrderMainEdit : NetWing.BPM.Core.BasePage.BpmBasePage
    {
        public string mytitle = "";//收入单标题
        public string otype = "";//收支单据类型
        public string edNumber = "";
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request["type"] == "IN")//收入
            {
                mytitle = "网翼收入单";
                otype = "IN";

            }
            else if (Request["type"] == "OUT")
            {
                mytitle = "翼通支出单";
                otype = "OUT";
            }
            else if (string.IsNullOrEmpty(Request["type"]))
            {
                mytitle = "翼通支出单";
                otype = "F";
            }

            edNumber = common.mjcommon.getedNumber(otype);
        }
    }
}