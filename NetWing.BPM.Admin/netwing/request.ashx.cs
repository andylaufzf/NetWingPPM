﻿using NetWing.BPM.Core;
using NetWing.Common;
using NetWing.Common.Data.SqlServer;
using NetWing.Model;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web;
using System.Web.SessionState;

namespace NetWing.BPM.Admin.netwing
{
    /// <summary>
    /// request 的摘要说明
    /// </summary>
    public class request : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string action = context.Request["action"];
            if (string.IsNullOrEmpty(action))
            {
                context.Response.Write("{\"code\":4000, \"msg\":\"请求失败,参数有误!\"}");
                return;
            }
            switch (action)
            {
               
                case "GetQRCode"://获取虚拟卡二维码
                    GetQRCode(context);
                    break;
                
                default:
                    context.Response.Write("{\"code\":1000, \"msg\":\"请求失败,请重新尝试!\"}");
                    break;
            }
        }

        #region------------获取虚拟卡二维码--------------
        public void GetQRCode(HttpContext context)
        {
            try
            {
                string KeyId = context.Request["KeyId"];
                if (string.IsNullOrEmpty(KeyId))
                {
                    context.Response.Write("{\"code\":4000, \"msg\":\"请求失败，参数有误！\"}");
                    return;
                }
                DataTable dt = SqlEasy.ExecuteDataTable("select * from jydOrder where KeyId=" + KeyId);
                if (dt.Rows.Count > 0)
                {
                    string dtJson = JSONhelper.ToJson(dt, false);
                    dtJson = dtJson.Substring(0, dtJson.Length - 1);
                    dtJson = dtJson.Substring(1);
                    JydOrderModel model = JsonConvert.DeserializeObject<JydOrderModel>(dtJson);
                    string OrderId = QRCode.Generate("http://" + HttpContext.Current.Request.Url.Host + "/JydModleOrder/QRCodeView.aspx?KeyID=" + KeyId, @"JydModleOrder\img\gongch.png", model.OrderID);
                    SqlEasy.ExecuteNonQuery("update jydOrder set OrderId='" + OrderId + "' where KeyId=" + KeyId);
                    context.Response.Write("{\"code\":0, \"msg\":\"二维码获取成功!\", \"QRCodeUrl\":\"" + OrderId + "\"}");
                    return;
                }
                context.Response.Write("{\"code\":200, \"msg\":\"二维码获取失败!\"}");
                return;
            }
            catch (Exception e)
            {
                WriteLogs.WriteLogsE("Logs", "Error >> GetQRCode", e.Message + " >>> " + e.StackTrace);
                context.Response.Write("{\"code\":4000, \"msg\":\"系统错误!\"}");
            }
        }
        #endregion


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}