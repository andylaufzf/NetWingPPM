using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJBankAccount")]
	[Description("银行账户设置")]
	public class MJBankAccountModel
	{
				/// <summary>
		/// 账户Id
		/// </summary>
		[Description("账户Id")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 账户简名
		/// </summary>
		[Description("账户简名")]
		public string accountName { get; set; }		
		/// <summary>
		/// 账户简名拼音
		/// </summary>
		[Description("账户简名拼音")]
		public string accountNamePinYin { get; set; }		
		/// <summary>
		/// 账户余额
		/// </summary>
		[Description("账户余额")]
		public decimal accountBalance { get; set; }		
		/// <summary>
		/// 开户银行
		/// </summary>
		[Description("开户银行")]
		public string accountBank { get; set; }		
		/// <summary>
		/// 账户全名
		/// </summary>
		[Description("账户全名")]
		public string accountFullName { get; set; }		
		/// <summary>
		/// 账号
		/// </summary>
		[Description("账号")]
		public string accountNo { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string accountNote { get; set; }		
		/// <summary>
		/// 删除标记
		/// </summary>
		[Description("删除标记")]
		public string is_del { get; set; }
        /// <summary>
        /// 数据所有者ID
        /// </summary>
        [Description("数据所有者ID")]
        public int ownner { get; set; }
        /// <summary>
        /// 部门ID
        /// </summary>
        [Description("部门ID")]
        public int depid { get; set; }
        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}