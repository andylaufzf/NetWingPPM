using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJBankAccountBll
    {
        public static MJBankAccountBll Instance
        {
            get { return SingletonProvider<MJBankAccountBll>.Instance; }
        }

        public int Add(MJBankAccountModel model)
        {
            return MJBankAccountDal.Instance.Insert(model);
        }

        public int Update(MJBankAccountModel model)
        {
            return MJBankAccountDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJBankAccountDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJBankAccountDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
