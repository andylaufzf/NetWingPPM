﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="print_order_sn.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.print_order_sn" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>礼品盒备料单</title>
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <script type="text/javascript" src="../scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <link type="text/css" rel="stylesheet" href="css/base.css" />
    <script>
        //窗口API
        var api = frameElement.api, W = api.opener;
        var myDate = new Date();
        api.button(
            {
                name: '确认打印',
                focus: true,
                callback: function () {
                    printWin();
                    return false;
                }
            },
            //{
            //    name: '导出Excel',
            //    callback: function () {
            //        ajaxExcel();
            //        return false;
            //    }
            //},
            {
                name: '取消'
            }
        );

        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("print_content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }

    </script>
</head>
<body>
    <div id="print_content">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <p class="MsoNormal" style="text-align: center; margin: 0 auto;">
            <img width="61" height="34" src="/JydWodrmb/img/dyt.png" style="text-align: center;"><b style="font-size: 24px; font-family: 微软雅黑; height: 30px; line-height: 30px; margin: 4px 0 0 0;">昆明聚源达彩印包装有限公司礼品盒备料单</b>&nbsp;&nbsp;&nbsp;<span style="font-family: 微软雅黑; font-size: 12px; height: 20px; line-height: 20px;"></span>
        </p>

        <tr>
            <td colspan="4">
                <table width="100%" border="0" align="left" id="3">
                    <tr style="width: 100%; float: left; margin: 0px 0 5px 0;">
                        <td style="width: 32% !important; font-family: 微软雅黑; font-size: 12px; float: left;">客户名称：<%=comname %></td>
                        <td style="width: 25% !important; font-family: 微软雅黑; font-size: 12px; float: left;">负责业务员：<%=salesman %></td>
                        <td style="width: 25% !important; font-family: 微软雅黑; font-size: 12px; float: left;">单号: <%=order_sn %></td>
                        <!---这里主要用来存值-->
                    </tr>
                </table>
            </td>
        </tr>
        <table  class="MsoTableGrid" border="1" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse; width: 100%; margin: 0 auto;">


               <tr>
                                <th width="10%" height="30"  align="center" style="font-family: 微软雅黑; font-size: 12px;">品名</th>
                                <th width="15%" height="30"  align="center" style="font-family: 微软雅黑; font-size: 12px;">纸张/板材</th>
                                <th width="10%"  align="center" style="font-family: 微软雅黑; font-size: 12px;">规格</th>
                                <th width="20%" height="30"  align="middle" style="font-family: 微软雅黑; font-size: 12px;">数量</th>
                                <th height="15%"  align="middle" style="font-family: 微软雅黑; font-size: 12px;">开料规格</th>
                                <th height="15%"  align="center" style="font-family: 微软雅黑; font-size: 12px;">工艺要求</th>
                                <th width="10%" align="center" style="font-family: 微软雅黑; font-size: 12px;">备料员</th>
                                <th width="20%" align="center" style="font-family: 微软雅黑; font-size: 12px;">备注</th>

                            </tr>
                    <%foreach (DataRow data in dt.Rows)
                        {%>
                           <tr>
                                <td width="10%" height="30"  align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=data["productname"].ToString() %></td>
                                <td width="15%" height="30"  align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=data["material"].ToString() %></td>
                                <td width="10%"  align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=data["specifications"].ToString() %></td>
                                <td width="20%" height="30"  align="middle" style="font-family: 微软雅黑; font-size: 12px;"><%=data["number"].ToString() %></td>
                                <td height="15%"  align="middle" style="font-family: 微软雅黑; font-size: 12px;"><%=data["materialopening"].ToString() %></td>
                                <td height="15%"  align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=data["technology"].ToString() %></td>
                               <td width="10%" align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=data["billingclerk"].ToString() %></td> 
                               <td width="20%" align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=data["note"].ToString() %></td>
                            </tr>
                            
                        <%} %>





            <%foreach (DataRow datae in dat.Rows)
                        {%>
                <td colspan="8" align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=datae["unit_man"].ToString() %></td>


            <%} %>
        </table>





                    <div style="width:98%; padding:10px 1% 5px 1%;">
                    <p style="float:left;">开单:<%=kdy %></p>                  <p style="float:right;">总经理签字:<%=manager %></p>
                    </div>
    </table>





        </div>
</body>
</html>
