﻿using System;
using System.Data;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;

namespace NetWing.BPM.Admin.JydWodrmb
{
    public partial class Orderform : System.Web.UI.Page
    {
        protected string unitname;//采购单位
        protected string unit_man;//采购单位联系人
        protected string unit_moble; //采购联系人
        protected string Contacts; //经手人
        protected string goodsName; //采购品名
        protected string edNumber;//采购编号
        protected int buyNum;//采购数量
        protected string goodsNo;//采购分类
        protected string buyPrice;//采购单价
        protected string buyAllMoney;//采购金额
        public DataTable dt = null;
        public DataTable de = null;
        protected void Page_Load(object sender, EventArgs e)
        {

            string KeyId = Request["KeyId"];
            string userkeyid = Request["userkeyid"];

            DataTable dataTable = SqlEasy.ExecuteDataTable("select * from Psi_BuyDetails where KeyId = "+ KeyId + "");

            unitname = dataTable.Rows[0]["unitname"].ToString();
            unit_man = dataTable.Rows[0]["unit_man"].ToString();
            unit_moble = dataTable.Rows[0]["unit_moble"].ToString();
            Contacts = dataTable.Rows[0]["Contacts"].ToString();
            goodsName = dataTable.Rows[0]["goodsName"].ToString();
            edNumber = dataTable.Rows[0]["edNumber"].ToString();
            buyNum = int.Parse(dataTable.Rows[0]["buyNum"].ToString());
            goodsNo = dataTable.Rows[0]["goodsNo"].ToString();
            string  buyPri = dataTable.Rows[0]["buyPrice"].ToString();
            string  buylMoney = dataTable.Rows[0]["buyAllMoney"].ToString();

            buyPrice = buyPri.Substring(1);
            buyAllMoney = buylMoney.Substring(1);

        }
    }
}