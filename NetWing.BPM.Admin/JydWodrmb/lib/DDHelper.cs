﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NetWing.BPM.Admin.JLDD.model;

namespace NetWing.BPM.Admin.JLDD.lib
{
    public static class DDHelper
    {
        /// <summary>
        /// 获取企业的access_token
        /// </summary>
        /// <param name="corpId">企业Id</param>
        /// <param name="corpSecret">企业应用的凭证密钥</param>
        /// <returns></returns>
        public static string GetAccessToken(string corpId, string corpSecret)
        {
            string url = string.Format("https://oapi.dingtalk.com/gettoken?corpid={0}&corpsecret={1}", corpId, corpSecret);
            try
            {
                string response = HttpRequestHelper.Get(url);
                AccessTokenModel oat = JsonConvert.DeserializeObject<AccessTokenModel>(response);

                if (oat != null)
                {
                    if (oat.errcode == 0)
                    {
                        return oat.access_token;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return string.Empty;
        }
        /// <summary>
        /// 获取企业的jsticket
        /// </summary>
        /// <param name="accessToken">调用接口凭证</param>
        /// <returns></returns>
        public static string GetJsApiTicket(string accessToken)
        {
            string url = string.Format("https://oapi.dingtalk.com/get_jsapi_ticket?access_token={0}", accessToken);
            try
            {
                string response = HttpRequestHelper.Get(url);
                JsApiTicketModel model = JsonConvert.DeserializeObject<JsApiTicketModel>(response);

                if (model != null)
                {
                    if (model.errcode == 0)
                    {
                        return model.ticket;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return string.Empty;
        }
        /// <summary>
        /// 生成签名的时间戳
        /// </summary>
        /// <returns></returns>
        public static long GetTimeStamp()
        {
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds);
        }
        /// <summary>
        /// 通过CODE换取用户身份
        /// </summary>
        /// <param name="accessToken">调用接口凭证</param>
        /// <param name="code">requestAuthCode接口中获取的CODE</param>
        /// <returns></returns>
        public static string GetUserId(string accessToken, string code)
        {
            string url = string.Format("https://oapi.dingtalk.com/user/getuserinfo?access_token={0}&code={1}", accessToken, code);
            try
            {
                string response = HttpRequestHelper.Get(url);
                GetUserInfoModel model = JsonConvert.DeserializeObject<GetUserInfoModel>(response);

                if (model != null)
                {
                    if (model.errcode == 0)
                    {
                        return model.userid;
                    }
                    else
                    {
                        throw new Exception(model.errmsg);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return string.Empty;
        }
        /// <summary>
        /// 通过userId获取用户详情json
        /// </summary>
        /// <param name="accessToken">调用接口凭证</param>
        /// <param name="userId">员工在企业内的UserID，企业用来唯一标识用户的字段</param>
        /// <returns></returns>
        public static string GetUserDetailJson(string accessToken, string userId)
        {
            string url = string.Format("https://oapi.dingtalk.com/user/get?access_token={0}&userid={1}", accessToken, userId);
            try
            {
                string response = HttpRequestHelper.Get(url);
                return response;
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }
        /// <summary>
        /// 通过userId获取用户详情model
        /// </summary>
        /// <param name="accessToken">调用接口凭证</param>
        /// <param name="userId">员工在企业内的UserID，企业用来唯一标识用户的字段</param>
        /// <returns></returns>
        public static UserDetailInfo GetUserDetail(string accessToken, string userId)
        {
            string url = string.Format("https://oapi.dingtalk.com/user/get?access_token={0}&userid={1}", accessToken, userId);
            try
            {
                string response = HttpRequestHelper.Get(url);
                UserDetailInfo model = JsonConvert.DeserializeObject<UserDetailInfo>(response);

                if (model != null)
                {
                    if (model.errcode == 0)
                    {
                        return model;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }
        /// <summary>
        /// 发送普通消息
        /// </summary>
        /// <param name="sender">消息发送者员工ID</param>
        /// <param name="cid">群消息或者个人聊天会话Id</param>
        /// <param name="msgtype">消息类型</param>
        /// <param name="text">消息内容</param>
        /// <param name="access_token">接口凭证</param>
        /// <returns></returns>
        public static string pickConversation(string sender, string cid, string msgtype, string text, string access_token)
        {
            string url = string.Format("https://oapi.dingtalk.com/message/send_to_conversation?access_token={0}", access_token);
            try
            {
                string skey = Operation.randString();
                string response = HttpRequestHelper.PostSend(url, skey, sender, cid, msgtype, text);
                pickConversation oat = JsonConvert.DeserializeObject<pickConversation>(response);

                if (oat != null)
                {
                    if (oat.errcode == 0)
                    {
                        return oat.errmsg;
                    }
                    else
                    {
                        return oat.errmsg;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return string.Empty;
        }
    }
}