﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="print_pringbylcd.aspx.cs" Inherits="NetWing.BPM.Admin.JydWodrmb.print_pringbylcd" %>

<!DOCTYPE html>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>聚源达流程单</title>
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <script type="text/javascript" src="../scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <link type="text/css" rel="stylesheet" href="css/base.css" />
    <script>
        //窗口API
        var api = frameElement.api, W = api.opener;
        var myDate = new Date();
        api.button(
            {
                name: '确认打印',
                focus: true,
                callback: function () {
                    printWin();
                    return false;
                }
            },
            //{
            //    name: '导出Excel',
            //    callback: function () {
            //        ajaxExcel();
            //        return false;
            //    }
            //},
            {
                name: '取消'
            }
        );

        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("print_content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="print_content">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="left" valign="top">
                        <table cellspacing="0" cellpadding="0" width="100%" border="0">

                            <tr>
                                <td height="11" align="left">


                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <p class="MsoNormal" style="text-align: center; margin: 0 auto;">
                                            <img width="61" height="34" src="/JydWodrmb/img/dyt.png" style="text-align: center;"><b style="font-size: 24px; font-family: 微软雅黑; height: 30px; line-height: 30px; margin: 4px 0 0 0;">昆明聚源达彩印包装有限公司流程单</b>&nbsp;&nbsp;&nbsp;<span style="font-family: 微软雅黑; font-size: 12px; height: 20px; line-height: 20px;">JYD<%=OrderID %></span>
                                        </p>

                                        <tr>
                                            <td colspan="4">
                                                <table width="100%" border="0" align="left" id="3">
                                                    <tr style="width: 100%; float: left; margin: 0px 0 5px 0;">
                                                        <td style="width: 32% !important; font-family: 微软雅黑; font-size: 12px; float: left;">客户名称：<%=comname %></td>
                                                        <td style="width: 25% !important; font-family: 微软雅黑; font-size: 12px; float: left;">下单日期：<%=DateTime.Parse(adddate).ToString("yyyy-MM-dd")%></td>
                                                        <td style="width: 25% !important; font-family: 微软雅黑; font-size: 12px; float: left;">
                                                            <%if (!string.IsNullOrEmpty(deliveryDate))
                                                                {%>
                                                        交货日期：<%=DateTime.Parse(deliveryDate).ToString("yyyy-MM-dd") %>
                                                            <%}
                                                            else
                                                            {%>
                                                        交货日期：<%=deliveryDate%>
                                                            <%} %>
                                                        </td>
                                                        <td style="width: 14% !important; font-family: 微软雅黑; font-size: 12px; float: left;">&nbsp;&nbsp;开单：<%=jjy %>

                                                            <!---这里主要用来存值-->
                                                            <input type="hidden" name="mypringid" value="40478," />
                                                        </td>



                                                        <!---这里主要用来存值-->

                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                        <table class="MsoTableGrid" border="1" cellspacing="0" bordercolor="#000000" style="border-collapse: collapse; width: 100%; margin: 0 auto;">
                            <tr>
                                <td width="15%" height="30" rowspan="2" align="center" style="font-family: 微软雅黑; font-size: 12px;">印刷品名称</td>
                                <td width="6%" height="30" rowspan="2" align="center" style="font-family: 微软雅黑; font-size: 12px;">类型</td>
                                <!--单位-->
                                <td width="6%" rowspan="2" align="center" style="font-family: 微软雅黑; font-size: 12px;">单位</td>
                                <!--成品要求-->
                                <td width="35%" height="30" colspan="6" align="middle" style="font-family: 微软雅黑; font-size: 12px;">成品要求</td>
                                <!--印刷用纸-->
                                <td height="13%" colspan="3" align="middle" style="font-family: 微软雅黑; font-size: 12px;">印刷用纸</td>
                                <!--P版-->
                                <td height="9%" colspan="2" align="center" style="font-family: 微软雅黑; font-size: 12px;">PS版</td>
                                <!--翻版-->
                                <td width="6%" rowspan="2" align="center" style="font-family: 微软雅黑; font-size: 12px;">翻版<br />
                                    要求</td>
                                <!--机型-->
                                <td width="6%" rowspan="2" align="center" style="font-family: 微软雅黑; font-size: 12px;">机型                                     </td>
                            </tr>
                            <tr>
                                <!--成品要求-->
                                <td width="5%" height="30" align="center" style="font-family: 微软雅黑; font-size: 12px;">纸张</td>
                                <td width="6%" align="center" style="font-family: 微软雅黑; font-size: 12px;">数量</td>
                                <td width="6%" align="center" style="font-family: 微软雅黑; font-size: 12px;">尺寸</td>
                                <td width="6%" align="center" style="font-family: 微软雅黑; font-size: 12px;">颜色</td>
                                <td width="6%" align="center" style="font-family: 微软雅黑; font-size: 12px;">页码</td>
                                <td width="6%" align="center" style="font-family: 微软雅黑; font-size: 12px;">客户<br />
                                    自带</td>

                                <!--印刷用纸-->
                                <td width="36" height="30" align="middle" style="font-family: 微软雅黑; font-size: 12px;">开纸</td>
                                <td width="36" align="middle" style="font-family: 微软雅黑; font-size: 12px;">正数</td>
                                <td width="36" align="middle" style="font-family: 微软雅黑; font-size: 12px;">放数</td>

                                <!--P版-->
                                <td width="36" height="30" align="middle" style="font-family: 微软雅黑; font-size: 12px;">色</td>
                                <td width="36" align="middle" style="font-family: 微软雅黑; font-size: 12px;">套</td>


                            </tr>

                            <%foreach (DataRow dr in dt.Rows)
                                { %>

                            <tr bgcolor="#ffffff">

                                <td height="25" align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["yspmc"].ToString() %></td>
                                <td align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["PrintType"].ToString() %>&nbsp;</td>
                                <td align="middle" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["danwei_m"].ToString() %></td>
                                <!--成品要求-->
                                <td width="36" align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["zzf"].ToString() %></td>
                                <td align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["sl"].ToString() %></td>
                                <td align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["cpcc"].ToString() %></td>
                                <td align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["sj"].ToString() %></td>
                                <td align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["ym"].ToString() %></td>
                                <td align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["zidaihz"].ToString() %></td>

                                <!--印刷用纸-->
                                <td width="36" align="middle" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["kzcc"].ToString() %></td>
                                <td width="36" align="middle" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["zs"].ToString() %></td>
                                <td width="36" align="middle" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["fs"].ToString() %></td>

                                <!--P版-->
                                <td width="36" align="middle" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["pss"].ToString() %></td>
                                <td align="middle" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["pst"].ToString() %></td>

                                <!--翻版-->
                                <td align="middle" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["fbyq"].ToString() %></td>
                                <!--机型-->
                                <td align="middle" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["jx"].ToString() %>&nbsp;</td>

                                <!--结束-->
                            </tr>



                            <tr>
                                <td height="30" colspan="16" align="left" style="font-family: 微软雅黑; font-size: 12px;">工艺>> 
                                             
                                        <%if (!string.IsNullOrEmpty(dr["fumoa"].ToString()) || !string.IsNullOrEmpty(dr["fumob"].ToString()))
                                            {%>


                                    <%=dr["fumoa"].ToString() %><%=dr["fumob"].ToString() %>,
										<%}%>

                                    <%if (!string.IsNullOrEmpty(dr["tana"].ToString()) || !string.IsNullOrEmpty(dr["tanb"].ToString()))
                                        {%>
                                    <%=dr["tana"].ToString() %><%=dr["tanb"].ToString() %>,
										<%}%>

                                    <%if (!string.IsNullOrEmpty(dr["uva"].ToString()) || !string.IsNullOrEmpty(dr["uvb"].ToString()))
                                        {%>
                                    <%=dr["uva"].ToString() %><%=dr["uvb"].ToString() %>,
										<%}%>

                                    <%if (!string.IsNullOrEmpty(dr["yaa"].ToString()) || !string.IsNullOrEmpty(dr["yab"].ToString()))
                                        {%>
                                    <%=dr["yaa"].ToString() %><%=dr["yab"].ToString() %>,
										<%}%>

                                    <%if (!string.IsNullOrEmpty(dr["db"].ToString()))
                                        {%>
										刀版:<%=dr["db"].ToString() %>,
										<%}%>

                                    <%if (!string.IsNullOrEmpty(dr["sh"].ToString()))
                                        {%>
										送货:<%=dr["sh"].ToString() %>,
										<%}%>

                                    <%if (!string.IsNullOrEmpty(dr["zheye"].ToString()))
                                        {%>
										折页:<%=dr["zheye"].ToString() %>,
										<%}%>

                                    <%if (!string.IsNullOrEmpty(dr["zd"].ToString()))
                                        {%>
										装订:<%=dr["zd"].ToString() %>,
										<%}%>

                                    <%if (!string.IsNullOrEmpty(dr["bh"].ToString()))
                                        {%>
										裱盒: <%=dr["bh"].ToString() %>,
										<%}%>

                                    <%if (!string.IsNullOrEmpty(dr["sk"].ToString()))
                                        {%>
										付款要求:<%=dr["sk"].ToString() %>,
										<%}%>

                                    <%if (!string.IsNullOrEmpty(dr["dddd"].ToString()))
                                        {%>
                                    <%=dr["dddd"].ToString() %>,

									<%}%>


                                            &nbsp;
                  
                  
                                </td>
                            </tr>
                            <%} %>

                            <%foreach (DataRow dr in dt.Rows)
                                { %>
                            <!--增加的一行-->
                            <tr style='display: none' bgcolor="#ffffff">
                                <td height="30" align="left" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["yspmcb"].ToString() %></td>
                                <td align="center" style="font-family: 微软雅黑; font-size: 12px;">单张&nbsp;</td>
                                <td align="middle" style="font-family: 微软雅黑; font-size: 12px;"></td>
                                <td width="36" align="center" style="font-family: 微软雅黑; font-size: 12px;">0()</td>
                                <td align="center" style="font-family: 微软雅黑; font-size: 12px;"></td>
                                <td align="center" style="font-family: 微软雅黑; font-size: 12px;"></td>
                                <td width="36" align="middle" style="font-family: 微软雅黑; font-size: 12px;"></td>
                                <td width="36" align="middle" style="font-family: 微软雅黑; font-size: 12px;"></td>
                                <td width="36" align="middle" style="font-family: 微软雅黑; font-size: 12px;"></td>
                                <td width="36" align="middle" style="font-family: 微软雅黑; font-size: 12px;"></td>
                                <td align="middle" style="font-family: 微软雅黑; font-size: 12px;"></td>
                                <td align="middle" style="font-family: 微软雅黑; font-size: 12px;"></td>
                                <td align="middle" style="font-family: 微软雅黑; font-size: 12px;">&nbsp;</td>

                                <!--结束-->
                            </tr>
                            <%} %>


                            <tr style='display: none'>
                                <td height="30" colspan="16" align="left" style="font-family: 微软雅黑; font-size: 12px;">工艺>>颜色:()&nbsp;客户自带:()
                  
                  
                 <!--工艺-->

                                    <!--工艺-->
                                    &nbsp;
                                </td>
                            </tr>
                            <!--增加的一行-->
                            <tr bgcolor="#ffffff">
                                <td height="35" colspan="16" align="left" valign="top" style="font-family: 微软雅黑; font-size: 12px;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">其他要求:&nbsp;&nbsp;                 <%=explain %></span>                      </td>

                            </tr>
                            <%-- <tr bgcolor="#ffffff">
                                        <td colspan="13" align="left" valign="top" style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">
                                            <span class="STYLE2">货&nbsp;&nbsp;发：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;收货人：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;电话：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;运输：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;费用：</span></td>
                                    </tr>--%>
                        </table>
                    </td>
                    <td align="middle"></td>
                </tr>

            </table>

            <span style="width: 100%; float: left; margin: 2% 0 0 0;">
                <img width="93" height="93" src="data:image/jpeg;base64,<%=b64%>" />

            </span>
        </div>
    </form>
</body>
</html>
