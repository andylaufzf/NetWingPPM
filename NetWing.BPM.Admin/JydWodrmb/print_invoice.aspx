﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="print_invoice.aspx.cs" Inherits="NetWing.BPM.Admin.JydWodrmb.print_invoice" %>


<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>聚源达接件办理开票单</title>
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <script type="text/javascript" src="../scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <link type="text/css" rel="stylesheet" href="css/base.css" />
    <link rel="stylesheet" href="//res.layui.com/layui/dist/css/layui.css" media="all">

    <script>
        //窗口API
        var api = frameElement.api, W = api.opener;
        var myDate = new Date();
        api.button(
            {
                name: '确认开票',
                callback: function () {
                    invoice();
                    return false;
                }
            },
            //{
            //    name: '确认打印',
            //    focus: true,
            //    callback: function () {
            //        printWin();
            //        return false;
            //    }
            //},

            {
                name: '取消'
            }
        );

        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("print_content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }

        function invoice() {
            var Invoice_no = document.getElementById("txt_Invoice_no").value;
            var orkeyid = <%=orkeyid %>;


            $.ajax({
                type: 'POST',
                url: '/JydInvoicet/ashx/invoicet.ashx',
                data: {
                    action: 'adds',
                    Invoice_no: Invoice_no,
                    orkeyid: orkeyid
                },
                dataType: 'json',
                beforeSend: function () { },
                complete: function () { },
                success: function (d) {
                    if (d.status == 0) {
                        alert(d.msg);
                        frameElement.api.close();
                    } else {
                        alert(d.msg);
                        frameElement.api.close();
                    }

                }
            });
        }

    </script>
    <style>
        body, p, span {
            font-family: 微软雅黑 !important;
        }

        .text01 {
            width: 96%;
            float: left;
            margin: 0 2% 0 2%;
        }

            .text01 span {
                font-size: 12px;
                color: #000;
                float: left;
                text-align: left;
                height: 24px;
                line-height: 24px;
            }

        .text02 {
            width: 96%;
            float: left;
            margin: 0 2% 0 2%;
        }

            .text02 span {
                font-size: 12px;
                color: #000;
                float: left;
                text-align: left;
                height: 24px;
                line-height: 24px;
            }

        .MsoTableGrid td {
            height: 30px !important;
            line-height: 30px !important;
        }

            .MsoTableGrid td p {
                height: 30px !important;
                line-height: 30px !important;
            }
    </style>
</head>

<body>

    <form id="form1" runat="server">
        

        <div id="print_content">
             <!--全局变量-->
              <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
			<td align="left" valign="top">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">

                        <tr>
                            <td height="11" align="left">
            <!--接件单名-->
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			
            <p class="MsoNormal" style="text-align:center;margin:0 auto;">
                <img width="61" height="34" src="/JydWodrmb/img/dyt.png" / style="text-align:center;">
                <b style="font-size:24px;font-family: 微软雅黑;height:30px; line-height:30px;margin:4px 0 0 0; ">昆明聚源达彩印包装有限公司接件开票单<span></span></b>
                
            </p>
			
			<tr>
                <br />
            <!--日期-->
                <table class="MsoTableGrid01" border="0" cellspacing="0" style="border-collapse: collapse; width:100%;margin:0 auto;">
                    <tbody>
                        <tr>
                            <td>订单号：</td>
                            <td colspan="3" scope="col"><%=OrderID %></td>
                            <td>公司名称:</td>
                            <td colspan="4" scope="col"><%=comname %></td>
                        </tr>
                        <tr>
                            <td>电&nbsp;&nbsp;&nbsp;话:</td>
                            <td colspan="3" scope="col"><%=tel %></td>
                            <td>接件员：</td>
                            <td colspan="4" scope="col"><%=jjy %></td>
                        </tr>
                        <tr>
                            <td>联系人:</td>
                            <td colspan="3" scope="col"><%=connman %></td>
                            <td>业务员：</td>
                            <td colspan="4" scope="col"><%=ywy %></td>
                        </tr>
                        <tr>
                            <td>接件时间:</td>
                            <td colspan="3" scope="col"><%=deliveryDate.ToString("yyyy年MM月dd日") %></td>
                            <td>交货日期：</td>
                            <td colspan="4" scope="col"><%=adddate.ToString("yyyy年MM月dd日") %></td>
                        </tr>
                        <tr>
                            <td>金&nbsp;&nbsp;&nbsp;额:</td>
                            <td colspan="3" scope="col"><%=money %></td>

                           
                        </tr>
                        
                    </tbody>
                </table>
			


        </div>
        <p class="MsoNormal">
               <br />
                <table class="MsoTableGrid01" border="0" cellspacing="0" style="border-collapse: collapse; width:100%;margin:0 auto;">
                    <tbody>
                       
                        <tr>
                            <td>发票号:</td>
                            <td colspan="3" scope="col">
                                 <input type="text" id="txt_Invoice_no" name="Invoice_no" runat="server" lay-verify="required" placeholder="请输入发票号" autocomplete="off" style="height: 30px;margin-bottom: 16px;margin-right: 6px;margin-top: 2px;outline: 0 none;padding: 1px 1px 1px 3px;width: 70%;font-size: 12px;line-height: 15px;" class="layui-input">
                            </td>
                            <td>开票日期：</td>
                            <td colspan="4" scope="col"><%=DateTime.Now.ToString("yyyy年MM月dd日") %></td>
                        </tr>
                    </tbody>
                </table>

            </p>
		</td>
                            <td align="middle"></td>
                        </tr>
		</table>

    </form>
</body>
</html>
