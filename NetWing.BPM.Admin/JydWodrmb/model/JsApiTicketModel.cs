﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetWing.BPM.Admin.JLDD.model
{
    public class JsApiTicketModel
    {
        public string ticket { get; set; }
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public long expires_in { get; set; }
    }
}