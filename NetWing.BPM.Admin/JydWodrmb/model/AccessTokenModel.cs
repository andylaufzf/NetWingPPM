﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NetWing.BPM.Admin.JLDD.model
{
    public class AccessTokenModel
    {
        public string access_token { get; set; }
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
}