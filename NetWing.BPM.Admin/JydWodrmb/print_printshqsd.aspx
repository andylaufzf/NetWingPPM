﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="print_printshqsd.aspx.cs" Inherits="NetWing.BPM.Admin.JydWodrmb.print_printshqsd" %>

<!DOCTYPE html>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>聚源达送货签收单</title>
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <script type="text/javascript" src="../scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <link type="text/css" rel="stylesheet" href="css/base.css" />
    <script>
        //窗口API
        var api = frameElement.api, W = api.opener;
        var myDate = new Date();
        api.button(
            {
                name: '确认打印',
                focus: true,
                callback: function () {
                    printWin();
                    return false;
                }
            },
            //{
            //    name: '导出Excel',
            //    callback: function () {
            //        ajaxExcel();
            //        return false;
            //    }
            //},
            {
                name: '取消'
            }
        );

        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("print_content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="print_content">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0" style="width:96%; margin:0px 2% 0 2%;text-align:center; float:left;">
<!--接件单名-->
           <p class="MsoNormal" style="text-align:center;margin:0 auto;">
                <img width="61" height="34" src="/JydWodrmb/img/dyt.png" / style="text-align:center;"><b style="font-size:24px;font-family: 微软雅黑;height:30px; line-height:30px;margin:4px 0 0 0; ">昆明聚源达彩印包装有限公司签收单</b>&nbsp;&nbsp;&nbsp;<span style="font-family: 微软雅黑; font-size: 12px;height:20px; line-height:20px;">JYD<%=OrderID %></span>
            </p>


        <tr>
          <td colspan="2" class="STYLE1">
<table width="100%" border="0" cellpadding="0" cellspacing="0" id="3">
              <tr class="STYLE1">
                <td colspan="4">
<table border="0" align="left"  style="width:100%; float:left; margin:5px 0 5px 0;">
<tr>



<td style="width:32% !important;font-family: 微软雅黑;font-size: 12px;float:left; text-align:left;">客户名称：<%=comname %></td>
 <td style="width:16% !important;font-family: 微软雅黑;font-size: 12px;float:left;text-align:left; ">联系人：<%=connman %></td>
 <td style="width:25% !important;font-family: 微软雅黑;font-size: 12px;float:left;text-align:left; ">联系电话：<%=tel %></td>
 <td style="width:25% !important;font-family: 微软雅黑;font-size: 12px;float:left; text-align:left;">送货日期：
                  <%if (!string.IsNullOrEmpty(deliveryDate))
                        {%>
                    <%=DateTime.Parse(deliveryDate).ToString("yyyy-MM-dd") %>
                     <%}else{%>
                    <%=deliveryDate%>
                    <%} %>
                    <!---这里主要用来存值-->
</td>
                    
                    <input type="hidden"  name="mypringid" value="" />
                <!---这里主要用来存值-->  
</tr>
</table>              
</td>
              </tr>
          </table></td>
        </tr>
      </table>
      <br /></td>
    </tr>
    <tr>
      <td><table border="1" bordercolor="#000000" cellspacing="0" style="border-collapse: collapse; width:96%;margin:0 auto;">
        <tr>
          <td height="30" align="center" style="font-family: 微软雅黑; font-size: 12px;">印刷品名称</td>
          <td align="center" style="font-family: 微软雅黑; font-size: 12px;">成品尺寸</td>
          <td align="center" style="font-family: 微软雅黑; font-size: 12px;">纸张</td>
          <td width="80" align="center" style="font-family: 微软雅黑; font-size: 12px;">总数量</td>
          <td width="35" align="center" style="font-family: 微软雅黑; font-size: 12px;">单位</td>
          
          <td width="80" align="center" style="font-family: 微软雅黑; font-size: 12px;">单价</td>
          <td width="80" align="center" style="font-family: 微软雅黑; font-size: 12px;">金额</td>
          <td width="80" align="center" style="font-family: 微软雅黑; font-size: 12px;">送货数量</td>
        </tr>
        <%foreach (DataRow dr in dt.Rows)
            { %>
        <tr>
          <td width="229" height="30" align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["yspmc"].ToString() %> </td>
          <td width="90" align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["cpcc"].ToString() %></td>
          <td width="130" align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["zzf"].ToString() %>&nbsp;</td>
          <td align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["sl"].ToString() %></td>
           <td align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["danwei_m"].ToString() %></td>
          <td align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["dj"].ToString() %></td>
          <td align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=dr["je"].ToString() %></td>
          <td align="center" style="font-family: 微软雅黑; font-size: 12px;"><%=aaaaa %></td>
        </tr>
       <%} %>
        <tr>
          <td height="30" colspan="4" style="font-family: 微软雅黑; font-size: 12px;">合计￥：<%=jexx %>元   大写：<%=jedx %></td>
          <td colspan="4" rowspan="2" style="font-family: 微软雅黑; font-size: 12px;">
          <table width="100%" border="0">
            <tr>
              <td height="30" valign="middle" style="font-family: 微软雅黑; font-size: 12px;">验收人(签字)：<br />              </td>
            </tr>
            <tr>
              <td height="30" valign="bottom" style="font-family: 微软雅黑; font-size: 12px;"></td>
            </tr>
          </table></td>
        </tr>
        <tr class="STYLE222">
          <td height="30" colspan="3" style="font-family: 微软雅黑; font-size: 12px;">注：以上货物均未付款，无任何质量问题，我单位已验收合格。</td>
        </tr>
        <tr class="STYLE222">
          <td height="30" colspan="3" style="font-family: 微软雅黑; font-size: 12px;">电话：<%=tel %>          投诉电话：13708420639</td>
              <td height="30" colspan="5" style="font-family: 微软雅黑; font-size: 12px;">送货人：</td>
        </tr>
      </table></td>
    </tr>
  </table  >

        </div>
    </form>
</body>
</html>
