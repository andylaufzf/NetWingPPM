﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="print_printdysale.aspx.cs" Inherits="NetWing.BPM.Admin.JydWodrmb.print_printdysale" %>

<!DOCTYPE html>

<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>聚源达销售单</title>
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <script type="text/javascript" src="../scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <link type="text/css" rel="stylesheet" href="css/base.css" />
    <script>
        //窗口API
        var api = frameElement.api, W = api.opener;
        var myDate = new Date();
        api.button(
            {
                name: '确认打印',
                focus: true,
                callback: function () {
                    printWin();
                    return false;
                }
            },
            //{
            //    name: '导出Excel',
            //    callback: function () {
            //        ajaxExcel();
            //        return false;
            //    }
            //},
            {
                name: '取消'
            }
        );

        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("print_content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }

    </script>
<style>
body,p,span{font-family:微软雅黑 !important;}
.text01{width:96%; float:left; margin:0 2% 0 2%;}
.text01 span{font-size:12px; color:#000;float:left; text-align:left; height:24px; line-height:24px;}
.text02{width:96%; float:left; margin:0 2% 0 2%;}
.text02 span{font-size:12px; color:#000;float:left; text-align:left; height:24px; line-height:24px;}
.MsoTableGrid td{height:30px !important; line-height:30px !important;}
.MsoTableGrid td p{height:30px !important; line-height:30px !important;}

</style>
</head>

<body>
    <form id="form1" runat="server">
        <div id="print_content">
              
            <!--全局变量-->
              
            <!--接件单名-->
<div class="title_box" style="width:96%; margin:30px 2% 0 2%;text-align:center;">
<p class="MsoNormal" style="text-align:center;margin:0 auto;">
                <img width="61" height="34" src="/JydWodrmb/img/dyt.png" / style="text-align:center;"><b style="font-size:24px;font-family: 微软雅黑;height:30px; line-height:30px;margin:4px 0 0 0; ">昆明聚源达彩印包装有限公司销售合同</b>&nbsp;&nbsp;&nbsp;<span style="font-family: 微软雅黑; font-size: 12px;height:20px; line-height:20px;">JYD<%=OrderID %></span>
            </p>
            
</div>

            
            <!--日期-->
            <%--<p class="MsoNormal text01" style="width:100%; float:left; margin:5px 0 5px 0;">
                <span style="width:32% !important;font-family: 微软雅黑;font-size: 12px; float:left;">客户名称：<%=comname %></span>
                <span style="width:25% !important;font-family: 微软雅黑;font-size: 12px;float:left; ">联系电话：<%=tel %></span>
                <span style="width:17% !important;font-family: 微软雅黑;font-size: 12px;float:left; ">联系人：<%=connman %></span> 
                <span style="width:25% !important;font-family: 微软雅黑;font-size: 12px;float:left; ">接件员：<%=jjy %></span>
            </p>
                
            <p class="text02" style="width:100%; float:left; margin:0 0 5px 0;"> 
                <span style="width:32% !important;font-family: 微软雅黑;font-size: 12px; float:left;">接件日期：<%=DateTime.Parse(adddate).ToString("yyyy-MM-dd")%></span>
                <%if (!string.IsNullOrEmpty(deliveryDate))
                    {%>
                <span style="width:25% !important;font-family: 微软雅黑;font-size: 12px;float:left;">交货日期：<%=DateTime.Parse(deliveryDate).ToString("yyyy-MM-dd") %></span>
                <%}else{%>
                <span style="width:25% !important;font-family: 微软雅黑;font-size: 12px;float:left; ">交货日期：<%=deliveryDate%></span>
                <%} %>
                <span style="width:17% !important;font-family: 微软雅黑;font-size: 12px;float:left; ">业务员：<%=ywy %></span>
            </p>--%>
            
                <table class="MsoTableGrid01" border="0" cellspacing="0" style="border-collapse: collapse; width:100%;margin:0 auto;">
                    <tbody>
                        <tr>
                            <td>委托方:</td>
                            <td colspan="3" scope="col"><%=comname %></td>
                            <td>生产方：</td>
                            <td colspan="4" scope="col">昆明聚源达彩印包装有限公司</td>
                        </tr>
                        <tr>
                            <td>电&nbsp;&nbsp;&nbsp;话:</td>
                            <td colspan="3" scope="col"><%=tel %></td>
                            <td>电&nbsp;&nbsp;&nbsp;话：</td>
                            <td colspan="4" scope="col">0871-63849573</td>
                        </tr>
                        <tr>
                            <td>联系人:</td>
                            <td colspan="3" scope="col"><%=connman %></td>
                            <td>传&nbsp;&nbsp;&nbsp;真：</td>
                            <td colspan="4" scope="col"></td>
                        </tr>
                        <tr>
                            <td>地&nbsp;&nbsp;&nbsp;址:</td>
                            <td colspan="3" scope="col"><%=address %></td>
                            <td>地&nbsp;&nbsp;&nbsp;址：</td>
                            <td colspan="4" scope="col">昆明市寺瓦路省一监金马仓库</td>
                        </tr>
                    </tbody>
                </table>
            

            <div align="center">
                <table class="MsoTableGrid" border="1" cellspacing="0" style="border-collapse: collapse; width:100%;margin:0 auto;">
                    <tbody>
                        <!--首行名称-->
                        <tr>
                            <td width="98" valign="center" style=" height:24px; line-height:24px;">
                                <p class="MsoNormal" align="center">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">产品名称</span>
                                </p>
                            </td>
                            <td width="48" valign="center">
                                <p class="MsoNormal" align="center">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">类型</span>
                                </p>
                            </td>
                            <td width="39" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">单位</span><span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                </p>
                            </td>
                            <td width="90" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">成品尺寸<span>(cm)</span></span><span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                </p>
                            </td>
                            <td width="64" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">纸张</span><span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                </p>
                            </td>
                            <td width="44" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">材料</span><span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                </p>
                            </td>
                            <td width="54" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size:12px;">客户自带</span><span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                </p>
                            </td>
                            <td width="40" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">颜色</span><span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                </p>
                            </td>
                            <td width="43" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">页码</span><span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                </p>
                            </td>
                            <td width="43" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">数量</span><span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                </p>
                            </td>
                            <td width="48" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">单价</span><span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                </p>
                            </td>
                            <td width="40" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">金额</span><span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                </p>
                            </td>
                            
                        </tr>
                        <!--数值-->

                         <%foreach (DataRow dr in dt.Rows)
                             { %>
                        <tr>
                            <td width="98" valign="center" style="border: 1.0000pt solid windowtext;height:24px; line-height:24px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;"><%=dr["yspmc"].ToString() %></span>
                                </p>
                            </td>
                            <td width="48" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;"><%=dr["PrintType"].ToString() %></span>
                                </p>
                            </td>
                            <td width="39" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                             <span style="font-family: 微软雅黑; font-size: 12px;"><%=dr["danwei_m"].ToString() %></span>
                                </p>
                            </td>
                            <td width="90" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;"><%=dr["cpcc"].ToString() %></span>
                                </p>
                            </td>
                            <td width="64" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;"><%=dr["zzf"].ToString() %></span>
                                </p>
                            </td>
                           <td width="44" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                 <span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                </p>
                            </td>
                             <td width="54" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;"><%=dr["zidaihz"].ToString() %></span>
                                </p>
                            </td>
                             <td width="40" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;"><%=dr["sj"].ToString() %></span>
                                </p>
                            </td>
                            <td width="43" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;"><%=dr["ym"].ToString() %></span>
                                </p>
                            </td>
                            <td width="43" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;"><%=dr["sl"].ToString() %></span>
                                </p>
                            </td>
                            <td width="48" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;"><%=dr["dj"].ToString() %></span>
                                </p>
                            </td>
                            <td width="40" valign="center" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;"><%=dr["je"].ToString() %></span>
                                </p>
                            </td>
                           
                        </tr>
                        <!--工艺-->
                       <%-- <tr>
                        </tr>--%>
                       <!--注释-->
                        <tr>
                            <td width="618" valign="center" colspan="12" style="border: 1.0000pt solid windowtext;height:24px; line-height:24px;">
                                <p class="MsoNormal">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">工艺>>
                                        
                                        <%if (!string.IsNullOrEmpty(dr["fumoa"].ToString())||!string.IsNullOrEmpty(dr["fumob"].ToString()))
                                            {%>
                                       

                                        <%=dr["fumoa"].ToString() %><%=dr["fumob"].ToString() %>,
										<%}%>
										
										<%if (!string.IsNullOrEmpty(dr["tana"].ToString())||!string.IsNullOrEmpty(dr["tanb"].ToString()))
                                            {%>
										<%=dr["tana"].ToString() %><%=dr["tanb"].ToString() %>,
										<%}%>
										
										<%if (!string.IsNullOrEmpty(dr["uva"].ToString())||!string.IsNullOrEmpty(dr["uvb"].ToString()))
                                            {%>
                                        <%=dr["uva"].ToString() %><%=dr["uvb"].ToString() %>,
										<%}%>
										
										<%if (!string.IsNullOrEmpty(dr["yaa"].ToString())||!string.IsNullOrEmpty(dr["yab"].ToString()))
                                            {%>
										<%=dr["yaa"].ToString() %><%=dr["yab"].ToString() %>,
										<%}%>
										
										<%if (!string.IsNullOrEmpty(dr["db"].ToString()))
                                            {%>
										刀版:<%=dr["db"].ToString() %>,
										<%}%>
										
										<%if (!string.IsNullOrEmpty(dr["sh"].ToString()))
                                            {%>
										送货:<%=dr["sh"].ToString() %>,
										<%}%>
										
										<%if (!string.IsNullOrEmpty(dr["zheye"].ToString()))
                                            {%>
										折页:<%=dr["zheye"].ToString() %>,
										<%}%>
										
										<%if (!string.IsNullOrEmpty(dr["zd"].ToString()))
                                            {%>
										装订:<%=dr["zd"].ToString() %>,
										<%}%>
										
										<%if (!string.IsNullOrEmpty(dr["bh"].ToString()))
                                            {%>
										裱盒: <%=dr["bh"].ToString() %>,
										<%}%>
										
										<%if (!string.IsNullOrEmpty(dr["sk"].ToString()))
                                            {%>
										付款要求:<%=dr["sk"].ToString() %>,
										<%}%>
										
										<%if (!string.IsNullOrEmpty(dr["dddd"].ToString()))
                                            {%>
										<%=dr["dddd"].ToString() %>,

									<%}%>


                                       </span>
                                    
                                </p>
                            </td>
                        </tr>
                         <%}%>

                       <!--合计-->
                        <tr>
                            <td width="618" valign="top" colspan="12" style="border: 1.0000pt solid windowtext;">
                                <p class="MsoNormal" align="justify" style="text-align: justify;">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">其他要求：</span>
                                    <span style="font-family: 微软雅黑; color: #000; font-size: 12px;"><%=explain %></span>
                                    <span style="font-family: 微软雅黑; color: #000; font-size: 12px;"></span>
                                </p>
                               
                                <p class="MsoNormal" align="justify" style="text-align: justify;">
                                    <span style="font-family: 微软雅黑; color: #FF0000; font-size: 12px;">&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                    <span style="font-family: 微软雅黑; font-size: 12px;">&nbsp;</span>
                                    <span style="font-family: 微软雅黑; color: #FF0000; font-size: 12px;"></span>
                                </p>
                            </td>
                        </tr>
                       
                        <tr>
                            <td width="618" valign="center" colspan="12" style="border: 1.0000pt solid windowtext;height:24px; line-height:24px;">
                                <p class="MsoNormal">
                                    <span style="font-family: 微软雅黑; font-size: 12px;">合计：<%=jexx %>元&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        大写：<%=jedx %>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        预付款：<%=yufuk %>元 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 大写：<%=yufukuana %></span>
                                    <span style="font-family: 微软雅黑; font-size: 12px;"></span>
                                    
                                </p>
                            </td>
                        </tr>
                    </tbody>
                </table>

            </div>
            <p class="MsoNormal">
                <span style="font-family: Calibri; font-size: 10.5000pt;">&nbsp;一、对产品质量提出异议的期限为货物签收的25小时内，超出期限后，承印方不予受理。</span><br />
                <span style="font-family: Calibri; font-size: 10.5000pt;">&nbsp;二、承印方式按照委托方要求及提供稿件等资料进行设计、加工、印刷、如印刷品内容（如商标、版权等）所引起的其他法律责任概由委托方负责。</span><br />
                <span style="font-family: Calibri; font-size: 10.5000pt;">&nbsp;三、此合同经双方同意签立，如发生争执，交当地法院仲裁。</span><br />
                <span style="font-family: Calibri; font-size: 10.5000pt;">&nbsp;四、本合同一式三份，承运方两份，委托方一份，由签订之日生效。</span><br />
                <br /><br />
                <table class="MsoTableGrid01" border="0" cellspacing="0" style="border-collapse: collapse; width:100%;margin:0 auto;">
                    <tbody>
                        <tr>
                            <td>委托方（盖章）:</td>
                            <td colspan="3" scope="col"><%=comname %></td>
                            <td>生产方（盖章）：</td>
                            <td colspan="4" scope="col">昆明聚源达彩印包装有限公司</td>
                        </tr>
                        <tr>
                            <td>经手人:</td>
                            <td colspan="3" scope="col"></td>
                            <td>经手人：</td>
                            <td colspan="4" scope="col"></td>
                        </tr>
                        <tr>
                            <td>日&nbsp;&nbsp;&nbsp;期:</td>
                            <td colspan="3" scope="col"><%=DateTime.Now.ToString("yyyy年MM月dd日") %></td>
                            <td>日&nbsp;&nbsp;&nbsp;期：</td>
                            <td colspan="4" scope="col"><%=DateTime.Now.ToString("yyyy年MM月dd日") %></td>
                        </tr>
                    </tbody>
                </table>

            </p>
        </div>
        <div class="cnzz" style="display: none;"></div>

    </form>
</body>
</html>
