﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="psi_supplement.aspx.cs" Inherits="NetWing.BPM.Admin.JydWodrmb.psi_supplement" %>

<!DOCTYPE html>



<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<%@ Import Namespace="NetWing.Common" %>
<%@ Import Namespace="NetWing.Common.Data" %>
<%@ Import Namespace="NetWing.Common.Data.SqlServer" %>
<%@ Import Namespace="NetWing.Common.Provider" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
       <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>聚源达采购单</title>
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <script type="text/javascript" src="../scripts/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../scripts/lhgdialog/lhgdialog.js?skin=idialog"></script>
    <link type="text/css" rel="stylesheet" href="css/base.css" />
    <script>
        //窗口API
        var api = frameElement.api, W = api.opener;
        var myDate = new Date();
        api.button(
            {
                name: '确认打印',
                focus: true,
                callback: function () {
                    printWin();
                    return false;
                }
            },
            //{
            //    name: '导出Excel',
            //    callback: function () {
            //        ajaxExcel();
            //        return false;
            //    }
            //},
            {
                name: '取消'
            }
        );

        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("print_content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }





        //小写转大写
function cmycurd(num) {  //转成人民币大写金额形式
	var str1 = '零壹贰叁肆伍陆柒捌玖';  //0-9所对应的汉字
	var str2 = '万仟佰拾亿仟佰拾万仟佰拾元角分'; //数字位所对应的汉字
	var str3;    //从原num值中取出的值
	var str4;    //数字的字符串形式
	var str5 = '';  //人民币大写金额形式
	var i;    //循环变量
	var j;    //num的值乘以100的字符串长度
	var ch1;    //数字的汉语读法
	var ch2;    //数字位的汉字读法
	var nzero = 0;  //用来计算连续的零值是几个

	num = Math.abs(num).toFixed(2);  //将num取绝对值并四舍五入取2位小数
	str4 = (num * 100).toFixed(0).toString();  //将num乘100并转换成字符串形式
	j = str4.length;      //找出最高位
	if (j > 15) { return '溢出'; }
	str2 = str2.substr(15 - j);    //取出对应位数的str2的值。如：200.55,j为5所以str2=佰拾元角分

	//循环取出每一位需要转换的值
	for (i = 0; i < j; i++) {
		str3 = str4.substr(i, 1);   //取出需转换的某一位的值
		if (i != (j - 3) && i != (j - 7) && i != (j - 11) && i != (j - 15)) {    //当所取位数不为元、万、亿、万亿上的数字时
			if (str3 == '0') {
				ch1 = '';
				ch2 = '';
				nzero = nzero + 1;
			}
			else {
				if (str3 != '0' && nzero != 0) {
					ch1 = '零' + str1.substr(str3 * 1, 1);
					ch2 = str2.substr(i, 1);
					nzero = 0;
				}
				else {
					ch1 = str1.substr(str3 * 1, 1);
					ch2 = str2.substr(i, 1);
					nzero = 0;
				}
			}
		}
		else { //该位是万亿，亿，万，元位等关键位
			if (str3 != '0' && nzero != 0) {
				ch1 = "零" + str1.substr(str3 * 1, 1);
				ch2 = str2.substr(i, 1);
				nzero = 0;
			}
			else {
				if (str3 != '0' && nzero == 0) {
					ch1 = str1.substr(str3 * 1, 1);
					ch2 = str2.substr(i, 1);
					nzero = 0;
				}
				else {
					if (str3 == '0' && nzero >= 3) {
						ch1 = '';
						ch2 = '';
						nzero = nzero + 1;
					}
					else {
						if (j >= 11) {
							ch1 = '';
							nzero = nzero + 1;
						}
						else {
							ch1 = '';
							ch2 = str2.substr(i, 1);
							nzero = nzero + 1;
						}
					}
				}
			}
		}
		if (i == (j - 11) || i == (j - 3)) {  //如果该位是亿位或元位，则必须写上
			ch2 = str2.substr(i, 1);
		}
		str5 = str5 + ch1 + ch2;

		if (i == j - 1 && str3 == '0') {   //最后一位（分）为0时，加上"整"
			str5 = str5 + '整';
		}
	}
	if (num == 0) {
		str5 = '零元整';
	}
	return str5;
}
//小写转大写

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="print_content">
             <!--全局变量-->
              <table cellspacing="0" cellpadding="0" width="100%" border="0">
            <tr>
			<td align="left" valign="top">
			<table cellspacing="0" cellpadding="0" width="100%" border="0">

                        <tr>
                            <td height="11" align="left">
            <!--接件单名-->
			<table width="100%" border="0" cellspacing="0" cellpadding="0">
			
            <p class="MsoNormal" style="text-align:center;margin:0 auto;">
                <img width="61" height="34" src="/JydWodrmb/img/dyt.png" / style="text-align:center;"><b style="font-size:24px;font-family: 微软雅黑;height:30px; line-height:30px;margin:4px 0 0 0; ">昆明聚源达彩印包装有限公司补料单</span></span></b><b><span style="font-family: 宋体; font-size: 18pt;"></span></b>
            </p>
			
			<tr>
            <!--日期-->
                <table class="MsoTableGrid01" border="0" cellspacing="0" style="border-collapse: collapse; width:100%;margin:0 auto;">
                    <tbody>
                        <tr>
                            <td>（甲）:</td>
                            <td colspan="3" scope="col">昆明聚源达彩印包装有限公司</td>
                            <td>（乙）：</td>
                            <td colspan="4" scope="col"><%=unitname %></td>
                        </tr>
                        <tr>
                            <td>电&nbsp;&nbsp;&nbsp;话:</td>
                            <td colspan="3" scope="col">0871-63849573</td>
                            <td>电&nbsp;&nbsp;&nbsp;话：</td>
                            <td colspan="4" scope="col"><%=unit_moble %></td>
                        </tr>
                        <tr>
                            <td>联系人:</td>
                            <td colspan="3" scope="col"><%=Contacts %></td>
                            <td>联系人：</td>
                            <td colspan="4" scope="col"><%=unit_man %></td>
                        </tr>
                        <tr>
                            <td>地&nbsp;&nbsp;&nbsp;址:</td>
                            <td colspan="3" scope="col"></td>
                            <td>地&nbsp;&nbsp;&nbsp;址：</td>
                            <td colspan="4" scope="col"></td>
                        </tr>
                        
                    </tbody>
                </table>
			

            <div align="center">
                <table class="MsoTableGrid" border="1" cellspacing="0" style="border-collapse: collapse; width:100%;">
                    <tbody>
                        <!--首行名称-->
                        <tr>
                            <td width="90" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;">采购品名</span>
                                </p>
                            </td>
                            <td width="98" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;">采购编号</span>
                                </p>
                            </td>
                           
                            <td width="39" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;">采购数量</span>
                                </p>
                            </td>
                             <td width="48" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;">商品分类</span>
                                </p>
                            </td>
                            
                            <td width="64" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                   <span style="font-family: 微软雅黑; font-size: 13.9px;">单价</span>
                                </p>
                            </td>
                            <td width="44" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;">金额</span>
                                </p>
                            </td>
                            
                            
                        </tr>
                        <!--数值-->

                        
                        <tr>
                             <td width="90" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;"><%=goodsName %></span>
                                </p>
                            </td>
                            <td width="98" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;"><%=edNumber %></span>
                                </p>
                            </td>
                           
                            <td width="39" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;"><%=buyNum %></span>
                                </p>
                            </td>
                             <td width="48" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                   <span style="font-family: 微软雅黑; font-size: 13.9px;"><%=goodsNo %></span>
                                </p>
                            </td>
                           
                            <td width="64" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;"><%=buyPrice %></span>
                                </p>
                            </td>
                           <td width="44" valign="center" style="font-family: 微软雅黑; font-size: 13.9px;">
                                <p class="MsoNormal" align="center" style="text-align: center;">
                                   <span style="font-family: 微软雅黑; font-size: 13.9px;"><%=buyAllMoney %></span>
                                </p>
                            </td>
                             
                           
                        </tr>
                        <!--工艺-->
                       <%-- <tr>
                        </tr>--%>
                       <!--注释-->
                       

                       <!--合计-->
                        <tr>
                            <td width="618" valign="top" colspan="13.9" style="border: 1.0000pt solid windowtext;">

                                <p class="MsoNormal" align="justify" style="text-align: justify;">
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;">金额：</span>
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;" id="daxje">
                                        <script>
                                            //这里用了两个方法,一个是数字转换为大写,一个HTML中应用js值
                                            var vv = "<%=buyAllMoney %>";
                                            var cc = cmycurd(vv);
                                            
                                            window.onload = function() {
			                                    document.getElementById("daxje").innerHTML = cc;
		                                    };
                                        </script>
                                        
                                    </span>
                                    <span style="font-family: 微软雅黑; font-size: 13.9px;"></span>
                                </p>
                               
                                
                            </td>
                        </tr>
                       
                        
                    </tbody>
                </table>
            </div>
        </div>
        
		
    </form>
</body>
</html>