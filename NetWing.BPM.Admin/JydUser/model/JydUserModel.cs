using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("jydUser")]
	[Description("用户管理")]
	public class JydUserModel
	{
				/// <summary>
		/// 系统编号
		/// </summary>
		[Description("系统编号")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 用户名
		/// </summary>
		[Description("用户名")]
		public string UserName { get; set; }		
		/// <summary>
		/// 密码
		/// </summary>
		[Description("密码")]
		public string Password { get; set; }		
		/// <summary>
		/// 真实姓名
		/// </summary>
		[Description("真实姓名")]
		public string realname { get; set; }		
		/// <summary>
		/// 助记码
		/// </summary>
		[Description("助记码")]
		public string zjm { get; set; }		
		/// <summary>
		/// 性别
		/// </summary>
		[Description("性别")]
		public string xb { get; set; }		
		/// <summary>
		/// QQ
		/// </summary>
		[Description("QQ")]
		public string QQ { get; set; }		
		/// <summary>
		/// 单位名称
		/// </summary>
		[Description("单位名称")]
		public string comname { get; set; }		
		/// <summary>
		/// 单位ID
		/// </summary>
		[Description("单位ID")]
		public int comnameid { get; set; }		
		/// <summary>
		/// 邮政编码
		/// </summary>
		[Description("邮政编码")]
		public string postcode { get; set; }		
		/// <summary>
		/// 地址
		/// </summary>
		[Description("地址")]
		public string address { get; set; }		
		/// <summary>
		/// Email
		/// </summary>
		[Description("Email")]
		public string email { get; set; }		
		/// <summary>
		/// 电话
		/// </summary>
		[Description("电话")]
		public string tell { get; set; }		
		/// <summary>
		/// 传真
		/// </summary>
		[Description("传真")]
		public string fax { get; set; }		
		/// <summary>
		/// 手机
		/// </summary>
		[Description("手机")]
		public string mobile { get; set; }		
		/// <summary>
		/// 网址
		/// </summary>
		[Description("网址")]
		public string homepage { get; set; }		
		/// <summary>
		/// 订单号
		/// </summary>
		[Description("订单号")]
		public int ordernum { get; set; }		
		/// <summary>
		/// 最后登录
		/// </summary>
		[Description("最后登录")]
		public DateTime LastUpDate { get; set; }		
		/// <summary>
		/// 存款
		/// </summary>
		[Description("存款")]
		public int moneyin { get; set; }		
		/// <summary>
		/// 欠款
		/// </summary>
		[Description("欠款")]
		public int moneyout { get; set; }		
		/// <summary>
		/// 授权
		/// </summary>
		[Description("授权")]
		public int shouquan { get; set; }		
		/// <summary>
		/// myjjkd
		/// </summary>
		[Description("myjjkd")]
		public int myjjkd { get; set; }		
		/// <summary>
		/// mysclcd
		/// </summary>
		[Description("mysclcd")]
		public int mysclcd { get; set; }		
		/// <summary>
		/// myscshd
		/// </summary>
		[Description("myscshd")]
		public int myscshd { get; set; }		
		/// <summary>
		/// myxgjjd
		/// </summary>
		[Description("myxgjjd")]
		public int myxgjjd { get; set; }		
		/// <summary>
		/// myxglcd
		/// </summary>
		[Description("myxglcd")]
		public int myxglcd { get; set; }		
		/// <summary>
		/// mydjcx
		/// </summary>
		[Description("mydjcx")]
		public int mydjcx { get; set; }		
		/// <summary>
		/// mysk
		/// </summary>
		[Description("mysk")]
		public int mysk { get; set; }		
		/// <summary>
		/// mygy
		/// </summary>
		[Description("mygy")]
		public int mygy { get; set; }		
		/// <summary>
		/// mysjkz
		/// </summary>
		[Description("mysjkz")]
		public int mysjkz { get; set; }		
		/// <summary>
		/// myqdsk
		/// </summary>
		[Description("myqdsk")]
		public int myqdsk { get; set; }		
		/// <summary>
		/// mywsjg
		/// </summary>
		[Description("mywsjg")]
		public int mywsjg { get; set; }
		/// <summary>
		/// qtall
		/// </summary>
		[Description("qtall")]
		public int qtall { get; set; }
		/// <summary>
		/// scxy
		/// </summary>
		[Description("scxy")]
		public int scxy { get; set; }
        /// <summary>
        /// deptid
        /// </summary>
        [Description("deptid")]
        public int deptid { get; set; }
        /// <summary>
        /// fenlei
        /// </summary>
        [Description("分类")]
        public string fenlei { get; set; }

        [Description("微信openid")]
        public string OpenID { get; set; }

        [Description("微信头像")]
        public string headimgurl { get; set; }

        [Description("微信名称")]
        public string weixin { get; set; }

        

        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}