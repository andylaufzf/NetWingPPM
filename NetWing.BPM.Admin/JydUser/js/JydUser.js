var actionURL = '/JydUser/ashx/JydUserHandler.ashx';
var formurl = '/JydUser/html/JydUser.html';

$(function () {

    //为避免数据字典加载不完整。这段在后面加载
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });

    $('#a_add').click(CRUD.add);
    $('#a_edit').click(CRUD.edit);
    $('#a_delete').click(CRUD.del);
    //高级查询
    $('#a_search').click(function () {
        search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#a_getSearch').click(function () {//自定义搜索框
        mySearch();
    });

    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });

    //批量删除
    $("#a_alldel").click(function () {
        var ids = [];
        var rows = $('#list').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].KeyId);
        }
        var allid = ids.join(',');//所有的id
        var o = {};
        o.action = "alldel";
        o.KeyIds = allid;
        var param = "json=" + JSON.stringify(o);

        if (confirm('确认要执行批量删除操作吗？')) {
            jQuery.ajaxjson(actionURL, param, function (d) {
                if (parseInt(d) > 0) {
                    msg.ok('批量删除成功！');
                    grid.reload();
                } else {
                    MessageOrRedirect(d);
                }
            });
        }


    });

});
//这段后加载，避免数据字典加载不完整的问题
window.onload = function () {
    autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
};


//editor:'datetimebox' 日期及时间选择框  editor:'datebox' 日期选择框    editor:'numberspinner' 数字调节器  editor:{type: 'numberspinner',options:{value:0,required:true}}

//定义一个$JQ为$.　以后在js 中就可以用${JQ}AJAX了在前台这样写(定义)://通用数据字典start
var getDic = {
    jsonData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?categoryId=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            //alert(newData);
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
    jsonParentData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?action=parent&parentid=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
}

//通用数据字典end
//调用数据字典和使用数据字典 如果不需要请不要打开否则会导致系统速度慢
//getDic.jsonData(9);//取得性别数据字典
//在onLoad:的地方如下使用
//top.$('#txt_user_sex').combobox({ data: eval($('body').data('data9')), valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '请选择性别', disabled: false });





var grid = {
    bind: function (winSize) {
        $('#list').datagrid({
            url: actionURL,
            toolbar: '#toolbar',
            title: "客户管理",
            iconCls: 'icon icon-list',
            width: winSize.width,
            height: winSize.height,
            nowrap: false, //折行
            rownumbers: true, //行号
            striped: true, //隔行变色
            idField: 'KeyId',//主键
            showFooter: true,//显示页脚
            singleSelect: true, //单选
            frozenColumns: [[]],
            columns: [[//应为宽度不是很需要所以注释了宽度
                { title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                { title: '系统编号', field: 'KeyId', sortable: true, width: '', hidden: false },
                { title: '字母简拼', field: 'UserName', sortable: true, width: '', hidden: false },
                { title: '密码', field: 'Password', sortable: true, width: '', hidden: true },
                { title: '真实姓名', field: 'realname', sortable: true, width: '', hidden: false },
                { title: '助记码', field: 'zjm', sortable: true, width: '', hidden: true },
                { title: '性别', field: 'xb', sortable: true, width: '', hidden: true },
                { title: 'QQ', field: 'QQ', sortable: true, width: '', hidden: true },
                { title: '单位名称', field: 'comname', sortable: true, width: '', hidden: false },
                { title: '单位ID', field: 'comnameid', sortable: true, width: '', hidden: true },
                { title: '邮政编码', field: 'postcode', sortable: true, width: '', hidden: true },
                { title: '地址', field: 'address', sortable: true, width: '', hidden: false },
                { title: 'Email', field: 'email', sortable: true, width: '', hidden: true },
                { title: '电话', field: 'tell', sortable: true, width: '', hidden: false },
                { title: '传真', field: 'fax', sortable: true, width: '', hidden: true },
                { title: '手机', field: 'mobile', sortable: true, width: '', hidden: false },
                { title: '网址', field: 'homepage', sortable: true, width: '', hidden: true },
                { title: '订单号', field: 'ordernum', sortable: true, width: '', hidden: true },
                { title: '最后登录', field: 'LastUpDate', sortable: true, width: '', hidden: true },
                { title: '类型', field: 'fenlei', sortable: true, width: '', hidden: false },
                //{ title: '存款', field: 'moneyin', sortable: true, width: '', hidden: false },
                //{ title: '欠款', field: 'moneyout', sortable: true, width: '', hidden: false },
                //{ title: '授权', field: 'shouquan', sortable: true, width: '', hidden: false },
                //{ title: 'myjjkd', field: 'myjjkd', sortable: true, width: '', hidden: false },
                //{ title: 'mysclcd', field: 'mysclcd', sortable: true, width: '', hidden: false },
                //{ title: 'myscshd', field: 'myscshd', sortable: true, width: '', hidden: false },
                //{ title: 'myxgjjd', field: 'myxgjjd', sortable: true, width: '', hidden: false },
                //{ title: 'myxglcd', field: 'myxglcd', sortable: true, width: '', hidden: false },
                //{ title: 'mydjcx', field: 'mydjcx', sortable: true, width: '', hidden: false },
                //{ title: 'mysk', field: 'mysk', sortable: true, width: '', hidden: false },
                //{ title: 'mygy', field: 'mygy', sortable: true, width: '', hidden: false },
                //{ title: 'mysjkz', field: 'mysjkz', sortable: true, width: '', hidden: false },
                //{ title: 'myqdsk', field: 'myqdsk', sortable: true, width: '', hidden: false },
                //{ title: 'mywsjg', field: 'mywsjg', sortable: true, width: '', hidden: false },
                //{ title: 'qtall', field: 'qtall', sortable: true, width: '', hidden: false },
                //{ title: 'scxy', field: 'scxy', sortable: true, width: '', hidden: false },
                

                { title: '总金额', field: 'jexx', sortable: true, widtd: '', hidden: false },
                { title: '预付款', field: 'yufuk', sortable: true, widtd: '', hidden: false },
                {
                    title: '应收', field: 'copewith', sortable: true, widtd: '', hidden: false,
                    formatter: function (v, r, i) {
                        if (r.KeyId != undefined) {
                            var copewi = r.jexx - r.yufuk;
                            if (copewi == 0) {
                                return '<div style="white-space:normal;height:auto;" class="datagrid-cell datagrid-cell-c1-copewith"></div>';
                            } else {
                                return '<div style="white-space:normal;height:auto;" class="datagrid-cell datagrid-cell-c1-copewith">' + copewi + '元</div>';
                            }
                        } else {
                            return v;
                        }
                    }
                },
                {
                    title: '应付外协', field: 'myysf', sortable: true, widtd: '', hidden: false,
                    formatter: function (v, r, i) {
                        if (r.keyid != undefined) {
                            if (r.myysf == 0) {
                                return '<div style="white-space:normal;height:auto;" class="datagrid-cell datagrid-cell-c1-copewith"></div>';
                            } else {
                                return '<div style="white-space:normal;height:auto;" class="datagrid-cell datagrid-cell-c1-copewith">' + r.myysf + '元</div>';
                            }
                        } else {
                            return v;
                        }
                    }
                },
                //{
                //    title: '应付供应商', field: 'suppliers  ', sortable: true, widtd: '', hidden: false,
                //    formatter: function (v, r, i) {
                //        if (r.myysf == 0) {
                //            return '<div style="white-space:normal;height:auto;" class="datagrid-cell datagrid-cell-c1-copewith"></div>';
                //        } else {
                //            return '<div style="white-space:normal;height:auto;" class="datagrid-cell datagrid-cell-c1-copewith">' + r.myysf + '元</div>';
                //        }
                //    }
                //},
            ]],
            onEndEdit: onEndEdit,//结束编辑时函数 这里为了简洁 该函数写在下面
            onUnselect: onUnselect,
            onLoadSuccess: function (data) {
                //alert($('body').data('data70'));
                //alert($('body').data('data69'));
            },
            onCancelEdit: onCancelEdit,//在用户取消编辑一行的时候触发
            onSelect: onSelect,//在用户选择一行的时候触发
            onClickRow: onClickRow,//在用户点击一行的时候触发
            //onAfterEdit: onAfterEdit,//在用户完成编辑一行的时候触发
            onDblClickCell: onDblClickCell,//为了程序逻辑清楚函数写在外面
            onHeaderContextMenu: function (e, field) {//列菜单实现动态隐藏列
                e.preventDefault();
                if (!cmenu) {
                    createColumnMenu();
                }
                cmenu.menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            pagination: true,
            pageSize: PAGESIZE,
            pageList: [20, 40, 50, 100, 200]
        });
    },
    getSelectedRow: function () {
        return $('#list').datagrid('getSelected');
    },
    reload: function () {
        $('#list').datagrid('clearSelections').datagrid('reload', { filter: '' });
    }
};

function createParam(action, keyid) {
    var o = {};
    var query = top.$('#uiform').serializeArray();
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}


var CRUD = {
    add: function () {
        var hDialog = top.jQuery.hDialog({
            title: '添加', width: 800, height: 600, href: formurl, iconCls: 'icon-add',
            //初始化添加
            onLoad: function () {
                top.$('#txt_fenlei').combobox({ url: '/sys/ashx/dichandler.ashx?categoryId=105', valueField: 'Title', textField: 'Title', editable: false, required: true, missingMessage: '', disabled: false });
                top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
            },
            submit: function () {
                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate')) {
                    var query = createParam('add', '0');
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (parseInt(d) > 0) {
                            msg.ok('添加成功！');
                            hDialog.dialog('close');
                            grid.reload(top.$('#txt_fenlei').combobox('getValue'));
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                }
                msg.warning('请填写必填项！');
                return false;
            }
        });

        top.$('#uiform').validate();
    },
    edit: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '编辑', width: 1000, height: 700, href: formurl, iconCls: 'icon-save',
                //编辑初始化
                onLoad: function () {
                    top.$('#txt_KeyId').numberspinner('setValue', row.KeyId);
                    top.$('#txt_UserName').textbox('setValue', row.UserName);
                    top.$('#txt_Password').textbox('setValue', row.Password);
                    top.$('#txt_realname').textbox('setValue', row.realname);
                    top.$('#txt_zjm').textbox('setValue', row.zjm);
                    top.$('#txt_xb').textbox('setValue', row.xb);
                    top.$('#txt_QQ').textbox('setValue', row.QQ);
                    top.$('#txt_comname').textbox('setValue', row.comname);
                    top.$('#txt_comnameid').numberspinner('setValue', row.comnameid);
                    top.$('#txt_postcode').textbox('setValue', row.postcode);
                    top.$('#txt_address').textbox('setValue', row.address);
                    top.$('#txt_email').textbox('setValue', row.email);
                    top.$('#txt_tell').textbox('setValue', row.tell);
                    top.$('#txt_fax').textbox('setValue', row.fax);
                    top.$('#txt_mobile').textbox('setValue', row.mobile);
                    top.$('#txt_homepage').textbox('setValue', row.homepage);
                    top.$('#txt_ordernum').numberspinner('setValue', row.ordernum);
                    top.$('#txt_LastUpDate').datetimebox('setValue', row.LastUpDate);
                    top.$('#txt_moneyin').numberspinner('setValue', row.moneyin);
                    top.$('#txt_moneyout').numberspinner('setValue', row.moneyout);
                    top.$('#txt_shouquan').numberspinner('setValue', row.shouquan);
                    top.$('#txt_myjjkd').numberspinner('setValue', row.myjjkd);
                    top.$('#txt_mysclcd').numberspinner('setValue', row.mysclcd);
                    top.$('#txt_myscshd').numberspinner('setValue', row.myscshd);
                    top.$('#txt_myxgjjd').numberspinner('setValue', row.myxgjjd);
                    top.$('#txt_myxglcd').numberspinner('setValue', row.myxglcd);
                    top.$('#txt_mydjcx').numberspinner('setValue', row.mydjcx);
                    top.$('#txt_mysk').numberspinner('setValue', row.mysk);
                    top.$('#txt_mygy').numberspinner('setValue', row.mygy);
                    top.$('#txt_mysjkz').numberspinner('setValue', row.mysjkz);
                    top.$('#txt_myqdsk').numberspinner('setValue', row.myqdsk);
                    top.$('#txt_mywsjg').numberspinner('setValue', row.mywsjg);
                    top.$('#txt_qtall').numberspinner('setValue', row.qtall);
                    top.$('#txt_scxy').numberspinner('setValue', row.scxy);

                    top.$('#txt_fenlei').combobox({ url: '/sys/ashx/dichandler.ashx?categoryId=105', valueField: 'Title', textField: 'Title', editable: false, required: true, missingMessage: '', disabled: false, onLoadSuccess() { top.$('#txt_fenlei').combobox('setValue', row.fenlei); } });

                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');    
                },
                submit: function () {
                    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                    //alert(top.$("#uiform").form('validate'));
                    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {
                        var query = createParam('edit', row.KeyId);
                        jQuery.ajaxjson(actionURL, query, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    }
                    msg.warning('请填写必填项！');
                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

//mygrid.reload(top.$('#txt_fenlei').combobox('getValue'));




//实现动态隐藏列
var cmenu = null;
function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#list').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#list').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#list').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#list').datagrid('getColumnOption', field);
        cmenu.menu('appendItem', {
            text: col.title,
            name: field,
            iconCls: 'icon-ok'
        });
    }
}

//实现动态隐藏列结束

//双击编辑表单焦点消失后保存
var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#list').datagrid('validateRow', editIndex)) {
        $('#list').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function onDblClickCell(index, field) {
    if (editIndex != index) {
        if (endEditing()) {
            $('#list').datagrid('selectRow', index)
            $('#list').datagrid('beginEdit', index);
            var ed = $('#list').datagrid('getEditor', { index: index, field: field });
            if (ed) {
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                //这个写法很有意思
                //为了方便 ,类似字段要同事写两个值 1把部门id写入隐藏字段2把标题换成值写入 显示字段

            }
            editIndex = index;
        } else {
            setTimeout(function () {
                $('#list').datagrid('selectRow', editIndex);
            }, 0);
        }
    }
}

function onUnselect(index, row) {
    //alert(index);
}

function onCancelEdit(index, row) {
    //alert(index);
}
function onClickRow(index, row) {
    if (editIndex != undefined) {
        //alert("正在编辑的行是：" + editIndex);
        //alert("验证正在编辑的行：" + $('#list').datagrid('validateRow', editIndex));
        if ($('#list').datagrid('validateRow', editIndex)) {//如果验证正在编辑的行数据有效则
            $("#list").datagrid('endEdit', editIndex);//如果点其他行，结束编辑正在编辑的行
            editIndex = undefined;
        } else {
            msg.warning("还有一行没编辑完啦！！");
        }
    }
    $("#list").datagrid('selectRow', index);


}

function onSelect(index, row) {

}

function onEndEdit(index, row, changes) {
    //alert('结束编辑'+index+JSON.stringify(row));
    var o = {};
    var query = JSON.stringify(row);
    o.jsonEntity = query;
    o.action = 'edit';
    o.keyid = row.KeyId;
    query = "json=" + JSON.stringify(o);
    //alert(query);
    //表格内编辑模式编辑成功不提示信息
    jQuery.ajaxjson(actionURL, query, function (d) {
        //if (parseInt(d) > 0) {
        //    msg.ok('编辑成功！');
        //    hDialog.dialog('close');
        grid.reload();
        // } else {
        //     MessageOrRedirect(d);
        // }
    });

}
function append() {
    if (endEditing()) {
        $('#list').datagrid('appendRow', { status: 'P' });
        editIndex = $('#list').datagrid('getRows').length - 1;
        $('#list').datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
    }
}
function removeit() {
    if (editIndex == undefined) { return }
    $('#list').datagrid('cancelEdit', editIndex)
        .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept() {
    if (endEditing()) {
        $('#list').datagrid('acceptChanges');
    }
}
function reject() {
    $('#list').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges() {
    var rows = $('#list').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}
//双击编辑表单焦点消失后保存结束

//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var mobile = $("#mobile").textbox('getValue');
    var realname = $("#realname").textbox('getValue');
    var fenei = $("#fenei").combobox('getValue');
    var addtime = $("#addtime").datebox('getValue');
    var desiredtime = $("#desiredtime").datebox('getValue');
    //var copewithe = $("#copewithe").textbox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    o.rules[0] = JSON.parse('{"field":"1","op":"cn","data":"1"}');
    var e = JSON.parse(query);
    e.rules[0] = JSON.parse('{"field":"1","op":"cn","data":"1"}');
    var i = 1;
    if (mobile != '' && mobile != undefined) {//假如单位搜索不为空
        o.rules[i] = JSON.parse('{"field":"mobile","op":"cn","data":"' + mobile + '"}');
        i = i + 1;
    }
    if (realname != '' & realname != undefined) {//联系人不为空
        o.rules[i] = JSON.parse('{"field":"realname","op":"cn","data":"' + realname + '"}');
        i = i + 1;
    }
    //if (copewithe != '' & copewithe != undefined) {//金额
    //    o.rules[i] = JSON.parse('{"field":"copewith","op":"gte","data":"' + copewithe + '"}');
    //    i = i + 1;
    //}
    if (fenei != '' & fenei != undefined) {
        o.rules[i] = JSON.parse('{"field":"fenlei","op":"cn","data":"' + fenei + '"}');
        i = i + 1;
    } else {
        o.rules[i] = JSON.parse('{"field":"KeyId","op":"ge","data":"0"}');
        i = i + 1;
    }
    var j=1;
    if (addtime != '' && addtime != undefined) {
        e.rules[j] = JSON.parse('{"field":"adddate","op":"ge","data":"' + addtime + '"}');
        j++;
    }
    if (desiredtime != '' && desiredtime != undefined) {
        e.rules[j] = JSON.parse('{"field":"adddate","op":"le","data":"' + desiredtime + '"}');
        j++;
    }
    $('#list').datagrid('reload', { filter: JSON.stringify(o), filtere: JSON.stringify(e) });

}


//自定义搜索结束


//单选多选开关
$('#selectSwitch').switchbutton({
    checked: false,
    onChange: function (checked) {
        if (checked) {
            $('#list').datagrid({ singleSelect: false });//多选
        } else {
            $('#list').datagrid({ singleSelect: true });//单选
        }
    }
})

