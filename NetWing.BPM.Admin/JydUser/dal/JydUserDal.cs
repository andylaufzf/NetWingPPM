using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class JydUserDal : BaseRepository<JydUserModel>
    {
        public static JydUserDal Instance
        {
            get { return SingletonProvider<JydUserDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(JydUserModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}