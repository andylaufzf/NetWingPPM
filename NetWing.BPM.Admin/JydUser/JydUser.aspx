﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="JydUser.aspx.cs" Inherits="NetWing.BPM.Admin.JydUser.JydUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->


    <!-- 工具栏按钮 -->
    <div id="toolbar"><%= base.BuildToolbar()%><a  href="#" plain="true" class="easyui-linkbutton" title="模式">模式</a><input id="selectSwitch" class="easyui-switchbutton" data-options="onText:'多选',offText:'单选'"> 
        电话:<input id="mobile" name="mobile" class="easyui-textbox" style="width: 130px"/>
        真实姓名:<input id="realname" name="realname" class="easyui-textbox" style="width: 80px"/>
        类型:<select id="fenei" name="fenei" class="easyui-combobox" style="width: 80px;">
                <option value="">全部</option>
                <option value="客户">客户</option>
                <option value="加工客户">加工客户</option>
                <option value="供应商">供应商</option>
           </select>
        日期:<input id="addtime" style="width: 130px" type="text" class="easyui-datetimebox"/>
        至<input id="desiredtime" style="width: 130px" type="text" class="easyui-datetimebox"/>

        <%--应收金额:<input id="copewithe" name="copewithe" class="easyui-textbox" style="width:130px"/>--%>
       <a id="a_getSearch" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
    </div>

    <!-- datagrid 列表 -->
    <table id="list" ></table>


	<!--Uploader-->
	<!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
	<!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>


    <!-- 引入js文件 -->
      <script src="js/JydUser.js"></script>
</asp:Content>



