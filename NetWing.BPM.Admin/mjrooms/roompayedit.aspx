﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="roompayedit.aspx.cs" Inherits="NetWing.BPM.Admin.mjrooms.roompayedit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->



    <!-- 工具栏按钮 -->
    <!-- datagrid 列表 -->
    <div id="tt" class="easyui-tabs" style="width: ; height: ;">
        <div title="修改续费收据" style="padding: 20px; display: none;">
            特别提示：退房宽带费不退，房租、垃圾费、停车费等按月计费项目不足一个月按一个月计算，就餐卡及电费卡剩余费用及押金项目全额退还，房间设施按入住清单检查验收，丢失损坏按价赔。<br />
            <br />
            <!--请修改项目顺序和修改样式-->
            <table id="list"></table>
            <br />
            合计金额：
        <input id="total" name="total" class="easyui-numberbox" style="width: 100px;" />经手人：<input id="contactid" name="contactid" style="width: 100px;" /><input id="contact" name="contact" style="display: none; width: 100px;" />

            <!--房间id:-->
            <input id="roomid" style="display: none;" name="roomid" value="" />
            <!--用户id:-->
            <input id="userids" style="display: none;" name="userids" value="" />
            <!--主订单id:-->
            <input id="orderid" style="display: none;" name="orderid" value="" />

            <br />
            <br />

            <div id="paytypediv">
                <li>
                    <input class="account" name="account" value="" style="display: none;" />
                    收款方式:<input id="accountid" name="accountid" class="accountid" style="width: 100px;" value="" />
                    实收金额:<input type="text" name="shifu" class="easyui-numberbox shifu" value="0" style="width: 100px;" data-options="min:0,precision:2"><a id="addpaytype" onclick="addpaytype();" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'">增加</a>
                </li>

            </div>
            <br />
            <br />
            <input name="edNumber" value="<%=edNumber %>" style="display: none;" />
            <input name="fmainkeyid" value="<%=fmainkeyid %>" style="display: none;" />
            <a id="submit" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'">修改</a>


        </div>

    </div>




    <!--Uploader-->
    <!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
    <!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>

    <!-- 引入js文件 -->
    <script src="js/roompayEdit.js?roomid=<%=roomid %>&userids=<%=userids %>&orderid=<%=orderid %>&edNumber=<%=edNumber %>&odids=<%=orderdetailids %>"></script>
    <div id="odids" style="display: none;"><%=orderdetailids %></div>
</asp:Content>




