using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("MJUser")]
	[Description("")]
	public class MJUserModel
	{
				/// <summary>
		/// ID
		/// </summary>
		[Description("ID")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 姓名
		/// </summary>
		[Description("姓名")]
		public string fullname { get; set; }		
		/// <summary>
		/// 用户名
		/// </summary>
		[Description("用户名")]
		public string username { get; set; }		
		/// <summary>
		/// 密码
		/// </summary>
		[Description("密码")]
		public string password { get; set; }		
		/// <summary>
		/// 证件类型
		/// </summary>
		[Description("证件类型")]
		public string cardtype { get; set; }		
		/// <summary>
		/// 证件号码
		/// </summary>
		[Description("证件号码")]
		public string cardid { get; set; }		
		/// <summary>
		/// 性别
		/// </summary>
		[Description("性别")]
		public string sex { get; set; }		
		/// <summary>
		/// 年龄
		/// </summary>
		[Description("年龄")]
		public int age { get; set; }		
		/// <summary>
		/// 生日
		/// </summary>
		[Description("生日")]
		public DateTime birthday { get; set; }		
		/// <summary>
		/// 联系电话
		/// </summary>
		[Description("联系电话")]
		public string tel { get; set; }		
		/// <summary>
		/// 职业
		/// </summary>
		[Description("职业")]
		public string careres { get; set; }		
		/// <summary>
		/// 邮箱
		/// </summary>
		[Description("邮箱")]
		public string emaile { get; set; }		
		/// <summary>
		/// QQ
		/// </summary>
		[Description("QQ")]
		public string qq { get; set; }		
		/// <summary>
		/// 地址
		/// </summary>
		[Description("地址")]
		public string address { get; set; }		
		/// <summary>
		/// 微信ID
		/// </summary>
		[Description("微信ID")]
		public string openid { get; set; }		
		/// <summary>
		/// 用户头像
		/// </summary>
		[Description("用户头像")]
		public string avater { get; set; }		
		/// <summary>
		/// 余额
		/// </summary>
		[Description("余额")]
		public decimal amount { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string remarks { get; set; }		
		/// <summary>
		/// 系统时间
		/// </summary>
		[Description("系统时间")]
		public DateTime add_time { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}