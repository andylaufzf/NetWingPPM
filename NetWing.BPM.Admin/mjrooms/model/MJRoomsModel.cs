using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
    [TableName("MJRooms")]
    [Description("")]
    public class MJRoomsModel
    {
        /// <summary>
        /// ID
        /// </summary>
        [Description("ID")]
        public int KeyId { get; set; }
        /// <summary>
        /// 所属门店
        /// </summary>
        [Description("所属门店")]
        public string vest { get; set; }
        /// <summary>
        /// 门店ID
        /// </summary>
        [Description("门店ID")]
        public int vestid { get; set; }
        /// <summary>
        /// 房间号
        /// </summary>
        [Description("房间号")]
        public string roomnumber { get; set; }
        /// <summary>
        /// 房间类型
        /// </summary>
        [Description("房间类型")]
        public string roomtype { get; set; }
        /// <summary>
        /// 价格
        /// </summary>
        [Description("价格")]
        public decimal price { get; set; }



        [Description("原价")]
        public decimal oprice { get; set; }


        [Description("季度折扣价")]
        public decimal quarterprice { get; set; }


        [Description("半年折扣价")]
        public decimal halfyearprice { get; set; }


        [Description("年度折扣价")]
        public decimal yearprice { get; set; }


        /// <summary>
        /// 状态
        /// </summary>
        [Description("状态")]
        public string state { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        [Description("备注")]
        public string remarks { get; set; }
        /// <summary>
        /// 系统时间
        /// </summary>
        [Description("系统时间")]
        public DateTime add_time { get; set; }

        [Description("更新时间")]
        public DateTime up_time { get; set; }
        [Description("到期时间")]
        public DateTime exp_time { get; set; }
        /// <summary>
        /// 数据所有者ID
        /// </summary>
        [Description("数据所有者ID")]
        public int ownner { get; set; }
        /// <summary>
        /// 部门ID
        /// </summary>
        [Description("部门ID")]
        public int depid { get; set; }
        /// <summary>
        /// 房间使用者ID
        /// </summary>
        [Description("房间使用者IDs")]
        public string userids { get; set; }
        /// <summary>
        /// 房间使用者姓名s
        /// </summary>
        [Description("房间使用者姓名s")]
        public string usernames { get; set; }
        /// <summary>
        /// 房间使用者手机号s
        /// </summary>
        [Description("房间使用者手机号s")]
        public string usermobiles { get; set; }
        [Description("使用者身份证号")]
        public string useridcards { get; set; }

        [Description("租期")]
        public int zuqi { get; set; }

        [Description("居住人数")]
        public int jzrs { get; set; }
        [Description("合同开始时间")]
        public DateTime orderstart_time { get; set; }
        [Description("合同结束时间")]
        public DateTime orderend_time { get; set; }

        [Description("订单号")]
        public int OrderId { get; set; }
        [Description("入住率")]
        public decimal rzl { get; set; }
        /// <summary>
        /// 付款方式：年付 月付 季付
        /// </summary>
        [Description("付款方式")]
        public string paytype { get; set; }

        [Description("下次付款日期")]
        public DateTime nextpaydate { get; set; }

        [Description("是否决定退款")]
        public string istui { get; set; }
        [Description("是否决定退款备注")]
        public string istuinote { get; set; }
        [Description("催费短信发送日期")]
        public DateTime smsdate { get; set; }
        [Description("是否预订")]
        public string isding { get; set; }
        [Description("预订备注")]
        public string isdingnote { get; set; }

        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}