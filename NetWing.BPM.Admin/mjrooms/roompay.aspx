﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="roompay.aspx.cs" Inherits="NetWing.BPM.Admin.mjrooms.roompay" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.BPM.Core" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <%=WebExtensions.CombresLink(SysVisitor.Instance.ThemeName) %>
    <%=WebExtensions.CombresLink("siteCss") %>
    <%=WebExtensions.CombresLink("alertifyCss") %>

    <%=WebExtensions.CombresLink("jquery") %>
    <%=WebExtensions.CombresLink("siteJs") %>
    <script type="text/javascript">
        if (top.location == self.location) {
            top.location = '/';
        }
        var urlRoot = ""; // 设置后台程序所在的目录 如： /admin
        var currend_Date = '<%=DateTime.Now.ToString("yyyy-MM-dd") %>';
        var PAGESIZE = <%=SysVisitor.Instance.GridRows%>;


        $(function () {
            $('#toolbar,.toolbar').css({
                height: '28px',
                //background:#efefef;
                padding: '1px 2px',
                'padding-bottom': '0px'
                //,'border-bottom':'1px solid #ccc'
            });
        });


    </script>
    <script type="text/javascript">
        //窗口API
        var api = frameElement.api, W = api.opener;
        api.button(
            {
                name: '提交',
                focus: true,
                callback: function () {
                    var r = tijiao();//提交动作
                    return false;
                    //返回false 的时候不会关闭当前窗口 return false;
                }
            },
            {
                name: '关闭'
            });
        //打印方法
        function printWin() {
            var oWin = window.open("", "_blank");
            oWin.document.write(document.getElementById("content").innerHTML);
            oWin.focus();
            oWin.document.close();
            oWin.print()
            oWin.close()
        }
    </script>

</head>
<body>
    <br />
    <form id="form1" runat="server">
        <table id="list"></table>
        <br />
        合计金额：
        <input id="total" name="total" class="easyui-numberbox" />经手人：<input id="contactid" name="contactid" /><input id="contact" name="contact" style="display: none;" />

        <!--房间id:-->
        <input id="roomid" style="display: none;" name="roomid" value="" />
        <!--用户id:-->
        <input id="userids" style="display: none;" name="userids" value="" />
        <!--主订单id:-->
        <input id="orderid" style="display: none;" name="orderid" value="" />

        <br />
        <br />

        <div id="paytypediv">
            <li>
                <input class="account" name="account" value="" style="display: none;" />
                收款方式:<input id="accountid" name="accountid" class="accountid" value="" />
                实收金额:<input type="text" name="shifu" class="easyui-numberbox shifu" value="0" style="width: 100px;" data-options="min:0,precision:2"><a id="addpaytype" onclick="addpaytype();" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'">增加</a>
            </li>

        </div>
    </form>
    <script src="js/roompay.js?roomid=?<%=roomid %>&userids=<%=userids %>&orderid=<%=orderid %>"></script>
    <div id="win" style="display:none;"></div> 
</body>
</html>
