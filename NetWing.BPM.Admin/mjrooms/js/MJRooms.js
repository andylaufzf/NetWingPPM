var actionURL = '/MJRooms/ashx/MJRoomsHandler.ashx';
var formurl = '/MJRooms/html/MJRooms.html';

$(function () {

    //为避免数据字典加载不完整。这段在后面加载
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });

    $('#a_add').click(CRUD.add);
    $('#a_edit').click(CRUD.edit);
    $('#a_delete').click(CRUD.del);
    //高级查询
    $('#a_search').click(function () {
        search.go('list');
    });
    $('#a_refresh').click(grid.reload);//刷新
    $('#a_getSearch').click(function () {//自定义搜索框
        mySearch();
    });

    /*导出EXCEL*/
    $('#a_export').click(function () {
        var ee = new ExportExcel('list', actionURL);
        ee.go();
    });

    /*导入EXCEL*/
    $('#a_inport').click(function () {
        var ii = new ImportExcel('list', actionURL);
        ii.go();
    });

    //批量删除
    $("#a_alldel").click(function () {
        var ids = [];
        var rows = $('#list').datagrid('getSelections');
        for (var i = 0; i < rows.length; i++) {
            ids.push(rows[i].KeyId);
        }
        var allid = ids.join(',');//所有的id
        var o = {};
        o.action = "alldel";
        o.KeyIds = allid;
        var param = "json=" + JSON.stringify(o);

        if (confirm('确认要执行批量删除操作吗？')) {
            jQuery.ajaxjson(actionURL, param, function (d) {
                if (parseInt(d) > 0) {
                    msg.ok('批量删除成功！');
                    grid.reload();
                } else {
                    MessageOrRedirect(d);
                }
            });
        }


    });

});
//这段后加载，避免数据字典加载不完整的问题
window.onload = function () {
    autoResize({ dataGrid: '#list', gridType: 'datagrid', callback: grid.bind, height: 0 });
};


//editor:'datetimebox' 日期及时间选择框  editor:'datebox' 日期选择框    editor:'numberspinner' 数字调节器  editor:{type: 'numberspinner',options:{value:0,required:true}}

//定义一个$JQ为$.　以后在js 中就可以用${JQ}AJAX了在前台这样写(定义)://通用数据字典start
var getDic = {
    jsonData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?categoryId=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            //alert(newData);
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
    jsonParentData: function (dicID) {
        $.getJSON('/sys/ashx/dichandler.ashx?action=parent&parentid=' + dicID + '', function (data) {
            var newData = JSON.stringify(data).replace(/KeyId/g, "id").replace(/Title/g, "text");
            $('body').data('data' + dicID + '', newData); //意思是得到数据并赋值给body
        });
    },
}

//通用数据字典end
//调用数据字典和使用数据字典 如果不需要请不要打开否则会导致系统速度慢
//getDic.jsonData(9);//取得性别数据字典
//在onLoad:的地方如下使用
//top.$('#txt_user_sex').combobox({ data: eval($('body').data('data9')), valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '请选择性别', disabled: false });





var grid = {
    bind: function (winSize) {
        $('#list').datagrid({
            url: actionURL,
            toolbar: '#toolbar',
            title: "房间列表",
            iconCls: 'icon icon-list',
            width: winSize.width,
            height: winSize.height,
            nowrap: false, //折行
            rownumbers: true, //行号
            striped: true, //隔行变色
            idField: 'KeyId',//主键
            singleSelect: true, //单选
            frozenColumns: [[]],
            columns: [[//应为宽度不是很需要所以注释了宽度
                { title: '选择', field: 'ck', checkbox: true },//后加进去全选字段数据库里是没有的
                { title: 'ID', field: 'KeyId', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏
                { title: '所属门店', field: 'vest', sortable: true, width: '', editor: { type: 'combotree', options: { required: true, validType: 'length[1,1000]', url: '/sys/ashx/userhandler.ashx?json={"jsonEntity":"{}","action":"deps"}', textField: 'Title', valueField: 'Code', missingMessage: '内容不能为空,只允许1到1000之间个字符。' } } },
                { title: '门店ID', field: 'vestid', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏
                { title: '房间号', field: 'roomnumber', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'length[1,1000]', missingMessage: '内容不能为空,只允许1到1000之间个字符。' } } },
                { title: '房间类型', field: 'roomtype', sortable: true, width: '', editor: { type: 'combobox', options: { required: true, validType: 'length[1,1000]', url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=72', textField: 'Title', valueField: 'Code', missingMessage: '内容不能为空,只允许1到1000之间个字符。' } } },
                { title: '原价', field: 'oprice', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'length[1,1000]', missingMessage: '内容不能为空,只允许1到1000之间个字符。' } } },
                { title: '季度折扣价', field: 'quarterprice', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'length[1,1000]', missingMessage: '内容不能为空,只允许1到1000之间个字符。' } } },
                { title: '半年折扣价', field: 'halfyearprice', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'length[1,1000]', missingMessage: '内容不能为空,只允许1到1000之间个字符。' } } },
                { title: '年度折扣价', field: 'yearprice', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'length[1,1000]', missingMessage: '内容不能为空,只允许1到1000之间个字符。' } } },
                { title: '最后一次成交价', field: 'price', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'length[1,1000]', missingMessage: '内容不能为空,只允许1到1000之间个字符。' } } },
                { title: '状态', field: 'state', sortable: true, width: '', editor: { type: 'combobox', options: { required: true, validType: 'length[1,1000]', url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=71', textField: 'Title', valueField: 'Code', missingMessage: '内容不能为空,只允许1到1000之间个字符。' } } },
                { title: '备注', field: 'remarks', sortable: true, width: '', hidden: false, editor: { type: 'textbox', options: { required: true, validType: 'length[1,1000]', missingMessage: '内容不能为空,只允许1到1000之间个字符。' } } },
                { title: '使用人', field: 'usernames', sortable: true, width: '', hidden: false },
                { title: '系统时间', field: 'add_time', sortable: true, editor: 'textbox', width: '', hidden: true },//主键自动判断隐藏

            ]],
            onEndEdit: onEndEdit,//结束编辑时函数 这里为了简洁 该函数写在下面
            onUnselect: onUnselect,
            onLoadSuccess: function (data) {
                //alert($('body').data('data70'));
                //alert($('body').data('data69'));
            },
            onCancelEdit: onCancelEdit,//在用户取消编辑一行的时候触发
            onSelect: onSelect,//在用户选择一行的时候触发
            //onClickRow: onClickRow,//在用户点击一行的时候触发
            //onAfterEdit: onAfterEdit,//在用户完成编辑一行的时候触发
            //onDblClickCell: onDblClickCell,//为了程序逻辑清楚函数写在外面
            onHeaderContextMenu: function (e, field) {//列菜单实现动态隐藏列
                e.preventDefault();
                if (!cmenu) {
                    createColumnMenu();
                }
                cmenu.menu('show', {
                    left: e.pageX,
                    top: e.pageY
                });
            },
            pagination: true,
            pageSize: PAGESIZE,
            pageList: [20, 40, 50, 100, 200]
        });
    },
    getSelectedRow: function () {
        return $('#list').datagrid('getSelected');
    },
    reload: function () {
        $('#list').datagrid('clearSelections').datagrid('reload', { filter: '' });
    }
};

function createParam(action, keyid) {
    var o = {};
    var query = top.$('#uiform').serializeArray();
    query = convertArray(query);
    o.jsonEntity = JSON.stringify(query);
    o.action = action;
    o.keyid = keyid;
    return "json=" + JSON.stringify(o);
}


//价格关联每个减少50
function reprice() {
    top.$('#txt_oprice').numberbox({
        onChange: function (newValue, oldValue) {
            //alert(newValue);
            top.$("#txt_quarterprice").numberbox("setValue", (newValue - 50));//季度折扣价
            top.$("#txt_halfyearprice").numberbox("setValue", (newValue - 100));//半年折扣价
            top.$("#txt_yearprice").numberbox("setValue", (newValue - 150));//年度折扣价
        }
    });
}


var CRUD = {
    add: function () {
        var hDialog = top.jQuery.hDialog({
            title: '添加房间基础信息', width: 1000, height: 600, href: formurl, iconCls: 'icon-add',
            onLoad: function () {
                top.$('#txt_vest').combotree({ url: '/sys/ashx/userhandler.ashx?json={"jsonEntity":"{}","action":"deps"}', textField: 'text', editable: false, required: true, missingMessage: '内容不能为空,只允许1到1000之间个字符。', onSelect: function (node) { top.$('#txt_vest').combotree('setValue', node.text); top.$('#txt_vestid').textbox('setValue', node.id);/*这里要设置隐藏字段的值*/ }, disabled: false });//系统部门树数据字典
                top.$('#txt_roomtype').combobox({ url: '/sys/ashx/dichandler.ashx?categoryId=72', valueField: 'Title', textField: 'Code', editable: false, required: true, missingMessage: '内容不能为空,只允许1到1000之间个字符。', disabled: false });
                top.$('#txt_state').combobox({ url: '/sys/ashx/dichandler.ashx?categoryId=71', valueField: 'Title', textField: 'Code', editable: false, required: true, missingMessage: '内容不能为空,只允许1到1000之间个字符。', disabled: false });
                top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                reprice();//价格关联 每次减少50

            },
            submit: function () {
                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#uiform").form('validate')) {
                    var query = createParam('add', '0');
                    jQuery.ajaxjson(actionURL, query, function (d) {
                        if (parseInt(d) > 0) {
                            msg.ok('添加成功！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                } else {
                    msg.warning('请填写必填项！');
                }
                //
                return false;
            }
        });

        top.$('#uiform').validate();
    },
    edit: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '编辑房间基础信息', width: 1000, height: 600, href: formurl, iconCls: 'icon-save',
                onLoad: function () {
                    top.$('#txt_KeyId').textbox('setValue', row.KeyId);
                    top.$('#txt_vest').combotree({ url: '/sys/ashx/userhandler.ashx?json={"jsonEntity":"{}","action":"deps"}', valueField: 'text', textField: 'text', editable: false, required: true, missingMessage: '内容不能为空,只允许1到1000之间个字符。', disabled: false, onSelect: function (node) { top.$('#txt_vestid').textbox('setValue', node.id); node.id = node.text;/* 系统数据字典的用法 先设置隐藏字段的值，然后把部门设置为中文*/ } });

                    top.$('#txt_vest').combotree('setValue', row.vestid); //因为这里是上面先初始化控件，这里要把隐藏的值赋给显示的值
                    top.$('#txt_vestid').textbox('setValue', row.vestid);
                    top.$('#txt_roomnumber').textbox('setValue', row.roomnumber);
                    top.$('#txt_roomtype').combobox({ url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=72', valueField: 'Code', textField: 'Title', editable: false, required: true, missingMessage: '内容不能为空,只允许1到1000之间个字符。', disabled: false, onLoadSuccess() { top.$('#txt_roomtype').combobox('setValue', row.roomtype); } });
                    //top.$('#txt_price').numberbox('setValue', row.price);
                    top.$('#txt_state').combobox({ url: '/sys/ashx/dichandler.ashx?showType=noselected&categoryId=71', valueField: 'Code', textField: 'Title', editable: false, required: true, missingMessage: '内容不能为空,只允许1到1000之间个字符。', disabled: false, onLoadSuccess() { top.$('#txt_state').combobox('setValue', row.state); } });
                    top.$('#txt_remarks').textbox('setValue', row.remarks);
                    top.$('#txt_add_time').textbox('setValue', row.add_time);
                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');
                    //使用该方法省去很多麻烦  直接把数据加载到表单里。
                    top.$('#uiform').form('load', row);
                    reprice();//价格关联 每次减少50

                },
                submit: function () {
                    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                    //alert(top.$("#uiform").form('validate'));
                    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {
                        var query = createParam('edit', row.KeyId);;
                        jQuery.ajaxjson(actionURL, query, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功,本次修改仅更新了房间类型、备注、原价、月度折扣价、半年折扣价、年度折扣价、最后一次成交价。其他数据并未发生改变！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    }
                    else {
                        msg.warning('请填写必填项！');
                    }
                    //
                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

//实现动态隐藏列
var cmenu = null;
function createColumnMenu() {
    cmenu = $('<div/>').appendTo('body');
    cmenu.menu({
        onClick: function (item) {
            if (item.iconCls == 'icon-ok') {
                $('#list').datagrid('hideColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-empty'
                });
            } else {
                $('#list').datagrid('showColumn', item.name);
                cmenu.menu('setIcon', {
                    target: item.target,
                    iconCls: 'icon-ok'
                });
            }
        }
    });
    var fields = $('#list').datagrid('getColumnFields');
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var col = $('#list').datagrid('getColumnOption', field);
        cmenu.menu('appendItem', {
            text: col.title,
            name: field,
            iconCls: 'icon-ok'
        });
    }
}

//实现动态隐藏列结束

//双击编辑表单焦点消失后保存
var editIndex = undefined;
function endEditing() {
    if (editIndex == undefined) { return true }
    if ($('#list').datagrid('validateRow', editIndex)) {
        $('#list').datagrid('endEdit', editIndex);
        editIndex = undefined;
        return true;
    } else {
        return false;
    }
}
function onDblClickCell(index, field) {
    if (editIndex != index) {
        if (endEditing()) {
            $('#list').datagrid('selectRow', index)
            $('#list').datagrid('beginEdit', index);
            var ed = $('#list').datagrid('getEditor', { index: index, field: field });
            if (ed) {
                ($(ed.target).data('textbox') ? $(ed.target).textbox('textbox') : $(ed.target)).focus();
                //这个写法很有意思
                //为了方便 ,类似字段要同事写两个值 1把部门id写入隐藏字段2把标题换成值写入 显示字段
                if (field == 'vest') //如果是系统字典的时候才执行
                {
                    $(ed.target).combotree({
                        onSelect: function (node) {
                            //alert(node.id);
                            var edid = $('#list').datagrid('getEditor', { index: index, field: field + 'id' });
                            //设置隐藏列ID值
                            $(edid.target).textbox('setValue', node.id);
                            //把当前节点ID设置为可读性文本。这样用户才能看的出来，关联性也好了。
                            node.id = node.text;
                        }
                    });
                }

            }
            editIndex = index;
        } else {
            setTimeout(function () {
                $('#list').datagrid('selectRow', editIndex);
            }, 0);
        }
    }
}

function onUnselect(index, row) {
    //alert(index);
}

function onCancelEdit(index, row) {
    //alert(index);
}
function onClickRow(index, row) {
    if (editIndex != undefined) {
        //alert("正在编辑的行是：" + editIndex);
        //alert("验证正在编辑的行：" + $('#list').datagrid('validateRow', editIndex));
        if ($('#list').datagrid('validateRow', editIndex)) {//如果验证正在编辑的行数据有效则
            $("#list").datagrid('endEdit', editIndex);//如果点其他行，结束编辑正在编辑的行
            editIndex = undefined;
        } else {
            msg.warning("还有一行没编辑完啦！！");
        }
    }
    $("#list").datagrid('selectRow', index);


}

function onSelect(index, row) {

}

function onEndEdit(index, row, changes) {
    //alert('结束编辑'+index+JSON.stringify(row));
    var o = {};
    var query = JSON.stringify(row);
    o.jsonEntity = query;
    o.action = 'edit';
    o.keyid = row.KeyId;
    query = "json=" + JSON.stringify(o);
    //alert(query);
    //表格内编辑模式编辑成功不提示信息
    jQuery.ajaxjson(actionURL, query, function (d) {
        //if (parseInt(d) > 0) {
        //    msg.ok('编辑成功！');
        //    hDialog.dialog('close');
        grid.reload();
        // } else {
        //     MessageOrRedirect(d);
        // }
    });

}
function append() {
    if (endEditing()) {
        $('#list').datagrid('appendRow', { status: 'P' });
        editIndex = $('#list').datagrid('getRows').length - 1;
        $('#list').datagrid('selectRow', editIndex)
            .datagrid('beginEdit', editIndex);
    }
}
function removeit() {
    if (editIndex == undefined) { return }
    $('#list').datagrid('cancelEdit', editIndex)
        .datagrid('deleteRow', editIndex);
    editIndex = undefined;
}
function accept() {
    if (endEditing()) {
        $('#list').datagrid('acceptChanges');
    }
}
function reject() {
    $('#list').datagrid('rejectChanges');
    editIndex = undefined;
}
function getChanges() {
    var rows = $('#list').datagrid('getChanges');
    alert(rows.length + ' rows are changed!');
}
//双击编辑表单焦点消失后保存结束

//自定义搜索开始
function mySearch() {
    //page=1&rows=20&filter={"groupOp":"AND","rules":[{"field":"unit","op":"cn","data":"昆明"},{"field":"connman","op":"cn","data":"朱光明"}],"groups":[]}
    var myunit = $("#myUnit").textbox('getValue');
    var connman = $("#myConnMan").textbox('getValue');
    var query = '{"groupOp":"AND","rules":[],"groups":[]}';
    var o = JSON.parse(query);
    var i = 0;
    if (myunit != '' && myunit != undefined) {//假如单位搜索不为空
        o.rules[i] = JSON.parse('{"field":"unit","op":"cn","data":"' + myunit + '"}');
        i = i + 1;
    }
    if (connman != '' & connman != undefined) {//联系人不为空
        o.rules[i] = JSON.parse('{"field":"connman","op":"cn","data":"' + connman + '"}');
    }

    $('#list').datagrid('reload', { filter: JSON.stringify(o) });


}


//自定义搜索结束


//单选多选开关
$('#selectSwitch').switchbutton({
    checked: false,
    onChange: function (checked) {
        if (checked) {
            $('#list').datagrid({ singleSelect: false });//多选
        } else {
            $('#list').datagrid({ singleSelect: true });//单选
        }
    }
})

