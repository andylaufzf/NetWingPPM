﻿function getUrlVars() {
    var vars = [],
        hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
//接下来就是获取指定参数的值了，代码如下。
var roomid = getUrlVars()["roomid"];
var userids = getUrlVars()["userids"];
var orderid = getUrlVars()["orderid"];
//alert("roomid:" + roomid + "userids:" + userids + "orderid:" + orderid);

$(document).ready(function () {
    //alert(document.body.clientHeight);
    //autoResize({ dataGrid: '#list', gridType: 'datagrid', width:0, height: 0 });
    $('#list').datagrid({
        url: '/MJOrderDetail/ashx/MJOrderDetailHandler.ashx?filter={"groupOp":"AND","rules":[{"field":"usersid","op":"eq","data":"' + userids + '"},{"field":"roomid","op":"eq","data":"' + roomid + '"},{"field":"mjorderid","op":"eq","data":"' + orderid + '"}],"groups":[]}',
        singleSelect: true,//只能选择一行
        width: 1000,
        height: 400,
        toolbar: [{
            iconCls: 'icon-save',
            text: '保存',
            handler: function () {
                var isValid = $("#form1").form('validate');//验证表单是否符合验证格式
                if (isValid) {
                    //alert("p");
                } else {
                    $.messager.alert('提醒', '表格没有填写完整');
                    return false;//返回false 程序不执行
                }

                //得到正在编辑的所有行 以下商业逻辑用来做参考
                var k = $('#list').datagrid('getEditingRowIndexs');
                //alert("正在编辑的行" + k.length);
                for (var i = 0; i < k.length; i++) {
                    var osped = $('#list').datagrid('getEditor', { index: k[i], field: 'sprice' });
                    var onumed = $('#list').datagrid('getEditor', { index: k[i], field: 'num' });
                    var oallpriceed = $('#list').datagrid('getEditor', { index: k[i], field: 'allprice' });
                    var sped = $('#list').datagrid('getEditor', { index: k[i], field: 'newsprice' });
                    var numed = $('#list').datagrid('getEditor', { index: k[i], field: 'newnum' });
                    var allpriceed = $('#list').datagrid('getEditor', { index: k[i], field: 'newallprice' });
                    var spv = $(sped.target).numberbox('getValue');
                    var numv = $(numed.target).numberspinner('getValue');//取得数量和单价的值
                    //alert(spv*numv);
                    $(allpriceed.target).numberbox('setValue', spv * numv);//设置总价
                    //设置原始价格
                    $(osped.target).numberbox('setValue', spv);
                    $(onumed.target).numberspinner('setValue', numv);
                    $(oallpriceed.target).numberbox('setValue', spv * numv);

                }

                //asp.net 自动生成了一个form1 所以这里验证表单时有个#form1
                $('#list').datagrid('acceptChanges');
                var rows = $('#list').datagrid('getRows');//获取当前的数据行
                //alert(JSON.stringify(rows));
                var fzc = 0;//统计房租出现次数
                var total = 0;//计算sumMoney的总和
                for (var i = 0; i < rows.length; i++) {
                    if (rows[i]['serviceId'] == 3) {
                        fzc = fzc + 1;
                    }
                    //alert(rows[i]['type']);
                    //alert(rows[i]['newallprice']);
                    total += parseFloat(rows[i]['newallprice']);
                }
                if (fzc > 1) {
                    $.messager.alert('提醒', '一个合同只允许出现一次房租！');
                    return false;
                }
                //alert(total);

                total = total.toFixed(2);//保留小数点两位注意是字符哦  
                //$('#payment').textbox({ value: total });//加金额给实付金额
                $('#total').numberbox("setValue", total);//加金额给实付金额
                $('.shifu').eq(0).numberbox('setValue', total);//实付金额第一个给初始值
            }
        }, {
            iconCls: 'icon-add',
            text: '新增收费项目',
            handler: function () {
                var rows = $('#list').datagrid('getRows');//获取当前的数据行
                //alert(rows);
                var add_time = new Date();
                var exp_time = new Date();
                for (var i = 0; i < rows.length; i++) {
                    if (rows[i]['serviceId'] == 3) {
                        //fzc = fzc + 1;
                        //保证新增的到期时间和房租一样
                        add_time = rows[i]["add_time"];
                        exp_time = rows[i]["exp_time"];
                    }
                    //alert(rows[i]['type']);
                    //alert(rows[i]['newallprice']);
                    total += parseFloat(rows[i]['newallprice']);
                }

                $('#list').datagrid('appendRow', { type: '新增', KeyId: 0, MJOrderid: 0, serviceName: '', sprice: 0, newsprice: 0, num: 1, newnum: 1, allprice: 0, newallprice: 0, add_time: new Date().Format(add_time), exp_time: new Date().Format(exp_time), note: '', dep: '', depid: 0, });

            }

        }, {
            iconCls: 'icon-application',
            text: '重置表单',
            handler: function () {
                $('#list').datagrid('reload');
            }

        }],
        rowStyler: function (index, row) {
            //格式化行 这里也只用来做参考
            if (row.KeyId == 0) {//新增行，直接就返回颜色
                return 'background-color:#6293BB;color:#fff;';
            }
            if (row.KeyId > 0) {//如果是原始数据，改过的也要变颜色
                if (row.newsprice > 0) {
                    return 'background-color:#6293BB;color:#fff;';
                }
            }
        },
        rownumbers: true, //行号
        columns: [[

            {
                title: '类型', field: 'type', editor: {
                    type: 'textbox', options: {
                        readonly: true,
                    }
                }, formatter: function (value, row, index) {
                    if (row.KeyId > 0) {
                        if (row.newsprice > 0) {
                            return '续费';

                        } else {
                            return '原始';

                        }

                    } else {
                        return '新增';

                    }
                }
            },
            { title: '系统编号', field: 'KeyId', sortable: true, width: '', hidden: true, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '订单ID', field: 'MJOrderid', sortable: true, width: '', hidden: true, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },
            {
                title: '项目(品名)', field: 'serviceName', sortable: true, width: '100', editor: {
                    type: 'combogrid', options: {
                        required: true, panelWidth: 150, validType: 'length[1,1000]', url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJaccountingSubjects&field=subjectType&q=收入&wherefield=usetype&wherestr=room', columns: [[
                            { field: 'KeyId', title: 'ID', width: 20, hidden: false },
                            { field: 'subjectType', title: '类型', width: 30, hidden: false },
                            { field: 'subjectName', title: '项目', width: 100 },
                            { field: 'note', title: '备注', hidden: true, width: 100 },
                        ]], textField: 'subjectName', idField: 'subjectName', missingMessage: '服务名称不能为空！',
         
                    }
                }
            },
            { title: '项目ID', field: 'serviceId', sortable: true, width: '', hidden: true, editor: { type: 'textbox', options: { required: true, validType: 'number', missingMessage: '请输入数字' } } },
            { title: '单价', field: 'sprice', sortable: true, width: '', hidden: true, editor: { type: 'numberbox', options: { required: true, validType: 'number', missingMessage: '请输入数字' } } },
            {
                title: '单价', field: 'newsprice', formatter: function (value, row, index) {
                    //alert(value);
                    return row.sprice;
                    //if (value == undefined) {
                    //    //return 0;
                    //    return row.sprice;
                    //} else {
                    //    //return value;
                    //    return row.sprice;
                    //}
                }, sortable: true, width: '', hidden: false, editor: { type: 'numberbox', options: { required: true, validType: 'number', missingMessage: '请输入数字' } }
            },
            { title: '数量', field: 'num', sortable: true, width: '', hidden: true, editor: { type: 'numberspinner', options: { required: true, validType: 'number', missingMessage: '请输入数字' } } },
            {
                title: '数量', field: 'newnum', formatter: function (value, row, index) {
                    //alert(value);
                    return row.num;
                    //if (value == undefined) {
                    //    //return 0;
                    //    return row.num;
                    //} else {
                    //    return value;
                    //}
                }, sortable: true, width: '', hidden: false, editor: { type: 'numberspinner', options: { required: true, validType: 'number', missingMessage: '请输入数字' } }
            },
            { title: '总价', field: 'allprice', sortable: true, width: '', hidden: true, editor: { type: 'numberbox', options: { required: true, validType: 'money', readonly: true, missingMessage: '请输入正确的金额' } } },
            {
                title: '总价', field: 'newallprice', formatter: function (value, row, index) {
                    return row.allprice;
                    //if (value == undefined) {
                    //    //return 0;
                    //    return row.allprice;
                    //} else {
                    //    return value;
                    //}
                }, sortable: true, width: '', hidden: false, editor: { type: 'numberbox', options: { required: true, validType: 'money', value: 0, readonly: true, missingMessage: '请输入正确的金额' } }
            },
            {
                title: '开始时间', field: 'add_time', formatter: function (value, row, index) {
                    return new Date(value).Format("yyyy-MM-dd");
                }, sortable: true, width: '90', hidden: false, editor: { type: 'datebox', options: { required: true, validType: 'date', missingMessage: '请输入合法的日期' } }
            },
            {
                title: '到期时间', formatter: function (value, row, index) {
                    return new Date(value).Format("yyyy-MM-dd");
                }, field: 'exp_time', sortable: true, width: '90', hidden: false, editor: { type: 'datebox', options: { required: true, validType: 'date', missingMessage: '请输入合法的日期' } }
            },
            { title: '备注', field: 'note', sortable: true, width: '300', hidden: false, editor: { type: 'textbox', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '系统备注', field: 'setno', sortable: true, width: '100', hidden: false, editor: { type: 'combobox', options: { required: false, validType: '', missingMessage: '', readonly: true, disabled: true } } },
            { title: '部门名称', field: 'dep', sortable: true, width: '', hidden: true, editor: { type: 'combotree', options: { required: false, validType: '', missingMessage: '' } } },
            { title: '部门id', field: 'depid', sortable: true, width: '', hidden: true, editor: { type: 'numberspinner', options: { required: false, validType: '', missingMessage: '' } } },

        ]],
        onLoadSuccess: function (data) {
            for (var i = 0; i < data.total; i++) {
                $('#list').datagrid('updateRow', {
                    index: i,
                    row: {
                        newallprice: 0,
                        newnum: 0,
                        newprice: 0,
                        type: '原始'

                    }
                });
            }
            //数据加载完后 给列赋初始值

        },
        onClickCell: function (index, field, value) { //双击可以修改
            //alert(index);
            //alert($('#list').form('validate'));
            //$('#list').datagrid('beginEdit', index);
            //if (field == 'subject') {//如果当前选择的是科目名称
            //    var ed = $('#list').datagrid('getEditor', { index: index, field: 'subject' });//找到行editor
            //    $(ed.target).combobox({
            //        onSelect: function (rec) {//子节点选择时的动作
            //            var val = $(ed.target).combobox('getText');//得到combogrid的值
            //            var edb = $('#list').datagrid('getEditor', { index: index, field: 'subjectid' });//找到行editor 找到第一个列文本框编辑器
            //            $(edb.target).textbox('setText', val);//设置文本框
            //            var ft = $('#list').datagrid('getEditor', { index: index, field: 'ftype' });//得到收支类型
            //            $(ft.target).textbox('setValue', rec.subjectType);//设置收支类型

            //        }
            //    });//获得当前选择的值
            //}



        },
        //onAfterEdit: onEndEdit, //绑定datagrid结束编辑时触发事件
        onDblClickRow: function (index, row) {//单击row
            //alert(JSON.stringify(row));
            if (row.KeyId > 0) {//大于0 说明是原始数据 服务名称不需要编辑器
                $("#list").datagrid('removeEditor', 'serviceName');//移除编辑器不允许编辑
                $('#list').datagrid('beginEdit', index);
                var addtimeed = $('#list').datagrid('getEditor', { index: index, field: 'add_time' });
                var exptimeed = $('#list').datagrid('getEditor', { index: index, field: 'exp_time' });

                var newnumed = $('#list').datagrid('getEditor', { index: index, field: 'newnum' });
                $(newnumed.target).numberspinner('setValue', row.num);
                var newallpriceed = $('#list').datagrid('getEditor', { index: index, field: 'newallprice' });
                $(newallpriceed.target).numberbox('setValue', row.allprice);
                var newspriceed = $('#list').datagrid('getEditor', { index: index, field: 'newsprice' });
                $(newspriceed.target).numberbox('setValue', row.sprice);
                //$(addtimeed.target).datebox('setValue', row.add_time);//保持原始时间 设置添加时间为空
                //$(exptimeed.target).datebox('setValue', row.exp_time);
                //设置类型值
                var typeed = $('#list').datagrid('getEditor', { index: index, field: 'type' });
                $(typeed.target).textbox('setValue', '续费');
                //其他费用同步房租


                var allrows = $('#list').datagrid('getRows');//获取当前的数据行
                //alert(rows);
                var fzadd_time = null;
                var fzexp_time = null;

                for (var i = 0; i < allrows.length; i++) {
                    if (allrows[i]['serviceId'] == 3) {
                        //fzc = fzc + 1;
                        //保证新增的到期时间和房租一样
                        var adted = $('#list').datagrid('getEditor', { index: i, field: 'add_time' });
                        var epted = $('#list').datagrid('getEditor', { index: i, field: 'exp_time' });
                        //alert(epted);
                        if (adted != null && epted != null) {
                            fzadd_time = $(adted.target).datebox('getValue');
                            fzexp_time = $(epted.target).datebox('getValue');
                            console.log("房租开始时间" + fzadd_time + "和到期时间" + fzexp_time);
                        }

                    }
                    //alert(rows[i]['type']);
                    //alert(rows[i]['newallprice']);
                }
                //水费 宽带费 垃圾清理费和房租同步
                if (fzadd_time != null && fzexp_time != null) {
                    if (row.serviceId == 9 || row.serviceId == 10 || row.serviceId == 12) {
                        $(addtimeed.target).datebox('setValue', fzadd_time);
                        $(exptimeed.target).datebox('setValue', fzexp_time);
                        $.messager.show({
                            title: '时间已和房租时间自动同步！',
                            msg: '消息将在2秒后关闭。',
                            timeout: 2000,
                            showType: 'slide'
                        });
                    }
                }












            } else {//说明是新增的行
                //$("#list").datagrid('addEditor', 'serviceName');//因为上面做了移除工作，这里要增加一个editor
                $("#list").datagrid('addEditor', [ //添加cardNo列editor
                    {
                        field: 'serviceName', editor: {
                            type: 'combogrid', options: {
                                required: true, panelWidth: 150, validType: 'length[1,1000]', url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJaccountingSubjects&field=subjectType&q=收入&wherefield=usetype&wherestr=room', columns: [[
                                    { field: 'KeyId', title: 'ID', width: 20, hidden: false },
                                    { field: 'subjectType', title: '类型', width: 30, hidden: false },
                                    { field: 'subjectName', title: '项目', width: 100 },
                                    { field: 'note', title: '备注', hidden: true, width: 100 },
                                ]], textField: 'subjectName', idField: 'subjectName', missingMessage: '服务名称不能为空！',
                                limitToList: true,//只能从下拉中选择值
                                reversed: true,//定义在失去焦点的时候是否恢复原始值。//这样就必须进行选择
                                onHidePanel: function () {
                                    var t = $(this).combogrid('getValue');//获取combogrid的值
                                    var g = $(this).combogrid('grid');	// 获取数据表格对象
                                    var r = g.datagrid('getSelected');	// 获取选择的行
                                    console.log(r);
                                    if (r == null || t != r.subjectName) {//没有选择或者选项不相等时清除内容
                                        $.messager.alert('警告', '请选择，不要直接输入!');
                                        $(this).combogrid('setValue', '');
                                    } else {
                                        //do something...
                                    }
                                },

                            }
                        }
                    }]);

                $('#list').datagrid('beginEdit', index);
                //以下代码主要是用于行内编辑文本框做一些判断留着做一些参考
                var ed = $('#list').datagrid('getEditor', { index: index, field: 'serviceName' });//找到行editor
                $(ed.target).combogrid({
                    onSelect: function (rowIndex, rowData) {//子节点选择时的动作
                        var noteed = $('#list').datagrid('getEditor', { index: index, field: 'note' });//找到行editor 找到第一个列文本框编辑器
                        var sided = $('#list').datagrid('getEditor', { index: index, field: 'serviceId' });//科目ID
                        $(noteed.target).textbox('setValue', rowData.note);//设置备注文本框
                        $(sided.target).textbox('setValue', rowData.KeyId);//给隐藏科目ID设置值
                        //alert(rowData.KeyId);//自行车停车费
                        if (rowData.KeyId == 14) {
                            var seted = $('#list').datagrid('getEditor', { index: index, field: 'setno' });
                            $(seted.target).combobox({////启用编辑器
                                url: '/MJDevice_child/ashx/MJDevice_childHandler.ashx',
                                valueField: 'KeyId',
                                textField: 'dcname',
                                disabled: false,
                                readonly: false,
                                required: true,
                                missingMessage: '请选择电瓶车',
                                loadFilter: function (data) {//过滤显示
                                    return data.rows;
                                }
                            });
                            //alert("d");
                        } else {
                            var seted = $('#list').datagrid('getEditor', { index: index, field: 'setno' });
                            $(seted.target).combobox({////启用编辑器
                                disabled: true,
                                readonly: true
                            });
                        }

                    }
                });//获得当前选择的值




                // var deped = $('#list').datagrid('getEditor', { index: index, field: 'dep' });//得到部门ed
                // $(deped.target).combotree({
                //     required: true,
                //     validType: 'length[0,100]',
                //     url: '/sys/ashx/userhandler.ashx?json={"jsonEntity":"{}","action":"deps"}',
                //     textField: 'Title',
                //     onSelect: function (node) {
                //         var ed = $('#list').datagrid('getEditor', { index: index, field: 'depid' });
                //         $(ed.target).numberspinner('setValue', node.id); node.id = node.text;
                //     },
                //     missingMessage: ''
                // });
            }

            //新数量计算
            var newallpriceed = $('#list').datagrid('getEditor', { index: index, field: 'newallprice' });
            var newnumed = $('#list').datagrid('getEditor', { index: index, field: 'newnum' });//数量ed
            var newsled = $('#list').datagrid('getEditor', { index: index, field: 'newsprice' });//数量单价
            $(newsled.target).numberbox({//改变数量时动作
                min: 0,
                //precision: 2,
                onChange: function (newValue, oldValue) {
                    //alert("o" + oldValue + "n" + newValue);
                    //计算单价
                    var num = $(newnumed.target).numberspinner('getValue');
                    $(newallpriceed.target).numberbox('setValue', (num * newValue));
                    //alert(num);
                    //$("#total").numberbox('setValue', compute('allprice'));//设置总价

                }
            });
            $(newnumed.target).numberspinner({//数量改变时动作
                min: 1,
                max: 99999999,
                value: 1,
                onChange: function (newValue, oldValue) {
                    var sl = $(newsled.target).numberbox('getValue');
                    $(newallpriceed.target).numberbox('setValue', (sl * newValue));//总价=数量*单价
                    //alert(compute('allprice'));
                    //设置总价
                    //$("#total").numberbox('setValue', compute('allprice'));
                }
                //editable: false
            });

            //新数量计算


        }

    });
});

//扩展datagrid:动态添加删除editor
$.extend($.fn.datagrid.methods, {
    addEditor: function (jq, param) {
        if (param instanceof Array) {
            $.each(param, function (index, item) {
                var e = $(jq).datagrid('getColumnOption', item.field);
                e.editor = item.editor;
            });
        } else {
            var e = $(jq).datagrid('getColumnOption', param.field);
            e.editor = param.editor;
        }
    },
    removeEditor: function (jq, param) {
        if (param instanceof Array) {
            $.each(param, function (index, item) {
                var e = $(jq).datagrid('getColumnOption', item);
                e.editor = {};
            });
        } else {
            var e = $(jq).datagrid('getColumnOption', param);
            e.editor = {};
        }
    }
});


// 对Date的扩展，将 Date 转化为指定格式的String
// 月(M)、日(d)、小时(h)、分(m)、秒(s)、季度(q) 可以用 1-2 个占位符， 
// 年(y)可以用 1-4 个占位符，毫秒(S)只能用 1 个占位符(是 1-3 位的数字) 
// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}
//调用： 

//var time1 = new Date().Format("yyyy-MM-dd");
//var time2 = new Date().Format("yyyy-MM-dd HH:mm:ss"); 

/*
*  datagrid 获取正在编辑状态的行，使用如下：
*  $('#id').datagrid('getEditingRowIndexs'); //获取当前datagrid中在编辑状态的行编号列表
*/
$.extend($.fn.datagrid.methods, {
    getEditingRowIndexs: function (jq) {
        var rows = $.data(jq[0], "datagrid").panel.find('.datagrid-row-editing');
        var indexs = [];
        rows.each(function (i, row) {
            var index = row.sectionRowIndex;
            if (indexs.indexOf(index) == -1) {
                indexs.push(index);
            }
        });
        return indexs;
    }
});


//经手人
$("#contactid").combogrid({
    panelWidth: 350,
    required: true,
    //value:'fullname',   
    idField: 'KeyId',
    editable: false,
    textField: 'TrueName',
    url: '/sys/ashx/userhandler.ashx?action=mydep',
    columns: [[
        { field: 'TrueName', title: '姓名', width: 60 },
        { field: 'KeyId', title: 'KeyId', width: 100 }
    ]],
    onSelect: function (rowIndex, rowData) {
        $("#contactid").val(rowData.KeyId);//给经手人设置ID
        $("#contact").val(rowData.TrueName);//经手人
        //alert(rowData.TrueName);
    }
});

//付款方式
function addpaytype() {
    var c = $("#paytypediv").children('li').length;
    if (c == 3) {//付款方式最多三个
        $.messager.alert('警告', '付款方式最多三个');
        return false;
    }
    var temp = '<li class="paytypeli"><input  name="account" class="account" value="" style="display: none;" />付款方式:<input  name="accountid" class="accountid" value="" />实收金额: <input type="text" name="shifu" class="easyui-numberbox shifu" value="0" style="width: 100px;" data-options="min:0,precision:2"></input><a  href="#"  class="easyui-linkbutton paytypea" data-options="iconCls:\'icon-cancel\'">删除</a></li> ';
    //alert(temp);
    $("#paytypediv").append(temp);
    initBank();//初始化银行
}

function initBank() {
    $(".easyui-linkbutton").linkbutton({});//初始化easyui按钮
    $(".shifu").numberbox({//初始化实付金额
        min: 0,
        precision: 2
    });
    //银行
    $(".accountid").combobox({
        valueField: 'KeyId',//用中文名作为名称
        required: true,
        editable: false,
        validType: 'selectValueRequired',
        textField: 'accountName',
        url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJBankAccount&field=accountName',
        onSelect: function (rec) {
            $(this).prev('.account').val(rec.accountName);
            //$("#account").val(rec.accountName);//设置隐藏ID
        }
    });

    //alert("00");
    $(".paytypea").click(function (e) {
        $(this).closest("li").remove();
    });
}


//银行 打开时用一次性加载
$(".accountid").combobox({
    valueField: 'KeyId',//用中文名作为名称
    required: true,
    editable: false,
    validType: 'selectValueRequired',
    textField: 'accountName',
    url: '/sys/ashx/DataSourceFieldHandler.ashx?tablename=MJBankAccount&field=accountName',
    onSelect: function (rec) {
        $(this).prev('.account').val(rec.accountName);
        //$("#account").val(rec.accountName);//设置隐藏ID
    }
});

function tijiao() {
    $('#form1').form('submit', {
        url: '/MJOrderDetail/ashx/MJOrderXF.ashx',
        onSubmit: function (param) {
            var isValid = $("#form1").form('validate');//验证表单是否符合验证格式
            if (isValid == false) {
                $.messager.alert('警告', '表单还有信息没有填完整！');
                rr = false;
                return false;
                //$.messager.progress('close');	// 如果表单是无效的则隐藏进度条
            } else {//表单验证通过
                $('#list').datagrid('acceptChanges');//datagrid接受改变
                //以下逻辑提供参考
                //if ($('#payment').numberbox('getValue') == '') {
                //alert("请先保存了再提交");
                //    $.messager.alert('警告', '请先保存再提交!');
                //    return false;
                //}

                //循环选择行 只有选择行了之后才能返回选中行
                var rows = $('#list').datagrid('getRows');//获取当前的数据行
                //var rows = $('#list').datagrid('getSelections');//返回选中的行
                if (rows == '') {//判断是否返回空数组 说明没有填写数据
                    //alert("空数组");
                    $.messager.alert('注意', '请填写或保存详细数据!');
                    rr = false;//返回失败
                    return false;
                } else {
                    //alert(JSON.stringify(rows));//有数据返回数据
                }
                param.main = $("#form1").serializeJSON();  //jquery方法把表单系列化成json后提交
                //以上用法参考https://github.com/macek/jquery-serialize-object
                param.detail = JSON.stringify(rows);//param  是EasyUI格式详细表序列化成json 
                //alert(param.main);
                //alert(param.detail);
                //return false;
                //rr = true;
                //return true;
            }
        },
        success: function (d) {
            var d = eval('(' + d + ')');  // change the JSON string to javascript object    
            if (d.status == 1) {//返回1标识成功！返回0失败
                //如果成功，关闭当前窗口
                // 添加一个未选中状态的选项卡面板

                //var hasTab = top.$('#tabs').tabs('exists', '入住手续主表');

                //if (!hasTab) {
                //    top.$('#tabs').tabs('add', {
                //        title: '入住手续主表',
                //        content: createFrame('/MJOrder/MJOrder.aspx?navid=109'),
                //        //closable: (subtitle != onlyOpenTitle),
                //        icon: '/css/icon/32/note.png'
                //    });
                //} else {
                //    top.$('#tabs').tabs('select', '入住手续主表');
                //    var tab = top.$('#tabs').tabs('getSelected');  // 获取选择的面板
                //    //刷新当前面板
                //    tab.panel('refresh', '/MJOrder/MJOrder.aspx?navid=109');

                //    //closeTab('refresh'); //选择TAB时刷新页面
                //}
                //top.$('#tabs').tabs('close', '办理入住手续');//关闭办理入住手续
                //alert(api);
                //$.messager.alert('恭喜', d.msg);
                $.messager.confirm(d.msg, '要继续打印续费收据吗？', function (r) {
                    if (r) {
                        console.log("要打印，先打开打印窗口。然后关闭本窗口！");
                        W.PrintXF(d.edNumber);
                        api.close();
                    } else {
                        console.log("不打印关闭窗口");
                        W.top.closeTab("refresh");
                        console.log("刷新父窗口成功，即将关闭当前窗口！");
                        api.close();//关闭窗口
                    }
                });




                //api.close();


                //location = '/MJOrder/MJOrder.aspx';//刷新本地网址
            } else {
                $.messager.alert('对不起!', d.msg);
                rr = false;
            }

            //
            //alert("成功动作");
            //$.messager.progress('close');	// 如果提交成功则隐藏进度条
        }
    });
}

//给公共变量赋值
$("#orderid").val(orderid);
$("#roomid").val(roomid);
$("#userids").val(userids);