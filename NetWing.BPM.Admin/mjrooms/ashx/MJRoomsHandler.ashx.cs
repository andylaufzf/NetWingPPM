using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.IO;
using System.Web.SessionState;
using NetWing.Model;
using NetWing.Bll;
using Omu.ValueInjecter;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Bll;
using NetWing.Common;
using NetWing.Common.Data;
using NetWing.Common.Data.SqlServer;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Excel;
using NetWing.Common.IO;
using NetWing.Common.IO.DirFile;
using NetWing.Common.Data.Filter;

namespace NetWing.BPM.Admin.MJRooms.ashx
{
    /// <summary>
    ///  的摘要说明
    /// </summary>
    public class MJRoomsHandler : IHttpHandler, IRequiresSessionState
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            int k;
            var json = HttpContext.Current.Request["json"];
            var rpm = new RequestParamModel<MJRoomsModel>(context) { CurrentContext = context, Action = context.Request["action"] };
            if (!string.IsNullOrEmpty(json))
            {
                rpm = JSONhelper.ConvertToObject<RequestParamModel<MJRoomsModel>>(json);
                rpm.CurrentContext = context;
            }

            switch (rpm.Action)
            {
                case "add":
                    //context.Response.Write(MJRoomsBll.Instance.Add(rpm.Entity));
                    MJRoomsModel a = new MJRoomsModel();
                    a.InjectFrom(rpm.Entity);
                    DataRow dr = SqlEasy.ExecuteDataRow("select * from Sys_Departments where keyid=" + SysVisitor.Instance.cookiesUserDepId + "");
                    a.vest = dr["DepartmentName"].ToString();
                    a.vestid = int.Parse(SysVisitor.Instance.cookiesUserDepId);//门店ID
                    a.depid = int.Parse(SysVisitor.Instance.cookiesUserDepId);//数据权限部门ID
                    a.ownner = int.Parse(SysVisitor.Instance.cookiesUserId);//数据所有者ID
                    context.Response.Write(MJRoomsBll.Instance.Add(a));
                    break;
                case "edit":
                    //编辑时间不改变所有者
                    MJRoomsModel d = new MJRoomsModel();
                    //d.InjectFrom(rpm.Entity);
                    //d.KeyId = rpm.KeyId;
                    //d.depid= SysVisitor.Instance.UserDepId;//数据权限部门ID
                    //d.ownner= SysVisitor.Instance.UserId;//数据所有者ID
                    //context.Response.Write(MJRoomsBll.Instance.Update(d));
                    d.InjectFrom(rpm.Entity);
                    d.KeyId = rpm.KeyId;

                    DataRow dre = SqlEasy.ExecuteDataRow("select * from Sys_Departments where keyid=" + SysVisitor.Instance.cookiesUserDepId + "");
                    d.vest = dre["DepartmentName"].ToString();
                    d.vestid = int.Parse(SysVisitor.Instance.cookiesUserDepId);//门店ID
                    string[] lgfields = { };//忽略字段
                    //只更新以下字段
                    string[] upfields = { "price", "oprice", "quarterprice", "halfyearprice", "yearprice", "remarks", "roomtype" };
                    context.Response.Write(DbUtils.UpdateOnlyUpdateFields(d, upfields));
                    //context.Response.Write(MJRoomsBll.Instance.Update(d));
                    break;
                case "export":
                    //string fields = rpm.fields;
                    string fields = rpm.CurrentContext.Request["fields"];
                    string tablename = TableConvention.Resolve(typeof(MJRoomsModel));//得到表名
                    DataTable xlsDt = SqlEasy.ExecuteDataTable("select " + fields + " from " + tablename + "");
                    ExcelHelper.NPIOtoExcel(xlsDt, HttpContext.Current.Server.MapPath("\\upload\\excel\\" + tablename + ".xls"));
                    context.Response.Write("{\"status\":\"ok\",\"filename\":\"" + tablename + ".xls\"}");
                    break;
                case "inport"://从Excel导入到数据库
                    if (context.Request["REQUEST_METHOD"] == "OPTIONS")
                    {
                        context.Response.End();
                    }
                    SaveFile("~/temp/", context);
                    break;
                case "kongsearch"://空房查询
                    //这里是查询所有空闲 所以不设权限
                    string sqlwherestr = " 1=1 and state='空闲' ";
                    if (!string.IsNullOrEmpty(rpm.Filter))//如果筛选不为空
                    {
                        string str = " and " + FilterTranslator.ToSql(rpm.Filter);
                        sqlwherestr = sqlwherestr + str;
                    }
                    //TableConvention.Resolve(typeof(MJUserModel)) 转换成表名
                    var pcp = new ProcCustomPage(TableConvention.Resolve(typeof(MJRoomsModel)))
                    {
                        PageIndex = rpm.Pageindex,
                        PageSize = rpm.Pagesize,
                        OrderFields = rpm.Sort,
                        WhereString = sqlwherestr
                    };
                    int recordCountstr;
                    DataTable dtstr = DbUtils.GetPageWithSp(pcp, out recordCountstr);
                    context.Response.Write(JSONhelper.FormatJSONForEasyuiDataGrid(recordCountstr, dtstr));
                    break;
                case "exptime"://最早退房时间
                    //这里是查询所有空闲 所以不设权限
                    string sqlwhereexp = " 1=1 and state='租赁中' ";
                    if (!string.IsNullOrEmpty(rpm.Filter))//如果筛选不为空
                    {
                        string str = " and " + FilterTranslator.ToSql(rpm.Filter);
                        sqlwhereexp = sqlwhereexp + str;
                    }
                    //其实是显示最近30天内到期的房间
                    sqlwhereexp = sqlwhereexp + " and  DATEDIFF(d,orderend_time,'" + DateTime.Now.ToString() + "')<30";

                    //TableConvention.Resolve(typeof(MJUserModel)) 转换成表名
                    var pcpexp = new ProcCustomPage(TableConvention.Resolve(typeof(MJRoomsModel)))
                    {
                        PageIndex = rpm.Pageindex,
                        PageSize = rpm.Pagesize,
                        OrderFields = "orderend_time",//按合同到期时间排序
                        WhereString = sqlwhereexp
                    };
                    int recordCountexp;
                    DataTable dtexp = DbUtils.GetPageWithSp(pcpexp, out recordCountexp);
                    context.Response.Write(JSONhelper.FormatJSONForEasyuiDataGrid(recordCountexp, dtexp));
                    break;
                case "roompersonnel"://房间入住人
                                     //因为每一个店只能看自己的数据，所以这里做了控制的数据权限
                    string sqlwhereroom = " 1=1 and state='租赁中' and (depid in (" + SysVisitor.Instance.cookiesDepartments + ")) ";
                    //"(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";
                    if (!string.IsNullOrEmpty(rpm.Filter))//如果筛选不为空
                    {
                        string str = " and " + FilterTranslator.ToSql(rpm.Filter);
                        sqlwhereroom = sqlwhereroom + str;
                    }

                    //sqlwhereroom = sqlwhereroom + " and  DATEDIFF(d,orderend_time," + DateTime.Now.ToString() + ")<30";

                    //TableConvention.Resolve(typeof(MJUserModel)) 转换成表名
                    var pcproom = new ProcCustomPage(TableConvention.Resolve(typeof(MJRoomsModel)))
                    {
                        PageIndex = rpm.Pageindex,
                        PageSize = rpm.Pagesize,
                        OrderFields = "orderend_time",//按合同到期时间排序
                        WhereString = sqlwhereroom
                    };
                    int recordroom;
                    DataTable dtroom = DbUtils.GetPageWithSp(pcproom, out recordroom);
                    context.Response.Write(JSONhelper.FormatJSONForEasyuiDataGrid(recordroom, dtroom));
                    break;
                case "delete":
                    context.Response.Write(MJRoomsBll.Instance.Delete(rpm.KeyId));
                    break;
                case "alldel"://2017-04-05新增的功能 批量删除删除结果返回删除条数
                    context.Response.Write(NetWing.Dal.MJRoomsDal.Instance.Delete(rpm.KeyIds));
                    break;
                default:
                    string sqlwhere = "(depid in (" + SysVisitor.Instance.cookiesUserDepId + "))";
                    if (SysVisitor.Instance.cookiesIsAdmin == "True")
                    { //判断是否是超管如果是超管理，所有显示
                        sqlwhere = " 1=1 ";//如果是超管则不显示
                    }
                    if (!string.IsNullOrEmpty(rpm.Filter))//如果筛选不为空
                    {
                        string str = " and " + FilterTranslator.ToSql(rpm.Filter);
                        sqlwhere = sqlwhere + str;
                    }
                    //TableConvention.Resolve(typeof(MJUserModel)) 转换成表名
                    var pcpstr = new ProcCustomPage(TableConvention.Resolve(typeof(MJRoomsModel)))
                    {
                        PageIndex = rpm.Pageindex,
                        PageSize = rpm.Pagesize,
                        OrderFields = rpm.Sort,
                        WhereString = sqlwhere
                    };
                    int recordCount;
                    DataTable dt = DbUtils.GetPageWithSp(pcpstr, out recordCount);
                    context.Response.Write(JSONhelper.FormatJSONForEasyuiDataGrid(recordCount, dt));


                    //以下是久的方法
                    //filter {"groupOp":"AND","rules":[{"field":"KeyId","op":"cn","data":"1,2,3"}],"groups":[]}
                    //{"groupOp":"AND","rules":[{"field":"KeyId","op":"cn","data":"1,2,3"},{"field":"roomnumber","op":"eq","data":"6112"}],"groups":[]}
                    //string deps = SysVisitor.Instance.Departments;//得到当前数据权限如1，100,200 改值从用户角色   用户数据权限里获得  用户角色优先
                    //context.Response.Write(MJRoomsBll.Instance.GetJson(rpm.Pageindex, rpm.Pagesize, rpm.Filter, rpm.Sort, rpm.Order));
                    break;
            }
        }

        /// <summary>
        /// 文件保存操作
        /// </summary>
        /// <param name="basePath"></param>
        private void SaveFile(string basePath, HttpContext context)
        {
            var name = string.Empty;
            basePath = (basePath.IndexOf("~") > -1) ? context.Server.MapPath(basePath) :
            basePath;
            HttpFileCollection files = context.Request.Files;

            if (!Directory.Exists(basePath))//如果文件夹不存在创建文件夹
                Directory.CreateDirectory(basePath);
            //清空temp文件夹
            DirFileHelper.ClearDirectory(basePath);

            var suffix = files[0].ContentType.Split('/');
            var _suffix = suffix[1].Equals("jpeg", StringComparison.CurrentCultureIgnoreCase) ? "" : suffix[1];
            var _temp = System.Web.HttpContext.Current.Request["name"];

            if (!string.IsNullOrEmpty(_temp))
            {
                name = _temp;
            }
            else
            {
                Random rand = new Random(24 * (int)DateTime.Now.Ticks);
                name = rand.Next() + "." + _suffix;
            }

            var full = basePath + name;
            files[0].SaveAs(full);

            DataTable dt = NPOIHelper.ImportExceltoDt(full);
            string connectionString = SqlEasy.connString;
            SqlBulkCopy sqlbulkcopy = new SqlBulkCopy(connectionString, SqlBulkCopyOptions.UseInternalTransaction);
            sqlbulkcopy.DestinationTableName = TableConvention.Resolve(typeof(MJRoomsModel));//数据库中的表名
                                                                                             //自定义的datatable和数据库的字段进行对应  
                                                                                             //sqlBC.ColumnMappings.Add("id", "tel");  
                                                                                             //sqlBC.ColumnMappings.Add("name", "neirong");  
            int k = dt.Rows.Count - 1;                                                                       //注意一个问题，最后一列是字段数
            for (int i = 0; i < (dt.Columns.Count - 1); i++)
            {
                sqlbulkcopy.ColumnMappings.Add(dt.Columns[i].ColumnName.ToString(), dt.Columns[i].ColumnName.ToString());

            }
            var _result = "";
            try
            {
                sqlbulkcopy.WriteToServer(dt);
                _result = "{\"msg\" : \"导入数据库成功!\", \"result\" : " + k + ", \"filename\" : \"" + name + "\"}";
            }
            catch (Exception)
            {
                _result = "{\"msg\" : \"导入失败,可能模板不对，或其他原因，建议导出数据作为模板重新处理。注意导入没有校验数据重复功能。请人工校验数据!\", \"result\" : 0, \"filename\" : \"" + name + "\"}";
                //throw;
            }



            System.Web.HttpContext.Current.Response.Write(_result);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}