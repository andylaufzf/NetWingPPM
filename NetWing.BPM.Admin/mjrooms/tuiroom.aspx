﻿<%@ Page Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="tuiroom.aspx.cs" Inherits="NetWing.BPM.Admin.mjrooms.tuiroom" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->



    <!-- 工具栏按钮 -->
    <!-- datagrid 列表 -->
    <div id="tt" class="easyui-tabs" style="width: ; height: ;">
        <div title="退租办理" style="padding: 20px; display: none;">
            特别提示：退房宽带费不退，房租、垃圾费、停车费等按月计费项目不足一个月按一个月计算，就餐卡及电费卡剩余费用及押金项目全额退还，房间设施按入住清单检查验收，丢失损坏按价赔。<br />
            <br />
            <br />
            房号:<input type="text" value="" class="easyui-textbox" data-options="readonly:true" style="width:100px;" name="roomno" /> 姓名:<input type="text" value="" data-options="readonly:true" class="easyui-textbox" style="width:100px;" name="users" />
            <br />
            <br />
            <!--请修改项目顺序和修改样式-->
            <table id="list"></table>
            <br />
            合计金额：
        <input id="total" name="total" readonly="readonly" class="easyui-numberbox" data-options="precision:2" style="width: 100px;" />经手人：<input id="contactid" name="contactid" style="width: 100px;" /><input id="contact" name="contact" style="display: none; width: 100px;" />

            <!--房间id:-->
            <input id="roomid" style="display: none;" name="roomid" value="" />
            <!--用户id:-->
            <input id="usersid" style="display: none;" name="usersid" value="" />
            <input id="KeyId" style="display: none;" name="KeyId" value="" />
            <!--主订单id:-->
            <input id="orderid" style="display: none;" name="orderid" value="" />

            <br />
            <br />

            <div id="paytypediv">
                <li>
                    
                    <input class="account" name="account" value="" style="display: none;" />
                    付款方式:<input id="accountid" name="accountid" class="accountid" style="width: 100px;" value="" />
                    实付金额:<input type="text" name="shifu" class="easyui-numberbox shifu" value="0" style="width: 100px;" data-options="precision:2"><a id="addpaytype" onclick="addpaytype();" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-add'">增加</a>
                </li>

            </div>
            <br />
            <br />
            <!--隐藏值：订单主表keyid-->
            <div id="mainkeyid" style="display:none;"><%=keyid %></div>

            <%if (guoqi)
                {
                    %>
           <font color="ff0000">房租已过期，请先补交房租再办理退租手续！房租到期时间是:<%=exp_time.ToString() %></font> 
<%
                }
                else
                {
                    %>
            <a id="submit" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-save'">确定退房</a>
               <% } %>
            


        </div>

    </div>




    <!--Uploader-->
    <!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>
    
    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
    <!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>

    <!-- 引入js文件 -->
    <script src="js/tuiroom.js"></script>
</asp:Content>





