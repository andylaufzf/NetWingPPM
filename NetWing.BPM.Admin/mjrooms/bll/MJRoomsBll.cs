using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJRoomsBll
    {
        public static MJRoomsBll Instance
        {
            get { return SingletonProvider<MJRoomsBll>.Instance; }
        }

        public int Add(MJRoomsModel model)
        {
            return MJRoomsDal.Instance.Insert(model);
        }

        public int Update(MJRoomsModel model)
        {
            return MJRoomsDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJRoomsDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJRoomsDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
