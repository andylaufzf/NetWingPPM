using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJUserBll
    {
        public static MJUserBll Instance
        {
            get { return SingletonProvider<MJUserBll>.Instance; }
        }

        public int Add(MJUserModel model)
        {
            return MJUserDal.Instance.Insert(model);
        }

        public int Update(MJUserModel model)
        {
            return MJUserDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJUserDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJUserDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
