﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using NetWing.Model;//系统所有的model 都在这个命名空间里，这个还是比较有好处的。
using NetWing.BPM.Core;
using System.Data.Sql;
using System.Data.SqlClient;
using NetWing.Common.Data.SqlServer;
using NetWing.Common.Data;

namespace NetWing.BPM.Admin.mjrooms
{
    public partial class roomstatus : System.Web.UI.Page
    {

        public DataTable dt = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            string sql = "select * from MJRooms where ";
            //数据权限筛选s
            string sqlwhere = " 1=1 ";

            if (SysVisitor.Instance.cookiesIsAdmin=="False")
            { //判断是否是超管如果是超管理，所有显示
                sqlwhere = "(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
            }
            sql = sql + sqlwhere+ " order by roomnumber asc";
            dt = SqlEasy.ExecuteDataTable(sql);
            //计算总房间数
            string csql = "select count(keyid) from MJRooms where";
            int allroom = (int)SqlEasy.ExecuteScalar(csql + sqlwhere);
            this.laballroom.Text = allroom.ToString();
            //计算入住几间房
            string rzsql = csql + sqlwhere + " and state='租赁中'";
            int rzroom = (int)SqlEasy.ExecuteScalar(rzsql);
            this.labruzhu.Text = rzroom.ToString();
            //计算空置房
            string kzsql = csql + sqlwhere + " and state='空闲'";
            int kzroom = (int)SqlEasy.ExecuteScalar(kzsql);
            this.labkong.Text = kzroom.ToString();
            //入住率总和
            string allrzl = dt.Compute("sum(rzl)", "true").ToString();
            if (allrzl=="")
            {
                allrzl = "0";
            }
            this.zrzl.Text = (int.Parse(allrzl) / allroom).ToString();


            //计算空置率
            decimal kzl = (decimal)kzroom / allroom;//需要把其中一个先转换成decimal格式再除，否则值是0
            this.labkzl.Text = (decimal.Round(kzl, 2) * 100).ToString();
            //计算平均租金
            string zjsql = "select avg(price) avgp from MJRooms where";
            DataRow avgdr = SqlEasy.ExecuteDataRow(zjsql + sqlwhere);
            this.labpjzj.Text = decimal.Round(decimal.Parse(avgdr["avgp"].ToString()), 2).ToString();

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string v = DropDownList1.SelectedValue;//选中的值
            string sql = "select * from MJRooms where ";
            //数据权限筛选s
            string sqlwhere = " 1=1 ";

            if (SysVisitor.Instance.cookiesIsAdmin=="False")
            { //判断是否是超管如果是超管理，所有显示
                sqlwhere = "(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
            }
            //SELECT DATEDIFF(day,'2008-11-29','2008-11-25') AS DiffDate =-4 说明超过四天
            //所以 已逾期应该是<0
            //SELECT DATEDIFF(day,'2008-11-20','2008-11-25') AS DiffDate  =5 说明还有5天到期
            //所以5天内应该是<5
            if (v != "000")//000标识全部
            {
                sql = sql + sqlwhere + " and DateDiff(dd,getdate(),exp_time)" + v + " and userids is not null";
            }
            else
            {
                sql = sql + sqlwhere;
            }

            dt = SqlEasy.ExecuteDataTable(sql);

        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string v = DropDownList2.SelectedValue;//选中的值
            string sql = "select * from MJRooms where ";
            //数据权限筛选s
            string sqlwhere = " 1=1 ";

            if (SysVisitor.Instance.cookiesIsAdmin=="False")
            { //判断是否是超管如果是超管理，所有显示
                sqlwhere = "(depid in (" + SysVisitor.Instance.cookiesDepartments + "))";//如果是超管则不显示
            }
            //SELECT DATEDIFF(day,'2008-11-29','2008-11-25') AS DiffDate =-4 说明超过四天
            //所以 已逾期应该是<0
            //SELECT DATEDIFF(day,'2008-11-20','2008-11-25') AS DiffDate  =5 说明还有5天到期
            //所以5天内应该是<5
            if (v != "000")//000标识全部
            {
                sql = sql + sqlwhere + " and DateDiff(dd,getdate(),orderend_time)" + v + " and userids is not null";
            }
            else
            {
                sql = sql + sqlwhere;
            }

            dt = SqlEasy.ExecuteDataTable(sql);
        }
    }
}