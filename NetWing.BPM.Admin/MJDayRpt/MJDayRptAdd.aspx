﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site1.Master" CodeBehind="MJDayRptAdd.aspx.cs" Inherits="NetWing.BPM.Admin.MJDayRpt.MJDayRptAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <!-- 也可以在页面中直接加入按钮
    <div class="toolbar">
        <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add1" title="添加">添加</a>
        <a id="a_edit" href="#" plain="true" class="easyui-linkbutton" icon="icon-edit1" title="修改">修改</a>
        <a id="a_delete" href="#" plain="true" class="easyui-linkbutton" icon="icon-delete16" title="删除">删除</a>
        <a id="a_search" href="#" plain="true" class="easyui-linkbutton" icon="icon-search" title="搜索">搜索</a>
        <a id="a_reload" href="#" plain="true" class="easyui-linkbutton" icon="icon-reload" title="刷新">刷新</a>
    </div>
    -->

    <div id="tt" class="easyui-tabs" style="width: ; height:700px;" data-options="fit:true">
        <div title="日报表上报" style="padding: 20px; display: none;" >
            <!--  
数字调节器 class="easyui-numberspinner" required="required" data-options="min:1,max:99999,editable:true"  
日期选择框 class="easyui-datebox" required="required"  
kindedit编辑器 class="kindeditor"
如果数据库里允许空择验证是否为空 required="required"
-->
            

            <form id="uiform">
                <table class="grid">
                    <tr style="display:none;">
                        <td>系统ID：</td>
                        <td>
                            <input type="text" id="txt_KeyId" name="KeyId" class="easyui-numberspinner" value="0" data-options="min:0,max:9999,editable:true" style="width:100px;"  /></td>
                    </tr>
                    <tr style="display: ;">
                        <td  style="width:150px;">当日支出：</td>
                        <td style="width:auto;">
                            <input type="text" id="txt_dayMoneyOut" name="dayMoneyOut" class="easyui-numberbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>当日收入：</td>
                        <td>
                            <input type="text" id="txt_dayMoneyIn" name="dayMoneyIn" class="easyui-numberbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>当日收入明细：</td>
                        <td>
                            <input type="text" id="txt_dayMoneyInInfo" name="dayMoneyInInfo" class="easyui-textbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>当日支出明细：</td>
                        <td>
                            <input type="text" id="txt_dayMoneyOutInfo" name="dayMoneyOutInfo" class="easyui-textbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>收入合计：</td>
                        <td>
                            <input type="text" id="txt_totalIn" name="totalIn" class="easyui-numberbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>支出合计：</td>
                        <td>
                            <input type="text" id="txt_totalOut" name="totalOut" class="easyui-numberbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>余额合计：</td>
                        <td>
                            <input type="text" id="txt_totalBalance" name="totalBalance" class="easyui-numberbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>利润（扣除定金、押金）：</td>
                        <td>
                            <input type="text" id="txt_totalProfit" name="totalProfit" class="easyui-numberbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>截止上日实际余款：</td>
                        <td>
                            <input type="text" id="txt_nowBalance" name="nowBalance" class="easyui-numberbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>当日现金余额：</td>
                        <td>
                            <input type="text" id="txt_nowCashBalance" name="nowCashBalance" class="easyui-numberbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>截止当日实际余款：</td>
                        <td>
                            <input type="text" id="txt_nowDayBalance" name="nowDayBalance" class="easyui-numberbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>电瓶车停车数量：</td>
                        <td>
                            <input type="text" id="txt_bicycleCar" name="bicycleCar" class="easyui-numberspinner" value="0" data-options="min:0,max:9999,editable:true" style="width:100px;"  /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>自行车停车数量：</td>
                        <td>
                            <input type="text" id="txt_bikeCar" name="bikeCar" class="easyui-numberspinner" value="0" data-options="min:0,max:9999,editable:true" style="width:100px;"  /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>截止当日预定人数：</td>
                        <td>
                            <input type="text" id="txt_bookNum" name="bookNum" class="easyui-numberspinner" value="0" data-options="min:0,max:9999,editable:true" style="width:100px;"  /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>预定最迟大概入住时间：</td>
                        <td>
                            <input type="text" id="txt_inLashTime" name="inLashTime" class="easyui-datetimebox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>截止当日空房数：</td>
                        <td>
                            <input type="text" id="txt_freeRoom" name="freeRoom" class="easyui-numberspinner" value="0" data-options="min:0,max:9999,editable:true" style="width:100px;" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>截止当日空房明细：</td>
                        <td>
                            <input type="text" id="txt_freeRoomInfo" name="freeRoomInfo" class="easyui-textbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>截止当日空房原因：</td>
                        <td>
                            <input type="text" id="txt_freeRoomWhat" name="freeRoomWhat" class="easyui-textbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>当日退房数：</td>
                        <td>
                            <input type="text" id="txt_quitRoomNum" name="quitRoomNum" class="easyui-numberspinner" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>当日退房明细：</td>
                        <td>
                            <input type="text" id="txt_quitRoomInfo" name="quitRoomInfo" class="easyui-textbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>是否安排入住：</td>
                        <td>
                            <input type="text" id="txt_isIn" name="isIn" class="easyui-textbox" /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>签月度合同房间数：</td>
                        <td>
                            <input type="text" id="txt_monthNum" name="monthNum" class="easyui-numberspinner" value="0" data-options="min:0,max:9999,editable:true" style="width:100px;"  /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>签季度合同房间数：</td>
                        <td>
                            <input type="text" id="txt_quarterNum" name="quarterNum" class="easyui-numberspinner" value="0" data-options="min:0,max:9999,editable:true" style="width:100px;"  /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>签半年合同房间数：</td>
                        <td>
                            <input type="text" id="txt_halfYearNum" name="halfYearNum" class="easyui-numberspinner" value="0" data-options="min:0,max:9999,editable:true" style="width:100px;"  /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>签年度合同房间数：</td>
                        <td>
                            <input type="text" id="txt_yearNum" name="yearNum" class="easyui-numberspinner" value="0" data-options="min:0,max:9999,editable:true" style="width:100px;"  /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>在住房间合计数：</td>
                        <td>
                            <input type="text" id="txt_allNum" name="allNum" class="easyui-numberspinner" value="0" data-options="min:0,max:9999,editable:true" style="width:100px;"  /></td>
                    </tr>
                    <tr style="display: ;">
                        <td>逾期交房租明细：</td>
                        <td>
                            <input type="text" id="txt_overdueInfo" name="overdueInfo" class="easyui-textbox" />
                            <div id="yuqiinfo">121212</div>
                        </td>
                    </tr>
                    <tr style="display: ;">
                        <td>备注：</td>
                        <td>
                            <input type="text" id="txt_note" name="note" class="easyui-textbox" /></td>
                    </tr>
                    <tr style="display:none;">
                        <td>添加时间：</td>
                        <td>
                            <input type="text" id="txt_add_time" name="add_time" class="easyui-datetimebox" /></td>
                    </tr>
                    <tr style="display:none;">
                        <td>更新时间：</td>
                        <td>
                            <input type="text" id="txt_up_time" name="up_time" class="easyui-datetimebox" /></td>
                    </tr>
                    <tr style="display:none;">
                        <td>数据所属部门：</td>
                        <td>
                            <input type="text" id="txt_depid" name="depid" class="easyui-numberspinner" /></td>
                    </tr>
                    <tr style="display:none;">
                        <td>数据所有者：</td>
                        <td>
                            <input type="text" id="txt_ownner" name="ownner" class="easyui-numberspinner" /></td>
                    </tr>
                </table>
                 <a id="a_add" href="#" plain="true" class="easyui-linkbutton" icon="icon-add" title="添加">添加</a>
            </form>
        </div>
    </div>
    <!--Uploader-->
    <!--上传组件皮肤已经在公共样式里-->
    <script src="../../scripts/webuploader/webuploader.min.js"></script>

    <!-- 引入多功能查询js -->
    <script src="../../scripts/Business/Search.js"></script>
    <!--导出Excel-->
    <script src="../../scripts/Export.js"></script>

    <!--导入Excel-->
    <script src="../../scripts/Inport.js"></script>
    <script src="../scripts/jsoneditor/dist/jsoneditor.js"></script>
    <script src="js/MJDayRptAdd.js"></script>
    <!-- 引入js文件 -->
                <script>
                    //加载json编辑器
                    // Initialize the editor with a JSON schema
                    var editor = new JSONEditor(document.getElementById("yuqiinfo"), {
                        //theme: 'bootstrap3',
                        theme: 'html',
                        schema: {//编辑器所需要的JSON Schema . 目前支持 版本 3 和版本 4
                            "type": "array",
                            "format": "table",
                            "title": "测试",
                            "uniqueItems": true,
                            "items": {
                                "type": "object",
                                "title": "明细",
                                "properties": {
                                    "房号": {
                                        "type": "string"
                                    },
                                    "姓名": {
                                        "type": "string"
                                    },
                                    "电话": {
                                        "type": "string"
                                    },
                                    "逾期天数": {
                                        "type": "string"
                                    },
                                    "店长处理意见": {
                                        "type": "string"
                                    }
                                }
                            },


                        }

                    });

                //加载json编辑器
            </script>

</asp:Content>
