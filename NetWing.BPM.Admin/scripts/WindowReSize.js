﻿$(function () {
    //alert("执行了吗");
    var size = { width: $(window).width(), height: $(window).height() };
    //$("p").height(50);
    //alert("size" + size);
    mylayout.init(size);
    $(window).resize(function () {
        size = { width: $(window).width(), height: $(window).height() };
        mylayout.resize(size);
       // alert("执行了吗a");

    });

});


var mylayout = {
    init: function (size) {
        $('#layout').width(size.width - 4).height(size.height - 4).layout();
        var center = $('#layout').layout('panel', 'center');

        center.panel({
            onResize: function (w, h) {
                $('#depGrid').datagrid('resize', { width: w, height: h });

                //alert(w);
            }
        });
    },
    resize: function (size) {
        mylayout.init(size);

        $('#layout').layout('resize');
    }
}

//时间格式化
//EasyUI用DataGrid用日期格式化
function formatterdate(val, row) {
    var date = new Date(val);
    return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
}
//时间格式化


//jquery格式化时间
//为date类添加一个format方法
//yyyy 年
//MM 月
//dd 日
//hh 小时
//mm 分
//ss 秒
//qq 季度
//S  毫秒
/**
* 时间对象的格式化
*/
Date.prototype.format = function (format) {
    /*
    * format="yyyy-MM-dd hh:mm:ss";
    */
    var o = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S": this.getMilliseconds()
    }

    if (/(y+)/.test(format)) {
        format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4
- RegExp.$1.length));
    }

    for (var k in o) {
        if (new RegExp("(" + k + ")").test(format)) {
            format = format.replace(RegExp.$1, RegExp.$1.length == 1
? o[k]
: ("00" + o[k]).substr(("" + o[k]).length));
        }
    }
    return format;
}
//jquery格式化时间