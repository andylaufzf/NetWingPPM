using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
    [TableName("MJComplaint")]
    [Description("")]
    public class MJComplaintModel
    {
        /// <summary>
        /// ID
        /// </summary>
        [Description("ID")]
        public int KeyId { get; set; }
        /// <summary>
        /// 用户名
        /// </summary>
        [Description("用户名")]
        public string username { get; set; }


        /// <summary>
        /// 用户ID
        /// </summary>
        [Description("用户ID")]
        public int userid { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [Description("手机号")]
        public string mobile { get; set; }
        /// <summary>
        /// 房间号
        /// </summary>
        [Description("房间ID")]
        public int room_id { get; set; }


        [Description("房间号")]
        public string roomno{get;set;}

        /// <summary>
        /// 被投诉部门
        /// </summary>
        [Description("被投诉部门")]
		public string ts_dep { get; set; }		
		/// <summary>
		/// 被投诉部门ID
		/// </summary>
		[Description("被投诉部门ID")]
		public int ts_depid { get; set; }		
		/// <summary>
		/// 投诉内容
		/// </summary>
		[Description("投诉内容")]
		public string ts_context { get; set; }		
		/// <summary>
		/// 处理状态
		/// </summary>
		[Description("处理状态")]
		public string cl_state { get; set; }		
		/// <summary>
		/// 操作员
		/// </summary>
		[Description("操作员")]
		public string cz_user { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string remarks { get; set; }		
		/// <summary>
		/// 系统时间
		/// </summary>
		[Description("系统时间")]
		public DateTime add_time { get; set; }

        [Description("数据所有者ID")]
        public int ownner { get; set; }
        [Description("数据所有部门ID")]
        public int depid { get; set; }
        [Description("店长处理意见")]
        public string dzclyj { get; set; }
        [Description("处理结果")]
        public string cljg { get; set; }
        public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}