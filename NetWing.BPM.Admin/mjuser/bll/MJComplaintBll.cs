using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class MJComplaintBll
    {
        public static MJComplaintBll Instance
        {
            get { return SingletonProvider<MJComplaintBll>.Instance; }
        }

        public int Add(MJComplaintModel model)
        {
            return MJComplaintDal.Instance.Insert(model);
        }

        public int Update(MJComplaintModel model)
        {
            return MJComplaintDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return MJComplaintDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return MJComplaintDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
