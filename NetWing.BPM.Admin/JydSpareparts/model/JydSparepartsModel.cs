using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using NetWing.Common;
using NetWing.Common.Data;

namespace NetWing.Model
{
	[TableName("jydSpareparts")]
	[Description("备料单")]
	public class JydSparepartsModel
	{
				/// <summary>
		/// 备料单id
		/// </summary>
		[Description("备料单id")]
		public int KeyId { get; set; }		
		/// <summary>
		/// 订单id
		/// </summary>
		[Description("订单id")]
		public int orderid { get; set; }		
		/// <summary>
		/// 开单人员id（备料单）
		/// </summary>
		[Description("开单人员id（备料单）")]
		public int kdry_id { get; set; }		
		/// <summary>
		/// 开单人员名字
		/// </summary>
		[Description("开单人员名字")]
		public string kdry_name { get; set; }		
		/// <summary>
		/// 品名
		/// </summary>
		[Description("品名")]
		public string pinming { get; set; }		
		/// <summary>
		/// 纸张/板材
		/// </summary>
		[Description("纸张/板材")]
		public string cailiao { get; set; }		
		/// <summary>
		/// 规格
		/// </summary>
		[Description("规格")]
		public string guige { get; set; }		
		/// <summary>
		/// 数量
		/// </summary>
		[Description("数量")]
		public string shuliang { get; set; }		
		/// <summary>
		/// 开料规格
		/// </summary>
		[Description("开料规格")]
		public string klguige { get; set; }		
		/// <summary>
		/// 工艺要求
		/// </summary>
		[Description("工艺要求")]
		public string gyyaoqiou { get; set; }		
		/// <summary>
		/// 备注
		/// </summary>
		[Description("备注")]
		public string beizhu { get; set; }		
		/// <summary>
		/// 备料人id
		/// </summary>
		[Description("备料人id")]
		public int beiliaor_id { get; set; }		
		/// <summary>
		/// 备料人姓名
		/// </summary>
		[Description("备料人姓名")]
		public string beiliaor_name { get; set; }		
		/// <summary>
		/// 备料状态
		/// </summary>
		[Description("备料状态")]
		public string beiliao_type { get; set; }		
		/// <summary>
		/// 客户名称
		/// </summary>
		[Description("客户名称")]
		public string kehu_name { get; set; }		
		/// <summary>
		/// 订单编号
		/// </summary>
		[Description("订单编号")]
		public string orderbianhao { get; set; }		
		/// <summary>
		/// 备料完成时间
		/// </summary>
		[Description("备料完成时间")]
		public DateTime beiliao_time { get; set; }		
		/// <summary>
		/// 备料发布时间
		/// </summary>
		[Description("备料发布时间")]
		public DateTime add_time { get; set; }		
				
		public override string ToString()
		{
			return JSONhelper.ToJson(this);
		}
	}
}