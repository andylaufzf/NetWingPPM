using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Dal;
using NetWing.Model;
using NetWing.Common.Provider;

namespace NetWing.Bll
{
    public class JydSparepartsBll
    {
        public static JydSparepartsBll Instance
        {
            get { return SingletonProvider<JydSparepartsBll>.Instance; }
        }

        public int Add(JydSparepartsModel model)
        {
            return JydSparepartsDal.Instance.Insert(model);
        }

        public int Update(JydSparepartsModel model)
        {
            return JydSparepartsDal.Instance.Update(model);
        }

        public int Delete(int keyid)
        {
            return JydSparepartsDal.Instance.Delete(keyid);
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "Keyid", string order = "asc")
        {
            return JydSparepartsDal.Instance.GetJson(pageindex, pagesize, filterJson, sort, order);
        }
    }
}
