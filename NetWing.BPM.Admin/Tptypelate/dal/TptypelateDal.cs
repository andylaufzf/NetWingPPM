using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NetWing.Common.Data;
using NetWing.Common.Provider;
using NetWing.Model;

namespace NetWing.Dal
{
    public class TptypelateDal : BaseRepository<TptypelateModel>
    {
        public static TptypelateDal Instance
        {
            get { return SingletonProvider<TptypelateDal>.Instance; }
        }

        public string GetJson(int pageindex, int pagesize, string filterJson, string sort = "keyid",
                              string order = "asc")
        {
            return base.JsonDataForEasyUIdataGrid(TableConvention.Resolve(typeof(TptypelateModel)), pageindex, pagesize, filterJson,sort, order);
        }
    }
}