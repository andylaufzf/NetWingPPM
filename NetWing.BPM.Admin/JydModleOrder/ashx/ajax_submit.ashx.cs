﻿using NetWing.Bll;
using NetWing.BPM.Core;
using NetWing.BPM.Core.Bll;
using NetWing.BPM.Core.Dal;
using NetWing.BPM.Core.Model;
using NetWing.Common;
using NetWing.Common.Data;
using NetWing.Common.Data.SqlServer;
using NetWing.Common.SMS;
using NetWing.Model;
using Senparc.Weixin.MP.AdvancedAPIs.TemplateMessage;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
namespace NetWing.BPM.Admin.JydModleOrder.ashx
{
    /// <summary>
    /// ajax_submit 的摘要说明
    /// </summary>
    public class ajax_submit : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string action = context.Request["action"];
            if (string.IsNullOrEmpty(action))
            {
                context.Response.Write("{\"code\":1000, \"msg\":\"请求失败,参数有误!\"}");
                return;
            }
            switch (action)
            {
                case "GetMyJobList"://获取工作单列表
                    GetMyJobList(context);
                    break;
                case "caozuo"://催单发送信息
                    caozuo(context);
                    break;
                case "xiugai"://修改生产订单
                    xiugai(context);
                    break;
                case "wancheng"://完成订单
                    wancheng(context);
                    break;
                case "wapajaxti":
                    wapajaxti(context);
                    break;
                case "butongguo":
                    butongguo(context);
                    break;
                case "tongguo":
                    tongguo(context);
                    break;
                case "actual":
                    actual(context);
                    break;
                case "upstatus":
                    upstatus(context);
                    break;
                case "shstatus":
                    shstatus(context);
                    break;
                case "statusorder_sn":
                    statusorder_sn(context);
                    break;
                case "buyprice":
                    buyprice(context);
                    break;
                case "fenqifk":
                    fenqifk(context);
                    break;
                case "fenqijefk":
                    fenqijefk(context);
                    break;
                default:
                    context.Response.Write("{\"code\":1000, \"msg\":\"请求失败,参数有误!\"}");
                    break;
            }
        }
        /// <summary>
        /// 分期金额付款
        /// </summary>
        private void fenqijefk(HttpContext context)
        {
            string orderid = context.Request["orderid"];
            decimal je = decimal.Parse(context.Request["je"]);
            DataRow data = SqlEasy.ExecuteDataRow("select * from jydOrderDetail where orderid='"+orderid+"'");
            SqlTransaction tran = SqlHelper.BeginTransaction(SqlEasy.connString);//取得一个事务
            JydorderfenqifkModel jydorderfenqifk = new JydorderfenqifkModel
            {
                comname = DateTime.Now.ToString(),
                orderid = data["orderid"].ToString(),
                yspmc = data["yspmc"].ToString(),
                sl = int.Parse(data["sl"].ToString()),
                dj = decimal.Parse(data["dj"].ToString()),
                je = je,
                jexx = je
            };
            DbUtils.tranInsert(jydorderfenqifk, tran);//提交流程操作记录
            tran.Commit();//提交事务
            decimal q = Convert.ToDecimal(SqlEasy.ExecuteScalar("select SUM(je) from jydorderfenqifk  where orderid='"+orderid+"'"));
            int i = SqlEasy.ExecuteNonQuery("update jydorderfenqifk set yufuk='"+q+"'+'"+je+"' where orderid='"+orderid+"'");
            SqlEasy.ExecuteNonQuery("update jydorder set yufuk='"+q+"'+'"+je+"' where orderid='"+orderid+"'");
            context.Response.Write("{\"status\":\"1\",\"code\":\"提交成功\"}");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        #region 列表分页==================================================
        private void fenqifk(HttpContext context)
        {
            string orderid = context.Request["orderid"];
            int pageindex = string.IsNullOrEmpty(context.Request["pageindex"]) ? 1 : int.Parse(context.Request["pageindex"]);//当前第几页
            int pagesize = string.IsNullOrEmpty(context.Request["pagesize"]) ? 9 : int.Parse(context.Request["pagesize"]);//每页记录条数 
            string strWhere = "1=1 and orderid='"+orderid+"'";
            int recordCount;
            var pcp = new ProcCustomPage("jydorderfenqifk")
            {
                PageIndex = pageindex,
                PageSize = pagesize,
                OrderFields = "KeyId desc",
                WhereString = strWhere
            };
            DataTable dt = DbUtils.GetPageWithSp(pcp, out recordCount);
            if (dt.Rows.Count > 0)
            {
                context.Response.Write("{\"code\":0,\"msg\":\"获取成功!\",\"data\":" + JSONhelper.ToJson(dt, false) + ",\"datanum\":" + recordCount + "}");
            }
            else
            {
                context.Response.Write("{\"code\":1000,\"msg\":\"暂无数据!\",\"data\":[]}");
            }
        }
        
        #endregion


        /// <summary>
        /// 修改后加工汇总付款方式
        /// </summary>
        /// <param name="context"></param>
        private void buyprice(HttpContext context)
        {
            var keyid = context.Request["keyid"];
            var yspmc = context.Request["yspmc"];
            try
            {
                int i = SqlEasy.ExecuteNonQuery("update jydorderhoujiagong set status_fk='已付' where yspmc='"+yspmc+"'");
                if(i >= 1)
                {
                    context.Response.Write("{\"status\":1,\"msg\":\"已付款\"}");
                }
                else
                {
                    context.Response.Write("{\"status\":2,\"msg\":订单已付款\"}");
                }
            }catch(Exception e)
            {
                context.Response.Write("{\"status\":2,\"msg\":操作失败请联系管理员\"}");
                WriteLogs.WriteLogsE("Logs", "Error >> caozuo", e.Message + " >>> " + e.StackTrace);
            }
        }

        
        /// <summary>
        /// 审核
        /// </summary>
        /// <param name="context"></param>
        public void statusorder_sn(HttpContext context)
        {
            var order = context.Request["order"];
            var user = SysVisitor.Instance.cookiesUserName;
            DataTable usere = SqlEasy.ExecuteDataTable("select * from Sys_Users where UserName='" + user + "'");
            if (!bool.Parse(usere.Rows[0]["IsAdmin"].ToString()))
            {
                context.Response.Write("{\"status\":3,\"msg\":\"抱歉,你没有权限审核\"}");
                return;
            }
            DataTable dataTable = SqlEasy.ExecuteDataTable("select * from spareparts where order_sn='" + order + "'");
            //var data = int.Parse(dataTable);
            SqlEasy.ExecuteNonQuery("update sparepartszhu set manager='1' where order_sn='" + order + "'");
            foreach (DataRow dr in dataTable.Rows)
            {
                //为模版中的各属性赋值
                var templateData = new ProductTemplateData()
                {
                    first = new TemplateDataItem("礼品盒<" + dr["productname"].ToString() + ">采购备料生产：", "#000000"),
                    keyword1 = new TemplateDataItem("采购备料", "#000000"),
                    keyword2 = new TemplateDataItem(dr["order_sn"].ToString(), "#000000"),
                    keyword3 = new TemplateDataItem("品名: " + dr["productname"].ToString() + ", 材料: " + dr["material"].ToString() + ", 规格: " + dr["specifications"].ToString() + " ,数量: " + dr["number"].ToString() + ", 开料: " + dr["materialopening"].ToString() + ", 工艺: " + dr["technology"].ToString() + ", 备注: " + dr["note"].ToString(), "#000000"),
                    keyword4 = new TemplateDataItem(DateTime.Now.ToString(), "#000000"),
                    keyword5 = new TemplateDataItem(dr["billingclerk"].ToString(), "#000000"),
                    remark = new TemplateDataItem("请尽快到办公室拿取单子进行采购备料！", "#000000")
                };
                string templateid = NetWing.Common.ConfigHelper.GetValue("templateid3");//从web.config 获得模板ID
                //通知所有员工就是openid不同
                string r = NetWing.BPM.Admin.weixin.wxhelper.sendTemplateMsg(dr["wechareopen"].ToString(), templateid, "", templateData);
                context.Response.Write("{\"status\":0,\"msg\":\"审核成功!\"}");
            }




        }


        //审核库存盘点
        public void shstatus(HttpContext context)
        {
            string keyid = context.Request["keyid"];
            string stock = context.Request["stock"];
            string goods_id = context.Request["goods_id"];
            int shstatus = SqlEasy.ExecuteNonQuery("update psi_inventory set toexamine_name='" + SysVisitor.Instance.cookiesUserName + "',toexamine_id='" + int.Parse(SysVisitor.Instance.cookiesUserId) + "',toexamine_time='" + DateTime.Now + "' where Keyid='" + keyid + "'");
            if (shstatus > 0)
            {
                SqlEasy.ExecuteNonQuery("update Psi_Goods set stock='" + stock + "' where keyid='" + goods_id + "'");
                context.Response.Write("{\"status\":0, \"msg\":\"审核成功\"}");
            }
            else
            {
                context.Response.Write("{\"status\":3000, \"msg\":\"审核失败\"}");
            }
        }




        //修改后加工
        public void upstatus(HttpContext context)
        {
            try
            {
                string keyid = context.Request["keyid"];
                int upstatu = SqlEasy.ExecuteNonQuery("update jydOrderDetail set status_fk='已付' where KeyId='" + keyid + "'");
                if (upstatu > 0)
                {
                    LogModel logxx = new LogModel();
                    logxx.BusinessName = SysVisitor.Instance.cookiesUserId + "修改后加工付款";
                    logxx.OperationIp = context.Request.ServerVariables["REMOTE_ADDR"];
                    logxx.OperationTime = DateTime.Now;
                    logxx.PrimaryKey = "";
                    logxx.UserId = int.Parse(SysVisitor.Instance.cookiesUserId);
                    logxx.SqlText = upstatu.ToString();
                    logxx.TableName = "jydOrder";
                    logxx.note = upstatu.ToString();
                    logxx.OperationType = (int)OperationType.Update;
                    LogDal.Instance.Insert(logxx);


                    context.Response.Write("{\"status\":0,\"msg\":\"此单已付款!\"}");
                }
                else
                {
                    context.Response.Write("{\"status\":1000,\"msg\":\"操作失败请与管理员联系!\"}");
                }
            }
            catch (Exception e)
            {
                WriteLogs.WriteLogsE("Logs", "Error >> caozuo", e.Message + " >>> " + e.StackTrace);
                context.Response.Write("{\"code\":3000,\"msg\":\"操作失败请与管理员联系!\"}");
            }
        }




        /// <summary>
        /// 实收数量
        /// </summary>
        /// <param name="context"></param>
        private void actual(HttpContext context)
        {


            try
            {


                string keyid = context.Request["KeyId"];
                string shuli = context.Request["actualquantity"];
                string open = context.Request["open"];



                DataTable dataTable = SqlEasy.ExecuteDataTable("select * from  Sys_Users where OpenID='" + open + "'");
                if (dataTable == null)
                {
                    context.Response.Write("{\"code\":30,\"msg\":\"抱歉，只有工厂人员可以操作!\"}");
                    return;
                }
                string username = dataTable.Rows[0]["username"].ToString();
                string dateTime = DateTime.Now.ToString();
                int shulsj = SqlEasy.ExecuteNonQuery("update jydorderhoujiagong set actual_quantity=actual_quantity+'," + shuli + "',receiving_time='" + dateTime + "',mysh=mysh+'," + username + "' where yspmc='" + keyid + "'");

                context.Response.Write("{\"code\":0,\"msg\":\"操作成功!\"}");

            }
            catch (Exception e)
            {
                WriteLogs.WriteLogsE("Logs", "Error >> caozuo", e.Message + " >>> " + e.StackTrace);
                context.Response.Write("{\"code\":3000,\"msg\":\"操作失败请与管理员联系!\"}");
            }

        }













        #region------------催单--------------
        private void caozuo(HttpContext context)
        {
            try
            {
                string OrderID = context.Request["OrderID"];
                int cuid = SqlEasy.ExecuteNonQuery("update jydOrder set FlowStatus=1 where OrderID='" + OrderID + "'");
                if (cuid > 0)
                {
                    //客户催单通知 所有人
                    //获得主订单ID
                    DataRow dr = SqlEasy.ExecuteDataRow("select * from jydorder where OrderID='" + OrderID + "'");

                    string zt = "已接件";
                    switch (dr["mystatus"].ToString())
                    {
                        case "1":
                            zt = "已走上流程";
                            break;
                        case "2":
                            zt = "正在送货";
                            break;
                        case "3":
                            zt = "已作废";
                            break;
                        default:
                            zt = "已接件";
                            break;
                    }




                    //为模版中的各属性赋值
                    var templateData = new ProductTemplateData()
                    {
                        first = new TemplateDataItem(SysVisitor.Instance.cookiesUserName + "您好!" + dr["comname"].ToString() + "催单提醒!!!", "#000000"),
                        keyword1 = new TemplateDataItem(dr["allyspmc"].ToString(), "#000000"),
                        keyword2 = new TemplateDataItem(zt, "#000000"),
                        keyword3 = new TemplateDataItem(DateTime.Now.ToString(), "#000000"),
                        remark = new TemplateDataItem("备注", "#000000")
                    };

                    string templateid = NetWing.Common.ConfigHelper.GetValue("templageid");//从web.config 获得模板ID
                    //通知所有员工就是openid不同
                    DataTable dt = SqlEasy.ExecuteDataTable("select * from Sys_Users where openid<>'' and DepartmentId=186");
                    foreach (DataRow ndr in dt.Rows)
                    {
                        string openid = ndr["openid"].ToString();
                        string r = NetWing.BPM.Admin.weixin.wxhelper.sendTemplateMsg(openid, templateid, NetWing.Common.ConfigHelper.GetValue("website") + "/JydModleOrder/taskQRcode.aspx?keyid=" + dr["keyid"].ToString() + "", templateData);
                    }




                    context.Response.Write("{\"code\":0,\"msg\":\"修改成功!\"}");

                }
                else
                {
                    context.Response.Write("{\"code\":1000,\"msg\":\"修改失败\"}");
                }
            }
            catch (Exception e)
            {
                WriteLogs.WriteLogsE("Logs", "Error >> caozuo", e.Message + " >>> " + e.StackTrace);
                context.Response.Write("{\"code\":3000,\"msg\":\"操作失败请与管理员联系!\"}");
                //发错误通知给管理员
                string tels = NetWing.Common.ConfigHelper.GetValue("syserr");
                string templateno = "SMS_5009300";
                string smsparam = "{\"code\":\"" + e.Message.Replace('.', '*') + "\",\"product\":\"聚源达\"}";
                //验证码${ code}，您正在尝试变更${ product}重要信息，请妥善保管账户信息。
                aliyunsms alisms = new aliyunsms();
                string res = alisms.send(tels, templateno, smsparam);
                WriteLogs.WriteLogsE("Logs", "Error >>用户催单发短信", res + " >>> " + DateTime.Now.ToString());



            }
        }
        #endregion

        #region------------查询订单--------------
        public void GetMyJobList(HttpContext context)
        {
            try
            {
                //int pageindex = int.Parse(context.Request["pageindex"]);//当前第几页
                //int pagesize = int.Parse(context.Request["pagesize"]);//每页记录条数 
                string search = context.Request["search"];//搜索条件
                string mystatus = context.Request["mystatus"];//工作单状态
                string strWSQL = "1=1";//模糊查询
                if (!string.IsNullOrEmpty(search))
                {   //模糊查询条件
                    strWSQL = " OrderID like '%" + search + "%' or tel like '%" + search + "%' ";
                }
                if (!string.IsNullOrEmpty(mystatus))
                {   //订单状态
                    mystatus = " mystatus=" + mystatus + " and ";

                }
                //if (!string.IsNullOrEmpty(search))
                //{
                //    search = " or OrderID like search=" + search + " ";
                //}
                //string sqls = "m jyselect count(KeyId) frodOrder where " + mystatus + " (" + strWSQL + ")";
                //int allsize = (int)SqlEasy.ExecuteScalar(sqls);//总条数
                //int allpagesiza = 0;
                //decimal de = 0M;
                //if (allsize != 0)
                //{
                //    de = Convert.ToDecimal(allsize) / Convert.ToDecimal(pagesize);
                //    allpagesiza = (int)Math.Ceiling(de);
                //}
                //int pagenum = (pageindex - 1) * pagesize;
                //数据库中可以直接查询出来,难道在这里遇见爱情了?
                DataTable dt = SqlEasy.ExecuteDataTable("select * from (select row_number() over(order by KeyId desc) as rownumber,* from jydOrder where mystatus is not null and (" + strWSQL + " or OrderID like " + search + "))  A where rownumber = rownumber");

                //DataTable dt = SqlEasy.ExecuteDataTable("select * from(select row_number() over(order by KeyId desc) as rownumber,* from jydOrder where " + mystatus + " and (" + strWSQL + ")) A where rownumber > " + pagenum + "");
                if (dt.Rows.Count > 0)
                {
                    context.Response.Write("{\"code\":0,\"msg\":\"获取成功!\",\"data\":" + JSONhelper.ToJson(dt, false) + ",\"datanum\":" + dt.Rows.Count + "}");
                }
                else
                {
                    context.Response.Write("{\"code\":1000,\"msg\":\"暂无数据!\"}");
                }
            }
            catch (Exception e)
            {
                WriteLogs.WriteLogsE("Logs", "Error >> GetMyJobList", e.Message + " >>> " + e.StackTrace);
                context.Response.Write("{\"code\":4000,\"msg\":\"系统错误!\"}");
            }
        }
        #endregion

        /// <summary>
        /// 修改生产状态
        /// </summary>
        #region 修改生产状态===============================================
        public void xiugai(HttpContext context)
        {
            try
            {
                string KeyI = context.Request["KeyId"];

                int Keyid = SqlEasy.ExecuteNonQuery("update jydOrder set mystatus = 1 where KeyId=" + KeyI + "");

                if (Keyid > 0)
                {
                    context.Response.Write("{\"status\":0,\"msg\":\"修改成功！\"}");
                }
                else
                {
                    context.Response.Write("{\"status\":15,\"msg\":\"修改失败!\"}");
                }

            }
            catch (Exception e)
            {
                WriteLogs.WriteLogsE("Logs", "Error >> xiugai", e.Message + " >>> " + e.StackTrace);
            }
        }
        #endregion

        /// <summary>
        /// 完成订单
        /// </summary>
        /// <param name="context"></param>
        #region  完成订单=============================================
        private void wancheng(HttpContext context)
        {
            string kid = context.Request["KeyId"];
            int keyID = SqlEasy.ExecuteNonQuery("update jydOrder SET mystatus=2 where KeyId=" + kid + " ");
            if (keyID > 0)
            {
                context.Response.Write("{\"keyid\":1,\"msg\":\"加工已结束,准备送货!\"}");
                context.Response.End();
            }
            else
            {
                context.Response.Write("{\"keyid\":0,\"msg\":\"修改失败!\"}");
                context.Response.End();
            }

        }
        #endregion

        #region  手机提交订单==========================================
        private void wapajaxti(HttpContext context)
        {
            try
            {
                //判断库里是否有这个手机号码
                string mytel = context.Request["tel"];
                string sfyt = "select count(*) from jydOrder where tel='" + mytel + "'";
                int dhtel = Convert.ToInt32(SqlEasy.ExecuteScalar(sfyt));

                if (dhtel > 1)
                {
                    //我们接收页面传参
                    string OrderID = context.Request["OrderID"];
                    string comname = context.Request["comname"];
                    string connman = context.Request["connman"];
                    string tel = context.Request["tel"];
                    string yspmc = context.Request["yspmc"];
                    string allyspmc = context.Request["allyspmc"];
                    string PrintType = context.Request["PrintType"];
                    string danwei_m = context.Request["danwei_m"];
                    string cpcc = context.Request["cpcc"];
                    string zzf = context.Request["zzf"];
                    string ym = context.Request["ym"];
                    string sj = context.Request["sj"];
                    string zidaihz = context.Request["zidaihz"];
                    string sl = context.Request["sl"];
                    string huofa = context.Request["huofa"];
                    string ys = context.Request["ys"];
                    string fy = context.Request["fy"];
                    string dha = context.Request["dha"];
                    string dhb = context.Request["dhb"];
                    string explain = context.Request["explain"];
                    string adddate = context.Request["adddate"];

                    //参数方法保存到数据库
                    StringBuilder sql = new StringBuilder();
                    int icode = (int)SqlEasy.ExecuteScalar("select count(KeyId) from jydOrder where OrderID='" + OrderID + "' ");
                    if (icode > 0)
                    {
                        context.Response.Write("{\"code\":1000,\"msg\":\"该订单已存在!\"}");
                        return;
                    }
                    //主表中的实体类
                    JydOrderBll bll = new JydOrderBll();
                    JydOrderModel model = new JydOrderModel();
                    //订单明细
                    JydOrderDetailBll bll1 = new JydOrderDetailBll();
                    JydOrderDetailModel model1 = new JydOrderDetailModel();
                    //返回订单号
                    string edNumbe = common.mjcommon.getedNumberJyd();
                    model.OrderID = edNumbe;

                    string[] arr = edNumbe.Split('-');

                    model.OrderInt = int.Parse(arr[3]);







                    //model1.omainid = int.Parse(Keyid);
                    model.comname = comname;
                    model.connman = connman;
                    model.tel = tel;
                    model1.yspmc = yspmc;
                    model.allyspmc = allyspmc;
                    model1.zidaihz = zidaihz;
                    model1.PrintType = PrintType;
                    model1.danwei_m = danwei_m;
                    model1.cpcc = cpcc;
                    model1.zzf = zzf;
                    model1.ym = ym;
                    model1.sj = sj;
                    model.huofa = huofa;
                    model.ys = ys;
                    model.fy = fy;
                    model.dha = dha;
                    model.dhb = dhb;
                    model.explain = explain;
                    model.OrderYear = DateTime.Now.ToString("yyyy-MM-dd");
                    if (!string.IsNullOrEmpty(adddate))
                    {
                        model.adddate = Convert.ToDateTime(adddate);
                    }
                    else
                    {
                        model.adddate = DateTime.Now;
                    }
                    //model.OrderYear = DateTime.Now.ToString("yyyy-MM-dd");

                    model.OrderID = edNumbe;
                    model1.orderid = edNumbe;




                    //添加
                    int code = bll.Add(model);
                    model1.omainid = code;
                    //SqlTransaction tran = SqlHelper.BeginTransaction(SqlEasy.connString);//取得一个事务
                    //int mainInsertedId = 0;//（主表公用）主表插入的ID
                    //mainInsertedId = DbUtils.tranInsert(model, tran);//往主表插入一条
                    //JydOrderDetailModel detailModel = new JydOrderDetailModel();//初始化明细表模型



                    //生成二维码
                    string QRCodeUrl = QRCode.Generate("http://" + HttpContext.Current.Request.Url.Host + "/JydModleOrder/QRCodeView.aspx?KeyID=" + code, @"JydModleOrder\img\gongch.png", DateTime.Now.ToString("yyyyMMdd"));
                    ////每添加修改一条数据动态改变二维码
                    //DbUtils.tranExecuteNonQuery("update JydOrder set order_img='" + QRCodeUrl + "' where KeyId=" + mainInsertedId + "", tran);
                    int mycode = bll1.Add(model1);
                    if (code > 0)
                    {
                        context.Response.Write("{\"code\":1,\"msg\":\"订单添加成功!,请等待管理员通知\"}");
                    }
                    else
                    {
                        context.Response.Write("{\"code\":1000,\"err\":\"主表添加失败,请重试!\"}");
                    }

                    //if (mycode > 0)
                    //{
                    //    context.Response.Write("{\"mycode\":1,\"msg\":\"明细表添加成功!\"}");
                    //}
                    //else
                    //{
                    //    context.Response.Write("{\"mycode\":1000,\"err\":\"明细添加失败,请重试!!\"}");
                    //}
                }
            }
            catch (Exception e)
            {
                WriteLogs.WriteLogsE("Logs", "Error >> addajax", e.Message + " >>> " + e.StackTrace);
            }
        }

        #endregion


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// 定义模版中的字段属性（需与微信模版中的一致）
        /// </summary>
        public class ProductTemplateData
        {
            public TemplateDataItem first { get; set; }
            public TemplateDataItem keyword1 { get; set; }
            public TemplateDataItem keyword2 { get; set; }
            public TemplateDataItem keyword3 { get; set; }
            public TemplateDataItem keyword4 { get; set; }
            public TemplateDataItem keyword5 { get; set; }
            public TemplateDataItem keyword6 { get; set; }
            public TemplateDataItem keyword7 { get; set; }
            public TemplateDataItem keyword8 { get; set; }
            public TemplateDataItem keyword9 { get; set; }
            public TemplateDataItem keyword10 { get; set; }
            public TemplateDataItem remark { get; set; }
        }


        public void tongguo(HttpContext context)
        {
            string kid = context.Request["KeyId"];
            int keyID = SqlEasy.ExecuteNonQuery("update Psi_BuyDetails SET examine_status=2 where KeyId=" + kid + " ");
            if (keyID > 0)
            {
                context.Response.Write("{\"keyid\":0,\"msg\":\"审核通过!\"}");

            }
            else
            {
                context.Response.Write("{\"keyid\":1,\"msg\":\"审核通过失败!\"}");

            }
        }





        public void butongguo(HttpContext context)
        {
            string kid = context.Request["KeyId"];
            int keyID = SqlEasy.ExecuteNonQuery("update Psi_BuyDetails SET examine_status=0 where KeyId=" + kid + " ");
            if (keyID > 0)
            {
                context.Response.Write("{\"keyid\":0,\"msg\":\"审核不通过!\"}");

            }
            else
            {
                context.Response.Write("{\"keyid\":1,\"msg\":\"审核不通过失败!\"}");

            }
        }




    }
}