﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="gcguanlizhe.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.gcguanlizhe" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">

    <title>加工管理者处理</title>
    <link href="dist/lib/weui.min.css" rel="stylesheet" />
    <link href="dist/css/jquery-weui.css" rel="stylesheet" />
    <link href="dist/demos/css/demos.css" rel="stylesheet" />
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <script src="../scripts/layui/layui.all.js"></script>
</head>
<body>

    <!-- 容器 -->
    <div class="weui-tab">
        <%--<div class="weui-navbar">
            <a class="weui-navbar__item weui-bar__item--on" href="#tab1">未完成
            </a>
            <a class="weui-navbar__item" href="#tab2">已完成
            </a>
        </div>--%>
        <div class="weui-tab__bd">
            <%if (data >= 1)
                {%>
            <div id="tab1" class="weui-tab__bd-item weui-tab__bd-item--active">
                <%foreach (DataRow dataRow in DataTablea.Rows)
                    {%>
                <div class="weui-form-preview">
                    <div class="weui-form-preview__hd">
                        <label class="weui-form-preview__label">节点</label>
                        <em class="weui-form-preview__value"><%=dataRow["Operationalcontent"].ToString() %></em>
                    </div>
                    <div class="weui-form-preview__bd">
                        <div class="weui-form-preview__item">
                            <label class="weui-form-preview__label">工作人员</label>
                            <span class="weui-form-preview__value"><%=dataRow["Operctionname"].ToString() %></span>
                        </div>
                        <div class="weui-form-preview__item">
                            <label class="weui-form-preview__label">工作时间</label>
                            <span class="weui-form-preview__value"><%=dataRow["Operationtime"].ToString() %></span>
                        </div>
                        <div class="weui-form-preview__item">
                            <label class="weui-form-preview__label">备注</label>
                            <span class="weui-form-preview__value"><%=dataRow["Remarks"].ToString() %></span>
                        </div>
                    </div>
                    <div class="weui-form-preview__ft">
                        <button type="submit" class="weui-form-preview__btn weui-form-preview__btn_primary" href="javascript:">
                            ------------分隔符------------
                        </button>
                    </div>
                </div>
                <%} %>
            </div>
            <%}
                else
                {%>
            <div>

                
                <div class="weui-msg">
                    <div class="weui-msg__icon-area"><i class="weui-icon-warn weui-icon_msg-primary"></i></div>
                    <div class="weui-msg__text-area">
                        <h2 class="weui-msg__title">出去了,这里只有工厂管理人员能看</h2>
                    </div>
                    <div class="weui-msg__opr-area">
                        <p class="weui-btn-area">
                            <a href="/JydModleOrder/JydWapindex.aspx" class="weui-btn weui-btn_primary">首页</a>
                        </p>
                    </div>
                    <div class="weui-msg__extra-area">
                        <div class="weui-footer">
                            <p class="weui-footer__links">
                                <a href="javascript:void(0);" class="weui-footer__link"><%=ConfigHelper.GetValue("systemName") %></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <%} %>
        </div>
    </div>

    <script src="dist/lib/jquery-2.1.4.js"></script>
    <script src="dist/lib/fastclick.js"></script>
    <script src="dist/js/jquery-weui.js"></script>
</body>
</html>
