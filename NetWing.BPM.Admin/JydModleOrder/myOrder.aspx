﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="myOrder.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.myOrder" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <title>我的订单</title>
    <link href="dist/lib/weui.min.css" rel="stylesheet" />
    <link href="dist/css/jquery-weui.css" rel="stylesheet" />
    <link href="dist/demos/css/demos.css" rel="stylesheet" />
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <script src="dist/lib/fastclick.js"></script>
    <script src="dist/js/jquery-weui.js"></script>
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <script src="../scripts/layui/layui.all.js"></script>


    <script>

        function showloading(t) {
            if (t) {//如果是true则显示loading
                console.log(t);
                loading = layer.load(1, {
                    shade: [0.1, '#fff'] //0.1透明度的白色背景
                });
            } else {//如果是false则关闭loading
                console.log("关闭loading层:" + t);
                layer.closeAll('loading');
            }
        }
        function caozuo(id) {
            console.log("获取的订单号:" + id);
            $.ajax({
                url: "/JydModleOrder/ashx/ajax_submit.ashx",
                type: "POST",
                datatype: "JSON",
                data: {
                    action: 'caozuo',
                    OrderID: id
                },
                beforeSend: function () {
                    showloading(true);
                },
                success: function (d) {
                    showloading(false);
                    console.log(JSON.stringify(d));
                    if (d.code===3000) {
                        layer.msg("系统错误!!请联系管理员."+d.msg);
                    } else {
                        layer.msg("已催促工厂印刷,请勿重复操作!");
                    }

                    
                }
            });
        }

    </script>
    <style>
        .weui-btn_primary {width:30% !important;float:right !important;margin:16px 0 0 0;
        }
        .weui-cell__bd p {width:60%;float:left;text-align:left;font-size:14px; color:#999;line-height:30px;
        }
            .weui-cell__bd p span {
            font-size:16px; color:#333;margin:0 0 0 6px;}

    </style>
</head>
<body>
    <!--搜索框-->
    <div class="weui-search-bar" id="searchBar">
        <form class="weui-search-bar__form" id="search_from">
            <div class="weui-search-bar__box" style="background: #fff; border-radius: 5px;">
                <i class="weui-icon-search"></i>
                <input type="search" class="weui-search-bar__input" id="searchInput" placeholder="输入订单号可查询订单状态" required="">
                <a href="javascript:" class="weui-icon-clear" id="searchClear"></a>
            </div>
            <label class="weui-search-bar__label" id="searchText">
                <i class="weui-icon-search"></i>
                <span>输入订单号</span>
            </label>
        </form>
        <a href="javascript:" class="weui-search-bar__cancel-btn" id="searchCancel">取消</a>
    </div>
    <div class="weui-content">
        <div class="list-main-mian infinite weui-pull-to-refresh" id="listwrap" style="height: 100%; margin-top: 1px; overflow: auto; z-index: 1">
            <!--下拉刷新-->
            <%--<div class="weui-pull-to-refresh__layer" style="padding: 5px;">
                <div class="weui-pull-to-refresh__arrow"></div>
                <div class="weui-pull-to-refresh__preloader"></div>
                <div class="down">下拉刷新</div>
                <div class="up">释放刷新</div>
                <div class="refresh">正在刷新</div>
            </div>--%>
            <div class="weui-panel weui-panel_access">
                
                 <div class="weui-panel__bd" id="project_list">
                    <!--内容展示区域-->
                </div>
               <%-- <div class="weui-msg__opr-area">
                    <p class="weui-btn-area">
                        <a href="JydWapindex.aspx" class="weui-btn weui-btn_primary">返回</a>
                    </p>
                </div>--%>
            </div>
            
          
        </div>

    </div>












    <div class="weui-msg" style="float:left;margin:-56px 0 0 0;width:100%;">
        <%--<div class="weui-cells__title">我的未完成订单</div>--%>
        <!--<div class="weui-cells__title">我的订单</div>-->
        <div class="weui-cells weui-cells_radio">
            <%--  <%if (!string.IsNullOrEmpty(de.Rows[0]["tell"].ToString()) ){%>--%>

            <% string myzt = sta; %>
            <%if (values != null)
                {%>
            <%for (int i = 0; i < values.Count; i++)
                {
                    string[] strArr = values[i].ToString().Split(',');
            %>
            <label class="weui-cell weui-check__label">
                <div class="weui-cell__bd">
                    <p>
                        印刷品名称:<span><%=strArr[0] %></span> <br>
                    订单号:<span><%=strArr[1] %></span>
                    </p>
                    <input class="weui-btn weui-btn_mini weui-btn_primary" onclick="caozuo('<%=strArr[1] %>')" type="button" value="催单" />

                </div>
            </label>
            
            <%} %>
            <% } %>
            <%-- <%if (myzt.IndexOf("3") >= 0)
            {%>--%>
            <%--<div class="weui-cells__title">已完成订单</div>
            <%foreach (DataRow dy in dw.Rows)
                {%>


            <label class="weui-cell weui-check__label">

                <div class="weui-cell__bd">
                    <p><%=dy["yspmc"].ToString() %></p>
                </div>
                <div class="weui-cell__ft">
                    <span class="weui-icon-checked"><%=dy["orderid"].ToString() %></span>
                </div>
            </label>



            <%} %>--%>
            <%--<%} %>--%>

            <%-- <%} else{%>
            <%Response.Write("<script>alert('手机绑定之后才可进入我的订单');location.href='http://szh.s3.natapp.cc/weixin/bindyg.aspx';</script>"); %>
            <%} %>--%>
        </div>
    </div>







    
    <script>
        var pageindex = 1;//页数
        var pagesize = 10;//每页条数
        var search = '';//搜索关键词
        var mystatus = 1;//工作单状态
        function Refresh() {
            pageindex = 1;
            $.ajax({
                url: '/JydModleOrder/ashx/ajax_submit.ashx',
                type: 'POST',
                dataType: 'json',
                data: {
                    action: 'GetMyJobList',
                    pageindex: pageindex,
                    pagesize: pagesize,
                    search: search,
                    mystatus: mystatus
                },
                success: function (d) {
                    $("#project_list").html('');
                    if (d.code == 0) {
                        var str = "";
                        jQuery.each(d.data, function (i, o) {

                            str += '<div class="weui-form-preview" >';
                            str += '<div class="weui-form-preview__bd">';
                            str += '<p class="weui-media-box__desc"><div class="weui-form-preview__item"><label class="weui-form-preview__label">公司名称</label><span class="weui-form-preview__value">' + o.comname + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">联系人</label><span class="weui-form-preview__value">' + o.connman + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">订单号</label><span class="weui-form-preview__value">' + o.OrderID + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">联系人电话</label><span class="weui-form-preview__value">' + o.tel + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">订单总额</label><span class="weui-form-preview__value">' + o.jexx + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">货发</label><span class="weui-form-preview__value">' + o.huofa + '</span></div>';
                            str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">运输</label><span class="weui-form-preview__value">' + o.ys + '</span></div>';
                            if (o.mystatus == 0) {
                                str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">状态</label><span class="weui-form-preview__value">未走上流程</span></div>';

                            }
                            else if (o.mystatus == 1) {
                                str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">状态</label><span class="weui-form-preview__value">正在生产中</span></div>';

                            }
                            else if (o.mystatus == 2) {
                                str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">状态</label><span class="weui-form-preview__value">已送货签收</span></div>';

                            }
                            else if (o.mystatus == 3) {
                                str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">状态</label><span class="weui-form-preview__value">全单收款</span></div>';

                            }
                            else {
                                 str += '<div class="weui-form-preview__item"><label class="weui-form-preview__label">状态</label><span class="weui-form-preview__value">(已作废)</span></div>';
                            }
                            str += '<div class="weui-form-preview__ft"><span class="weui-form-preview__btn weui-form-preview__btn_primary" onclick="caozuo(\'' + o.OrderID + '\')">催单</span></div>';
                            str += '</div>';
                            str += '</div>';
                        });
                        $("#project_list").append(str);
                        if (d.datanum < pagesize) {
                            var loadmore = '<div class="weui-loadmore weui-loadmore_line">' +
                                '<span class="weui-loadmore__tips">暂无更多数据</span>' +
                                '</div>';
                            $("#loadmore").html(loadmore);
                        } else {
                            var loadmore = '<div class="weui-loadmore weui-loadmore_line">' +
                                '<span class="weui-loadmore__tips" onclick="jzmore()">暂无更多数据</span>' +
                                '</div>';
                            $("#loadmore").html(loadmore);
                        }
                    }
                    
                    if (d.code == 1000) {
                        console.log("789");
                        var syu = "";
                        syu += '<i class="weui-icon-warn weui-icon_msg-primary" >暂无数据</i >';
                        console.log("369");
                    }
                },
                error: function (e) {
                }
            });
        }
        //监听搜索
        document.getElementById('search_from').onsubmit = function (e) {
            document.activeElement.blur();//软键盘收起
            search = $('#searchInput').val();
            Refresh();
            return false;
        }
        //搜索取消
        $('#searchCancel').click(function () {
            search = '';
        });

        function caozuo(id) {
    $.ajax({
        url: "/JydModleOrder/ashx/ajax_submit.ashx",
        type: "POST",
        datatype: "JSON",
        data: {
            action: 'caozuo',
            OrderID: id
        },
        success: function (e) {
            alert("催单成功");
        }
    });
}

    </script>




</body>
</html>
