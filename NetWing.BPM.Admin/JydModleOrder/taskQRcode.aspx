﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="taskQRcode.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.taskQRcode" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

    <title><%=dr["comname"].ToString() %>-<%=dr["allyspmc"].ToString() %></title>

    <link rel="stylesheet" href="https://cdn.bootcss.com/weui/1.1.3/style/weui.min.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/jquery-weui/1.2.1/css/jquery-weui.min.css">
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <script src="https://cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
    <script src="https://cdn.bootcss.com/jquery-weui/1.2.1/js/jquery-weui.min.js"></script>
    <link rel="stylesheet" type="text/css" href="/scripts/gooflow1.3/codebase/GooFlow.css" />
    <script type="text/javascript" src="/scripts/gooflow1.3/codebase/GooFunc.js"></script>
    <script type="text/javascript" src="/scripts/gooflow1.3/plugin/json2.js"></script>
    <script type="text/javascript" src="/scripts/gooflow1.3/codebase/GooFlow.js"></script>
    <script type="text/javascript" src="/scripts/gooflow1.3/codebase/GooFlow.color.js"></script>
    <style>
        .weui-btn_mini {
            line-height: 2.8 !important;
            font-size: 14px !important;
        }

        .weui-form-preview__label {
            color: #000;
            width: 90px;
            margin-right: 0;
            padding-right: 1em;
            text-align: right;
            text-align-last: right;
            height: 44px;
            line-height: 44px;
        }

        .weui-form-preview__item {
            border-top: solid 1px #ececec;
        }

        .weui-form-preview__value {
            text-align: left;
            padding-left: 1em;
            border-left: solid 1px #ececec;
            /*padding: 3px 3px 3px 6px;*/
            height: 44px;
            line-height: 44px;
        }

        .weui-pull-to-refresh {
            margin-top: -80px;
            transition: transform .4s;
        }

            .weui-pull-to-refresh.refreshing {
                transform: translate3d(0, 80px, 0);
            }

        .weui-pull-to-refresh__layer {
            padding: 20px 0 0 0;
            height: 60px;
            line-height: 60px;
        }

        .weui-msg {
            width: 100%;
            float: left;
            margin: -50px 0 0 0;
        }

        .weui-cells__title {
            width: 100%;
            font-size: 16px;
            color: #000;
            text-align: center;
            padding: 0 !important;
            margin: 0 !important;
            line-height: 40px;
        }

        .weui-cell__bd {
            width: 90%;
            float: left;
            padding: 3% 5%;
            border: #f4f4f4 1px solid;
        }

            .weui-cell__bd p {
                width: 100%;
                float: left;
                line-height: 30px;
            }

                .weui-cell__bd p span {
                    width: 100%;
                    float: left;
                    font-size: 14px;
                    color: #999;
                    text-align: left;
                }

                .weui-cell__bd p em {
                    width: 100%;
                    float: left;
                    font-size: 16px;
                    color: #333;
                    text-decoration: none;
                    text-align: left;
                    font-style: normal;
                }

        .btn_box {
            width: 100%;
            float: right;
            font-size: 14px;
            color: #f00;
            line-height: 30px;
            text-align: left !important;
        }

        .weui-cell {
            width: 100%;
            float: left; /*height:120px;*/
        }

        .GooFlow_work_inner {
            width: 100% !important;
            margin: 60px 0 0 -20px !important;
            height: 450px !important;
        }

        .close-popup { /*margin:0 !important;*/ /*padding:0 !important;*/ /*border-radius:0 !important;*/
        }

        .weui-article {
            background: #fff !important;
        }

        .flowcontent {
            text-align: left !important;
        }

        .weui-cell {
            padding: 10px 0 10px 10px !important;
            width: 96% !important;
        }

        .weui-cell__bd {
            border: none !important;
            padding: 0 !important;
        }

            .weui-cell__bd input {
                height: 45px !important;
            }

        .weui-select {
            padding-left: 0 !important;
        }

        .weui-popup__modal {
            width: 96% !important;
            padding: 0 2% !important;
            margin: 0px 0 0 0 !important;
            text-align: left;
            font-size: 12px;
            line-height: 24px;
        }
    </style>
    <script>
        //查看任务
        function openflow(keyid) {
            $.showLoading();
            $.ajax({
                url: "",
                type: "GET",
                dataType: "JSON",
                success: function () {

                }
            })
            console.log("keyid:" + keyid);
            $("#full" + keyid).popup();
            console.log("123");
            $.hideLoading();
        }

        function doflow(keyid) {//手机流程处理
            $.showLoading();
            $("#doflow" + keyid).find(".VerificationOpinion").val("");//打开时设置为空
            $("#doflow" + keyid).find(".FlowInstanceId").val("");//打开事设置为空
            $("#doflow" + keyid).find(".flowcontent").html("");
            $.ajax({
                url: '/Jydflowtasklist/ashx/JydflowtasklistHandler.ashx?action=mobileViewFlow&keyid=' + keyid + '',
                type: 'POST', //GET
                async: false,  //或false,是否异步 同步则执行完才回执行后面的东西
                timeout: 5000,    //超时时间
                dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
                beforeSend: function (xhr) {
                    //console.log(xhr);
                    //console.log('发送前');
                },
                success: function (d, textStatus, jqXHR) {
                    $.hideLoading();
                    console.log("测试:" + d.status);
                    if (d.status != 1) {
                        if (d.ActivityType == 4) {//说明流程已结束
                            $.toast("流程已结束", "forbidden");
                            $.closePopup();
                        } else {
                            //flowdet
                            $("#doflow" + keyid).find(".FlowInstanceId").val(d.KeyId);
                            $("#doflow" + keyid).find(".flowcontent").html("流程:" + d.Printedmattername + "<br/>当前需要处理:" + d.nodename);
                            console.log(KeyId);
                            $("#doflow" + keyid).popup();
                            //处理文字


                        }
                    } else {
                        console.log("测试1" + d.filename);
                        $.toast(d.filename, "forbidden");
                        $.closePopup();
                    }
                    //flowdet
                    $("#doflow" + keyid).find(".FlowInstanceId").val(d.KeyId);


                },
                error: function (xhr, textStatus) {
                    //console.log('错误');
                    //console.log(xhr);
                    //console.log(textStatus);
                },
                complete: function () {
                    //console.log('结束');
                }
            });
            //FlowInstanceId
        }
        function submitflow(keyid) {//流程处理
            if ($("#doflow" + keyid).find(".VerificationOpinion").val() == "") {
                $.toast("请填写备注", "forbidden");
                return false;
            }
            $.showLoading();
            $.ajax({
                url: '/Jydflowtasklist/ashx/JydflowtasklistHandler.ashx',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {
                    VerificationOpinion: $("#doflow" + keyid).find(".VerificationOpinion").val(),
                    FlowInstanceId: $("#doflow" + keyid).find(".FlowInstanceId").val(),
                    VerificationFinally: $("#doflow" + keyid).find(".VerificationFinally").val(),
                    keyid: keyid,
                    action: "doflow"
                },
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success: function (d, textStatus, jqXHR) {
                    console.log("我来了" + d.status);
                    $.hideLoading();
                    if (d.status == 1) {
                        $.toast(d.filename);
                        $.closePopup();
                    } else if (d.status == 2) {
                        $.toast(d.filename, "forbidden");
                        $.closePopup();
                    } else if (d.status == 3) {
                        $.toast(d.filename, "forbidden");
                        $.closePopup();
                    } else {
                        $.toast(d.filename);
                        $.closePopup();
                    }
                },

            });
        }

        function viewdet(keyid) {//流程明细
            $.showLoading();
            console.log(123);
            $.ajax({
                url: '/Jydflowtasklist/ashx/JydflowtasklistHandler.ashx?action=mobileViewFlowDit&keyid=' + keyid + '',
                type: 'POST', //GET
                async: false,  //或false,是否异步 同步则执行完才回执行后面的东西
                timeout: 5000,    //超时时间
                dataType: 'json', //返回的数据格式：json/xml/html/script/jsonp/text
                beforeSend: function (xhr) {
                    //console.log(xhr);
                    //console.log('发送前');
                },
                success: function (d, textStatus, jqXHR) {
                    //flowdet
                    $.hideLoading();

                    console.log("测试1");
                    $("#flowdet" + keyid).empty();
                    $.each(d, function (i, v) {
                        var temp = "已处理";
                        if (v.VerificationFinally === "2") {
                            temp = "拒绝";
                        } else if (v.VerificationFinally === "3") {
                            temp = "驳回";
                        }

                        var remark = "备注";
                        $("#flowdet" + keyid).append(i + "." + v.processID + "," + v.Operationtime + "," + v.Operctionname + "," + temp + ", " + v.Operationalcontent + ", " + remark + "," + v.Remarks + "<br/>");

                    });

                },
                error: function (xhr, textStatus) {
                    //console.log('错误');
                    //console.log(xhr);
                    //console.log(textStatus);
                },
                complete: function () {
                    //console.log('结束');
                }
            });
            $("#view" + keyid).popup();
        }
    </script>
</head>
<body ontouchstart>
    <div class="weui-form-preview">
        <%foreach (DataRow de in dt.Rows)
            {%>
        <div class="weui-form-preview__bd" style="color: #000; padding: 0;">
            <div class="weui-form-preview__item" style="display: none;">
                <label class="weui-form-preview__label">订单编号<%=isyg %></label>
                <span class="weui-form-preview__value" id="KeyId"><%=de["KeyId"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">订单编号</label>
                <span class="weui-form-preview__value" id="OrderID"><%=de["OrderID"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">单位名称</label>
                <span class="weui-form-preview__value" id="comname"><%=de["comname"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">电话</label>
                <span class="weui-form-preview__value" id="tel"><%=de["tel"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">接件时间</label>
                <span class="weui-form-preview__value" id="adddate"><%=de["adddate"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">交货时间</label>
                <span class="weui-form-preview__value" id="deliveryDate"><%=de["deliveryDate"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">印刷品名称</label>
                <span class="weui-form-preview__value" id="allyspmc"><%=de["allyspmc"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">联系人</label>
                <span class="weui-form-preview__value" id="connman"><%=de["connman"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">接件员</label>
                <span class="weui-form-preview__value" id="jjy"><%=de["jjy"].ToString() %></span>
            </div>

        </div>
        <%} %>
    </div>
    <br />
    <div class="weui-msg">



        <%int i = 1; %>
        <%foreach (DataRow ddr in detailDt.Rows)
            {%>
        <div class="weui-cells__title"><%=i++ %>.<%=ddr["yspmc"].ToString() %></div>

        <div class="button-sp-area">
            <%--<a onclick="openflow(<%=ddr["keyid"].ToString() %>)" class="weui-btn weui-btn_mini weui-btn_primary">查看流程</a>--%>
            <a onclick="viewdet(<%=ddr["keyid"].ToString() %>)" class="weui-btn weui-btn_mini weui-btn_primary">流程明细</a>
            <a onclick="doflow(<%=ddr["keyid"].ToString() %>)" class="weui-btn weui-btn_mini weui-btn_warn">流程处理</a>
        </div>
        <div id="full<%=ddr["keyid"].ToString() %>" class='weui-popup__container'>
            <div class="weui-popup__overlay"></div>
            <div class="weui-popup__modal" style="margin-left: 5px; margin-right: 5px;">

                <div id="flowPanel<%=ddr["keyid"].ToString() %>"></div>
                <a href="javascript:;" class="weui-btn weui-btn_primary close-popup">关闭</a>
            </div>

        </div>
        <div id="view<%=ddr["keyid"].ToString() %>" class='weui-popup__container'>
            <div class="weui-popup__overlay"></div>
            <div class="weui-popup__modal" style="margin-left: 5px; margin-right: 5px;">

                <div id="flowdet<%=ddr["keyid"].ToString() %>"></div>
                <a href="javascript:;" class="weui-btn weui-btn_primary close-popup">关闭</a>
            </div>

        </div>
        <div id="doflow<%=ddr["keyid"].ToString() %>" class='weui-popup__container'>
            <div class="weui-popup__overlay"></div>
            <div class="weui-popup__modal" style="margin-left: 5px; margin-right: 5px;">
                <input type="hidden" class="FlowInstanceId" name="FlowInstanceId" />
                <article class="weui-article">

                    <section>
                        <h3 class="flowcontent"></h3>
                    </section>
                </article>
                <div class="weui-cells__title">结果</div>

                <div class="weui-cells">
                    <div class="weui-cell weui-cell_select">
                        <div class="weui-cell__bd">
                            <select class="weui-select VerificationFinally" name="VerificationFinally">
                                <option selected="" value="1">处理</option>
                                <option value="2">不处理</option>
                                <option value="3">有问题,找主管</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="weui-cells__title">备注</div>
                <div class="weui-cells">
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <input class="weui-input VerificationOpinion" type="text" placeholder="请输入备注">
                        </div>
                    </div>
                </div>
                <center>
                    <a href="javascript:;" class="weui-btn weui-btn_mini weui-btn_primary close-popup">关闭</a>
                    <a onclick="submitflow(<%=ddr["keyid"].ToString() %>)" class="weui-btn weui-btn_mini weui-btn_warn">流程处理</a>
                        </center>
            </div>




        </div>


        <%} %>
    </div>
</body>
</html>
