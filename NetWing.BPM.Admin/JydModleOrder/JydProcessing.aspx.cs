﻿using NetWing.Bll;
using NetWing.BPM.Core;
using NetWing.Common;
using NetWing.Common.Data.SqlServer;
using NetWing.Model;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web;


namespace NetWing.BPM.Admin.JydModleOrder
{
    public partial class JydProcessing : System.Web.UI.Page
    {
        protected string OrderID;
        protected string status;
        protected int KeyId;
        protected string copyRight = Global.copyRight;
        protected string order_img;
        protected JydOrderModel model;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //判断用户是否登陆
                OrderID = Request["OrderID"];
                DataTable dt = SqlEasy.ExecuteDataTable("select top 1 * from jydOrder where OrderID='" + OrderID + "' and mystatus=1");
                if (dt.Rows.Count > 0)
                {
                    string dtJson = JSONhelper.ToJson(dt, false);
                    dtJson = dtJson.Substring(0, dtJson.Length - 1);
                    dtJson = dtJson.Substring(1);
                    model = JsonConvert.DeserializeObject<JydOrderModel>(dtJson);
                    OrderID = model.OrderID;
                    return;
                }
               // Response.Redirect("reg.aspx");
            }
        }
        /// <summary>
        /// 回调地址
        /// </summary>
        //public void IndexUrl()
        //{
        //    Response.Redirect(redirect_uri);
        //}
    }
}