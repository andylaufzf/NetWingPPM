﻿var actionURL = '/JydModleOrder/ashx/ajax_submit.ashx';
var formurl = '/JydModleOrder/QRCodeView.aspx';


//使用单个参数提交数据,感谢同事的指导和参考

//开始加工
function xiugai(id) {
    $.ajax({
        url: "/JydModleOrder/ashx/ajax_submit.ashx",
        type: "POST",
        datatype: "JSON",
        data: {
            action: 'xiugai',
            KeyId: id
        },
        success: function (d) {
            alert("单子已开始加工,请尽快加工");
        }
    });
}

//完成加工
function wancheng(id) {
    $.ajax({
        url: "/JydModleOrder/ashx/ajax_submit.ashx",
        type: "POST",
        datatype: "json",
        async: true, 
        data: {
            action: 'wancheng',
            KeyId: id
        },
        success: function (data) {
            
                alert("加工已结束,准备送货");
           
        }
    });
}








function init() {
    //初始化时克隆模板过来
    var t = top.$(".mymytemp").html();//得到不带样式的模板 注意修改时也要修改模板
    //alert(top.$(".mymy:last").html());
    if (top.$(".mymy:last").html() == "") {//如果mymy里面为空则追加进去，否则
        top.$(".mymy").append(t);//把模板追加到mymy
    } else {//如果有多个
        top.$(".mymy:last").after('<tbody class="mymy">' + t + '</tbody>');//把模板追加到mymy
    }


    top.$("#jedx").textbox({});//金额大写




    //获得当前时间 作为接件时间


    //得到接件员名字
    top.$.ajax({
        type: "POST",
        url: '/JydOrder/ashx/JydOrderHandler.ashx?json={"action":"jjy"}',
        data: {},
        dataType: "json",
        beforeSend: function () { },
        complete: function () { },
        success: function (d) {
            console.log("得到的结果是：" + d.jjy);
            top.$("#jjy").textbox("setValue", d.jjy);
            top.$("#adddate").datetimebox("setValue", d.dt);

            top.$("#jjy").textbox({//设置为只读
                readonly: true,
            });
            //alert("发送结果：" + JSON.stringify(d));
        }
    });

    //初始化总金额
    top.$(".jexx").numberbox({
        min: 0,
        precision: 2,
        value: 0,
        required: true,
        validType: 'money',
        onChange: function (newValue, oldValue) {

        }
        //validType: 'email' 
    });



    top.$("#comname").combogrid({
        delay: 500, //自动完成功能实现搜索
        mode: 'remote',//开启后系统会自动传一个参数q到后台 
        panelWidth: 350,
        required: true,
        //value:'fullname',   
        editable: true,
        idField: 'comname',
        textField: 'comname',
        url: '/JydUser/ashx/JydUserHandler.ashx',
        columns: [[
            { field: 'KeyId', title: 'KeyId', width: 50 },
            { field: 'comname', title: '客户', width: 120 },
            { field: 'tell', title: '电话', width: 120 },

        ]],
        limitToList: true,//只能从下拉中选择值
        //reversed: true,//定义在失去焦点的时候是否恢复原始值。
        onHidePanel: function () {
            var t = top.$(this).combogrid('getValue');//获取combogrid的值
            var g = top.$(this).combogrid('grid');	// 获取数据表格对象
            var r = g.datagrid('getSelected');	// 获取选择的行
            console.log("选择的行是：" + r + "选择的值是:" + t);
            if (r == null || t != r.comname) {//没有选择或者选项不相等时清除内容
                top.$.messager.alert('警告', '请选择，不要直接输入!');
                top.$(this).combogrid('setValue', '');
            } else {
                //do something...
            }
        },
        onSelect: function (rowIndex, rowData) {
            console.log("设置的值是：" + rowData.KeyId);
            //$("#contactid").val(rowData.KeyId);//给经手人设置ID
            top.$("#comname").combogrid('setValue', rowData.comname);//客户
            top.$("#connman").textbox('setValue', rowData.realname);//经手人
            top.$("#tel").textbox('setValue', rowData.tell)
            //alert(rowData.TrueName);
        }
    });

    top.$("#jzrs").numberspinner({
        editable: false,
        onSpinUp: function () {//鼠标向上点
            top.$(".mymy:last").after('<tbody class="mymy">' + t + '</tbody>');//把模板追加到mymy

            //insertjzr();//插入居住人
            //在最后一个后面克隆一个元素包含事件
            console.log("克隆页面");
            //mymy 和mymy1有父子关系 mymy1默认不显示
            //var temp = top.$(".mymy:last").html();//保存渲染前的模板
            ////console.log("得到的模板是：" + temp);



            //var mod = top.$(".mymy:last").clone();//克隆得到模板
            //top.$(".mymy:last").after(mod);//克隆后插入
            //top.$(".mymy:last").find(".myPrintType").empty();//找到印刷品类型并清空easyui渲染过的印刷品类型
            //top.$(".mymy:last").find(".myPrintType").append('<input name="PrintType" style="width:80px;" value="" class="PrintType" />');
            ////移除easyui工艺渲染新的easyui工艺
            //top.$(".mymy:last").find('.mygongyi').empty();
            //top.$(".mymy:last").find(".mygongyi").append('<input class="myswigongyi">');
            initbody();//克隆完毕重新渲染-只渲染body里面的内容

        },
        onSpinDown: function () {//鼠标向下点
            //var v = top.$("#jzrsdiv").children("li").length;
            var v = top.$(".mymy").length;
            if (v == 1) {//只有一个时不能删除
                top.$.messager.alert("注意", "至少接一件！！！");
                //$.messager.alert('警告', '表单还有信息没有填完整！');
            } else {
                top.$(".mymy:last").remove();//移除最后一个

            }




        }
    });
    console.log("加载印刷类型");

    //运输
    top.$('#ys').combobox({
        url: '/sys/ashx/dichandler.ashx?categoryId=86',
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        required: true,
        valueField: 'Title',
        textField: 'Title'
    });

    //客户自带
    top.$('#zidaihz').combobox({
        url: '/sys/ashx/dichandler.ashx?categoryId=102',
        limitToList: true,
        required: true,
        valueField: 'Title',
        textField: 'Title'
    });



    //费用
    top.$('#fy').combobox({
        url: '/sys/ashx/dichandler.ashx?categoryId=87',
        limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
        required: true,
        valueField: 'Title',
        textField: 'Title'
    });

    initbody();//渲染body里面的内容
}




var CRUD = {
    add: function () {
        var hDialog = top.jQuery.hDialog({
            title: '接件开单', width: 1300, height: 700, href: formurl, iconCls: 'icon-add',
            onLoad: function () {
                top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                init();//渲染
            },
            submit: function () {
                //alert(top.$("#uiform").form('enableValidation').form('validate'));
                //alert(top.$("#uiform").form('validate'));
                //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                if (top.$("#wapform").form('validate')) {
                    var query = createParam('add', '0');
                    //console.log(query);


                    var temp = top.$(".mymy :input").serializeArray();//serializeArray 要么选择form 要么 $(".mymy :input") 这样选择
                    //先循环mymy下所有html元素
                    var mymylenth = top.$(".mymy").length;//得到有几个.mymy
                    // alert(mymylenth);
                    var detailjson = "[";
                    var j = 1;

                    top.$(".mymy").each(function () {
                        var temp2 = top.$(this).find(":input").serializeArray();
                        var jsonstr = JSON.stringify(convertArrayNew(temp2));
                        console.log("序列化后得到的jsonstr：" + jsonstr);


                        if (j < mymylenth) {
                            detailjson = detailjson + '{"d":' + jsonstr + '},';
                        } else {
                            detailjson = detailjson + '{"d":' + jsonstr + '}';

                        }


                        //alert(top.$(this).html())
                        j = j + 1;
                        //alert("转化后是："+JSON.stringify(convertArray(temp2)));
                    });
                    detailjson = detailjson + ']';

                    console.log("得到的json数据是：" + detailjson);


                    //console.log("没有经过转化前：" + temp);
                    //alert(temp);
                    //console.log("序列化后是：" + JSON.stringify(convertArray(temp)));//转化
                    //alert(JSON.stringify(convertArray(temp)));
                    //return false;
                    jQuery.ajaxjson(actionURL, query + "&d=" + detailjson + "", function (d) {
                        if (parseInt(d) > 0) {
                            msg.ok('添加成功！');
                            hDialog.dialog('close');
                            grid.reload();
                        } else {
                            MessageOrRedirect(d);
                        }
                    });
                }
                //msg.warning('请填写必填项！');
                return false;
            }
        });

        top.$('#uiform').validate();
    },
    edit: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '修改接件单', width: 1300, height: 700, href: formurl, iconCls: 'icon-save',
                onLoad: function () {
                    //init();//初始化
                    top.$('#uiform').form('load', row);

                    top.$.ajax({
                        type: "POST",
                        url: '/JydOrderDetail/ashx/JydOrderDetailHandler.ashx?filter={"groupOp":"AND","rules":[{"field":"orderid","op":"eq","data":"' + row.OrderID + '"}],"groups":[]}',
                        data: {},
                        dataType: "json",
                        beforeSend: function () { },
                        complete: function () { },
                        success: function (d) {
                            console.log("加载字表数据：" + JSON.stringify(d));
                            console.log("字表个数：" + d.rows.length);

                            for (var i = 1; i <= d.rows.length; i++) {
                                //console.log("渲染次数：" + i + "值是:" + JSON.stringify(d.rows[i]));
                                //top.$('.mymy').form('load', d.rows[i]);
                                init();//渲染完成

                                //top.$('.mymy').eq(i).form('load', d.rows[i]);

                            }

                            for (var i = 0; i < d.rows.length; i++) {

                                top.$('.KeyId').eq(i).val(d.rows[i].KeyId);
                                top.$('.orderid').eq(i).textbox('setValue', d.rows[i].orderid);
                                top.$('.omainid').eq(i).textbox('setValue', d.rows[i].omainid);
                                top.$('.yspmc').eq(i).textbox('setValue', d.rows[i].yspmc);
                                top.$('.PrintType').eq(i).textbox('setValue', d.rows[i].PrintType);
                                top.$('.cpcc').eq(i).textbox('setValue', d.rows[i].cpcc);
                                top.$('.zzf').eq(i).textbox('setValue', d.rows[i].zzf);
                                top.$('.zzn').eq(i).textbox('setValue', d.rows[i].zzn);
                                top.$('.ym').eq(i).textbox('setValue', d.rows[i].ym);
                                top.$('.sj').eq(i).textbox('setValue', d.rows[i].sj);
                                //top.$('.rj').eq(i).textbox('setValue', d.rows[i].rj);
                                //top.$('.jp').eq(i).textbox('setValue', d.rows[i].jp);
                                top.$('.zz').eq(i).val(d.rows[i].zz);
                                if (d.rows[i].zz == "自带") {
                                    top.$('.zz').eq(i).attr("checked", true);
                                    //console.log("判断纸张自带！");
                                }
                                if (d.rows[i].jp == "自带") {
                                    top.$('.jp').eq(i).attr("checked", true);
                                    //console.log("判断纸张自带！");
                                }
                                if (d.rows[i].rj == "自带") {
                                    top.$('.rj').eq(i).attr("checked", true);
                                    //console.log("判断纸张自带！");
                                }
                                top.$('.zidaihz').eq(i).textbox('setValue', d.rows[i].zidaihz);
                                top.$('.sl').eq(i).textbox('setValue', d.rows[i].sl);
                                top.$('.cc').eq(i).textbox('setValue', d.rows[i].cc);
                                top.$('.dj').eq(i).textbox('setValue', d.rows[i].dj);
                                top.$('.je').eq(i).textbox('setValue', d.rows[i].je);
                                top.$('.note').eq(i).textbox('setValue', d.rows[i].note);
                                top.$('.kzcc').eq(i).textbox('setValue', d.rows[i].kzcc);
                                top.$('.zs').eq(i).textbox('setValue', d.rows[i].zs);
                                top.$('.fs').eq(i).textbox('setValue', d.rows[i].fs);
                                top.$('.pss').eq(i).textbox('setValue', d.rows[i].pss);
                                top.$('.pst').eq(i).textbox('setValue', d.rows[i].pst);
                                top.$('.fbyq').eq(i).textbox('setValue', d.rows[i].fbyq);
                                top.$('.jx').eq(i).textbox('setValue', d.rows[i].jx);
                                top.$('.fumoa').eq(i).textbox('setValue', d.rows[i].fumoa);
                                top.$('.fumob').eq(i).textbox('setValue', d.rows[i].fumob);
                                top.$('.yaa').eq(i).textbox('setValue', d.rows[i].yaa);
                                top.$('.yab').eq(i).textbox('setValue', d.rows[i].yab);
                                top.$('.tana').eq(i).textbox('setValue', d.rows[i].tana);
                                top.$('.tanb').eq(i).textbox('setValue', d.rows[i].tanb);
                                top.$('.zheye').eq(i).textbox('setValue', d.rows[i].zheye);
                                top.$('.uva').eq(i).textbox('setValue', d.rows[i].uva);
                                top.$('.uvb').eq(i).textbox('setValue', d.rows[i].uvb);
                                top.$('.zd').eq(i).textbox('setValue', d.rows[i].zd);
                                top.$('.db').eq(i).textbox('setValue', d.rows[i].db);
                                top.$('.bh').eq(i).textbox('setValue', d.rows[i].bh);
                                top.$('.sh').eq(i).textbox('setValue', d.rows[i].sh);
                                top.$('.sk').eq(i).textbox('setValue', d.rows[i].sk);
                                top.$('.sjwcsj').eq(i).textbox('setValue', d.rows[i].sjwcsj);
                                top.$('.cpwcsj').eq(i).textbox('setValue', d.rows[i].cpwcsj);
                                top.$('.ctpsj').eq(i).textbox('setValue', d.rows[i].ctpsj);
                                top.$('.sbwcsj').eq(i).textbox('setValue', d.rows[i].sbwcsj);
                                top.$('.qslsj').eq(i).textbox('setValue', d.rows[i].qslsj);
                                top.$('.yswcsj').eq(i).textbox('setValue', d.rows[i].yswcsj);
                                top.$('.hjgkssj').eq(i).textbox('setValue', d.rows[i].hjgkssj);
                                top.$('.hjgwcsj').eq(i).textbox('setValue', d.rows[i].hjgwcsj);
                                top.$('.wjgjcsj').eq(i).textbox('setValue', d.rows[i].wjgjcsj);
                                top.$('.qcpsj').eq(i).textbox('setValue', d.rows[i].qcpsj);
                                top.$('.zdkssj').eq(i).textbox('setValue', d.rows[i].zdkssj);
                                top.$('.dzwcsj').eq(i).textbox('setValue', d.rows[i].dzwcsj);
                                top.$('.shryccsj').eq(i).textbox('setValue', d.rows[i].shryccsj);
                                top.$('.shryhcsj').eq(i).textbox('setValue', d.rows[i].shryhcsj);
                                top.$('.dbsj').eq(i).textbox('setValue', d.rows[i].dbsj);
                                top.$('.rksj').eq(i).textbox('setValue', d.rows[i].rksj);
                                top.$('.cksj').eq(i).textbox('setValue', d.rows[i].cksj);
                                top.$('.zrr').eq(i).textbox('setValue', d.rows[i].zrr);
                                top.$('.kkyy').eq(i).textbox('setValue', d.rows[i].kkyy);
                                top.$('.sxj').eq(i).textbox('setValue', d.rows[i].sxj);
                                top.$('.szp').eq(i).textbox('setValue', d.rows[i].szp);
                                top.$('.zph').eq(i).textbox('setValue', d.rows[i].zph);
                                top.$('.myws').eq(i).textbox('setValue', d.rows[i].myws);
                                top.$('.myysf').eq(i).textbox('setValue', d.rows[i].myysf);
                                top.$('.O_myhjg').eq(i).textbox('setValue', d.rows[i].O_myhjg);
                                top.$('.qianshou').eq(i).textbox('setValue', d.rows[i].qianshou);
                                top.$('.skje').eq(i).textbox('setValue', d.rows[i].skje);
                                top.$('.wsjgbz').eq(i).textbox('setValue', d.rows[i].wsjgbz);
                                top.$('.dddd').eq(i).textbox('setValue', d.rows[i].dddd);
                                top.$('.kkkje').eq(i).textbox('setValue', d.rows[i].kkkje);
                                top.$('.yspmcb').eq(i).textbox('setValue', d.rows[i].yspmcb);
                                top.$('.FileTypeb').eq(i).textbox('setValue', d.rows[i].FileTypeb);
                                top.$('.mianb').eq(i).textbox('setValue', d.rows[i].mianb);
                                top.$('.munb').eq(i).textbox('setValue', d.rows[i].munb);
                                top.$('.colorb').eq(i).textbox('setValue', d.rows[i].colorb);
                                top.$('.pagerb').eq(i).textbox('setValue', d.rows[i].pagerb);
                                top.$('.O_my_jpb').eq(i).textbox('setValue', d.rows[i].O_my_jpb);
                                top.$('.O_my_zzb').eq(i).textbox('setValue', d.rows[i].O_my_zzb);
                                top.$('.ddddb').eq(i).textbox('setValue', d.rows[i].ddddb);
                                top.$('.zznb').eq(i).textbox('setValue', d.rows[i].zznb);
                                top.$('.ymb').eq(i).textbox('setValue', d.rows[i].ymb);
                                top.$('.sjb').eq(i).textbox('setValue', d.rows[i].sjb);
                                top.$('.rjb').eq(i).textbox('setValue', d.rows[i].rjb);
                                top.$('.jpb').eq(i).textbox('setValue', d.rows[i].jpb);
                                top.$('.zzb').eq(i).textbox('setValue', d.rows[i].zzb);
                                top.$('.fumoab').eq(i).textbox('setValue', d.rows[i].fumoab);
                                top.$('.fumobb').eq(i).textbox('setValue', d.rows[i].fumobb);
                                top.$('.yaab').eq(i).textbox('setValue', d.rows[i].yaab);
                                top.$('.yabb').eq(i).textbox('setValue', d.rows[i].yabb);
                                top.$('.tanab').eq(i).textbox('setValue', d.rows[i].tanab);
                                top.$('.tanbb').eq(i).textbox('setValue', d.rows[i].tanbb);
                                top.$('.zheyeb').eq(i).textbox('setValue', d.rows[i].zheyeb);
                                top.$('.uvab').eq(i).textbox('setValue', d.rows[i].uvab);
                                top.$('.uvbb').eq(i).textbox('setValue', d.rows[i].uvbb);
                                top.$('.zdb').eq(i).textbox('setValue', d.rows[i].zdb);
                                top.$('.dbb').eq(i).textbox('setValue', d.rows[i].dbb);
                                top.$('.bhb').eq(i).textbox('setValue', d.rows[i].bhb);
                                top.$('.shb').eq(i).textbox('setValue', d.rows[i].shb);
                                top.$('.skb').eq(i).textbox('setValue', d.rows[i].skb);
                                top.$('.PrintTypeb').eq(i).textbox('setValue', d.rows[i].PrintTypeb);
                                top.$('.zzfb').eq(i).textbox('setValue', d.rows[i].zzfb);
                                top.$('.noteb').eq(i).textbox('setValue', d.rows[i].noteb);
                                top.$('.cpccb').eq(i).textbox('setValue', d.rows[i].cpccb);
                                top.$('.printyes').eq(i).textbox('setValue', d.rows[i].printyes);
                                top.$('.kzccb').eq(i).textbox('setValue', d.rows[i].kzccb);
                                top.$('.zsb').eq(i).textbox('setValue', d.rows[i].zsb);
                                top.$('.fsb').eq(i).textbox('setValue', d.rows[i].fsb);
                                top.$('.pssb').eq(i).textbox('setValue', d.rows[i].pssb);
                                top.$('.pstb').eq(i).textbox('setValue', d.rows[i].pstb);
                                top.$('.fbyqb').eq(i).textbox('setValue', d.rows[i].fbyqb);
                                top.$('.jxb').eq(i).textbox('setValue', d.rows[i].jxb);
                                top.$('.kkkjeb').eq(i).textbox('setValue', d.rows[i].kkkjeb);
                                top.$('.nysl').eq(i).textbox('setValue', d.rows[i].nysl);
                                top.$('.ctpa').eq(i).textbox('setValue', d.rows[i].ctpa);
                                top.$('.ctpb').eq(i).textbox('setValue', d.rows[i].ctpb);
                                top.$('.zhizhangbb').eq(i).textbox('setValue', d.rows[i].zhizhangbb);
                                top.$('.songhuo').eq(i).textbox('setValue', d.rows[i].songhuo);
                                top.$('.xiaoyang').eq(i).textbox('setValue', d.rows[i].xiaoyang);
                                top.$('.danwei_m').eq(i).textbox('setValue', d.rows[i].danwei_m);
                                top.$('.danwei_mb').eq(i).textbox('setValue', d.rows[i].danwei_mb);
                                top.$('.bz_hx').eq(i).textbox('setValue', d.rows[i].bz_hx);
                                top.$('.bz_wbz_cc').eq(i).textbox('setValue', d.rows[i].bz_wbz_cc);
                                top.$('.bz_wbz_gy').eq(i).textbox('setValue', d.rows[i].bz_wbz_gy);
                                top.$('.bz_nbz_zz').eq(i).textbox('setValue', d.rows[i].bz_nbz_zz);
                                top.$('.bz_nbz_cc').eq(i).textbox('setValue', d.rows[i].bz_nbz_cc);
                                top.$('.bz_nbz_gy').eq(i).textbox('setValue', d.rows[i].bz_nbz_gy);
                                top.$('.bz_wkz_zz').eq(i).textbox('setValue', d.rows[i].bz_wkz_zz);
                                top.$('.bz_wkz_cc').eq(i).textbox('setValue', d.rows[i].bz_wkz_cc);
                                top.$('.bz_wkz_gy').eq(i).textbox('setValue', d.rows[i].bz_wkz_gy);
                                top.$('.bz_bc_yl').eq(i).textbox('setValue', d.rows[i].bz_bc_yl);
                                top.$('.bz_bc_sl').eq(i).textbox('setValue', d.rows[i].bz_bc_sl);
                                top.$('.bz_bc_mx').eq(i).textbox('setValue', d.rows[i].bz_bc_mx);
                                top.$('.bz_nt').eq(i).textbox('setValue', d.rows[i].bz_nt);
                                top.$('.bz_nt_cc').eq(i).textbox('setValue', d.rows[i].bz_nt_cc);
                                top.$('.bz_nt_mx').eq(i).textbox('setValue', d.rows[i].bz_nt_mx);
                                top.$('.bz_wbz').eq(i).textbox('setValue', d.rows[i].bz_wbz);
                            }


                        }
                    });

                    //top.$('.mymy').form('load', {comname:'北京'});

                    //top.$('#txt_KeyId').numberspinner('setValue', row.KeyId);
                    //top.$('#txt_ServiceType').numberspinner('setValue', row.ServiceType);
                    //top.$('#txt_OrderYear').textbox('setValue', row.OrderYear);
                    //top.$('#txt_OrderInt').numberspinner('setValue', row.OrderInt);
                    //top.$('#txt_OrderID').textbox('setValue', row.OrderID);
                    //top.$('#txt_username').textbox('setValue', row.username);
                    //top.$('#txt_comname').textbox('setValue', row.comname);
                    //top.$('#txt_comnameid').numberspinner('setValue', row.comnameid);
                    //top.$('#txt_jpm').textbox('setValue', row.jpm);
                    //top.$('#txt_name').textbox('setValue', row.name);
                    //top.$('#txt_tel').textbox('setValue', row.tel);
                    //top.$('#txt_address').textbox('setValue', row.address);
                    //top.$('#txt_explain').textbox('setValue', row.explain);
                    //top.$('#txt_deliveryDate').datetimebox('setValue', row.deliveryDate);
                    //top.$('#txt_delivery').textbox('setValue', row.delivery);
                    //top.$('#txt_adddate').datetimebox('setValue', row.adddate);
                    //top.$('#txt_fk').numberspinner('setValue', row.fk);
                    //top.$('#txt_yf').numberspinner('setValue', row.yf);
                    //top.$('#txt_qt').numberspinner('setValue', row.qt);
                    //top.$('#txt_totalprice').numberspinner('setValue', row.totalprice);
                    //top.$('#txt_cfzt').textbox('setValue', row.cfzt);
                    //top.$('#txt_jjy').textbox('setValue', row.jjy);
                    //top.$('#txt_fumoa').textbox('setValue', row.fumoa);
                    //top.$('#txt_fumob').textbox('setValue', row.fumob);
                    //top.$('#txt_yaa').textbox('setValue', row.yaa);
                    //top.$('#txt_yab').textbox('setValue', row.yab);
                    //top.$('#txt_tana').textbox('setValue', row.tana);
                    //top.$('#txt_tanb').textbox('setValue', row.tanb);
                    //top.$('#txt_zheye').textbox('setValue', row.zheye);
                    //top.$('#txt_uva').textbox('setValue', row.uva);
                    //top.$('#txt_uvb').textbox('setValue', row.uvb);
                    //top.$('#txt_zd').textbox('setValue', row.zd);
                    //top.$('#txt_db').textbox('setValue', row.db);
                    //top.$('#txt_bh').textbox('setValue', row.bh);
                    //top.$('#txt_sh').textbox('setValue', row.sh);
                    //top.$('#txt_sk').textbox('setValue', row.sk);
                    //top.$('#txt_huofa').textbox('setValue', row.huofa);
                    //top.$('#txt_fahuoren').textbox('setValue', row.fahuoren);
                    //top.$('#txt_ys').textbox('setValue', row.ys);
                    //top.$('#txt_fy').textbox('setValue', row.fy);
                    //top.$('#txt_dha').textbox('setValue', row.dha);
                    //top.$('#txt_dhb').textbox('setValue', row.dhb);
                    //top.$('#txt_status').textbox('setValue', row.status);
                    //top.$('#txt_jexx').textbox('setValue', row.jexx);
                    //top.$('#txt_jedx').textbox('setValue', row.jedx);
                    //top.$('#txt_yufuk').numberspinner('setValue', row.yufuk);
                    //top.$('#txt_yufukuana').textbox('setValue', row.yufukuana);
                    //top.$('#txt_sckd').textbox('setValue', row.sckd);
                    //top.$('#txt_sjwcsj').textbox('setValue', row.sjwcsj);
                    //top.$('#txt_cpwcsj').textbox('setValue', row.cpwcsj);
                    //top.$('#txt_ctpsj').textbox('setValue', row.ctpsj);
                    //top.$('#txt_sbwcsj').textbox('setValue', row.sbwcsj);
                    //top.$('#txt_qslsj').textbox('setValue', row.qslsj);
                    //top.$('#txt_yswcsj').textbox('setValue', row.yswcsj);
                    //top.$('#txt_hjgkssj').textbox('setValue', row.hjgkssj);
                    //top.$('#txt_hjgwcsj').textbox('setValue', row.hjgwcsj);
                    //top.$('#txt_wjgjcsj').textbox('setValue', row.wjgjcsj);
                    //top.$('#txt_qcpsj').textbox('setValue', row.qcpsj);
                    //top.$('#txt_zdkssj').textbox('setValue', row.zdkssj);
                    //top.$('#txt_dzwcsj').textbox('setValue', row.dzwcsj);
                    //top.$('#txt_shryccsj').textbox('setValue', row.shryccsj);
                    //top.$('#txt_shryhcsj').textbox('setValue', row.shryhcsj);
                    //top.$('#txt_dbsj').textbox('setValue', row.dbsj);
                    //top.$('#txt_szp').textbox('setValue', row.szp);
                    //top.$('#txt_sxj').textbox('setValue', row.sxj);
                    //top.$('#txt_skje').numberspinner('setValue', row.skje);
                    //top.$('#txt_zph').textbox('setValue', row.zph);
                    //top.$('#txt_kkyy').textbox('setValue', row.kkyy);
                    //top.$('#txt_zrr').textbox('setValue', row.zrr);
                    //top.$('#txt_rksj').textbox('setValue', row.rksj);
                    //top.$('#txt_cksj').textbox('setValue', row.cksj);
                    //top.$('#txt_myws').textbox('setValue', row.myws);
                    //top.$('#txt_myysf').textbox('setValue', row.myysf);
                    //top.$('#txt_O_myhjg1').textbox('setValue', row.O_myhjg1);
                    //top.$('#txt_jgzysx').textbox('setValue', row.jgzysx);
                    //top.$('#txt_connman').textbox('setValue', row.connman);
                    //top.$('#txt_mystatus').numberspinner('setValue', row.mystatus);
                    //top.$('#txt_kkkje').numberspinner('setValue', row.kkkje);
                    //top.$('#txt_ds').numberspinner('setValue', row.ds);
                    //top.$('#txt_allyspmc').textbox('setValue', row.allyspmc);
                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');    
                },
                submit: function () {
                    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                    //alert(top.$("#uiform").form('validate'));
                    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {

                        var query = createParam('edit', row.KeyId);
                        console.log("编辑时query:" + query);
                        //alert(query);


                        var temp = top.$(".mymy :input").serializeArray();//serializeArray 要么选择form 要么 $(".mymy :input") 这样选择
                        //先循环mymy下所有html元素
                        var mymylenth = top.$(".mymy").length;//得到有几个.mymy
                        //alert(mymylenth);
                        var detailjson = "[";
                        var j = 1;

                        top.$(".mymy").each(function () {
                            var temp2 = top.$(this).find(":input").serializeArray();
                            var jsonstr = JSON.stringify(convertArrayNew(temp2));


                            if (j < mymylenth) {
                                detailjson = detailjson + '{"d":' + jsonstr + '},';
                            } else {
                                detailjson = detailjson + '{"d":' + jsonstr + '}';

                            }


                            //alert(top.$(this).html())
                            j = j + 1;
                            //alert("转化后是："+JSON.stringify(convertArray(temp2)));
                        });
                        detailjson = detailjson + ']';

                        console.log("得到的json数据是：" + detailjson);


                        jQuery.ajaxjson(actionURL, query + "&d=" + detailjson, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    }
                    //msg.warning('请填写必填项！');
                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    editlcd: function () {
        var row = grid.getSelectedRow();
        if (row) {
            var hDialog = top.jQuery.hDialog({
                title: '流程单', width: 1300, height: 700, href: '/JydOrder/html/JydOrderlcd.html', iconCls: 'icon-save',
                onLoad: function () {
                    //init();//初始化
                    top.$('#uiform').form('load', row);

                    top.$.ajax({
                        type: "POST",
                        url: '/JydOrderDetail/ashx/JydOrderDetailHandler.ashx?filter={"groupOp":"AND","rules":[{"field":"orderid","op":"eq","data":"' + row.OrderID + '"}],"groups":[]}',
                        data: {},
                        dataType: "json",
                        beforeSend: function () { },
                        complete: function () { },
                        success: function (d) {
                            //console.log("加载字表数据：" + JSON.stringify(d));
                            //console.log("字表个数：" + d.rows.length);

                            for (var i = 1; i <= d.rows.length; i++) {
                                //console.log("渲染次数：" + i + "值是:" + JSON.stringify(d.rows[i]));
                                //top.$('.mymy').form('load', d.rows[i]);
                                init();//渲染完成

                                //top.$('.mymy').eq(i).form('load', d.rows[i]);

                            }

                            for (var i = 0; i < d.rows.length; i++) {

                                top.$('.KeyId').eq(i).val(d.rows[i].KeyId);
                                top.$('.orderid').eq(i).textbox('setValue', d.rows[i].orderid);
                                top.$('.omainid').eq(i).textbox('setValue', d.rows[i].omainid);
                                //top.$('.yspmc').eq(i).textbox('setValue', d.rows[i].yspmc);
                                top.$('.yspmcshow').eq(i).text(d.rows[i].yspmc);
                                //top.$('.PrintType').eq(i).textbox('setValue', d.rows[i].PrintType);
                                top.$('.PrintTypeshow').eq(i).text(d.rows[i].PrintType);//印刷品类型

                                //top.$('.cpcc').eq(i).textbox('setValue', d.rows[i].cpcc);
                                top.$('.cpccshow').eq(i).text(d.rows[i].cpcc);//成品尺寸
                                //top.$('.zzf').eq(i).textbox('setValue', d.rows[i].zzf);
                                top.$('.zzfshow').eq(i).text(d.rows[i].zzf);//纸张
                                top.$('.zzn').eq(i).textbox('setValue', d.rows[i].zzn);
                                //top.$('.ym').eq(i).textbox('setValue', d.rows[i].ym);
                                top.$('.ymshow').eq(i).text(d.rows[i].ym);//页码
                                //top.$('.sj').eq(i).textbox('setValue', d.rows[i].sj);
                                top.$('.sjshow').eq(i).text(d.rows[i].sj);//颜色
                                //top.$('.rj').eq(i).textbox('setValue', d.rows[i].rj);
                                //top.$('.jp').eq(i).textbox('setValue', d.rows[i].jp);
                                //top.$('.zz').eq(i).val(d.rows[i].zz);
                                if (d.rows[i].zz == "自带") {
                                    //top.$('.zz').eq(i).attr("checked", true);
                                    //console.log("判断纸张自带！");
                                    var pt = top.$('.khzdshow').eq(i).text();
                                    top.$('.khzdshow').eq(i).text(pt + "纸张");
                                }
                                if (d.rows[i].jp == "自带") {
                                    //top.$('.jp').eq(i).attr("checked", true);
                                    //console.log("判断纸张自带！");
                                    var pt = top.$('.khzdshow').eq(i).text();
                                    top.$('.khzdshow').eq(i).text(pt + "CPT");
                                }
                                if (d.rows[i].rj == "自带") {
                                    //top.$('.rj').eq(i).attr("checked", true);
                                    var pt = top.$('.khzdshow').eq(i).text();
                                    top.$('.khzdshow').eq(i).text(pt + "软件");
                                    //console.log("判断纸张自带！");
                                }
                                //top.$('.sl').eq(i).textbox('setValue', d.rows[i].sl);
                                top.$('.slshow').eq(i).text(d.rows[i].sl);//数量
                                top.$('.cc').eq(i).textbox('setValue', d.rows[i].cc);
                                top.$('.dj').eq(i).textbox('setValue', d.rows[i].dj);
                                top.$('.je').eq(i).textbox('setValue', d.rows[i].je);
                                top.$('.note').eq(i).textbox('setValue', d.rows[i].note);
                                //top.$('.kzcc').eq(i).textbox('setValue', d.rows[i].kzcc);
                                //top.$('.zs').eq(i).textbox('setValue', d.rows[i].zs);
                                //top.$('.fs').eq(i).textbox('setValue', d.rows[i].fs);
                                //top.$('.pss').eq(i).textbox('setValue', d.rows[i].pss);
                                //top.$('.pst').eq(i).textbox('setValue', d.rows[i].pst);

                                top.$('.fbyq').eq(i).combobox({
                                    url: '/sys/ashx/dichandler.ashx?categoryId=100',
                                    limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
                                    //required: true,
                                    valueField: 'Title',
                                    textField: 'Title',
                                    onLoadSuccess: function () {

                                    }
                                });
                                top.$('.fbyq').eq(i).combobox('setValue', d.rows[i].fbyq);

                                top.$('.jx').eq(i).combobox({
                                    url: '/sys/ashx/dichandler.ashx?categoryId=101',
                                    limitToList: true,//设置为true时，输入的值只能是列表框中的内容。
                                    //required: true,
                                    valueField: 'Title',
                                    textField: 'Title',
                                    onLoadSuccess: function () {

                                    }
                                });
                                top.$('.jx').eq(i).combobox('setValue', d.rows[i].jx);


                                //top.$('.jx').eq(i).textbox('setValue', d.rows[i].jx);
                                top.$('.fumoa').eq(i).textbox('setValue', d.rows[i].fumoa);
                                top.$('.fumob').eq(i).textbox('setValue', d.rows[i].fumob);
                                top.$('.yaa').eq(i).textbox('setValue', d.rows[i].yaa);
                                top.$('.yab').eq(i).textbox('setValue', d.rows[i].yab);
                                top.$('.tana').eq(i).textbox('setValue', d.rows[i].tana);
                                top.$('.tanb').eq(i).textbox('setValue', d.rows[i].tanb);
                                top.$('.zheye').eq(i).textbox('setValue', d.rows[i].zheye);
                                top.$('.uva').eq(i).textbox('setValue', d.rows[i].uva);
                                top.$('.uvb').eq(i).textbox('setValue', d.rows[i].uvb);
                                top.$('.zd').eq(i).textbox('setValue', d.rows[i].zd);
                                top.$('.db').eq(i).textbox('setValue', d.rows[i].db);
                                top.$('.bh').eq(i).textbox('setValue', d.rows[i].bh);
                                top.$('.sh').eq(i).textbox('setValue', d.rows[i].sh);
                                top.$('.sk').eq(i).textbox('setValue', d.rows[i].sk);
                                top.$('.sjwcsj').eq(i).textbox('setValue', d.rows[i].sjwcsj);
                                top.$('.cpwcsj').eq(i).textbox('setValue', d.rows[i].cpwcsj);
                                top.$('.ctpsj').eq(i).textbox('setValue', d.rows[i].ctpsj);
                                top.$('.sbwcsj').eq(i).textbox('setValue', d.rows[i].sbwcsj);
                                top.$('.qslsj').eq(i).textbox('setValue', d.rows[i].qslsj);
                                top.$('.yswcsj').eq(i).textbox('setValue', d.rows[i].yswcsj);
                                top.$('.hjgkssj').eq(i).textbox('setValue', d.rows[i].hjgkssj);
                                top.$('.hjgwcsj').eq(i).textbox('setValue', d.rows[i].hjgwcsj);
                                top.$('.wjgjcsj').eq(i).textbox('setValue', d.rows[i].wjgjcsj);
                                top.$('.qcpsj').eq(i).textbox('setValue', d.rows[i].qcpsj);
                                top.$('.zdkssj').eq(i).textbox('setValue', d.rows[i].zdkssj);
                                top.$('.dzwcsj').eq(i).textbox('setValue', d.rows[i].dzwcsj);
                                top.$('.shryccsj').eq(i).textbox('setValue', d.rows[i].shryccsj);
                                top.$('.shryhcsj').eq(i).textbox('setValue', d.rows[i].shryhcsj);
                                top.$('.dbsj').eq(i).textbox('setValue', d.rows[i].dbsj);
                                top.$('.rksj').eq(i).textbox('setValue', d.rows[i].rksj);
                                top.$('.cksj').eq(i).textbox('setValue', d.rows[i].cksj);
                                top.$('.zrr').eq(i).textbox('setValue', d.rows[i].zrr);
                                top.$('.kkyy').eq(i).textbox('setValue', d.rows[i].kkyy);
                                top.$('.sxj').eq(i).textbox('setValue', d.rows[i].sxj);
                                top.$('.szp').eq(i).textbox('setValue', d.rows[i].szp);
                                top.$('.zph').eq(i).textbox('setValue', d.rows[i].zph);
                                top.$('.myws').eq(i).textbox('setValue', d.rows[i].myws);
                                top.$('.myysf').eq(i).textbox('setValue', d.rows[i].myysf);
                                top.$('.O_myhjg').eq(i).textbox('setValue', d.rows[i].O_myhjg);
                                top.$('.qianshou').eq(i).textbox('setValue', d.rows[i].qianshou);
                                top.$('.skje').eq(i).textbox('setValue', d.rows[i].skje);
                                top.$('.wsjgbz').eq(i).textbox('setValue', d.rows[i].wsjgbz);
                                top.$('.dddd').eq(i).textbox('setValue', d.rows[i].dddd);
                                top.$('.kkkje').eq(i).textbox('setValue', d.rows[i].kkkje);
                                top.$('.yspmcb').eq(i).textbox('setValue', d.rows[i].yspmcb);
                                top.$('.FileTypeb').eq(i).textbox('setValue', d.rows[i].FileTypeb);
                                top.$('.mianb').eq(i).textbox('setValue', d.rows[i].mianb);
                                top.$('.munb').eq(i).textbox('setValue', d.rows[i].munb);
                                top.$('.colorb').eq(i).textbox('setValue', d.rows[i].colorb);
                                top.$('.pagerb').eq(i).textbox('setValue', d.rows[i].pagerb);
                                top.$('.O_my_jpb').eq(i).textbox('setValue', d.rows[i].O_my_jpb);
                                top.$('.O_my_zzb').eq(i).textbox('setValue', d.rows[i].O_my_zzb);
                                top.$('.ddddb').eq(i).textbox('setValue', d.rows[i].ddddb);
                                top.$('.zznb').eq(i).textbox('setValue', d.rows[i].zznb);
                                top.$('.ymb').eq(i).textbox('setValue', d.rows[i].ymb);
                                top.$('.sjb').eq(i).textbox('setValue', d.rows[i].sjb);
                                top.$('.rjb').eq(i).textbox('setValue', d.rows[i].rjb);
                                top.$('.jpb').eq(i).textbox('setValue', d.rows[i].jpb);
                                top.$('.zzb').eq(i).textbox('setValue', d.rows[i].zzb);
                                top.$('.fumoab').eq(i).textbox('setValue', d.rows[i].fumoab);
                                top.$('.fumobb').eq(i).textbox('setValue', d.rows[i].fumobb);
                                top.$('.yaab').eq(i).textbox('setValue', d.rows[i].yaab);
                                top.$('.yabb').eq(i).textbox('setValue', d.rows[i].yabb);
                                top.$('.tanab').eq(i).textbox('setValue', d.rows[i].tanab);
                                top.$('.tanbb').eq(i).textbox('setValue', d.rows[i].tanbb);
                                top.$('.zheyeb').eq(i).textbox('setValue', d.rows[i].zheyeb);
                                top.$('.uvab').eq(i).textbox('setValue', d.rows[i].uvab);
                                top.$('.uvbb').eq(i).textbox('setValue', d.rows[i].uvbb);
                                top.$('.zdb').eq(i).textbox('setValue', d.rows[i].zdb);
                                top.$('.dbb').eq(i).textbox('setValue', d.rows[i].dbb);
                                top.$('.bhb').eq(i).textbox('setValue', d.rows[i].bhb);
                                top.$('.shb').eq(i).textbox('setValue', d.rows[i].shb);
                                top.$('.skb').eq(i).textbox('setValue', d.rows[i].skb);
                                top.$('.PrintTypeb').eq(i).textbox('setValue', d.rows[i].PrintTypeb);
                                top.$('.zzfb').eq(i).textbox('setValue', d.rows[i].zzfb);
                                top.$('.noteb').eq(i).textbox('setValue', d.rows[i].noteb);
                                top.$('.cpccb').eq(i).textbox('setValue', d.rows[i].cpccb);
                                top.$('.printyes').eq(i).textbox('setValue', d.rows[i].printyes);
                                top.$('.kzccb').eq(i).textbox('setValue', d.rows[i].kzccb);
                                top.$('.zsb').eq(i).textbox('setValue', d.rows[i].zsb);
                                top.$('.fsb').eq(i).textbox('setValue', d.rows[i].fsb);
                                top.$('.pssb').eq(i).textbox('setValue', d.rows[i].pssb);
                                top.$('.pstb').eq(i).textbox('setValue', d.rows[i].pstb);
                                top.$('.fbyqb').eq(i).textbox('setValue', d.rows[i].fbyqb);
                                top.$('.jxb').eq(i).textbox('setValue', d.rows[i].jxb);
                                top.$('.kkkjeb').eq(i).textbox('setValue', d.rows[i].kkkjeb);
                                top.$('.nysl').eq(i).textbox('setValue', d.rows[i].nysl);
                                top.$('.ctpa').eq(i).textbox('setValue', d.rows[i].ctpa);
                                top.$('.ctpb').eq(i).textbox('setValue', d.rows[i].ctpb);
                                top.$('.zhizhangbb').eq(i).textbox('setValue', d.rows[i].zhizhangbb);
                                top.$('.songhuo').eq(i).textbox('setValue', d.rows[i].songhuo);
                                //top.$('.xiaoyang').eq(i).textbox('setValue', d.rows[i].xiaoyang);//小样
                                //top.$('.danwei_m').eq(i).textbox('setValue', d.rows[i].danwei_m);
                                top.$('.danwei_mshow').eq(i).text(d.rows[i].danwei_m); //单位

                                top.$('.danwei_mb').eq(i).textbox('setValue', d.rows[i].danwei_mb);
                                top.$('.bz_hx').eq(i).textbox('setValue', d.rows[i].bz_hx);
                                top.$('.bz_wbz_cc').eq(i).textbox('setValue', d.rows[i].bz_wbz_cc);
                                top.$('.bz_wbz_gy').eq(i).textbox('setValue', d.rows[i].bz_wbz_gy);
                                top.$('.bz_nbz_zz').eq(i).textbox('setValue', d.rows[i].bz_nbz_zz);
                                top.$('.bz_nbz_cc').eq(i).textbox('setValue', d.rows[i].bz_nbz_cc);
                                top.$('.bz_nbz_gy').eq(i).textbox('setValue', d.rows[i].bz_nbz_gy);
                                top.$('.bz_wkz_zz').eq(i).textbox('setValue', d.rows[i].bz_wkz_zz);
                                top.$('.bz_wkz_cc').eq(i).textbox('setValue', d.rows[i].bz_wkz_cc);
                                top.$('.bz_wkz_gy').eq(i).textbox('setValue', d.rows[i].bz_wkz_gy);
                                top.$('.bz_bc_yl').eq(i).textbox('setValue', d.rows[i].bz_bc_yl);
                                top.$('.bz_bc_sl').eq(i).textbox('setValue', d.rows[i].bz_bc_sl);
                                top.$('.bz_bc_mx').eq(i).textbox('setValue', d.rows[i].bz_bc_mx);
                                top.$('.bz_nt').eq(i).textbox('setValue', d.rows[i].bz_nt);
                                top.$('.bz_nt_cc').eq(i).textbox('setValue', d.rows[i].bz_nt_cc);
                                top.$('.bz_nt_mx').eq(i).textbox('setValue', d.rows[i].bz_nt_mx);
                                top.$('.bz_wbz').eq(i).textbox('setValue', d.rows[i].bz_wbz);

                                //加载完后渲染

                            }
                            top.$(".kzcc,.zs,.fs,.pss,.pst").textbox({});//开纸尺寸,正数,放数,色,套
                            initlc();//加载流程--完了之后加载流程


                        }
                    });

                    //top.$('.mymy').form('load', {comname:'北京'});

                    //top.$('#txt_KeyId').numberspinner('setValue', row.KeyId);
                    //top.$('#txt_ServiceType').numberspinner('setValue', row.ServiceType);
                    //top.$('#txt_OrderYear').textbox('setValue', row.OrderYear);
                    //top.$('#txt_OrderInt').numberspinner('setValue', row.OrderInt);
                    //top.$('#txt_OrderID').textbox('setValue', row.OrderID);
                    //top.$('#txt_username').textbox('setValue', row.username);
                    //top.$('#txt_comname').textbox('setValue', row.comname);
                    //top.$('#txt_comnameid').numberspinner('setValue', row.comnameid);
                    //top.$('#txt_jpm').textbox('setValue', row.jpm);
                    //top.$('#txt_name').textbox('setValue', row.name);
                    //top.$('#txt_tel').textbox('setValue', row.tel);
                    //top.$('#txt_address').textbox('setValue', row.address);
                    //top.$('#txt_explain').textbox('setValue', row.explain);
                    //top.$('#txt_deliveryDate').datetimebox('setValue', row.deliveryDate);
                    //top.$('#txt_delivery').textbox('setValue', row.delivery);
                    //top.$('#txt_adddate').datetimebox('setValue', row.adddate);
                    //top.$('#txt_fk').numberspinner('setValue', row.fk);
                    //top.$('#txt_yf').numberspinner('setValue', row.yf);
                    //top.$('#txt_qt').numberspinner('setValue', row.qt);
                    //top.$('#txt_totalprice').numberspinner('setValue', row.totalprice);
                    //top.$('#txt_cfzt').textbox('setValue', row.cfzt);
                    //top.$('#txt_jjy').textbox('setValue', row.jjy);
                    //top.$('#txt_fumoa').textbox('setValue', row.fumoa);
                    //top.$('#txt_fumob').textbox('setValue', row.fumob);
                    //top.$('#txt_yaa').textbox('setValue', row.yaa);
                    //top.$('#txt_yab').textbox('setValue', row.yab);
                    //top.$('#txt_tana').textbox('setValue', row.tana);
                    //top.$('#txt_tanb').textbox('setValue', row.tanb);
                    //top.$('#txt_zheye').textbox('setValue', row.zheye);
                    //top.$('#txt_uva').textbox('setValue', row.uva);
                    //top.$('#txt_uvb').textbox('setValue', row.uvb);
                    //top.$('#txt_zd').textbox('setValue', row.zd);
                    //top.$('#txt_db').textbox('setValue', row.db);
                    //top.$('#txt_bh').textbox('setValue', row.bh);
                    //top.$('#txt_sh').textbox('setValue', row.sh);
                    //top.$('#txt_sk').textbox('setValue', row.sk);
                    //top.$('#txt_huofa').textbox('setValue', row.huofa);
                    //top.$('#txt_fahuoren').textbox('setValue', row.fahuoren);
                    //top.$('#txt_ys').textbox('setValue', row.ys);
                    //top.$('#txt_fy').textbox('setValue', row.fy);
                    //top.$('#txt_dha').textbox('setValue', row.dha);
                    //top.$('#txt_dhb').textbox('setValue', row.dhb);
                    //top.$('#txt_status').textbox('setValue', row.status);
                    //top.$('#txt_jexx').textbox('setValue', row.jexx);
                    //top.$('#txt_jedx').textbox('setValue', row.jedx);
                    //top.$('#txt_yufuk').numberspinner('setValue', row.yufuk);
                    //top.$('#txt_yufukuana').textbox('setValue', row.yufukuana);
                    //top.$('#txt_sckd').textbox('setValue', row.sckd);
                    //top.$('#txt_sjwcsj').textbox('setValue', row.sjwcsj);
                    //top.$('#txt_cpwcsj').textbox('setValue', row.cpwcsj);
                    //top.$('#txt_ctpsj').textbox('setValue', row.ctpsj);
                    //top.$('#txt_sbwcsj').textbox('setValue', row.sbwcsj);
                    //top.$('#txt_qslsj').textbox('setValue', row.qslsj);
                    //top.$('#txt_yswcsj').textbox('setValue', row.yswcsj);
                    //top.$('#txt_hjgkssj').textbox('setValue', row.hjgkssj);
                    //top.$('#txt_hjgwcsj').textbox('setValue', row.hjgwcsj);
                    //top.$('#txt_wjgjcsj').textbox('setValue', row.wjgjcsj);
                    //top.$('#txt_qcpsj').textbox('setValue', row.qcpsj);
                    //top.$('#txt_zdkssj').textbox('setValue', row.zdkssj);
                    //top.$('#txt_dzwcsj').textbox('setValue', row.dzwcsj);
                    //top.$('#txt_shryccsj').textbox('setValue', row.shryccsj);
                    //top.$('#txt_shryhcsj').textbox('setValue', row.shryhcsj);
                    //top.$('#txt_dbsj').textbox('setValue', row.dbsj);
                    //top.$('#txt_szp').textbox('setValue', row.szp);
                    //top.$('#txt_sxj').textbox('setValue', row.sxj);
                    //top.$('#txt_skje').numberspinner('setValue', row.skje);
                    //top.$('#txt_zph').textbox('setValue', row.zph);
                    //top.$('#txt_kkyy').textbox('setValue', row.kkyy);
                    //top.$('#txt_zrr').textbox('setValue', row.zrr);
                    //top.$('#txt_rksj').textbox('setValue', row.rksj);
                    //top.$('#txt_cksj').textbox('setValue', row.cksj);
                    //top.$('#txt_myws').textbox('setValue', row.myws);
                    //top.$('#txt_myysf').textbox('setValue', row.myysf);
                    //top.$('#txt_O_myhjg1').textbox('setValue', row.O_myhjg1);
                    //top.$('#txt_jgzysx').textbox('setValue', row.jgzysx);
                    //top.$('#txt_connman').textbox('setValue', row.connman);
                    //top.$('#txt_mystatus').numberspinner('setValue', row.mystatus);
                    //top.$('#txt_kkkje').numberspinner('setValue', row.kkkje);
                    //top.$('#txt_ds').numberspinner('setValue', row.ds);
                    //top.$('#txt_allyspmc').textbox('setValue', row.allyspmc);
                    //top.$('#txt_$item.colAttribute').val(row.$item.colAttribute);//$item.coltitle
                    //top.$('.kindeditor').kindeditor();//初始化kingdeditor编辑器
                    //注意 如果控件被EasyUI初始化过，赋值的方法有所改变 下面提供几个例子。程序员根据情况改动一下
                    //$('#nn').numberbox('setValue', 206.12);
                    //$('#nn').textbox('setValue',1);
                    //$('#cc').combobox('setValues', ['001','002']);
                    //$('#dt').datetimebox('setValue', '6/1/2012 12:30:56');    
                },
                submit: function () {
                    //alert(top.$("#uiform").form('enableValidation').form('validate'));
                    //alert(top.$("#uiform").form('validate'));
                    //原来用的是这种方法 alert(top.$('#uiform').validate().form());
                    if (top.$("#uiform").form('validate')) {

                        var query = createParam('edit', row.KeyId);
                        console.log("编辑时query:" + query);
                        //alert(query);


                        var temp = top.$(".mymy :input").serializeArray();//serializeArray 要么选择form 要么 $(".mymy :input") 这样选择
                        //先循环mymy下所有html元素
                        var mymylenth = top.$(".mymy").length;//得到有几个.mymy
                        //alert(mymylenth);
                        var detailjson = "[";
                        var j = 1;

                        top.$(".mymy").each(function () {
                            var temp2 = top.$(this).find(":input").serializeArray();
                            var jsonstr = JSON.stringify(convertArrayNew(temp2));


                            if (j < mymylenth) {
                                detailjson = detailjson + '{"d":' + jsonstr + '},';
                            } else {
                                detailjson = detailjson + '{"d":' + jsonstr + '}';

                            }


                            //alert(top.$(this).html())
                            j = j + 1;
                            //alert("转化后是："+JSON.stringify(convertArray(temp2)));
                        });
                        detailjson = detailjson + ']';

                        console.log("得到的json数据是：" + detailjson);


                        jQuery.ajaxjson(actionURL, query + "&d=" + detailjson, function (d) {
                            if (parseInt(d) > 0) {
                                msg.ok('修改成功！');
                                hDialog.dialog('close');
                                grid.reload();
                            } else {
                                MessageOrRedirect(d);
                            }
                        });
                    }
                    //msg.warning('请填写必填项！');
                    return false;
                }
            });

        } else {
            msg.warning('请选择要修改的行。');
        }
    },
    del: function () {
        var row = grid.getSelectedRow();
        if (row) {
            if (confirm('确认要执行删除操作吗？')) {
                var rid = row.KeyId;
                jQuery.ajaxjson(actionURL, createParam('delete', rid), function (d) {
                    if (parseInt(d) > 0) {
                        msg.ok('删除成功！');
                        grid.reload();
                    } else {
                        MessageOrRedirect(d);
                    }
                });
            }
        } else {
            msg.warning('请选择要删除的行。');
        }
    }
};

