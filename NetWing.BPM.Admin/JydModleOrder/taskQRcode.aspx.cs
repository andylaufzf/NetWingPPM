﻿using System;
using NetWing.BPM.Admin.weixin;
using NetWing.Common;
using NetWing.Common.Data.SqlServer;
using NetWing.Model;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Web;

namespace NetWing.BPM.Admin.JydModleOrder
{
    public partial class taskQRcode : System.Web.UI.Page
    {
        


        protected string KeyId;
        public DataRow dr = null;
        public DataTable detailDt = null;//子订单DT

        public DataTable lcDt = null;
        public DataTable dt = null;
        public int isyg = 0;
        protected string myopid;

        protected void Page_Load(object sender, EventArgs e)
        {

            //清除cookie缓存
            CookieHelper.ClearUserCookie("", "openid");
            myopid = CookieHelper.GetCookie("openid");
            if (!string.IsNullOrEmpty(myopid))
            {
                string sql = ("select * from Sys_Users where openid='" + myopid + "'");

                string sql1 = ("select * from jydUser where openid='" + myopid + "'");
                DataRow dp = SqlEasy.ExecuteDataRow(sql);//员工绑定
                                                         //DataRow op = SqlEasy.ExecuteDataRow(sql1);

                if (dp == null)//如果没绑定，则提示去绑定
                {
                    Response.Redirect("" + ConfigHelper.GetValue("website") + "/weixin/bindyg.aspx");
                }
                else//如果绑定，
                {
                    CookieHelper.WriteCookie("currentUserID", dp["keyid"].ToString());//登录成功,吧用户ID写入cookies
                    CookieHelper.WriteCookie("currentUserName", HttpUtility.UrlEncode(dp["username"].ToString())); //写cookies 当前用户
                }
            }
            else
            {
                //当用户访问dingd.aspx 页面时,系统就会自动获得openid并返回当前页面
                string reurl = Request.Url.ToString();//获取当前url
                CookieHelper.WriteCookie("returnurl", reurl);//把返回信息写入cookies
                bool r = wxhelper.checkLogin();//wxhelper.checkLogin() 该函数会自动去获得用户openid 获得openid后需要返回当前入口页面 reurl 就是获得openid后的入口页面

                string kk = "";
            }

            //判断只有员工才可以访问
            string oid = CookieHelper.GetCookie("openid");
            isyg = (int)SqlEasy.ExecuteScalar("select count(keyid) from sys_users where openid='" + oid + "'");
            if (isyg <= 0)//如果不是员工
            {
                Response.Redirect("/weixin/error.aspx?msgs=只有我公司员工才能访问本页!&type=warn");
            }





            KeyId = Request["KeyId"];
            if (!string.IsNullOrEmpty(KeyId))
            {
                dt = SqlEasy.ExecuteDataTable("select top 1 * from jydOrder where orderid in (select orderid from jydOrderDetail) and KeyId=" + KeyId + "");
                //永远只可能有一行 主订单DR
                dr = SqlEasy.ExecuteDataRow("select * from jydOrder where KeyId=" + KeyId + "");
                detailDt = SqlEasy.ExecuteDataTable("select * from jydOrderDetail where orderid='" + dr["orderid"].ToString() + "'");
                lcDt = SqlEasy.ExecuteDataTable("select * from jydDo where orderid='" + dr["orderid"].ToString() + "'");

            }
        }

    }
    
}