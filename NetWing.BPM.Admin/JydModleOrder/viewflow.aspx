﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="viewflow.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.viewflow" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>

<!DOCTYPE html>
<html>
  <head>
    <title>审核</title>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.
">

      <link href="dist/lib/weui.min.css" rel="stylesheet" />
      <link href="dist/css/jquery-weui.css" rel="stylesheet" />
  </head>

  <body ontouchstart>


    <header class='demos-header'>
     
    </header>
       <%if (data >= 1)
                {%>
    <%foreach (DataRow dataRow in Dt.Rows) {%>
    <div class="weui-form-preview">
      <div class="weui-form-preview__hd">
        <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">付款金额</label>
          <em class="weui-form-preview__value"><%=dataRow["buyAllMoney"].ToString() %></em>
        </div>
      </div>
      <div class="weui-form-preview__bd">
        <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">商品</label>
          <span class="weui-form-preview__value"><%=dataRow["edNumber"].ToString() %><%=dataRow["goodsNo"].ToString() %></span>
        </div>
        <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">商品数量</label>
          <span class="weui-form-preview__value"><%=dataRow["buyNum"].ToString() %></span>
        </div>
        <div class="weui-form-preview__item">
          <label class="weui-form-preview__label">备注</label>
          <span class="weui-form-preview__value"><%=dataRow["note"].ToString() %></span>
        </div>
      </div>
      <div class="weui-form-preview__ft">
        <a class="weui-form-preview__btn weui-form-preview__btn_default" href="javascript:" onclick="butongguo(<%=dataRow["keyid"].ToString() %>)">不通过</a>
        <a class="weui-form-preview__btn weui-form-preview__btn_primary" href="javascript:" onclick="tongguo(<%=dataRow["keyid"].ToString() %>)">通过</a>
      </div>
    </div>

      <br />
      <%} %>
       <%}
                else
                {%>
            <div>

                
                <div class="weui-msg">
                    <div class="weui-msg__icon-area"><i class="weui-icon-warn weui-icon_msg-primary"></i></div>
                    <div class="weui-msg__text-area">
                        <h2 class="weui-msg__title">出去了,这里只有老总能看</h2>
                    </div>
                    <div class="weui-msg__opr-area">
                        <p class="weui-btn-area">
                            <a href="/JydModleOrder/JydWapindex.aspx" class="weui-btn weui-btn_primary">首页</a>
                        </p>
                    </div>
                    <div class="weui-msg__extra-area">
                        <div class="weui-footer">
                            <p class="weui-footer__links">
                                <a href="javascript:void(0);" class="weui-footer__link"><%=ConfigHelper.GetValue("systemName") %></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <%} %>

      <script src="dist/lib/jquery-2.1.4.js"></script>
      <script src="dist/lib/fastclick.js"></script>
      <script src="dist/js/jquery-weui.js"></script>
      <script>
          function butongguo(id) {
               $.showLoading("数据加载中");
                $.ajax({
                    url: "/JydModleOrder/ashx/ajax_submit.ashx",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        action: 'butongguo',
                        keyid: id
                    },
                    
                    success: function (e) {
                        console.log(e);
                       $.hideLoading();
                        if (e.keyid == 0) {
                            $.alert(e.msg);
                            window.location.href = window.location.href+"?id="+10000*Math.random();
                        } else {
                             $.alert(e.msg);
                        }
                    }

                });
          }


          function tongguo(id) {
              $.showLoading("数据加载中");
                $.ajax({
                    url: "/JydModleOrder/ashx/ajax_submit.ashx",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        action: 'tongguo',
                        keyid: id
                    },
                    success: function (e) {
                        console.log(e);
                        $.hideLoading();
                        if (e.keyid == 0) {
                            $.alert(e.msg);
                            window.location.href = window.location.href+"?id="+10000*Math.random();
                        } else {                     
                             $.alert(e.msg);
                        }
                    }
                });
            }
      </script>
  </body>
</html>

