﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JydWapindex.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.JydWapindex" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
    <title>聚源达彩印印刷厂</title>
    <link href="dist/lib/weui.min.css" rel="stylesheet" />
    <link href="dist/css/jquery-weui.css" rel="stylesheet" />
    <link href="dist/demos/css/demos.css" rel="stylesheet" />
    <link href="../scripts/layui/css/layui.css" rel="stylesheet" />
    <script src="../scripts/layui/layui.all.js"></script>

    <style>
        .toux {
            width: 90px;
            height: 90px;
            overflow: hidden;
            border-radius: 50%;
            margin: 0 0 0 5%;
            float: left;
        }

            .toux img {
                width: 100%;
                display: block;
            }

        .dd_text {
            width: 60%;
            float: left;
            margin: 0 0 0 5%;
        }

            .dd_text span {
                margin: 1% 0 0 0;
                width: 100%;
                float: left;
                font-size: 14px;
                color: #fff;
            }

            .dd_text em {
                margin: 1% 0 1% 0;
                width: 100%;
                float: left;
                font-size: 12px;
                color: #fff;
                font-weight: normal;
                font-style: normal;
            }

            .dd_text b {
                width: 100%;
                float: left;
                font-size: 12px;
                color: #fff;
                font-weight: normal;
                font-style: normal;
            }
    </style>
</head>
<body ontouchstart>


    <form id="form1" runat="server">
        <header class='demos-header' style="background: #374B6F;">
            <%string tx = CookieHelper.GetCookie("headimgurl"); %>
            <%string nc = CookieHelper.GetCookie("nickname"); %>
            <div class="toux">
                <img src="<%=tx %>" />
            </div>
            <div class="dd_text">
                <span>简介:尊敬的客户.您好,您需要绑定手机号,才可查询到你的所有订单</span>
                <br />
                <em>订单数量:<%=de %></em>
                <%--<b>订单号:</b>--%>
            </div>
        </header>

        <div class="weui-grids">
            <%if (dww >= 1)
                {%>

            <a onclick="layer.msg('您已成功绑定!');" class="weui-grid js_grid">
                <div class="weui-grid__icon">
                    <img src="img/khbd.png" alt="您已绑定">
                </div>
                <p class="weui-grid__label">
                    您已绑定
       
                </p>
            </a>

            <%}
                else
                {%>
            <a href="/weixin/kehbd.aspx" class="weui-grid js_grid">
                <div class="weui-grid__icon">
                    <img src="img/khbd.png" alt="员工绑定">
                </div>
                <p class="weui-grid__label">
                    客户绑定
       
                </p>
            </a>
            <%} %>


            <%--<a href="JydOrderFrom.aspx" class="weui-grid js_grid">
                <div class="weui-grid__icon">
                    <img src="img/dan.png" alt="客户下单">
                </div>
                <p class="weui-grid__label">
                    客户下单
       
                </p>
            </a>--%>


            <a href="myOrder.aspx" class="weui-grid js_grid">
                <div class="weui-grid__icon">
                    <img src="img/wddd.png" alt="我的订单">
                </div>
                <p class="weui-grid__label">
                    我的订单
       
                </p>
            </a>
            <%if (dw >= 1)
                {%>
            <a onclick="layer.msg('您已成功绑定!');" class="weui-grid js_grid">
                <div class="weui-grid__icon">
                    <img src="img/ygbd.png" alt="您已绑定">
                </div>
                <p class="weui-grid__label">
                    您已绑定
       
                </p>
            </a>


            <%}
                else
                {%>
            <a href="/weixin/bindyg.aspx" class="weui-grid js_grid">
                <div class="weui-grid__icon">
                    <img src="img/ygbd.png" alt="员工绑定">
                </div>
                <p class="weui-grid__label">
                    员工绑定
                </p>
            </a>

            <%} %>
            <%--<a href="JydOrdercx.aspx" class="weui-grid js_grid">
                <div class="weui-grid__icon">
                    <img src="img/cha.png" alt="查询订单">
                </div>
                <p class="weui-grid__label">
                    查询订单
       
                </p>
            </a>--%>
            <%if (dw >= 1)
                {%>
            <a href="lopjh.aspx" class="weui-grid js_grid">
                <div class="weui-grid__icon">
                    <img src="img/wddd.png" alt="任务待办">
                </div>
                <p class="weui-grid__label">
                    任务待办
       
                </p>
            </a>
            <%} %>
            <%if (sh != 0)/*只有超级管理员才能审核*/
                {%>
            <a href="viewflow.aspx" class="weui-grid js_grid">
                <div class="weui-grid__icon">
                    <img src="img/cha.png" alt="采购审核">
                </div>
                <p class="weui-grid__label">
                    采购审核
       
                </p>
            </a>
            <%} %>

            <a href="gcguanlizhe.aspx" class="weui-grid js_grid">
                <div class="weui-grid__icon">
                    <img src="img/gongch.png" alt="主管处理">
                </div>
                <p class="weui-grid__label">
                    主管处理
                </p>
            </a>

        </div>

        <script src="dist/lib/jquery-2.1.4.js"></script>
        <script src="dist/lib/fastclick.js"></script>
        <script src="dist/js/jquery-weui.js"></script>

    </form>
</body>
</html>
