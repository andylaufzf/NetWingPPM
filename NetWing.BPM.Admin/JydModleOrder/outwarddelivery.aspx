﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="outwarddelivery.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.outwarddelivery" %>
<!DOCTYPE html>
<html>
  <head>
    <title>外加实收</title>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<meta name="description" content="Write an awesome description for your new site here. You can edit this line in _config.yml. It will appear in your document head meta (for Google search results) and in your feed.xml site description.">
      <link href="dist/lib/weui.min.css" rel="stylesheet" />
      <link href="dist/css/jquery-weui.css" rel="stylesheet" />
  </head>

  <body ontouchstart>


    <header class='demos-header'>
      
    </header>



    <div class="weui-cells__title">外协单</div>
    <div class="weui-cells weui-cells_form">
      <div class="weui-cell weui-cell_warn">
        <div class="weui-cell__hd"><label for="" class="weui-label">加工厂</label></div>
        <div class="weui-cell__bd">
          <input class="weui-input" readonly="readonly" type="number" pattern="[0-9]*" value="weui input error" placeholder="<%=myws %>">
        </div>
      </div>
    </div>
      <div class="weui-cells weui-cells_form">
      <div class="weui-cell weui-cell_warn">
        <div class="weui-cell__hd"><label for="" class="weui-label">印刷品</label></div>
        <div class="weui-cell__bd">
          <input class="weui-input" readonly="readonly" type="number" pattern="[0-9]*" value="weui input error" placeholder="<%=myzzxm %>">
        </div>
      </div>
    </div>
      <div class="weui-cells weui-cells_form">
      <div class="weui-cell weui-cell_warn">
        <div class="weui-cell__hd"><label for="" class="weui-label">交货日期</label></div>
        <div class="weui-cell__bd">
          <input class="weui-input" readonly="readonly" type="number" pattern="[0-9]*" value="weui input error" placeholder="<%=myjhrq %>">
        </div>
      </div>
    </div>

    

    <div class="weui-cells weui-cells_form">
      <div class="weui-cell weui-cell_warn">
        <div class="weui-cell__hd"><label for="" class="weui-label">加工数量</label></div>
        <div class="weui-cell__bd">
          <input class="weui-input" readonly="readonly" type="number" pattern="[0-9]*" value="weui input error" placeholder="<%=mynum %>">
        </div>
      </div>
    </div>
   
       <div class="weui-cells weui-cells_form">
      <div class="weui-cell weui-cell_warn">
        <div class="weui-cell__hd"><label for="" class="weui-label">实收数量</label></div>
        <div class="weui-cell__bd">
            <input class="weui-input" id="actualquantity" name="actualquantity" type="number" pattern="[0-9]*"  placeholder="请输入实收数量">
        </div>
      </div>
    </div>
      <div class="weui-cells__title">实收</div>
     
    <div class="weui-cells weui-cells_form">
      <div class="weui-cell weui-cell_warn">
        <div class="weui-cell__hd"><label for="" class="weui-label">实收汇总</label></div>
        <div class="weui-cell__bd">
            <input class="weui-input" readonly="readonly" readonly="readonly" type="number" placeholder="实收数量:<%=actual_quantity %>">
            <input class="weui-input" readonly="readonly" readonly="readonly" type="number" placeholder="实收时间:<%=receiving_time %>">
            <input class="weui-input" readonly="readonly" readonly="readonly" type="number" placeholder="实收人:<%=mysh %>">
        </div>
      </div>
    </div>
        <div class="weui-btn-area ">
          <a class="weui-btn weui-btn_primary" onclick="actual('<%=yspmc %>','<%=open%>')" href="javascript:" >确定</a>
        </div>
      
       


      <script src="dist/lib/jquery-2.1.4.js"></script>
      <script src="dist/lib/fastclick.js"></script>
      <script src="dist/js/jquery-weui.js"></script>
      <script>
          function actual(keyid, open) {
              console.log(keyid);
              console.log(open);
              //var actualqu = document.getElementById("actualquantity");
              var shuli = $('#actualquantity').val();
              
              if (shuli == null || shuli == undefined || shuli == 0) {
                  $.alert("实收数量不能为空");
                  return;
              }
              document.activeElement.blur();//软键盘收起
              $.ajax({
                  url: '/JydModleOrder/ashx/ajax_submit.ashx',
                  type: 'post',
                  dataType: 'json',
                  data: {
                      action: 'actual',
                      KeyId: keyid,
                      actualquantity: shuli,
                      open: open,
                  },
                  beforeSend: function () {
                      // Handle the beforeSend event
                      //loading层
                     $.showLoading("数据加载中");
                  },
                  success: function (data, textStatus, jqXHR) {
                      $.hideLoading();
                      if (data.code === 0) {
                          $.alert(data.msg);
                          window.location.href= "/JydModleOrder/JydWapindex.aspx";
                      } else {
                          $.alert(data.msg);
                      }
                  },

              });
          }
          
      </script>
  </body>
</html>
