﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QRCodeView.aspx.cs" Inherits="NetWing.BPM.Admin.JydModleOrder.QRCodeView" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.Sql" %>
<%@ Import Namespace="NetWing.Common" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <title>加工产品信息</title>
    <link href="dist/lib/weui.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="dist/css/jquery-weui.min.css" />
    <link href="dist/demos/css/demos.css" rel="stylesheet" />
    <script src="../scripts/jquery-1.10.2.min.js"></script>
    <script src="js/ajax_submit.js"></script>
        <script>
        function ido(s) {//我做
            console.log("我做" + s);
            $.ajax({
                url: '/jyddo/ashx/JydDoHandler.ashx',
                type: 'POST', //GET
                async: true,    //或false,是否异步
                data: {
                    action: 'add',
                    s: s
                },
                timeout: 5000,    //超时时间
                dataType: 'json',    //返回的数据格式：json/xml/html/script/jsonp/text
                success: function (d, textStatus, jqXHR) {
                    if (d >= 1) {
                        window.location.reload();
                    }
                }

            });

        }
    </script>
    <style>
        .weui-form-preview__label {
            color: #000;
            width: 90px;
            margin-right: 0;
            padding-right: 1em;
            text-align: right;
            text-align-last: right;
        }

        .weui-form-preview__item {
            border-top: solid 1px #ececec;
        }

        .weui-form-preview__value {
            text-align: left;
            padding-left: 1em;
            border-left: solid 1px #ececec;
            min-height: 22px;
            line-height: 22px;
            padding: 3px;
        }

        .weui-pull-to-refresh {
            margin-top: -80px;
            transition: transform .4s;
        }

            .weui-pull-to-refresh.refreshing {
                transform: translate3d(0, 80px, 0);
            }

        .weui-pull-to-refresh__layer {
            padding: 20px 0 0 0;
            height: 60px;
            line-height: 60px;
        }
    </style>
</head>
<body ontouchstart>
    <div class="weui-form-preview">
        <%foreach (DataRow de in dt.Rows)
            {%>
        <div class="weui-form-preview__bd" style="color: #000; padding: 0;">
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">订单编号</label>
                <span class="weui-form-preview__value" id="KeyId"><%=de["KeyId"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">订单编号</label>
                <span class="weui-form-preview__value" id="OrderID"><%=de["OrderID"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">单位名称</label>
                <span class="weui-form-preview__value" id="comname"><%=de["comname"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">电话</label>
                <span class="weui-form-preview__value" id="tel"><%=de["tel"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">接件时间</label>
                <span class="weui-form-preview__value" id="adddate"><%=de["adddate"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">交货时间</label>
                <span class="weui-form-preview__value" id="deliveryDate"><%=de["deliveryDate"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">印刷品名称</label>
                <span class="weui-form-preview__value" id="allyspmc"><%=de["allyspmc"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">联系人</label>
                <span class="weui-form-preview__value" id="connman"><%=de["connman"].ToString() %></span>
            </div>
            <div class="weui-form-preview__item">
                <label class="weui-form-preview__label">接件员</label>
                <span class="weui-form-preview__value" id="jjy"><%=de["jjy"].ToString() %></span>
            </div>
            
        </div>
        <%} %>

    </div>

    <div class="weui-msg">
        <div class="weui-msg__opr-area">
            <p class="weui-btn-area">
                <%--<%if (mystatus == 0)
                {%>--%>
                <a id="<%=KeyId %>" onclick="xiugai('<%=KeyId %>')" class="weui-btn weui-btn_primary">开始加工</a>
                <%-- <%}
    else if (mystatus == 1)
    {%>--%>
                <%--<a id="<%=KeyId %>" onclick="xiugai('<%=KeyId %>')" class="weui-btn weui-btn_primary">正在加工</a>--%>
                <%--<%} %>--%>
                <%--<a id="<%=KeyId %>" onclick="wancheng('<%=KeyId %>')" class="weui-btn weui-btn_default">结束加工</a>--%>
            </p>
        </div>


        <%int i = 1; %>
        <%foreach (DataRow ddr in detailDt.Rows)
            {%>
        <div class="weui-cells__title"><%=i %>.<%=ddr["yspmc"].ToString() %>工艺流程</div>
        <div class="weui-cells weui-cells_radio">
            <!--循环工艺-->


            <!--子订单判断-->
  



             <%if (!string.IsNullOrEmpty(ddr["fumoa"].ToString()))//如果覆膜工艺
                {%>
            <label class="weui-cell">
                <div class="weui-cell__bd">
                    <p>覆膜:<%=ddr["fumoa"].ToString() %>覆<%=ddr["fumob"].ToString() %>

                    
                        </p>
                    <!--判断-->
                     <%
                         DataRow[] fmlcrows = lcDt.Select(" gytype='fumoa'");
                         if (fmlcrows.Length == 0)//假如时间流程表李没有这条记录 说明事情没做,显示让人做
                         {%>
                     <input class="weui-btn weui-btn_mini weui-btn_primary" onclick="ido('fumoa:<%=dr["keyid"].ToString()%>:<%=dr["orderid"].ToString()%>:<%=ddr["keyid"].ToString()%>');" type="button" value="我来做" style="width:100%;display:block;">
                    <input class="weui-btn weui-btn_mini weui-btn_default" type="button" value="催别人做" style="width:100%;display:block;"/>
                        <% }
    else//如果已有人做过.显示是谁做的
    {%>
               <%=fmlcrows[0]["doman"].ToString() %>于<%=DateTime.Parse(fmlcrows[0]["finishTime"].ToString()).ToString("MM-dd hh:MM") %>完工

                    <%} %>
                     <!--判断-->

                   
                </div>

            </label>
            <% } %>

            <%if (!string.IsNullOrEmpty(ddr["yaa"].ToString()))//如果有压工艺
                {%>
            <label class="weui-cell">
                <div class="weui-cell__bd">
                    <p>压:<%=ddr["yaa"].ToString() %>压<%=ddr["yab"].ToString() %>

                    <!--判断-->
                     <%
                         DataRow[] fmlcrows = lcDt.Select(" gytype='yaa'");
                         if (fmlcrows == null)//假如时间流程表李没有这条记录 说明事情没做,显示让人做
                         {%>
                     <input class="weui-btn weui-btn_mini weui-btn_primary" onclick="ido('yaa:<%=dr["keyid"].ToString()%>:<%=dr["orderid"].ToString()%>:<%=ddr["keyid"].ToString()%>');" type="button" value="我来做" />
                    <input class="weui-btn weui-btn_mini weui-btn_default" type="button" value="催别人做" />
                        <% }
    else//如果已有人做过.显示是谁做的
    {%>
                        <%--<%=fmlcrows[0]["doman"].ToString() %>于<%=DateTime.Parse(fmlcrows[0]["finishTime"].ToString()).ToString("MM-dd hh:MM") %>1完工--%>
                    <%} %>
                        </p>
                     <!--判断-->
                </div>

            </label>
            <% } %>







            <%if (!string.IsNullOrEmpty(ddr["tana"].ToString()))//如果烫工艺
                {%>
            <label class="weui-cell">
                <div class="weui-cell__bd">
                    <p>烫:<%=ddr["tana"].ToString() %>烫<%=ddr["tanb"].ToString() %>

                    <!--判断-->
                     <%
                         DataRow[] fmlcrows = lcDt.Select(" gytype='tana'");
                         if (fmlcrows.Length == 0)//假如时间流程表李没有这条记录 说明事情没做,显示让人做
                         {%>
                     <input class="weui-btn weui-btn_mini weui-btn_primary" onclick="ido('tana:<%=dr["keyid"].ToString()%>:<%=dr["orderid"].ToString()%>:<%=ddr["keyid"].ToString()%>');" type="button" value="我来做" />
                    <input class="weui-btn weui-btn_mini weui-btn_default"  type="button" value="催别人做" />
                        <% }
    else//如果已有人做过.显示是谁做的
    {%>
               <%=fmlcrows[0]["doman"].ToString() %>于<%=DateTime.Parse(fmlcrows[0]["finishTime"].ToString()).ToString("MM-dd hh:MM") %>完工

                    <%} %>
                        </p>
                     <!--判断-->

                   
                </div>

            </label>
            <% } %>


            <%if (!string.IsNullOrEmpty(ddr["zheye"].ToString()))//如果折页工艺
                {%>
            <label class="weui-cell">
                <div class="weui-cell__bd">
                    <p>折页:<%=ddr["zheye"].ToString() %>

                    <!--判断-->
                     <%
                         DataRow[] fmlcrows = lcDt.Select(" gytype='zheye'");
                         if (fmlcrows.Length == 0)//假如时间流程表李没有这条记录 说明事情没做,显示让人做
                         {%>
                     <input  class="weui-btn weui-btn_mini weui-btn_primary" onclick="ido('zheye:<%=dr["keyid"].ToString()%>:<%=dr["orderid"].ToString()%>:<%=ddr["keyid"].ToString()%>');" type="button" value="我来做" />
                    <input class="weui-btn weui-btn_mini weui-btn_default" type="button" value="催别人做" />
                        <% }
                    else//如果已有人做过.显示是谁做的
                    {%>
               <%=fmlcrows[0]["doman"].ToString() %>于<%=DateTime.Parse(fmlcrows[0]["finishTime"].ToString()).ToString("MM-dd hh:MM") %>完工

                    <%} %>
                        </p>
                     <!--判断-->

                   
                </div>

            </label>
            <% } %>

            
            <%if (!string.IsNullOrEmpty(ddr["uva"].ToString()))//如果uv工艺
                {%>
            <label class="weui-cell">
                <div class="weui-cell__bd">
                    <p>UV:<%=ddr["uva"].ToString() %>UV<%=ddr["uvb"].ToString() %>

                    <!--判断-->
                     <%
                         DataRow[] fmlcrows = lcDt.Select(" gytype='uva'");
                         if (fmlcrows.Length == 0)//假如时间流程表李没有这条记录 说明事情没做,显示让人做
                         {%>
                     <input  class="weui-btn weui-btn_mini weui-btn_primary" onclick="ido('uva:<%=dr["keyid"].ToString()%>:<%=dr["orderid"].ToString()%>:<%=ddr["keyid"].ToString()%>');" type="button" value="我来做" />
                    <input   class="weui-btn weui-btn_mini weui-btn_default" type="button" value="催别人做" />
                        <% }
                else//如果已有人做过.显示是谁做的
                {%>
               <%=fmlcrows[0]["doman"].ToString() %>于<%=DateTime.Parse(fmlcrows[0]["finishTime"].ToString()).ToString("MM-dd hh:MM") %>完工

                    <%} %>
                        </p>
                     <!--判断-->

                   
                </div>

            </label>
            <% } %>

            <%if (!string.IsNullOrEmpty(ddr["zd"].ToString()))//如果装订工艺
                {%>
            <label class="weui-cell">
                <div class="weui-cell__bd">
                    <p>装订:<%=ddr["zd"].ToString() %>

                    <!--判断-->
                     <%
                         DataRow[] fmlcrows = lcDt.Select(" gytype='zd'");
                         if (fmlcrows.Length == 0)//假如时间流程表李没有这条记录 说明事情没做,显示让人做
                         {%>
                     <input  class="weui-btn weui-btn_mini weui-btn_primary" onclick="ido('zd:<%=dr["keyid"].ToString()%>:<%=dr["orderid"].ToString()%>:<%=ddr["keyid"].ToString()%>');" type="button" value="我来做" />
                    <input  class="weui-btn weui-btn_mini weui-btn_default" type="button" value="催别人做" />
                        <% }
                else//如果已有人做过.显示是谁做的
                {%>
               <%=fmlcrows[0]["doman"].ToString() %>于<%=DateTime.Parse(fmlcrows[0]["finishTime"].ToString()).ToString("MM-dd hh:MM") %>完工

                    <%} %>
                        </p>
                     <!--判断-->

                   
                </div>

            </label>
            <% } %>

             <%if (!string.IsNullOrEmpty(ddr["db"].ToString()))//如果刀版工艺
                {%>
            <label class="weui-cell">
                <div class="weui-cell__bd">
                    <p>刀版:<%=ddr["db"].ToString() %>

                    <!--判断-->
                     <%
                         DataRow[] fmlcrows = lcDt.Select(" gytype='db'");
                         if (fmlcrows.Length == 0)//假如时间流程表李没有这条记录 说明事情没做,显示让人做
                         {%>
                     <input class="weui-btn weui-btn_mini weui-btn_primary" onclick="ido('db:<%=dr["keyid"].ToString()%>:<%=dr["orderid"].ToString()%>:<%=ddr["keyid"].ToString()%>');" type="button" value="我来做" />
                    <input class="weui-btn weui-btn_mini weui-btn_default" type="button" value="催别人做" />
                        <% }
                else//如果已有人做过.显示是谁做的
                {%>
               <%=fmlcrows[0]["doman"].ToString() %>于<%=DateTime.Parse(fmlcrows[0]["finishTime"].ToString()).ToString("MM-dd hh:MM") %>完工

                    <%} %>
                        </p>
                     <!--判断-->

                   
                </div>

            </label>
            <% } %>



            <%if (!string.IsNullOrEmpty(ddr["bh"].ToString()))//如果裱盒工艺
                {%>
            <label class="weui-cell">
                <div class="weui-cell__bd">
                    <p>裱盒:<%=ddr["bh"].ToString() %>

                    <!--判断-->
                     <%
                         DataRow[] fmlcrows = lcDt.Select(" gytype='bh'");
                         if (fmlcrows.Length == 0)//假如时间流程表李没有这条记录 说明事情没做,显示让人做
                         {%>
                     <input class="weui-btn weui-btn_mini weui-btn_primary" onclick="ido('bh:<%=dr["keyid"].ToString()%>:<%=dr["orderid"].ToString()%>:<%=ddr["keyid"].ToString()%>');" type="button" value="我来做"/>
                    <input class="weui-btn weui-btn_mini weui-btn_default" type="button" value="催别人做"/>
                        <% }
                else//如果已有人做过.显示是谁做的
                {%>
               <%=fmlcrows[0]["doman"].ToString() %>于<%=DateTime.Parse(fmlcrows[0]["finishTime"].ToString()).ToString("MM-dd hh:MM") %>完工

                    <%} %>
                        </p>
                     <!--判断-->

                   
                </div>

            </label>
            <% } %>

            <%if (!string.IsNullOrEmpty(ddr["sh"].ToString()))//如果送货工艺
                {%>
            <label class="weui-cell">
                <div class="weui-cell__bd">
                    <p>送货:</><%=ddr["sh"].ToString() %>

                    <!--判断-->
                     
                        </p>
                     <!--判断-->

                   
                </div>

            </label>

            <%
                         DataRow[] fmlcrows = lcDt.Select(" gytype='sh'");
                         if (fmlcrows.Length == 0)//假如时间流程表李没有这条记录 说明事情没做,显示让人做
                         {%>
                     <input class="weui-btn weui-btn_mini weui-btn_primary" onclick="ido('sh:<%=dr["keyid"].ToString()%>:<%=dr["orderid"].ToString()%>:<%=ddr["keyid"].ToString()%>');" type="button" value="我来做" />
                    <input class="weui-btn weui-btn_mini weui-btn_default" type="button" value="催别人做" />
                        <% }
                else//如果已有人做过.显示是谁做的
                {%>
               <%=fmlcrows[0]["doman"].ToString() %>于<%=DateTime.Parse(fmlcrows[0]["finishTime"].ToString()).ToString("MM-dd hh:MM") %>完工

                    <%} %>

            <% } %>




            <% } %>
            <!--循环工艺-->


        </div>



        <div class="weui-msg__opr-area">
            <p class="weui-btn-area">
                <%--<%if (mystatus == 0)
                {%>--%>
                <%--<a id="<%=KeyId %>" onclick="xiugai('<%=KeyId %>')" class="weui-btn weui-btn_primary">开始加工</a>--%>
                <%-- <%}
    else if (mystatus == 1)
    {%>--%>
                <a id="<%=KeyId %>" onclick="wancheng('<%=KeyId %>')" class="weui-btn weui-btn_primaryy">结束加工</a>
                <%--<%} %>--%>
                <%--<a id="<%=KeyId %>" onclick="wancheng('<%=KeyId %>')" class="weui-btn weui-btn_default">结束加工</a>--%>
            </p>
        </div>
       <%-- <div class="weui-msg__extra-area">
            <div class="weui-footer">
                <p class="weui-footer__links">
                    <a href="JydWapindex.aspx" class="weui-footer__link">聚源达</a>
                </p>
                <p class="weui-footer__text">Copyright © 2018-2020 </p>
            </div>
        </div>--%>
    </div>


</body>
</html>
