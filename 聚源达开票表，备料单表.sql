USE [jydtest]
GO
/****** Object:  Table [dbo].[jydSpareparts]    Script Date: 03/30/2019 08:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[jydSpareparts](
	[KeyId] [int] IDENTITY(1,1) NOT NULL,
	[orderid] [int] NULL,
	[kdry_id] [int] NULL,
	[kdry_name] [nvarchar](50) NULL,
	[pinming] [nvarchar](500) NULL,
	[cailiao] [nvarchar](500) NULL,
	[guige] [nvarchar](500) NULL,
	[shuliang] [nvarchar](500) NULL,
	[klguige] [nvarchar](500) NULL,
	[gyyaoqiou] [text] NULL,
	[beizhu] [text] NULL,
	[beiliaor_id] [int] NULL,
	[beiliaor_name] [nvarchar](50) NULL,
	[beiliao_type] [nvarchar](500) NULL,
	[kehu_name] [nvarchar](50) NULL,
	[orderbianhao] [nvarchar](500) NULL,
	[beiliao_time] [datetime] NULL,
	[add_time] [datetime] NULL,
 CONSTRAINT [PK_jydSpareparts] PRIMARY KEY CLUSTERED 
(
	[KeyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备料单id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'KeyId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'orderid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开单人员id（备料单）' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'kdry_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开单人员名字' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'kdry_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'品名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'pinming'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'纸张/板材' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'cailiao'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'guige'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'数量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'shuliang'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开料规格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'klguige'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'工艺要求' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'gyyaoqiou'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备注' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'beizhu'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备料人id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'beiliaor_id'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备料人姓名' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'beiliaor_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备料状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'beiliao_type'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'kehu_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'orderbianhao'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备料完成时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'beiliao_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'备料发布时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydSpareparts', @level2type=N'COLUMN',@level2name=N'add_time'
GO
/****** Object:  Table [dbo].[jydInvoicet]    Script Date: 03/30/2019 08:52:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[jydInvoicet](
	[KeyId] [int] IDENTITY(1,1) NOT NULL,
	[order_keyid] [int] NULL,
	[OrderID] [nvarchar](50) NULL,
	[money] [money] NULL,
	[Invoice_no] [nvarchar](50) NULL,
	[Invoice_time] [datetime] NULL,
	[Corporate_name] [nvarchar](500) NULL,
	[Invoicetype] [nvarchar](50) NULL,
 CONSTRAINT [PK_jydInvoicet] PRIMARY KEY CLUSTERED 
(
	[KeyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开票id' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydInvoicet', @level2type=N'COLUMN',@level2name=N'KeyId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydInvoicet', @level2type=N'COLUMN',@level2name=N'order_keyid'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单编号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydInvoicet', @level2type=N'COLUMN',@level2name=N'OrderID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydInvoicet', @level2type=N'COLUMN',@level2name=N'money'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'发票号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydInvoicet', @level2type=N'COLUMN',@level2name=N'Invoice_no'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开票日期' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydInvoicet', @level2type=N'COLUMN',@level2name=N'Invoice_time'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'公司名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydInvoicet', @level2type=N'COLUMN',@level2name=N'Corporate_name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'开票状态' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'jydInvoicet', @level2type=N'COLUMN',@level2name=N'Invoicetype'
GO
SET IDENTITY_INSERT [dbo].[jydInvoicet] ON
INSERT [dbo].[jydInvoicet] ([KeyId], [order_keyid], [OrderID], [money], [Invoice_no], [Invoice_time], [Corporate_name], [Invoicetype]) VALUES (17, 29578, N'2018-10-17-1', 0.0000, N'1234', CAST(0x0000AA17011768A7 AS DateTime), N'昆明马冬冬食品厂', N'开票')
INSERT [dbo].[jydInvoicet] ([KeyId], [order_keyid], [OrderID], [money], [Invoice_no], [Invoice_time], [Corporate_name], [Invoicetype]) VALUES (18, 29567, N'2018-09-30-1', 44320.0000, N'12355555555', CAST(0x0000AA1900965421 AS DateTime), N'昆明市五华区', N'开票')
INSERT [dbo].[jydInvoicet] ([KeyId], [order_keyid], [OrderID], [money], [Invoice_no], [Invoice_time], [Corporate_name], [Invoicetype]) VALUES (19, 29570, N'2018-09-30-4', 184800.0000, N'123', CAST(0x0000AA1E011D14C3 AS DateTime), N'刘居轮', N'开票')
SET IDENTITY_INSERT [dbo].[jydInvoicet] OFF
