﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Omu.ValueInjecter;

namespace NetWing.Common.Data
{
    public class FieldsBy : KnownTargetValueInjection<string>
    {
        private IEnumerable<string> _updateFields = new string[] { };//要更新的字段其余的不更新 新增的 by 朱光明
        private IEnumerable<string> _ignoredFields = new string[] { };
        private string _format = "{0}";
        private string _nullFormat;
        private string _glue = ",";

        public FieldsBy SetGlue(string g)
        {
            _glue = " " + g + " ";
            return this;
        }
        /// <summary>
        /// 要更新的字段、其余的不更新
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        public FieldsBy UpdateFields(params string[] fields)
        {
            _updateFields = fields;
            return this;
        }

        public FieldsBy IgnoreFields(params string[] fields)
        {
            _ignoredFields = fields;
            return this;
        }

        public FieldsBy SetFormat(string f)
        {
            _format = f;
            return this;
        }

        public FieldsBy SetNullFormat(string f)
        {
            _nullFormat = f;
            return this;
        }

        protected override void Inject(object source, ref string target)
        {
            var sourceProps = source.GetInfos().ToList();
            var s = string.Empty;
            for (var i = 0; i < sourceProps.Count(); i++)//循环model成员
            {
                var prop = sourceProps[i];
                
                if (prop.GetCustomAttributes(true).Length > 0)
                {
                    int k = prop.GetCustomAttributes(true).OfType<DbFieldAttribute>().Count(atr => !(atr).IsDbField);

                    if (k > 0)
                        continue;
                }
                //如果包含忽略字段时就继续执行循环。不执行下面的
                if (_ignoredFields.Contains(prop.Name.ToLower())) continue;
                //统计更新字段出现个数
                int upFieldsCount = _updateFields.Count();
                //如果不是更新字段时继续 否则跳出循环
                if (upFieldsCount>0)//有更新字段时才执行
                {
                    if (!_updateFields.Contains(prop.Name.ToLower())) continue;
                }
                
                //设置null字段    
                if (prop.GetValue(source,null) == DBNull.Value && _nullFormat != null)
                    s += string.Format(_nullFormat, prop.Name);
                else
                    s += string.Format(_format, prop.Name) + _glue;
            }
            s = s.RemoveSuffix(_glue);
            target += s;
        }
    }
}
