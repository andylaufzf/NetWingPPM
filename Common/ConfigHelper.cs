﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Configuration;
namespace NetWing.Common
{
    public class ConfigHelper
    {

        public static string GetValue(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings[key];
            }
            catch (Exception ex)
            {
                throw new System.Exception(ex.Message);
            }
        }
        /// <summary>   
        /// 设置应用程序配置节点，如果已经存在此节点，则会修改该节点的值，否则添加此节点  
        /// </summary>   
        /// <param name="key">节点名称</param>   
        /// <param name="value">节点值</param>   
        public static void SetAppSetting(string key, string value)
        {
            Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
            AppSettingsSection app = config.AppSettings;
            app.Settings[key].Value =value;
            config.Save(ConfigurationSaveMode.Modified);

        }


    }
}
