﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetWing.Common.EF
{
    public abstract class Entity
    {
        public int KeyId { get; set; }

        public Entity()
        {
            KeyId = 0;
            //Guid.NewGuid().ToString()
        }
    }
}
